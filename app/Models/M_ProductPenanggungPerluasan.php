<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_ProductPenanggungPerluasan extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_product_penanggung_perluasan';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_perluasan()
    {
        return $this->belongsTo(M_Perluasan::class,'m_perluasan_id','id')->select('id','name','code','description','calculation_method');
    }

    public function get_perluasan_rate()
    {
        return $this->hasMany(M_ProductPerluasanRate::class,'m_product_penanggung_perluasan_id','id')->select('id','m_product_penanggung_perluasan_id','rate','coverage_value');
    }

}
