<?php

namespace App\Models\NewOrder;

use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_SubProduct;
use App\Models\Payment\M_Invoice;
use App\Models\Payment\M_Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class NewOrderCompanyPICModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'new_order_data_company_pic';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];
}
