<?php

namespace App\Models\NewOrder;

use App\Models\Company\CompanyTypeModel;
use App\Models\M_City;
use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_Province;
use App\Models\M_SubProduct;
use App\Models\Payment\M_Invoice;
use App\Models\Payment\M_Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class NewOrderCompanyModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'new_order_data_company';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_direksi_komisaris(){
        return $this->hasMany(NewOrderCompanyDireksiKomisarisModel::class,'new_order_data_company_code','code')
                    ->select('new_order_data_company_code','type','name','identity_file','position','code');
    }


    public function get_pic(){
        return $this->hasMany(NewOrderCompanyPICModel::class,'new_order_data_company_code','code')
                    ->select('name','telepon','email','posisi','code_telepon','new_order_data_company_code','code');
    }
    public function get_province()
    {
        return $this->belongsTo(M_Province::class,'m_province_id','id')->select('id','name');
    }

    public function get_city()
    {
        return $this->belongsTo(M_City::class,'m_city_id','id')->select('id','name');
    }
    
    public function get_company_type()
    {
        return $this->belongsTo(CompanyTypeModel::class,'type','id')->select('id','name');
    }
}
