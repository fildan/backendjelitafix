<?php

namespace App\Models\NewOrder;

use App\Models\Company\CompanyTypeModel;
use App\Models\M_City;
use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_Province;
use App\Models\M_SubProduct;
use App\Models\Payment\M_Invoice;
use App\Models\Payment\M_Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class NewOrderSBObligeeModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'new_order_sb_obligee';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_province()
    {
        return $this->belongsTo(M_Province::class,'m_province_id','id')->select('id','name');
    }

    public function get_city()
    {
        return $this->belongsTo(M_City::class,'m_city_id','id')->select('id','name');
    }
    public function get_company_type()
    {
        return $this->belongsTo(CompanyTypeModel::class,'company_type','id')->select('id','name');
    }
}
