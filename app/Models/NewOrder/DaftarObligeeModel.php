<?php

namespace App\Models\NewOrder;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class DaftarObligeeModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'daftar_obligee';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];
}
