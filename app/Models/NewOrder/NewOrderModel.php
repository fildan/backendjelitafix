<?php

namespace App\Models\NewOrder;

use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_SubProduct;
use App\Models\Payment\M_Invoice;
use App\Models\Payment\M_Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class NewOrderModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'new_order';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_product(){
        return $this->belongsTo(M_Product::class,'m_product_code','code')->select('id','name','code','picture','url');
    }

    public function get_sub_product(){
        return $this->belongsTo(M_SubProduct::class,'m_sub_product_code','code')->select('id','name','code','url');
    }
    public function get_order_sb(){
        return $this->belongsTo(NewOrderSBModel::class,'code','new_order_code')
                    ->select('code','new_order_code','periode_type','periode_value',
                        'fix_rate','fix_rate_periode_type','fix_rate_periode_value','fix_premi_per_periode','total_fix_premi','estimation_rate','estimation_rate_periode_type',
                        'estimation_rate_periode_value','estimation_premi_per_periode','total_estimation_premi','fix_rate_type','estimation_rate_type');
    }

    public function get_order_company()
    {
        return $this->belongsTo(NewOrderCompanyModel::class,'code','new_order_code')
                    ->select('new_order_code','name','type','jenis_usaha','bidang_usaha','npwp','email',
                        'no_telepon','m_province_id','m_city_id','postcode','address','code');
    }

    public function get_document()
    {
        return $this->hasMany(NewOrderDocumentModel::class,'new_order_code','code')
                    ->select('new_order_code','m_document_upload_code','name','document_name','date_document','note_document','number_document','document_type');
    }

    public function get_list_asuransi()
    {
        return $this->belongsTo(NewOrderListAsuransiModel::class,'code','new_order_code')
                    ->select('new_order_code','name');
    }

    public function get_penanggung()
    {
        return $this->belongsTo(M_ProductPenanggung::class,'m_product_penanggung_code','code')
                    ->select(
                        'code','reff_code','name','description','benefit','benefit_exception','fitur','tnc','claim','is_package',
                        'document_claim','document_tnc','document_prodcut','daftar_bengkel','m_product_code','m_sub_product_code',
                        'm_penanggung_code','url','periode_premi','value_periode_premi','is_available_multi_years',
                        'minimum_years_vehicle_for_multi_years','depreciation','m_penanggung_id','estimation_rate','estimation_rate_type_periode',
                        'estimation_rate_value_periode','type_estimation_rate'
                    );
    }
    public function get_invoice_active()
    {
        return $this->hasOne(M_Invoice::class,'new_order_code','code')
                ->select('new_order_code','code','total_amount','status');
    }

}
