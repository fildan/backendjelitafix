<?php

namespace App\Models\NewOrder;

use App\Models\Company\CompanyTypeModel;
use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_SubProduct;
use App\Models\Payment\M_Invoice;
use App\Models\Payment\M_Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class NewOrderSBModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'new_order_sb';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_project()
    {
        return $this->belongsTo(NewOrderSBProjectModel::class,'code','new_order_sb_code')
                    ->select(
                                'code','name','date','description','contract_value','collateral_value',
                                'start_date_periode','end_date_periode','periode_type','periode_value','new_order_sb_code','number_support_document','date_support_document'
                            );
    }

    public function get_obligee()
    {
        return $this->belongsTo(NewOrderSBObligeeModel::class,'code','new_order_sb_code')
                    ->select(
                            'new_order_sb_code','code','company_type','company_name','jenis_usaha','bidang_usaha',
                            'website','email','no_telepon','m_province_id','m_province_name','m_city_id','m_city_name',
                            'postcode','address'
                    );
    }
    
    public function get_company_type()
    {
        return $this->belongsTo(CompanyTypeModel::class,'company_type','id')->select('id','name');
    }
}
