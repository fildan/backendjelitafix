<?php

namespace App\Models\DraftOrder;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class DraftOrderProjectModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'draft_order_project';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];
}
