<?php

namespace App\Models\DraftOrder;

use App\Models\Company\UserCompanyModel;
use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_SubProduct;
use App\Models\RateBySubProductModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class DraftOrderModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'draft_order';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];


    public function get_product(){
        return $this->belongsTo(M_Product::class,'m_product_id','id')->select('id','name','code','picture','url');
    }

    public function get_sub_product(){
        return $this->belongsTo(M_SubProduct::class,'m_sub_product_id','id')->select('id','name');
    }

    public function get_document(){
        return $this->hasMany(DraftOrderDocumentModel::class,'draft_order_code','code')->select('draft_order_code','m_document_upload_code','code',
                                'm_document_upload_name','file_name','date_document','number_document');
    }

    public function get_obligee(){
        return $this->hasOne(DraftOrderObligeeModel::class,'draft_order_code','code')->select('draft_order_code','code','company_type','company_name','jenis_usaha','bidang_usaha','website',
                                'email','no_telepon','m_province_id','m_province_name','m_city_id','m_city_name','postcode','address');
    }

    public function get_project(){
        return $this->hasOne(DraftOrderProjectModel::class,'draft_order_code','code')->select('draft_order_code','code','name','date','description','contract_value','collateral_value',
                                'start_date_periode','end_date_periode','periode_type','periode_value');
    }

    public function get_list_request_asuransi()
    {
        return $this->hasMany(DraftOrderListAsuransiModel::class,'draft_order_code','code')->select('draft_order_code','name','code');
    }


    public function get_fix_asuransi()
    {
        return $this->belongsTo(M_ProductPenanggung::class,'m_penanggung_code','code');
    }

    public function get_company(){
        return $this->belongsTo(UserCompanyModel::class,'user_company_code','code')->select('code','name','type','jenis_usaha','bidang_usaha','npwp','email',
                    'no_telepon','m_province_id','m_city_id','postcode','address');
    }

    public function get_rate()
    {
        return $this->belongsTo(RateBySubProductModel::class,'m_sub_product_code','m_sub_product_code')->select('m_sub_product_code','code','rate_type','rate','rate_periode_type','rate_periode_value');
    }
}
