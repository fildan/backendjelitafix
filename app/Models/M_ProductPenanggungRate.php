<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_ProductPenanggungRate extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_product_penanggung_rate';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_rate_by_region()
    {
        return $this->hasMany(M_ProductPenanggungRateByRegion::class,'m_product_penanggung_rate_id','id')->select('id','fix_rate','m_product_penanggung_rate_id','wilayah');
    }

}
