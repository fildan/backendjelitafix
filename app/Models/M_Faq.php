<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_Faq extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'faq';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function GetProduct()
    {
        return $this->belongsTo(\App\Models\M_Product::class,'m_product_id','id');
    }
}
