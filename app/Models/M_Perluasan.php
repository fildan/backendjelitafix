<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_Perluasan extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_perluasan_polis';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function GetProduct()
    {
        return $this->belongsTo(M_Product::class,'m_product_id','id');
    }
}
