<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_MasterMethodePayment extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_methode_payment';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_payment_methode_bank(){
        return $this->hasMany(M_MethodePaymentBank::class,'m_methode_payment_code','code')->select('m_methode_payment_code','m_bank_code','code','name');
    }
}
