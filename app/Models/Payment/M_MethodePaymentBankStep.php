<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_MethodePaymentBankStep extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_methode_payment_bank_step';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

}
