<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_Payment extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'payment';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_methode_payment(){
        return $this->belongsTo(M_MasterMethodePayment::class,'m_methode_payment_code','code')->select('code','description');
    }


    public function get_methode_payment_bank(){
        return $this->belongsTo(M_MethodePaymentBank::class,'m_methode_payment_bank_code','code')->select('code','name');
    }


    public function get_bank(){
        return $this->belongsTo(M_MasterBank::class,'m_bank_code','code')->select('code','name','logo');
    }
    
     public function get_payment_history(){
        return $this->hasMany(M_HistoryPayment::class,'payment_code','code');
    }
}
