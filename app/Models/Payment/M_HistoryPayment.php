<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_HistoryPayment extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'history_payment';
    // protected $dates = ['deleted_at'];
}
