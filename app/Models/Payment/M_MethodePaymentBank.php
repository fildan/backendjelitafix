<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_MethodePaymentBank extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_methode_payment_bank';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_bank(){
        return $this->belongsTo(M_MasterBank::class,'m_bank_code','code')->select('code','name','logo');
    }

    public function get_payment_bank_step(){
        return $this->hasMany(M_MethodePaymentBankStep::class,'m_methode_payment_bank_code','code')->select('id','m_methode_payment_bank_code','step','name','exception','exception_list');
    }

    public function get_master_methode_payment(){
        return $this->belongsTo(M_MasterMethodePayment::class,'m_methode_payment_code','code')->select('code','description');
    }

}
