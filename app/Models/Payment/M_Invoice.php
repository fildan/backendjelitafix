<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_Invoice extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'invoice';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_payment_active(){
        return $this->hasOne(M_Payment::class,'invoice_code','code')
                ->select('invoice_code','code','m_methode_payment_bank_code','status','total_amount','m_bank_code','no_rekening','payment_code','m_methode_payment_code','expired_time','expired_date_time','status','m_methode_payment_bank_code','url_payment');
    }

    public function get_payment(){
        return $this->hasMany(M_Payment::class,'invoice_code','code')
                ->select('invoice_code','code','m_methode_payment_bank_code','status','total_amount','m_bank_code','no_rekening','payment_code','m_methode_payment_code','expired_time','expired_date_time','status','m_methode_payment_bank_code','url_payment');
    }
}
