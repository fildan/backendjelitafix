<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_ProductPenanggung extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_product_penanggung';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_perluasan_penanggung()
    {
        return $this->hasMany(M_ProductPenanggungPerluasan::class,'m_product_penanggung_id','id')->select('id','m_product_penanggung_id','m_perluasan_id','rate_or_value');
    }

    public function get_product(){
        return $this->belongsTo(M_Product::class,'m_product_id','id')->select('id','name','code','picture','url');
    }

    public function get_sub_product(){
        return $this->belongsTo(M_SubProduct::class,'m_sub_product','id')->select('id','name','code','url');
    }

    public function get_penanggung(){
        return $this->belongsTo(M_Penanggung::class,'m_penanggung_id','id')->select('id','code','name','picture','description');
    }

    public function get_rate()
    {
        return $this->hasMany(M_ProductPenanggungRate::class,'m_product_penanggung_code','code')->select('id','m_product_penanggung_code','maximum_price','minimum_price','is_high_price');
    }

}
