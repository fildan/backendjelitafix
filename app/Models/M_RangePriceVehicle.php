<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_RangePriceVehicle extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_range_price_vehicle';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];


}
