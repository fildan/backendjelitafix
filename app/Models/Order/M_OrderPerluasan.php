<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_OrderPerluasan extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'order_perluasan';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

}
