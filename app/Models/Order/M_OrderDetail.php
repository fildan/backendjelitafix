<?php

namespace App\Models\Order;

use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_SubProduct;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_OrderDetail extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'order_detail';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_order_perluasan(){
        return $this->hasMany(M_OrderPerluasan::class,'order_detail_code','code')
                            ->select('order_detail_code','m_perluasan_polis_code','m_perluasan_name','price');
    }
}
