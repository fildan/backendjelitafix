<?php

namespace App\Models\Order;

use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_SubProduct;
use App\Models\Payment\M_Invoice;
use App\Models\Payment\M_Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_Order extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'order';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_product(){
        return $this->belongsTo(M_Product::class,'m_product_id','id')->select('id','name','code','picture','url');
    }

    public function get_sub_product(){
        return $this->belongsTo(M_SubProduct::class,'m_sub_product_id','id')->select('id','name');
    }

    public function get_product_penanggung(){
        return $this->belongsTo(M_ProductPenanggung::class,'m_product_penanggung_code','code')->select('id','name','code','m_penanggung_code','m_penanggung_id');
    }

    public function get_order_pemegang_polis(){
        return $this->hasOne(M_PemegangPolis::class,'order_code','code')->select('order_code','name','gender','birth_date','email','phone_number','m_province_id','m_city_id','address');
    }

    // public function get_order_perluasan(){
    //     return $this->hasMany(M_OrderPerluasan::class,'order_code','code')->select('order_code','m_perluasan_polis_code','m_perluasan_name','price');
    // }

    public function get_order_accessories(){
        return $this->hasMany(M_OrderAcc::class,'order_code','code')->select('order_code','name','brand','type','price');
    }
    public function get_order_kbm(){
        return $this->hasOne(M_OrderKBM::class,'order_code','code')
                        ->select('order_code','brand_vehicle','type_vehicle','series_vehicle','plat_number','m_plat_wilayah_code','vehicle_year',
                            'm_plat_wilayah_code','color_vehicle','ranka_number','engine_number','vehicle_condition','price_vehicle');
    }

    public function get_order_detail(){
        return $this->hasMany(M_OrderDetail::class,'order_code','code')
            ->select('order_code','code','total_pertanggungan_before_depreciation','depreciation_percent','total_pertanggungan_after_depreciation',
                        'total_accessories','rate','total_perluasan','premi','premi_after_perluasan','total_pertanggungan','start_date_periode','end_date_periode');
    }



    public function get_invoice_active()
    {
        return $this->hasOne(M_Invoice::class,'order_code','code')
                ->select('order_code','code','total_amount_payment','status');
    }
}
