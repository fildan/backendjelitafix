<?php

namespace App\Models\Order;

use App\Models\M_City;
use App\Models\M_Province;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_PemegangPolis extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'order_data_pemegang_polis';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_city()
    {
        return $this->belongsTo(M_City::class,'m_city_id','id')->select('id','name');
    }

    public function get_province()
    {
        return $this->belongsTo(M_Province::class,'m_province_id','id')->select('id','name');
    }

}
