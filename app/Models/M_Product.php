<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_Product extends Model
{

    use HasFactory;
    use SoftDeletes;

    protected $table = 'm_product';
    protected $guarded = [];

    public function get_faq()
    {
        return $this->hasMany(\App\Models\M_Faq::class, 'm_product_id', 'id');
    }
}
