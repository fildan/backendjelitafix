<?php

namespace App\Models;

use App\Models\Company\CompanyTypeModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_ReferencePenanggungModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_reference_penanggung';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_company_type()
    {
        return $this->belongsTo(CompanyTypeModel::class,'company_type','id')->select('id','name');
    }
}
