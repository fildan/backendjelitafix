<?php

namespace App\Models\Company;

use App\Models\M_City;
use App\Models\M_Province;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class UserCompanyModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'user_company';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_document(){
        return $this->hasMany(UserCompanyDocumentModel::class,'user_company_code','code')->select('user_company_code','m_document_upload_code','name','file_name','note');
    }

    public function get_pic(){
        return $this->hasMany(UserCompanyPICModel::class,'user_company_code','code')->select('user_company_code','code','name','telepon','email','posisi','code_telepon');
    }


    public function get_komisaris_dan_direksi(){
        return $this->hasMany(UserKomisarisDireksiModel::class,'user_company_code','code')->select('code','user_company_code','type','name','identity_file','position');
    }

    public function get_province()
    {
        return $this->belongsTo(M_Province::class,'m_province_id','id')->select('id','name');
    }

    public function get_city()
    {
        return $this->belongsTo(M_City::class,'m_city_id','id')->select('id','name');
    }

    public function get_company_type()
    {
        return $this->belongsTo(CompanyTypeModel::class,'type','id')->select('id','name');
    }

    public function get_user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

}
