<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class UserKomisarisDireksiModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'user_company_komisaris_direksi';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];
}
