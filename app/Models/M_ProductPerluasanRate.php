<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_ProductPerluasanRate extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_product_penanggung_perluasan_rate';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_perluasan()
    {
        return $this->belongsTo(M_Perluasan::class,'m_perluasan_id','id');
    }

}
