<?php

namespace App\Models;

use App\Models\M_Product;
use App\Models\M_SubProduct;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class RateBySubProductModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_rate_ojk_by_sub_product';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];
}
