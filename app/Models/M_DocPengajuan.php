<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
date_default_timezone_set('Asia/Jakarta');

class M_DocPengajuan extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_document_upload';
    // protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function get_product(){
        return $this->belongsTo(M_Product::class,'m_product_id','id')->select('id','name','code','picture','url');
    }

    public function get_sub_product(){
        return $this->belongsTo(M_SubProduct::class,'m_sub_product','id')->select('id','name','code','url');
    }

}
