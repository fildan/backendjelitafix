<?php

namespace App\Http\Controllers\ManageUserFrontend;

use App\Models\M_Profile;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;
use File;

class ManageUserController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('ManageUserFrontend.User.v_list_user');
    }

    public function getList(Request $request)
    {
        $get_data = User::with(['get_profile','get_profile.get_province','get_profile.get_city'])->get();

        return DataTables::of($get_data)
            ->addIndexColumn()
            ->addColumn('phone', function($row){
                return $row->get_profile->phone_code.$row->get_profile->phone;
            })
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".$row->code."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".$row->code."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function save(Request $request)
    {
        $name           =  $request->name;
        $no_ktp         =  $request->no_ktp;
        $email          =  $request->email;
        $phone_code     =  $request->phone_code;
        $phone_number   =  $request->phone_number;
        $gender         =  $request->gender;
        $tgl_lahir      =  $request->tgl_lahir;
        $alamat         =  $request->alamat;
        $password       =  $request->password;
        $id_user_login  =  $request->id_user_login;
        $ktp            =  $request->ktp;
        $code           = Uuid::uuid4();
        $city           = $request->city;
        $province       = $request->province;
        if($ktp)
        {
            $ktp_file = generate_url(strtolower('ktp-user'.$code)).'.'.$ktp->extension();
            $ktp_file = strtolower($ktp_file);
        }else{
            $ktp_file = null;
        }


        DB::beginTransaction();
        try{
            $user_data = [
                'name'      => $name,
                'email'     => $email,
                'code'      => $code,
                'password'  => bcrypt($password)
            ];
            $user               = User::create($user_data);

            $save_profile       = [
                'user_id'       => $user->id,
                'user_code'     => $user->code,
                'name'          => $name,
                'gender'        => $gender,
                'birth_date'    => $tgl_lahir,
                'email'         => $email,
                'phone'         => $phone_number,
                'phone_code'    => $phone_code,
                'ktp_number'    => $no_ktp,
                'alamat'        => $alamat,
                'm_province_id' => $province,
                'm_city_id'     => $city,
                'ktp'           => $ktp_file,
                'created_at'    => date('Y-m-d H:i:s'),
                'created_by'    => $id_user_login
            ];
            $save_profile       = M_Profile::create($save_profile);
            DB::commit();
            $upload_path_attachment = storage_path('app/public/user/'.$user->code);
            if (!file_exists($upload_path_attachment)) {
                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                    Storage::putFileAs('public/user/'.$user->code, $ktp, $ktp_file);
                }
            } else {
                Storage::putFileAs('public/user/'.$user->code, $ktp, $ktp_file);
            }
            return response()->json([
                'message'   => 'Registrasi Berhasil.',
                'status'    => 'success',
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }

    }

    public function getDataEdit(Request $request)
    {
        $code = $request->code;
        $get_data = User::with(['get_profile','get_profile.get_province','get_profile.get_city'])->where('code',$code)->first();
        return response()->json([
            'message'   => 'Data Berhasil Di Ambil.',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);

    }

    public function update(Request $request)
    {
        // dd($request->all());
        $name           =  $request->name;
        $no_ktp         =  $request->no_ktp;
        $email          =  $request->email;
        $phone_code     =  $request->phone_code;
        $phone_number   =  $request->phone_number;
        $gender         =  $request->gender;
        $tgl_lahir      =  $request->tgl_lahir;
        $alamat         =  $request->alamat;
        $password       =  $request->password;
        $id_user_login  =  $request->id_user_login;
        $ktp            =  $request->ktp;
        $city           = $request->city;
        $province       = $request->province;
        $code           = $request->code;
        $is_edit_pswd   = $request->is_edit_pswd;

        $cek            = User::with(['get_profile','get_profile.get_province','get_profile.get_city'])->where('code',$code)->first();

        if(!$cek){
            return response()->json([
                'message'   => 'Data not found.',
                'status'    => 'error',
                'code'      => 404,
            ], 200);

        }

        $old_ktp = $cek->get_profile->ktp;
        DB::beginTransaction();
        try{
            if($ktp)
            {
                $ktp_file = generate_url(strtolower('ktp-user'.$code)).'.'.$ktp->extension();
                $ktp_file = strtolower($ktp_file);
                $data_update['ktp'] = $ktp_file;
            }else{
                $ktp_file = null;
            }

            if($is_edit_pswd == 'on')
            {
                if($password){
                    $data_update_user['password'] = Hash::make($password);
                }
            }

            $data_update_user['name']           = $name;
            $data_update_user['email']          = $email;
            $data_update_user['updated_at']     = date('Y-m-d H:i:s');
            $data_update_user['updated_by']     = $id_user_login;
            $update_user                        = User::where('code',$code)->update($data_update_user);

            $data_update['name']          = $name;
            $data_update['gender']        = $gender;
            $data_update['birth_date']    = $tgl_lahir;
            $data_update['email']         = $email;
            $data_update['phone']         = $phone_number;
            $data_update['phone_code']    = $phone_code;
            $data_update['ktp_number']    = $no_ktp;
            $data_update['alamat']        = $alamat;
            $data_update['m_province_id'] = $province;
            $data_update['m_city_id']     = $city;
            $data_update['updated_at']    = date('Y-m-d H:i:s');
            $data_update['updated_by']    = $id_user_login;
            $update_profile               = M_Profile::where('user_code',$code)->update($data_update);

            DB::commit();

            if($ktp){
                Storage::delete('/public/user/'.$code.'/'.$old_ktp);
                $upload_path_attachment = storage_path('app/public/user/'.$code);
                if (!file_exists($upload_path_attachment)) {
                    if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                        Storage::putFileAs('public/user/'.$code, $ktp, $ktp_file);
                    }
                } else {
                    Storage::putFileAs('public/user/'.$code, $ktp, $ktp_file);
                }
            }
            return response()->json([
                'message'   => 'Registrasi Berhasil.',
                'status'    => 'success',
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $code = $request->code;
        $cek            = User::with(['get_profile','get_profile.get_province','get_profile.get_city'])->where('code',$code)->first();

        $id_user_login  =  $request->id_user_login;

        if(!$cek){
            return response()->json([
                'message'   => 'Data not found.',
                'status'    => 'error',
                'code'      => 404,
            ], 200);

        }
        DB::beginTransaction();
        try{
            $data_update['deleted_at'] = date('Y-m-d H:i:s');
            $data_update['deleted_by'] = $id_user_login;
            $update = User::where('code',$code)->update($data_update);
            $update_profiel = M_Profile::where('user_code',$code)->update($data_update);
            DB::commit();
            return response()->json([
                'message'   => 'Registrasi Berhasil.',
                'status'    => 'success',
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

}
