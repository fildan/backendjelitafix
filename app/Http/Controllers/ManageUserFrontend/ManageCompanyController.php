<?php

namespace App\Http\Controllers\ManageUserFrontend;

use App\Models\Company\CompanyTypeModel;
use App\Models\Company\UserCompanyDocumentModel;
use App\Models\Company\UserCompanyModel;
use App\Models\Company\UserCompanyPICModel;
use App\Models\Company\UserKomisarisDireksiModel;
use App\Models\M_DocPengajuan;
use App\Models\M_Profile;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;
use File;

class ManageCompanyController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('ManageUserFrontend.Company.v_list_company');
    }

    public function getList()
    {
        $get_data = UserCompanyModel::with(['get_company_type','get_pic','get_komisaris_dan_direksi','get_province','get_city'])->get();

        return DataTables::of($get_data)
        ->addIndexColumn()
        ->addColumn('company_type', function($row){
            return $row->get_company_type->name;
        })
        ->addColumn('no_telepon', function($row){
            $telp = json_decode($row->no_telepon);
            return $telp[0]->code.$telp[0]->number;
        })
        ->addColumn('action', function($row){
                $btn = "<a href='#' class='edit' onclick='editClick(\"".$row->code."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                || <a href='#' class='edit' onclick='deleteClick(\"".$row->code."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                return $btn;
        })
        ->make(true);
    }

    public function add(Request $request){

        $get_document           = getDocCompany();
        $data['get_document']   = $get_document;
        return view('ManageUserFrontend.Company.v_add_company',$data);
    }

    public function getType(Request $request)
    {

        $q              = $request->term;
        $qwhere         = strtolower($request->term).'%';
        $query_         = CompanyTypeModel::whereRaw('lower(name) like (?)',["%{$qwhere}%"])
                            ->select('id','name')->orderBy('id','ASC')->get();
        $total_count = count($query_);
        $data_array = array();
        foreach ($query_ as $row) {
            $data_array[] = array(
                'id'            => $row->id,
                'text'          => $row->name,
            );
        }
        return response()->json($data_array, 200);
    }

    public function save(Request $request)
    {
        // dd($request->all());
        $select_type_company    = $request->select_type_company;
        $name                   = strtolower($request->name);
        $jenis_usaha            = $request->jenis_usaha;
        $bidang_usaha           = $request->bidang_usaha;
        $no_npwp                = $request->no_npwp;
        $email                  = strtolower($request->email);

        $phone_code             = $request->phone_code;
        $phone_number           = $request->phone_number;

        $select_province        = $request->select_province;
        $select_city            = $request->select_city;
        $postcode               = $request->postcode;
        $address                = $request->address;

        $nama_komisaris         = $request->nama_komisaris;
        $jabatan_komisaris      = $request->jabatan_komisaris;
        $ktp_komisaris          = $request->ktp_komisaris;

        $nama_direksi           = $request->nama_direksi;
        $jabatan_direksi        = $request->jabatan_direksi;
        $ktp_direksi            = $request->ktp_direksi;

        $name_pic               = $request->name_pic;
        $email_pic              = $request->email_pic;
        $phone_code_pic         = $request->phone_code_pic;
        $phone_number_pic       = $request->phone_number_pic;
        $position_pic           = $request->position_pic;

        $note_document          = $request->note_document;
        $document_legality      = $request->document_legality;

        $name_user              = strtolower($request->name_user) ;
        $email_user             = strtolower($request->email_user) ;
        $phone_code_user        = $request->phone_code_user ;
        $phone_number_user      = $request->phone_number_user ;
        $ktp_number_user        = $request->ktp_number_user ;
        $birth_date             = $request->birth_date ;
        $postcode_user          = $request->postcode_user ;
        $alamat_user            = $request->alamat_user ;
        $password               = $request->password ;
        $select_province_user   = $request->province_user;
        $select_city_user       = $request->city_user;
        $ktp_user               = $request->ktp_user;
        $user_login             = $request->id_user_login;

        $cek = User::where('email',$email_user)->first();

        if($cek){
            return response()->json([
                'message'   => 'email user telah terdaftar, gunakan email lain',
                'status'    => 'error',
            ],200);
        }

        $user_code                  = Uuid::uuid4();
        
        $document_save              = [];
        $direksi_json               = [];
        DB::beginTransaction();
        try{
            $save_user['name']      = $name_user;
            $save_user['email']     = $email_user;
            $save_user['code']      = $user_code;
            $save_user['password']  = bcrypt($password);
            $user                   = User::create($save_user);


            if($ktp_user)
            {

                $ktp_file = generate_url(strtolower('ktp-user-'.$user->code)).'.'.$ktp_user->extension();
                $ktp_file = strtolower($ktp_file);

                $upload_path_attachment = storage_path('app/public/user/'.$user->code);

                if (!file_exists($upload_path_attachment)) {
                    if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                        Storage::putFileAs('public/user/'.$user->code, $ktp_user, $ktp_file);
                    }
                } else {
                    Storage::putFileAs('public/user/'.$user->code, $ktp_user, $ktp_file);
                }
            }else{
                $ktp_file = null;
            }

            $save_profile       = [
                'user_id'       => $user->id,
                'user_code'     => $user->code,
                'name'          => $name_user,
                'birth_date'    => $birth_date,
                'email'         => $email_user,
                'phone'         => $phone_number_user,
                'phone_code'    => $phone_code_user,
                'ktp_number'    => $ktp_number_user,
                'm_province_id' => $select_province_user,
                'm_city_id'     => $select_city_user,
                'alamat'        => $alamat_user,
                'postcode'      => $postcode_user,
                'ktp'           => $ktp_file
            ];
            $save_profile       = M_Profile::create($save_profile);


            $code_company = Uuid::uuid4();

            $phone_company = [];
            if($phone_number)
            {
                foreach($phone_number as $key => $val){
                    if(array_key_exists($key,$phone_code)){
                        $code_phone = $phone_code[$key];
                        $phone_company[] = [
                            'code'      => $code_phone,
                            'number'    => $val
                        ];
                    }
                }
            }

            $data_save_company['code']          = $code_company;
            $data_save_company['name']          = $name;
            $data_save_company['type']          = $select_type_company;
            $data_save_company['jenis_usaha']   = $jenis_usaha;
            $data_save_company['bidang_usaha']  = $bidang_usaha;
            $data_save_company['npwp']          = $no_npwp;
            $data_save_company['email']         = $email;
            $data_save_company['no_telepon']    = json_encode($phone_company);
            $data_save_company['m_province_id'] = $select_province;
            $data_save_company['m_city_id']     = $select_city;
            $data_save_company['postcode']      = $postcode;
            $data_save_company['address']       = $address;
            $data_save_company['created_at']    = date('Y-m-d H:i:s');
            $data_save_company['created_by']    = $user_login;
            $data_save_company['user_id']       = $user->id;

            $save_company       = UserCompanyModel::insertGetId($data_save_company);

            if($nama_direksi){
                foreach($nama_direksi as $kydireksi => $valdir){
                    
                    if($valdir){
                        
                        $nama_direksi_new = strtolower($valdir);
    
    
                        if(array_key_exists($kydireksi,$jabatan_direksi)){
                            $jabatan_direksi_new = $jabatan_direksi[$kydireksi];
                        }else{
                            $jabatan_direksi_new = 'no jabatan';
                        }
    
    
                        if(array_key_exists($kydireksi,$ktp_direksi)){
                            $ktp_direksi_file = generate_url(strtolower($nama_direksi_new.'-'.$jabatan_direksi_new.'-'.$code_company)).'.'.$ktp_direksi[$kydireksi]->extension();
                            $ktp_direksi_file = strtolower($ktp_direksi_file);
    
                            $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$code_company);
    
                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/company-document/company-code-'.$code_company, $ktp_direksi[$kydireksi], $ktp_direksi_file);
                                }
                            } else {
                                Storage::putFileAs('public/company-document/company-code-'.$code_company, $ktp_direksi[$kydireksi], $ktp_direksi_file);
                            }
                        }else{
                            $ktp_direksi_file = null;
                        }
    
    
                        $direksi_json[]   = [
                            'code'                  => Uuid::uuid4(),
                            'name'                  => $nama_direksi_new,
                            'identity_file'         => $ktp_direksi_file,
                            'position'              => $jabatan_direksi_new,
                            'type'                  => 2,
                            'user_company_id'       => $save_company,
                            'user_company_code'     => $code_company,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'created_by'            => $user_login
                        ];
                    }
                }
            }

            if($nama_komisaris){
                foreach($nama_komisaris as $kykomisaris => $valkomisaris){
                    if($valkomisaris){
                        $nama_komisaris_new = strtolower($valkomisaris);
    
                        if(array_key_exists($kykomisaris,$jabatan_komisaris)){
                            $jabatan_komisaris_new = $jabatan_komisaris[$kykomisaris];
                        }else{
                            $jabatan_komisaris_new = 'no jabatan';
                        }
    
    
                        if(array_key_exists($kykomisaris,$ktp_komisaris)){
                            $ktp_komisaris_file = generate_url(strtolower($nama_komisaris_new.'-'.$jabatan_komisaris_new.'-'.$code_company)).'.'.$ktp_komisaris[$kykomisaris]->extension();
                            $ktp_komisaris_file = strtolower($ktp_komisaris_file);
    
                            $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$code_company);
    
                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/company-document/company-code-'.$code_company, $ktp_komisaris[$kykomisaris], $ktp_komisaris_file);
                                }
                            } else {
                                Storage::putFileAs('public/company-document/company-code-'.$code_company, $ktp_komisaris[$kykomisaris], $ktp_komisaris_file);
                            }
                        }else{
                            $ktp_komisaris_file = null;
                        }
    
    
                        $direksi_json[]   = [
                            'code'                  => Uuid::uuid4(),
                            'name'                  => $nama_komisaris_new,
                            'identity_file'         => $ktp_komisaris_file,
                            'position'              => $jabatan_komisaris_new,
                            'type'                  => 1,
                            'user_company_id'       => $save_company,
                            'user_company_code'     => $code_company,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'created_by'            => $user_login
                        ];
                    }
                }
            }

             if(count($direksi_json) > 0){
                $save_komsaris_direksi = UserKomisarisDireksiModel::insert($direksi_json);
            }

            if($name_pic){
                $pic_data = [];
                foreach($name_pic as $kypic => $valpic)
                {
                    
                    if($valpic){
                        if(array_key_exists($kypic,$email_pic)){
                            $email_pic_new = strtolower($email_pic[$kypic]);
                        }else{
                            $email_pic_new = null;
                        }
    
                        if(array_key_exists($kypic,$phone_code_pic)){
                            $phone_code_pic_new = $phone_code_pic[$kypic];
                        }else{
                            $phone_code_pic_new = null;
                        }
    
                        if(array_key_exists($kypic,$phone_number_pic)){
                            $phone_number_pic_new = $phone_number_pic[$kypic];
                        }else{
                            $phone_number_pic_new = null;
                        }
    
                        if(array_key_exists($kypic,$position_pic)){
                            $position_pic_new = $position_pic[$kypic];
                        }else{
                            $position_pic_new = null;
                        }
                        $pic_data[] = [
                            'code_telepon'          => $phone_code_pic_new,
                            'code'                  => Uuid::uuid4(),
                            'name'                  => strtolower($valpic),
                            'telepon'               => $phone_number_pic_new,
                            'email'                 => $email_pic_new,
                            'posisi'                => $position_pic_new,
                            'user_company_id'       => $save_company,
                            'user_company_code'     => $code_company,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'created_by'            => $user_login
                        ];
                    }
                }
                 if(count($pic_data) > 0){
                    $save_pic = UserCompanyPICModel::insert($pic_data);
                 }
            }

            if($document_legality){

                foreach($document_legality as $keydoc => $valdoc)
                {
                    $get_doc_upload = M_DocPengajuan::where('code',$keydoc)->first();
                    $doc_name       = generate_url($get_doc_upload->name).'.'.$valdoc->extension();;
                    $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$code_company);

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/company-document/company-code-'.$code_company, $valdoc, $doc_name);
                        }
                    } else {
                        Storage::putFileAs('public/company-document/company-code-'.$code_company, $valdoc, $doc_name);
                    }

                    if($note_document){
                        if(array_key_exists($keydoc,$note_document)){
                            $note_document_new = $note_document[$keydoc];
                        }else{
                            $note_document_new = null;
                        }
                    }else{
                        $note_document_new = null;
                    }

                    $document_save[] = [
                        'code'                      =>  Uuid::uuid4(),
                        'user_company_id'           =>  $save_company,
                        'user_company_code'         =>  $code_company,
                        'm_document_upload_id'      =>  $get_doc_upload->id,
                        'm_document_upload_code'    =>  $get_doc_upload->code,
                        'name'                      =>  $get_doc_upload->name,
                        'file_name'                 =>  $doc_name,
                        'note'                      =>  $note_document_new,
                        'created_at'                =>  date('Y-m-d H:i:s'),
                        'created_by'                =>  $user_login
                    ];
                }

                $save_doc = UserCompanyDocumentModel::insert($document_save);


            }
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'code'      => 200
            ],200);

        }catch (\Exception $e) {
            DB::rollback();

            if($ktp_user){
                Storage::delete('/public/user/'.$user_code.'/'.$ktp_file);
            }

            if($direksi_json)
            {
                foreach($direksi_json as $kydirjs => $valdirjs)
                {
                    if($valdirjs['identity_file']){
                        Storage::delete('/public/company-document/company-code-'.$code_company.'/'.$valdirjs['identity_file']);
                    }
                }
            }


            if($document_save)
            {
                foreach($document_save as $kydocjs => $valdocjs)
                {
                    
                    if($valdocjs['file_name']){
                        Storage::delete('/public/company-document/company-code-'.$code_company.'/'.$valdocjs['file_name']);
                    }
                }
            }
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }
    }

    public function edit(Request $request){
        $get_data = UserCompanyModel::with([
                                            'get_document','get_pic','get_komisaris_dan_direksi','get_province','get_city',
                                            'get_company_type','get_user','get_user.get_profile','get_user.get_profile.get_province','get_user.get_profile.get_city'
                                        ])
                                    ->where('code',$request->code)->first();
            // dd($get_data->get_user->get_profile);
        $get_document           = getDocCompany();
        $data['get_document']   = $get_document;
        $data['get_data']       = $get_data;
        $data['code']           = $request->code;

        return view('ManageUserFrontend.Company.v_edit_company',$data);

    }


    public function update(Request $request)
    {
        $select_type_company    = $request->select_type_company;
        $name                   = strtolower($request->name);
        $jenis_usaha            = $request->jenis_usaha;
        $bidang_usaha           = $request->bidang_usaha;
        $no_npwp                = $request->no_npwp;
        $email                  = strtolower($request->email);

        $phone_code             = $request->phone_code;
        $phone_number           = $request->phone_number;

        $select_province        = $request->select_province;
        $select_city            = $request->select_city;
        $postcode               = $request->postcode;
        $address                = $request->address;

        $nama_komisaris         = $request->nama_komisaris;
        $jabatan_komisaris      = $request->jabatan_komisaris;
        $ktp_komisaris          = $request->ktp_komisaris;

        $nama_direksi           = $request->nama_direksi;
        $jabatan_direksi        = $request->jabatan_direksi;
        $ktp_direksi            = $request->ktp_direksi;

        $name_pic               = $request->name_pic;
        $email_pic              = $request->email_pic;
        $phone_code_pic         = $request->phone_code_pic;
        $phone_number_pic       = $request->phone_number_pic;
        $position_pic           = $request->position_pic;

        $note_document          = $request->note_document;
        $document_legality      = $request->document_legality;

        $name_user              = strtolower($request->name_user);
        $email_user             = strtolower($request->email_user);
        $phone_code_user        = $request->phone_code_user ;
        $phone_number_user      = $request->phone_number_user ;
        $ktp_number_user        = $request->ktp_number_user ;
        $birth_date             = $request->birth_date ;
        $postcode_user          = $request->postcode_user ;
        $alamat_user            = $request->alamat_user ;
        $password               = $request->password ;
        $select_province_user   = $request->province_user;
        $select_city_user       = $request->city_user;
        $ktp_user               = $request->ktp_user;
        $user_login             = $request->id_user_login;

        $code                   = $request->code;



        $get_data = UserCompanyModel::with([
                                        'get_document','get_pic','get_komisaris_dan_direksi','get_province','get_city',
                                        'get_company_type','get_user','get_user.get_profile','get_user.get_profile.get_province','get_user.get_profile.get_city'
                                    ])
                                    ->where('code',$request->code)->where('code',$code)->first();


        $cek_user = User::where('email',$email_user)->where('id','<>',$get_data->user_id)->first();

        if($cek_user){
            return response()->json([
                'message'   => 'email user telah terdaftar, gunakan email lain',
                'status'    => 'error',
            ],200);
        }
        $old_identity_file                  = [];
        $new_identity_file                  = [];
        DB::beginTransaction();
        try{

            $save_user['name']      = $name_user;
            $save_user['email']     = $email_user;
            $save_user['updated_at'] = date('Y-m-d H:i:s');
            $save_user['updated_by'] = $user_login;
            if($password){
                $save_user['password']  = bcrypt($password);
            }
            $update_user                   = User::where('id',$get_data->user_id)->update($save_user);


            if($ktp_user)
            {

                $ktp_file               = generate_url(strtolower('ktp-user-'.$get_data->get_user->code.'-new-'.time())).'.'.$ktp_user->extension();
                $ktp_file               = strtolower($ktp_file);

                $old_ktp                = $get_data->get_user->get_profile->ktp;
                $new_ktp                = $ktp_file;

                $upload_path_attachment = storage_path('app/public/user/'.$get_data->get_user->code);

                if (!file_exists($upload_path_attachment)) {
                    if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                        Storage::putFileAs('public/user/'.$get_data->get_user->code, $ktp_user, $ktp_file);
                    }
                } else {
                    Storage::putFileAs('public/user/'.$get_data->get_user->code, $ktp_user, $ktp_file);
                }
            }else{
                $ktp_file = $get_data->get_user->get_profile->ktp;
            }



            $save_profile       = [
                'name'          => $name_user,
                'birth_date'    => $birth_date,
                'email'         => $email_user,
                'phone'         => $phone_number_user,
                'phone_code'    => $phone_code_user,
                'ktp_number'    => $ktp_number_user,
                'm_province_id' => $select_province_user,
                'm_city_id'     => $select_city_user,
                'alamat'        => $alamat_user,
                'postcode'      => $postcode_user,
                'ktp'           => $ktp_file,
                'updated_at'    => date('Y-m-d H:i:s'),
                'updated_by'    => $user_login
            ];
            $save_profile       = M_Profile::where('user_id',$get_data->get_user->id)->update($save_profile);

            $phone_company = [];
            if($phone_number)
            {
                foreach($phone_number as $key => $val){
                    if(array_key_exists($key,$phone_code)){
                        $code_phone = $phone_code[$key];
                        $phone_company[] = [
                            'code'      => $code_phone,
                            'number'    => $val
                        ];
                    }
                }
            }

            $data_save_company['name']          = $name;
            $data_save_company['type']          = $select_type_company;
            $data_save_company['jenis_usaha']   = $jenis_usaha;
            $data_save_company['bidang_usaha']  = $bidang_usaha;
            $data_save_company['npwp']          = $no_npwp;
            $data_save_company['email']         = $email;
            $data_save_company['no_telepon']    = json_encode($phone_company);
            $data_save_company['m_province_id'] = $select_province;
            $data_save_company['m_city_id']     = $select_city;
            $data_save_company['postcode']      = $postcode;
            $data_save_company['address']       = $address;
            $data_save_company['updated_at']    = date('Y-m-d H:i:s');
            $data_save_company['updated_by']    = $user_login;

            $save_company                       = UserCompanyModel::where('code',$code)->update($data_save_company);

            if($nama_direksi){
                foreach($nama_direksi as $kydireksi => $valdir){

                    $cek_komisaris_direksi = UserKomisarisDireksiModel::where('code',$kydireksi)->first();


                    $nama_direksi_new = strtolower($valdir);


                    if(array_key_exists($kydireksi,$jabatan_direksi)){
                        $jabatan_direksi_new = $jabatan_direksi[$kydireksi];
                    }else{
                        $jabatan_direksi_new = $cek_komisaris_direksi->position;
                    }


                    if($ktp_direksi){
                        if(array_key_exists($kydireksi,$ktp_direksi)){
                            $ktp_direksi_file = generate_url(strtolower($nama_direksi_new.'-'.$jabatan_direksi_new.'-'.$code.'-new-at-'.time())).'.'.$ktp_direksi[$kydireksi]->extension();
                            $ktp_direksi_file = strtolower($ktp_direksi_file);

                            $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$code);

                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/company-document/company-code-'.$code, $ktp_direksi[$kydireksi], $ktp_direksi_file);
                                }
                            } else {
                                Storage::putFileAs('public/company-document/company-code-'.$code, $ktp_direksi[$kydireksi], $ktp_direksi_file);
                            }


                            if($cek_komisaris_direksi){
                                $old_identity_file[] = $cek_komisaris_direksi->identity_file;
                                $new_identity_file[] = $ktp_direksi_file;
                            }else{
                                $new_identity_file[] = $ktp_direksi_file;
                            }
                        }else{
                            if($cek_komisaris_direksi){
                                $ktp_direksi_file = $cek_komisaris_direksi->identity_file;
                            }else{
                                $ktp_direksi_file = null;
                            }
                        }
                    }else{
                        if($cek_komisaris_direksi){
                            $ktp_direksi_file = $cek_komisaris_direksi->identity_file;
                        }else{
                            $ktp_direksi_file = null;
                        }
                    }


                     $data_direksi_update['name']                  = $nama_direksi_new;
                     $data_direksi_update['identity_file']         = $ktp_direksi_file;
                     $data_direksi_update['position']              = $jabatan_direksi_new;

                    if($cek_komisaris_direksi){
                        $data_direksi_update['updated_at']          = date('Y-m-d H:i:s');
                        $data_direksi_update['updated_by']          = $user_login;
                        $update_direksi = UserKomisarisDireksiModel::where('code',$cek_komisaris_direksi->code)->update($data_direksi_update);
                    }else{

                        $data_direksi_update['code']                = Uuid::uuid4();
                        $data_direksi_update['type']                = 2;
                        $data_direksi_update['user_company_id']     = $get_data->id;
                        $data_direksi_update['user_company_code']   = $get_data->code;
                        $data_direksi_update['created_at']          = date('Y-m-d H:i:s');
                        $data_direksi_update['created_by']          = $user_login;
                        $update_direksi = UserKomisarisDireksiModel::insert($data_direksi_update);
                    }
                }

                $get_direksi_exists = $this->buildDireksiKomisarisarray($code,2);
                foreach($get_direksi_exists as $key => $val){
                    if(!array_key_exists($val,$nama_direksi)){
                        $delete_old = UserKomisarisDireksiModel::where('code',$val)->update([
                            'deleted_at'    => date('Y-m-d H:i:s'),
                            'deleted_by'    => $user_login
                        ]);
                    }
                }
            }

            if($nama_komisaris){
                foreach($nama_komisaris as $kykomisaris => $valdir){

                    $cek_komisaris_direksi = UserKomisarisDireksiModel::where('code',$kykomisaris)->first();


                    $nama_komisaris_new = strtolower($valdir);

                    if(array_key_exists($kykomisaris,$jabatan_komisaris)){
                        $jabatan_komisaris_new = $jabatan_komisaris[$kykomisaris];
                    }else{
                        $jabatan_komisaris_new = $cek_komisaris_direksi->position;
                    }


                    if($ktp_komisaris){
                        if(array_key_exists($kykomisaris,$ktp_komisaris)){
                            $ktp_komisaris_file = generate_url(strtolower($nama_komisaris_new.'-'.$jabatan_komisaris_new.'-'.$code.'-new-at-'.time())).'.'.$ktp_komisaris[$kykomisaris]->extension();
                            $ktp_komisaris_file = strtolower($ktp_komisaris_file);

                            $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$code);

                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/company-document/company-code-'.$code, $ktp_komisaris[$kykomisaris], $ktp_komisaris_file);
                                }
                            } else {
                                Storage::putFileAs('public/company-document/company-code-'.$code, $ktp_komisaris[$kykomisaris], $ktp_komisaris_file);
                            }


                            if($cek_komisaris_direksi){
                                $old_identity_file[] = $cek_komisaris_direksi->identity_file;
                                $new_identity_file[] = $ktp_komisaris_file;
                            }else{
                                $new_identity_file[] = $ktp_komisaris_file;
                            }
                        }else{
                            if($cek_komisaris_direksi){
                                $ktp_komisaris_file = $cek_komisaris_direksi->identity_file;
                            }else{
                                $ktp_komisaris_file = null;
                            }
                        }
                    }else{
                        if($cek_komisaris_direksi){
                            $ktp_komisaris_file = $cek_komisaris_direksi->identity_file;
                        }else{
                            $ktp_komisaris_file = null;
                        }
                    }


                     $data_direksi_update['name']                  = $nama_komisaris_new;
                     $data_direksi_update['identity_file']         = $ktp_komisaris_file;
                     $data_direksi_update['position']              = $jabatan_komisaris_new;

                    if($cek_komisaris_direksi){
                        $data_direksi_update['updated_at']          = date('Y-m-d H:i:s');
                        $data_direksi_update['updated_by']          = $user_login;
                        $update_direksi = UserKomisarisDireksiModel::where('code',$cek_komisaris_direksi->code)->update($data_direksi_update);
                    }else{

                        $data_direksi_update['code']                = Uuid::uuid4();
                        $data_direksi_update['type']                = 1;
                        $data_direksi_update['user_company_id']     = $get_data->id;
                        $data_direksi_update['user_company_code']   = $get_data->code;
                        $data_direksi_update['created_at']          = date('Y-m-d H:i:s');
                        $data_direksi_update['created_by']          = $user_login;
                        $update_direksi = UserKomisarisDireksiModel::insert($data_direksi_update);
                    }
                }

                $get_komisaris_exists = $this->buildDireksiKomisarisarray($code,1);
                foreach($get_komisaris_exists as $key => $val){
                    if(!array_key_exists($val,$nama_komisaris)){
                        $delete_old = UserKomisarisDireksiModel::where('code',$val)->update([
                            'deleted_at'    => date('Y-m-d H:i:s'),
                            'deleted_by'    => $user_login
                        ]);
                    }
                }

            }

            if($name_pic){
                $pic_data = [];
                foreach($name_pic as $kypic => $valpic)
                {

                    $cek_pic = UserCompanyPICModel::where('code',$kypic)->first();
                    if(array_key_exists($kypic,$email_pic)){
                        $email_pic_new = strtolower($email_pic[$kypic]);
                    }else{
                        if($cek_pic){
                            $email_pic_new = $cek_pic->email;
                        }else{
                            $email_pic_new = null;
                        }
                    }

                    if(array_key_exists($kypic,$phone_code_pic)){
                        $phone_code_pic_new = $phone_code_pic[$kypic];
                    }else{
                        if($cek_pic){
                            $phone_code_pic_new = $cek_pic->code_telepon;
                        }else{
                            $phone_code_pic_new = null;
                        }
                    }

                    if(array_key_exists($kypic,$phone_number_pic)){
                        $phone_number_pic_new = $phone_number_pic[$kypic];
                    }else{
                        if($cek_pic){
                            $phone_number_pic_new = $cek_pic->telepon;
                        }else{
                            $phone_number_pic_new = null;
                        }
                    }

                    if(array_key_exists($kypic,$position_pic)){
                        $position_pic_new = $position_pic[$kypic];
                    }else{
                        if($cek_pic){
                            $position_pic_new = $cek_pic->posisi;
                        }else{
                            $position_pic_new = null;
                        }
                    }
                    $data_pic['name']               = strtolower($valpic);
                    $data_pic['email']              = $email_pic_new;
                    $data_pic['posisi']             = $position_pic_new;
                    $data_pic['code_telepon']       = $phone_code_pic_new;
                    $data_pic['telepon']            = $phone_number_pic_new;

                    if($cek_pic)
                    {
                        $data_pic['updated_at']     = date('Y-m-d H:i:s');
                        $data_pic['updated_by']     = $user_login;
                        $save_pic                   = UserCompanyPICModel::where('code',$kypic)->update($data_pic);
                    }else{
                        $data_pic['code']           = Uuid::uuid4();
                        $data_pic['created_at']     = date('Y-m-d H:i:s');
                        $data_pic['created_by']     = $user_login;
                        $save_pic                   = UserCompanyPICModel::insert($data_pic);
                    }
                }

                $get_exist_pic = $this->buildArrayPICExists($code);

                foreach($get_exist_pic as $key => $val){
                    if(!array_key_exists($val,$name_pic)){
                        $delete_old = UserCompanyPICModel::where('code',$key)->update([
                            'deleted_at'    => date('Y-m-d H:i:s'),
                            'deleted_by'    => $user_login
                        ]);
                    }
                }
            }

            if($note_document)
            {
                foreach($note_document as $keydoc => $valdoc){
                    $note_doc           = $valdoc;
                    $data_save['note']  = $note_doc;
                    $cek_doc                = UserCompanyDocumentModel::where('m_document_upload_code',$keydoc)->where('user_company_code',$code)->first();


                    if($document_legality){
                        if(array_key_exists($keydoc,$document_legality)){
                            $get_doc_upload = M_DocPengajuan::where('code',$keydoc)->first();
                            $doc_name       = generate_url($get_doc_upload->name).'-new-at-'.time().'.'.$document_legality[$keydoc]->extension();;
                            $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$code);

                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/company-document/company-code-'.$code, $document_legality[$keydoc], $doc_name);
                                }
                            } else {
                                Storage::putFileAs('public/company-document/company-code-'.$code, $document_legality[$keydoc], $doc_name);
                            }


                            $data_save['file_name'] = $doc_name;

                            if($cek_doc){
                                $new_identity_file[]             = $doc_name;
                                $old_identity_file[]             = $cek_doc->file_name;
                            }else{
                                $new_identity_file[]             = $doc_name;
                            }
                        }
                    }


                    if($cek_doc)
                    {
                        // return $cek_doc;
                        $data_save['updated_at']     = date('Y-m-d H:i:s');
                        $data_save['updated_by']     = $user_login;
                        $save_doc                    = UserCompanyDocumentModel::where('code',$cek_doc->code)->update($data_save);
                    }else{

                        if(!empty($valdoc) || array_key_exists($keydoc,$document_legality)){
                            $data_save['code']                      =  Uuid::uuid4();
                            $data_save['user_company_id']           =  $get_data->id;
                            $data_save['user_company_code']         =  $code;
                            $data_save['m_document_upload_id']      =  $get_doc_upload->id;
                            $data_save['m_document_upload_code']    =  $get_doc_upload->code;
                            $data_save['name']                      =  $get_doc_upload->name;
                            $data_save['created_at']                =  date('Y-m-d H:i:s');
                            $data_save['created_by']                =  $user_login;
                            $save_doc                               = UserCompanyDocumentModel::insert($data_save);
                        }
                    }
                }
            }
            DB::commit();

            if($ktp_user){
                Storage::delete('/public/user/'.$get_data->get_user->code.'/'.$old_ktp);
            }

            if(count($old_identity_file) > 0){
                foreach($old_identity_file as $kydocjs => $valdocjs)
                {
                    if($valdocjs){
                        Storage::delete('/public/company-document/company-code-'.$code.'/'.$valdocjs);
                    }
                }
            }
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'code'      => 200
            ],200);

        }catch (\Exception $e) {
            DB::rollback();

            if($ktp_user){
                Storage::delete('/public/user/'.$get_data->get_user->code.'/'.$new_ktp);
            }

            if(count($new_identity_file) > 0){
                foreach($new_identity_file as $kydocjs => $valdocjs)
                {
                    if($valdocjs){
                        Storage::delete('/public/company-document/company-code-'.$code.'/'.$valdocjs);
                    }
                }
            }
            return response()->json([
                'message'       => $e->getMessage().' Line:'.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $code                   = $request->code;

        $user_login             = $request->id_user_login;

        DB::beginTransaction();
        try{
            $data_update['deleted_at']  = date('Y-m-d H:i:s');
            $data_update['deleted_by']  = $user_login;

            $delete_company             = UserCompanyModel::where('code',$code)->update($data_update);
            $delete_doc                 = UserCompanyDocumentModel::where('user_company_code',$code)->update($data_update);
            $delete_pic                 = UserCompanyPICModel::where('user_company_code',$code)->update($data_update);
            $delete_komisaris_direksi   = UserKomisarisDireksiModel::where('user_company_code',$code)->update($data_update);

            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'code'      => 200
            ],200);

        }catch (\Exception $e) {
            DB::rollback();


            return response()->json([
                'message'       => $e->getMessage().' Line:'.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }

    }
    public function buildDireksiKomisarisarray($company_code=null,$type=null){
        $get_data = UserKomisarisDireksiModel::where('user_company_code',$company_code)->where('type',$type)->select('code')->get();

        $data = [];

        foreach($get_data as $key => $val){
            $data[] = $val->code;
        }

        return $data;
    }

    public function buildArrayPICExists($company_code=null)
    {
        $get_data = UserCompanyPICModel::where('user_company_code',$company_code)->select('code')->get();

        $data = [];

        foreach($get_data as $key => $val){
            $data[] = $val->code;
        }

        return $data;
    }
}
