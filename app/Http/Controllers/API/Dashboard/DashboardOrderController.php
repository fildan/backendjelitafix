<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Models\Order\M_Order;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;

class DashboardOrderController extends BaseController
{
    public function __construct()
    {

        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getListOrder(Request $request)
    {

        $user_id            = $request->user()->token()->user_id;
        $by_tgl             = $request->by_tgl;
        $by_status          = $request->by_status;
        $by_polis           = $request->by_polis;
        $menu_type          = $request->menu_type;

        $get_order  = M_Order::with(['get_product','get_sub_product','get_product_penanggung','get_product_penanggung.get_penanggung'])->where('user_id',$user_id);

        if($by_tgl){
            $get_order = $get_order->whereRaw("to_char(rfq.created_at,'YYYY-MM-DD') <= (?)",["{$by_tgl}"]);
        }

        if($by_polis != 0){
            if($by_polis == 1)
            {
                $cob_code = 'KBM';
            }
            else if($by_polis == 2)
            {
                $cob_code = 'PA';
            }
            else if($by_polis == 3)
            {
                $cob_code = 'ATT';
            }
            $get_order = $get_order->where("m_product_code",$cob_code);
        }

        if($menu_type == 1)
        {
            if($by_status != 0){
                $status = array($by_status);
            }else{
                $status = array(1,2,4,7,8,9);
            }
        }
        else if($menu_type == 2)
        {
            if($by_status != 0){
                if($by_status == 3){
                    $status = array($by_status,10);
                }else{
                    $status = array($by_status);
                }
            }else{
                $status = array(3,5,10);
            }
        }
        else
        {
            if($by_status != 0){
                $status = array($by_status);
            }else{
                $status = array(6);
            }

            $get_order = $get_order->with([
                            'get_invoice_active' => function($query){
                                $query->where('status',2);
                            },
                            'get_invoice_active.get_payment_active' => function($query_){
                                $query_->where('status',1);
                            }
                        ]);
        }

        // if($by_status != 0){

        //     if($by_status == 1)
        //     {
        //         $status = array(1);
        //     }
        //     else if($by_status == 2)
        //     {
        //         $status = array(2,3);
        //     }
        //     else if($by_status == 4)
        //     {
        //         $status = array(4);
        //     }
        //     else if($by_status == 5)
        //     {
        //         $status = array(5);
        //     }
        //     else if($by_status == 6)
        //     {
        //         $status = array(6);
        //     }
        //     else if($by_status == 7)
        //     {
        //         $status = array(7);
        //     }
        //     else if($by_status == 8)
        //     {
        //         $status = array(8,9);
        //     }
        $get_order = $get_order->whereIn("status",$status);
        // }

        $get_order = $get_order->select('m_product_id','m_product_code','m_sub_product_id','m_sub_product_code','object_pertanggungan','total_all_premi',
                                        'protection_time','protection_time_type','status','date_active_polis','m_product_penanggung_id','id','code',
                                        'm_product_penanggung_code','history_progress')->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'data'      => $get_order,
            'code'      => 200
        ],200);
    }

    public function getDetailOrder(Request $request)
    {
        $order_number   = $request->order_number;
        $user_id        = $request->user()->token()->user_id;

        $cek_order      = M_Order::where('code',$order_number)->where('user_id',$user_id)->first();

        if(!$cek_order)
        {
            return response()->json([
                'message'   => 'Data not found',
                'status'    => 'error',
                'data'      => null,
                'code'      => 404
            ],200);
        }
        DB::beginTransaction();
        try{
            // $data_join = array('get_order_pemegang_polis','get_order_perluasan');


            $get_data_order = M_Order::with([
                                                'get_order_pemegang_polis','get_order_accessories',
                                                'get_product_penanggung','get_product','get_product_penanggung.get_penanggung',
                                                'get_order_pemegang_polis.get_city','get_order_pemegang_polis.get_province',
                                                'get_order_detail','get_order_detail.get_order_perluasan'
                                            ]);
            if($cek_order->m_product_code == 'KBM'){
                $get_data_order = $get_data_order->with('get_order_kbm');
            }
            $get_data_order = $get_data_order->where('code',$order_number)
                                ->where('user_id',$user_id)
                                ->select('code','m_product_code','m_product_id','m_product_code','m_sub_product_id','m_sub_product_code',
                                        'protection_time','protection_time_type','date_active_polis','status','note','history_progress',
                                            'total_all_premi','m_product_penanggung_id','m_product_penanggung_code','object_pertanggungan_detail',
                                            'total_accessories','object_pertanggungan_after_accessories')
                                ->first();

            if($get_data_order->protection_time_type == 1){
                $type = 'Days';
            }else if($get_data_order->protection_time_type == 2){
                $type = 'Week';
            }else if($get_data_order->protection_time_type == 3){
                $type = 'Month';
            }else if($get_data_order->protection_time_type == 4){
                $type = 'Years';
            }
            $start_periode_date         = date_create(date('Y-m-d',strtotime($get_data_order->date_active_polis)));
            $end_periode_date           = date_add($start_periode_date,date_interval_create_from_date_string($get_data_order->protection_time.' '.$type));
            $end_periode                = date_format($end_periode_date,'Y-m-d');

            $get_data_order->end_periode                = $end_periode;
            $get_data_order->end_periode_format         = date('d F Y',strtotime($end_periode));
            $get_data_order->date_active_polis_format   = date('d F Y',strtotime($get_data_order->date_active_polis));
            DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => 'Success save Data',
                'status'    => 'success',
                'data'      => $get_data_order,
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
            ],500);
        }
    }
}
