<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Models\NewOrder\NewOrderModel;
use App\Models\Order\M_Order;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;

class MyOrderController extends BaseController
{
    public function __construct()
    {

        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getListOrder(Request $request)
    {
        $user_id            = $request->user()->token()->user_id;
        $by_tgl             = $request->by_tgl;
        $by_status          = $request->by_status;
        $by_polis           = $request->by_polis;
        $menu_type          = $request->menu_type;

        $get_order  =   NewOrderModel::with(
                            [
                                'get_product','get_sub_product',
                                'get_penanggung','get_penanggung.get_penanggung',
                                'get_order_sb','get_order_sb.get_project'
                            ]
                        )->where('user_id',$user_id);

        if($by_tgl){
            $get_order = $get_order->whereRaw("to_char(created_at,'YYYY-MM-DD') <= (?)",["{$by_tgl}"]);
        }

        if($by_polis != 0){
            if($by_polis == 1)
            {
                $cob_code = 'SB';
            }
            else if($by_polis == 2)
            {
                $cob_code = 'KBM';
            }
            else if($by_polis == 3)
            {
                $cob_code = 'PA';
            }else{
                $cob_code = 'ATT';
            }
            $get_order = $get_order->where("m_product_code",$cob_code);
        }

        if($menu_type == 1)
        {
            if($by_status != 0){
                $status = array($by_status);
            }else{
                $status = array(0,1,2,3,4,5,6,7);
            }
        }
        else if($menu_type == 2)
        {
            if($by_status != 0){
                if($by_status == 3){
                    $status = array($by_status,10);
                }else{
                    $status = array($by_status);
                }
            }else{
                $status = array(3,5,10);
            }
        }
        else if($menu_type == 0)
        {
            $status = array(9);
        }
        else
        {
            if($by_status != 0){
                $status = array($by_status);
            }else{
                $status = array(6,11,12);
            }

            $get_order = $get_order->with([
                            'get_invoice_active' => function($query){
                                $query->where('status',2);
                            },
                            'get_invoice_active.get_payment_active' => function($query_){
                                $query_->where('status',1);
                            }
                        ]);
        }


        $get_order = $get_order->whereIn("status",$status);

        $get_order = $get_order->select(
                                        'code','m_product_code','m_sub_product_code','total_premi','type_penanggung','status','progress',
                                        'date_active_polis','protection_time','protection_time_type','start_periode_protection','end_periode_protection',
                                        'total_payment','remaining_payment','already_paid','type_payment','m_product_penanggung_code','status_payment'
                                    )->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'data'      => $get_order,
            'code'      => 200
        ],200);
    }
}

