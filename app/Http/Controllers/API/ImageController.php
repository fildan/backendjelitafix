<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class ImageController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getImage(Request $request)
    {
        // $path =storage_path('app/public/'.$request->folder.'/'.$request->file);
        // return response()->json([
        //     'message'   => 'Success Get Data',
        //     'status'    => 'success',
        //     'code'      => 200,
        //     'data'      => $path
        // ], 200);

        $path =storage_path('app/public/'.$request->folder.'/'.$request->file);
        // $path =storage_path('app/public/order/'.$request->location);
        // return response()->json([
        //     'message'   => 'Success Get Data',
        //     'status'    => 'success',
        //     'code'      => 200,
        //     'data'      => $path
        // ], 200);

        return response()->download($path);
       
    }
}
