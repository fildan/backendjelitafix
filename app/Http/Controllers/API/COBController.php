<?php

namespace App\Http\Controllers\API;

use App\Models\Company\CompanyTypeModel;
use App\Models\M_Acc;
use App\Models\M_BrandVehicle;
use App\Models\M_City;
use App\Models\M_DocPengajuan;
use App\Models\M_Product;
use App\Models\M_SubProduct;
use App\Models\M_Faq;
use App\Models\M_Konstruksi;
use App\Models\M_PenggunaanBangunan;
use App\Models\M_Policy;
use App\Models\M_Province;
use App\Models\M_RangePriceVehicle;
use App\Models\M_SeriesVehicle;
use App\Models\M_SeriuesVehicle;
use App\Models\M_TypeVehicle;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class COBController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getList(Request $request)
    {
        $get_data = M_Product::select('name','picture','url','code','description')->where('is_develope',1)->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }

    public function getSubProduct(Request $request)
    {

        $url        = $request->url;
        $get_data   = M_SubProduct::leftJoin('m_product','m_sub_product.m_product_id','=','m_product.id')->where('m_product.url',$url)
                    ->select('m_sub_product.name','m_sub_product.picture','m_sub_product.url','m_sub_product.code','m_sub_product.description')
                    ->where('m_sub_product.is_develope',1)->get();

        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }

    public function getFaq(Request $request)
    {
        $url        = $request->url;
        $get_data   =  M_Faq::leftJoin('m_product','faq.m_product_id','=','m_product.id')->where('m_product.url',$url)->select('faq.question','faq.answer')->get();
        $query = DB::getQueryLog();
        // dd($query);
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }

    public function getAcc(Request $request)
    {

        $m_product_code = $request->product_code;
        $get_data       = M_Acc::select('name','code','id')->where('m_product_code',$m_product_code)->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }


    public function functionalBuilding(Request $request)
    {
        $get_data = M_PenggunaanBangunan::orderBy('name','ASC')->select('id','name')->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }

    public function ConstructionBuilding(Request $request)
    {
        $get_data = M_Konstruksi::orderBy('name','ASC')->select('id','name')->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }

    public function getPlatWilayah(Request $request)
    {
        $get_data = DB::table('m_plat_wilayah')->whereNull('deleted_at')->select('code','wilayah','kategori_wilayah')->orderBy('code','ASC')->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }

    public function getProvince(Request $request)
    {
        $get_data = M_Province::select('id','name')->orderBy('name','ASC')->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }

    public function getCity(Request $request)
    {

        $province_id    = $request->province_id;
        $get_data       = M_City::select('id','name')->where('m_province_id',$province_id)->orderBy('name','ASC')->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }

    public function getDocumentPengajuan(Request $request)
    {

        $url_cob                = $request->url_cob;
        $url_sub_cob            = $request->url_sub_cob;
        $is_new_object          = $request->is_new_object;
        $company_or_project     = $request->company_or_project;

        // dd($request->all());
        if($url_cob){
            $get_cob_id     = M_Product::where('url',$url_cob)->first();
            if(!$get_cob_id)
            {
                return response()->json([
                    'message'   => 'COB Not Found',
                    'status'    => 'error',
                    'code'      => 404,
                    'data'      => null
                ], 200);
            }
        }
        if($url_sub_cob)
        {
            $get_sub_cob_id = M_SubProduct::where('url',$url_sub_cob)->first();
            if(!$get_sub_cob_id){
                return response()->json([
                    'message'   => 'SUB COB Not Found',
                    'status'    => 'error',
                    'code'      => 404,
                    'data'      => null
                ], 200);
            }
        }
        $get_data       = M_DocPengajuan::with(
                            [
                                'get_product'
                                ,
                                'get_sub_product'
                            ]
                            );



        if($is_new_object){
            // dd($is_new_object);
            $get_data = $get_data->where('is_for_new_object',$is_new_object);
        }else{
            $get_data = $get_data->where('is_for_new_object',0);
        }

        // if($url_cob)
        $get_data = $get_data->where('is_company_or_project_document',$company_or_project)->orderBy('sort','ASC')->get();



        $data = array();

        $doc_hehe = '';
        $hehe = [];
        foreach($get_data as $row)
        {

            if($row->doc_multiple_type_product == 0)
            {

                if($get_cob_id->id == $row->m_product_id)
                {
                    if($row->doc_multiple_type_sub_product == 1)
                    {
                        $data[] = [
                            'doc_id'                            => $row->id,
                            'doc_code'                          => $row->code,
                            'doc_name'                          => $row->name,
                            'is_company_or_project_document'    => $row->is_company_or_project_document,
                            'is_for_new_object'                 => $row->is_for_new_object,
                            'contoh_document'                   => $row->contoh_document
                        ];

                    }else if($row->doc_multiple_type_sub_product == 2)
                    {
                        if($url_sub_cob){
                            $multiple_sub_product_hehe = (array) json_decode($row->multiple_sub_product);
                            if (in_array($get_sub_cob_id->id, $multiple_sub_product_hehe['id'])) {
                                $data[] = [
                                    'doc_id'                            => $row->id,
                                    'doc_code'                          => $row->code,
                                    'doc_name'                          => $row->name,
                                    'is_company_or_project_document'    => $row->is_company_or_project_document,
                                    'is_for_new_object'                 => $row->is_for_new_object,
                                    'contoh_document'                   => $row->contoh_document
                                ];


                            }
                        }
                    }else{
                        if($get_sub_cob_id->id == $row->m_sub_product_id)
                        {
                            $data[] = [
                                'doc_id'                            => $row->id,
                                'doc_code'                          => $row->code,
                                'doc_name'                          => $row->name,
                                'is_company_or_project_document'    => $row->is_company_or_project_document,
                                'is_for_new_object'                 => $row->is_for_new_object,
                                'contoh_document'                   => $row->contoh_document
                            ];


                        }
                    }
                }
            }
            else if($row->doc_multiple_type_product == 1)
            {
                if($row->doc_multiple_type_sub_product == 1)
                {
                    $data[] = [
                        'doc_id'                            => $row->id,
                        'doc_code'                          => $row->code,
                        'doc_name'                          => $row->name,
                        'is_company_or_project_document'    => $row->is_company_or_project_document,
                        'is_for_new_object'                 => $row->is_for_new_object,
                    ];
                }
                else if($row->doc_multiple_type_sub_product == 2)
                {
                    if($url_sub_cob){
                        $multiple_sub_product = json_decode($row->multiple_sub_product);
                        if (in_array($get_sub_cob_id->id, $multiple_sub_product['id'])) {
                            $data[] = [
                                'doc_id'                            => $row->id,
                                'doc_code'                          => $row->code,
                                'doc_name'                          => $row->name,
                                'is_company_or_project_document'    => $row->is_company_or_project_document,
                                'is_for_new_object'                 => $row->is_for_new_object,
                            ];


                        }
                    }
                }else{
                    if($url_sub_cob){
                        if($row->m_sub_product_code == $get_sub_cob_id->code)
                        {
                            $data[] = [
                                'doc_id'                            => $row->id,
                                'doc_code'                          => $row->code,
                                'doc_name'                          => $row->name,
                                'is_company_or_project_document'    => $row->is_company_or_project_document,
                                'is_for_new_object'                 => $row->is_for_new_object,
                            ];
                        }
                    }
                }

            }
            else if($row->doc_multiple_type_product == 2)
            {
                if($get_cob_id){
                    $some_product = json_decode($row->multiple_product);
                    if (in_array($get_cob_id->id, $some_product['id'])) {
                        $data[] = [
                            'doc_id'                            => $row->id,
                            'doc_code'                          => $row->code,
                            'doc_name'                          => $row->name,
                            'is_company_or_project_document'    => $row->is_company_or_project_document,
                            'is_for_new_object'                 => $row->is_for_new_object,
                        ];


                    }
                }
            }

        }
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $data
        ], 200);

    }

    function getBrandVehicle(Request $request)
    {
        $get_data = M_BrandVehicle::select('name','code')->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }
    function getTypeVehicle(Request $request)
    {
        $vehicle_code   = $request->vehicle_code;
        $get_data       = M_TypeVehicle::select('name','code')->where('m_brand_vehicle_code',$vehicle_code)->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }
    function getSeriesVehicle(Request $request)
    {
        $type_vehicle_code      = $request->type_vehicle_code;
        $get_data               = M_SeriesVehicle::select('name','code')->where('m_type_vehicle_code',$type_vehicle_code)->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }
    function getRangePriceVehicle(Request $request)
    {
        $series_vehicle_code    = $request->series_vehicle_code;
        $year                   = $request->year;
        $get_data               = M_RangePriceVehicle::select('suggested_price','range_low_price','range_high_price')->where('m_series_vehicle_code',$series_vehicle_code)->where('year',$year)->first();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_data
        ], 200);
    }
    
    function getPolicy(Request $request)
    {
        $product_code = $request->product_code;

        if($product_code == 'umum')
        {
            $product = 0;
        }
        else
        {

            $get_product = M_Product::where('url',$product_code)->first();
            $product     = $get_product->id;
        }

        $get_policy = M_Policy::where('product',$product)->get();
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $get_policy
        ], 200);

    }
    
    
    public function getCompanyType(Request $request)
    {
        $get_data = CompanyTypeModel::select('id','name')->get();
        return response()->json([
            'message'       => 'Success Get Data',
            'status'        => "success",
            'code'          => 200,
            'data'          => $get_data
        ],200);
    }

}
