<?php

namespace App\Http\Controllers\API\Order;

use App\Models\Company\UserCompanyDocumentModel;
use App\Models\Company\UserCompanyModel;
use App\Models\Company\UserCompanyPICModel;
use App\Models\Company\UserKomisarisDireksiModel;
use App\Models\DraftOrder\DraftOrderDocumentModel;
use App\Models\DraftOrder\DraftOrderModel;
use App\Models\DraftOrder\DraftOrderObligeeModel;
use App\Models\DraftOrder\DraftOrderProjectModel;
use App\Models\M_DocPengajuan;
use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_ReferencePenanggungModel;
use App\Models\M_SubProduct;
use App\Models\NewOrder\NewOrderCompanyDireksiKomisarisModel;
use App\Models\NewOrder\NewOrderCompanyModel;
use App\Models\NewOrder\NewOrderCompanyPICModel;
use App\Models\NewOrder\NewOrderDocumentModel;
use App\Models\NewOrder\NewOrderListAsuransiModel;
use App\Models\NewOrder\NewOrderModel;
use App\Models\NewOrder\NewOrderSBModel;
use App\Models\NewOrder\NewOrderSBObligeeModel;
use App\Models\NewOrder\NewOrderSBProjectModel;
use App\Models\Order\M_Order;
use App\Models\RateBySubProductModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;

class NewOrderController extends BaseController
{
    public function __construct()
    {

        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function SaveOrder(Request $request)
    {
        $draft_order_code       = $request->draft_code;
        $get_draft_id           = DraftOrderModel::where('code',$draft_order_code)->first();
        $get_cob                = M_Product::where('id',$get_draft_id->m_product_id)->first();
        $get_sub_cob            = M_SubProduct::where('id',$get_draft_id->m_sub_product_id)->first();
        $order_number           = $this->checkCode($get_cob->code);
        $request->order_code    = $order_number;
        if($get_cob->code == 'SB'){
            $save_order = $this->saveOrderSurretyBond($request,$get_draft_id,$get_cob,$get_sub_cob);
        }else{
            $save_order = [
                'message'       => 'cob not available, please try again later',
                'status'        => "error",
                'code'          => 400
            ];
        }
        return response()->json($save_order);

    }

    function saveOrderSurretyBond($request,$get_draft_id,$get_cob,$get_sub_cob)
    {

        DB::beginTransaction();
        try{
            $url_asuransi   = $request->url_asuransi;
            if($url_asuransi)
            {
                $get_product_penanggung     = M_ProductPenanggung::where('url',$url_asuransi)->first();
                $m_product_penanggung_id    = $get_product_penanggung->id;
                $m_product_penanggung_code  = $get_product_penanggung->code;
                $penanggung_type            = 1;
            }else{
                $m_product_penanggung_id    = null;
                $m_product_penanggung_code  = null;
                $penanggung_type            = 2;
            }

            $get_draft_project  = DraftOrderProjectModel::where('draft_order_code',$request->draft_code)->first();

            $user_id                                        = $request->user()->token()->user_id;

            $progress[] = array(
                'date'      => date('Y-m-d H:i:s'),
                'status'    => 0,
                'note'      => 'Mohon untuk segera submit'
            );


            $object_pertanggungan_detail   = json_encode(array('nilai_jaminan' => $get_draft_project->collateral_value));
            $data_order     = [
                'code'                                      => $request->order_code,
                'user_id'                                   => $user_id,
                'm_product_id'                              => $get_cob->id,
                'm_product_code'                            => $get_cob->code,
                'm_sub_product_id'                          => $get_sub_cob->id,
                'm_sub_product_code'                        => $get_sub_cob->code,
                'm_product_penanggung_id'                   => $m_product_penanggung_id,
                'm_product_penanggung_code'                 => $m_product_penanggung_code,
                'type_penanggung'                           => $penanggung_type,
                'protection_time'                           => $get_draft_project->periode_value,
                'protection_time_type'                      => $get_draft_project->periode_type,
                'date_active_polis'                         => $get_draft_project->start_date_periode,
                'status'                                    => 0,
                'progress'                                  => json_encode($progress),
                'created_at'                                => date('Y-m-d H:i:s'),
                'created_by'                                => $user_id,
                'type_payment'                              => 1,
                'total_premi'                               => 0,
                'total_payment'                             => 0,
                'remaining_payment'                         => 0,
                'already_paid'                              => 0,
                'start_periode_protection'                  => $get_draft_project->start_date_periode,
                'end_periode_protection'                    => $get_draft_project->end_date_periode,
            ];
            $save_order = NewOrderModel::insertGetId($data_order);

            if($penanggung_type == 2)
            {
                $nama_asuransi          = $request->nama_asuransi;
                $reference_penanggung   = $request->reference_penanggung;

                if($reference_penanggung == 'lainnya'){
                    $m_reference_penanggung_code = null;
                    $m_reference_penanggung_id   = null;
                    $company_type                = null;
                    $name                        = $nama_asuransi;
                }else{
                    $get_reference = M_ReferencePenanggungModel::where('code',$reference_penanggung)->first();
                    $m_reference_penanggung_code = $get_reference->code;
                    $m_reference_penanggung_id   = $get_reference->id;
                    $company_type                = $get_reference->company_type;
                    $name                        = $get_reference->name;
                }
                $data_list_asuransi[] = [
                    'code'                          => $request->order_code.'-'.time(),
                    'new_order_id'                  => $save_order,
                    'new_order_code'                => $request->order_code,
                    'name'                          => $name,
                    'company_type'                  => $company_type,
                    'created_at'                    => date('Y-m-d H:i:s'),
                    'created_by'                    => $user_id,
                    'm_reference_penanggung_code'   => $m_reference_penanggung_code,
                    'm_reference_penanggung_id'     => $m_reference_penanggung_id
                ];
                $save_list_asuransi     = NewOrderListAsuransiModel::insert($data_list_asuransi);
            }

            $get_rate_sb        = RateBySubProductModel::where('m_sub_product_code',$get_sub_cob->code)->first();

            if($url_asuransi)
            {

                $estimation_rate                = $get_product_penanggung->estimation_rate;
                if($get_product_penanggung->type_estimation_rate == 1)
                {
                    $estimation_rate_type           = 1;
                    $estimation_rate_periode_type   = null;
                    $estimation_rate_periode_value  = null;
                    $estimation_premi_per_periode   = 0;
                    $total_estimation_premi         = $get_draft_project->collateral_value * ($get_product_penanggung->estimation_rate / 100);
                }
                else
                {

                    $estimation_rate_type           = 2;
                    $estimation_rate_periode_type   = $get_product_penanggung->estimation_rate_type_periode;
                    $estimation_rate_periode_value  = $get_product_penanggung->estimation_rate_value_periode;
                    $estimation_premi_per_periode   = $get_draft_project->collateral_value * ($get_product_penanggung->estimation_rate / 100);

                    $get_periode_project            = $get_draft_project->periode_value / $get_product_penanggung->estimation_rate_value_periode;
                    $get_periode_project            = ceil($get_periode_project);
                    $total_estimation_premi         = $estimation_premi_per_periode * $get_periode_project;
                }
            }else{
                $estimation_rate_type           = null;
                $estimation_rate_periode_type   = null;
                $estimation_rate_periode_value  = null;
                $estimation_premi_per_periode   = null;
                $total_estimation_premi         = null;
                $estimation_rate                = null;
            }


            $data_save_sb  = [
                'new_order_id'                              => $save_order,
                'new_order_code'                            => $request->order_code,
                'code'                                      => 'SB'.$request->order_code,
                'periode_type'                              => $get_draft_project->periode_type,
                'periode_value'                             => $get_draft_project->periode_value,
                'fix_rate'                                  => 0,
                'fix_rate_periode_type'                     => 0,
                'fix_rate_periode_value'                    => 0,
                'fix_rate_type'                             => 0,
                'fix_premi_per_periode'                     => 0,
                'total_fix_premi'                           => 0,
                'estimation_rate'                           => $estimation_rate,
                'estimation_rate_type'                      => $estimation_rate_type,
                'estimation_rate_periode_type'              => $estimation_rate_periode_type,
                'estimation_rate_periode_value'             => $estimation_rate_periode_value,
                'estimation_premi_per_periode'              => $estimation_premi_per_periode,
                'total_estimation_premi'                    => $total_estimation_premi,
                'created_at'                                 => date('Y-m-d H:i:s'),
                'created_by'                                => $user_id,
            ];

            $save_order_sb  = NewOrderSBModel::insertGetId($data_save_sb);

            $get_company    = UserCompanyModel::where('code',$get_draft_id->user_company_code)->first();
            $komisaris      = UserKomisarisDireksiModel::where('user_company_code',$get_company->code)->get();
            $save_company   = [
                'new_order_id'      => $save_order,
                'new_order_code'    => $request->order_code,
                'name'              => $get_company->name,
                'type'              => $get_company->type,
                'jenis_usaha'       => $get_company->jenis_usaha,
                'bidang_usaha'      => $get_company->bidang_usaha,
                'npwp'              => $get_company->npwp,
                'email'             => $get_company->email,
                'no_telepon'        => $get_company->no_telepon,
                'm_province_id'     => $get_company->m_province_id,
                'm_city_id'         => $get_company->m_city_id,
                'postcode'          => $get_company->postcode,
                'address'           => $get_company->address,
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => $user_id,
                'code'              => 'SB-COM-'.$request->order_code
            ];
            $company_save = NewOrderCompanyModel::insertGetId($save_company);


            foreach($komisaris as $row)
            {
                $data_komisaris = [
                    'type'                          =>  $row->type,
                    'name'                          =>  $row->name,
                    'identity_file'                 =>  $row->identity_file,
                    'position'                      =>  $row->position,
                    'new_order_data_company_id'     =>  $company_save,
                    'new_order_data_company_code'   =>  $save_company['code'],
                    'created_at'                    =>  date('Y-m-d H:i:s'),
                    'created_by'                    =>  $user_id,
                    'code'                          =>  ($row->type == '1'?'KOM':'DIR').rand(000,999).'-'.$save_company['code']
                ];

                Storage::copy('public/company-document/company-code-'.$get_company->code.'/'.$row->identity_file, 'public/order/sb/order-code-'.$request->order_code.'/'.'company/'.$row->identity_file);

                $save_komasirs = NewOrderCompanyDireksiKomisarisModel::insert($data_komisaris);
            }

            $get_pic = UserCompanyPICModel::where('user_company_code',$get_company->code)->get();
            foreach($get_pic as $row)
            {
                $data_pic = [
                    'name'                          =>  $row->name,
                    'telepon'                       =>  $row->telepon,
                    'email'                         =>  $row->email,
                    'posisi'                        =>  $row->posisi,
                    'code_telepon'                  =>  $row->code_telepon,
                    'new_order_data_company_id'     =>  $company_save,
                    'new_order_data_company_code'   =>  $save_company['code'],
                    'created_at'                    =>  date('Y-m-d H:i:s'),
                    'created_by'                    =>  $user_id,
                    'code'                          =>  'PIC'.'-'.$save_company['code'].'-'.rand(000,999)
                ];


                $save_pic = NewOrderCompanyPICModel::insert($data_pic);
            }

            $data_project = [
                'code'                      => $get_draft_project->code,
                'name'                      => $get_draft_project->name,
                'date'                      => $get_draft_project->date,
                'description'               => $get_draft_project->description,
                'contract_value'            => $get_draft_project->contract_value,
                'collateral_value'          => $get_draft_project->collateral_value,
                'start_date_periode'        => $get_draft_project->start_date_periode,
                'end_date_periode'          => $get_draft_project->end_date_periode,
                'periode_type'              => $get_draft_project->periode_type,
                'periode_value'             => $get_draft_project->periode_value,
                'new_order_sb_id'           => $save_order_sb,
                'new_order_sb_code'         => $data_save_sb['code'],
                'created_at'                => date('Y-m-d H:i:s'),
                'created_by'                => $user_id,
                'number_support_document'   => $get_draft_project->number_support_document,
                'date_support_document'     => $get_draft_project->date_support_document
            ];
            $save_project = NewOrderSBProjectModel::insert($data_project);

            $get_obligee  = DraftOrderObligeeModel::where('draft_order_code',$request->draft_code)->first();
            $data_obligee = [
                'new_order_sb_id'           => $save_order_sb,
                'new_order_sb_code'         => $data_save_sb['code'],
                'code'                      => $get_obligee->code,
                'company_type'              => $get_obligee->company_type,
                'company_name'              => $get_obligee->company_name,
                'jenis_usaha'               => $get_obligee->jenis_usaha,
                'bidang_usaha'              => $get_obligee->bidang_usaha,
                'website'                   => $get_obligee->website,
                'email'                     => $get_obligee->email,
                'no_telepon'                => $get_obligee->no_telepon,
                'm_province_id'             => $get_obligee->m_province_id,
                'm_province_name'           => $get_obligee->m_province_name,
                'm_city_id'                 => $get_obligee->m_city_id,
                'm_city_name'               => $get_obligee->m_city_name,
                'postcode'                  => $get_obligee->postcode,
                'address'                   => $get_obligee->address,
                'created_at'                => date('Y-m-d H:i:s'),
                'created_by'                => $user_id,
            ];

            $save_obligee = NewOrderSBObligeeModel::insert($data_obligee);

            $get_doc_company = getDocPengajuan($get_cob->url,$get_sub_cob->url,1);

            $data_document = [];

            foreach($get_doc_company as $row => $val){
                $cek_doc_user_company = UserCompanyDocumentModel::where('m_document_upload_code',$val['doc_code'])->where('user_company_code',$get_company->code)->first();

                if($cek_doc_user_company)
                {
                    $data_document[] = [
                        'new_order_id'              => $save_order,
                        'new_order_code'            => $request->order_code,
                        'm_document_upload_id'      => $cek_doc_user_company->m_document_upload_id,
                        'm_document_upload_code'    => $val['doc_code'],
                        'name'                      => $val['doc_name'],
                        'document_name'             => $cek_doc_user_company->file_name,
                        'document_type'             => 1,
                        'date_document'             => null,
                        'number_document'           => null,
                        'note_document'             => null,
                        'created_at'                => date('Y-m-d H:i:s'),
                        'created_by'                => $user_id,
                    ];

                    Storage::copy('public/company-document/company-code-'.$get_company->code.'/'.$cek_doc_user_company->file_name, 'public/order/sb/order-code-'.$request->order_code.'/'.'company/'.$cek_doc_user_company->file_name);
                }
            }


            $get_document  = DraftOrderDocumentModel::where('draft_order_code',$request->draft_code)->get();
            foreach($get_document as $row)
            {

                $data_document[] = [
                    'new_order_id'              => $save_order,
                    'new_order_code'            => $request->order_code,
                    'm_document_upload_id'      => $row->m_document_upload_id,
                    'm_document_upload_code'    => $row->m_document_upload_code,
                    'name'                      => $row->m_document_upload_name,
                    'document_name'             => $row->file_name,
                    'document_type'             => 2,
                    'date_document'             => $row->date_document,
                    'number_document'           => $row->number_document,
                    'note_document'             => $row->note_document,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'created_by'                => $user_id,
                ];

                Storage::copy('public/draft-order/'.$request->draft_code.'/'.$row->file_name, 'public/order/sb/order-code-'.$request->order_code.'/'.'project/'.$row->file_name);
            }

            $save_document = NewOrderDocumentModel::insert($data_document);

            DB::commit();

            DraftOrderModel::where('code', $get_draft_id->code)->delete();
            DraftOrderProjectModel::where('draft_order_code', $get_draft_id->code)->delete();
            DraftOrderObligeeModel::where('draft_order_code', $get_draft_id->code)->delete();
            DraftOrderDocumentModel::where('draft_order_code', $get_draft_id->code)->delete();
            $response = [
                'order_code'    => $request->order_code,
                'message'       => 'Success save Data',
                'status'        => 'success',
                'code'          => 200,
            ];

            Storage::deleteDirectory('public/draft-order/'.$request->draft_code);
            return $response;
        }catch (\Exception $e) {
            DB::rollback();
            Storage::deleteDirectory('public/order/sb/order-code-'.$request->order_code);
            $response = [
                'order_code'    => null,
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => 'error',
                'code'          => 500,
            ];
            return $response;

        }


    }




    public function GetDetailOrder(Request $request)
    {
        $order_number   = $request->order_number;
        $user_id        = $request->user()->token()->user_id;

        $cek_order      = NewOrderModel::where('code',$order_number)->where('user_id',$user_id)->first();
        if(!$cek_order)
        {
            return response()->json([
                'message'   => 'Data not found',
                'status'    => 'error',
                'data'      => null,
                'code'      => 404
            ],200);
        }
        // DB::beginTransaction();
        try{
            $get_data_order = NewOrderModel::with([
                                                    'get_document','get_list_asuransi',
                                                    'get_product','get_sub_product'
                                            ]);
            if($cek_order->m_product_code == 'SB')
            {
                $get_data_order = $get_data_order->with(
                                                            'get_order_sb','get_order_sb.get_project','get_order_sb.get_obligee','get_order_company','get_order_company.get_company_type',
                                                            'get_order_company.get_direksi_komisaris','get_order_company.get_pic','get_order_company.get_province',
                                                            'get_order_company.get_city','get_order_sb.get_obligee.get_province','get_order_sb.get_obligee.get_city','get_order_sb.get_obligee.get_company_type'
                                                    );
            }

            if($cek_order->type_penanggung == 1){
                $get_data_order = $get_data_order->with('get_penanggung','get_penanggung.get_penanggung');
            }
            $get_data_order  = $get_data_order->where('code',$cek_order->code)
                                                ->select(
                                                        "code","m_product_code","m_sub_product_code","total_premi","total_payment",
                                                        "remaining_payment","already_paid","type_payment","type_penanggung","m_product_penanggung_code",
                                                        "user_id","status","progress","date_active_polis","protection_time","protection_time_type",
                                                        "start_periode_protection","end_periode_protection"
                                                )->first();

            if($get_data_order->protection_time_type == 1){
                $type = 'Hari';
            }else if($get_data_order->protection_time_type == 2){
                $type = 'Minggu';
            }else if($get_data_order->protection_time_type == 3){
                $type = 'Bulan';
            }else if($get_data_order->protection_time_type == 4){
                $type = 'Tahun';
            }
            $get_data_order->time_protection_type   = $type;
            $get_data_order->start_periode_format   = date('d F Y',strtotime($get_data_order->start_periode_protection));
            $get_data_order->end_periode_format     = date('d F Y',strtotime($get_data_order->end_periode_protection));
            // DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => 'Success save Data',
                'status'    => 'success',
                'data'      => $get_data_order,
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            // DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
            ],500);
        }

    }

    function checkCode($cob_code)
    {
        $order_number   =   'ORD-'.$cob_code.date('dmy');
        $order_number    .=   rand(0000,9999);

        $cek_order_code = NewOrderModel::where('code',$order_number)->get();
        while (count($cek_order_code)>0) {
            $order_number    = substr($order_number,0,-4);
            $order_number   .= rand(0000,9999);
            $cek_order_code = NewOrderModel::where('code',$order_number)->get();
        }

        return $order_number;
    }

    public function GetDocument(Request $request)
    {
        $path =storage_path('app/public/order/'.$request->location);
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $path
        ], 200);
    }

    public function SubmitOrder(Request $request)
    {
        $order_code = $request->order_number;
        $user_id        = $request->user()->token()->user_id;
        $cek_order      = NewOrderModel::where('code',$order_code)->where('user_id',$user_id)->first();
        if(!$cek_order)
        {
            return response()->json([
                'message'   => 'Data not found',
                'status'    => 'error',
                'data'      => null,
                'code'      => 404
            ],200);
        }

        DB::beginTransaction();
        try{

            $get_progress = json_decode($cek_order->progress);
            $progress = [];
            foreach($get_progress as $row => $val)
            {
                $progress[] = [
                    'date'      => $val->date,
                    'status'    => $val->status,
                    'note'      => $val->note
                ];
            }
            if($cek_order->status == 0){
                $progress[] = array(
                    'date'      => date('Y-m-d H:i:s'),
                    'status'    => 1,
                    'note'      => 'Anda telah melakukan submit'
                );
                $status_now = 1;
            }else{
                $progress[] = array(
                    'date'      => date('Y-m-d H:i:s'),
                    'status'    => 7,
                    'note'      => 'Anda telah melakukan perubahan data'
                );

                $status_now = 7;
            }

            $update_order = NewOrderModel::where('code',$order_code)->update([
                'updated_at'    => date('Y-m-d H:i:s'),
                'updated_by'    => $user_id,
                'progress'      => json_encode($progress),
                'status'        => $status_now
            ]);
            DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => 'Success save Data',
                'status'    => 'success',
                'data'      => $update_order,
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
            ],500);
        }
    }
    
}
