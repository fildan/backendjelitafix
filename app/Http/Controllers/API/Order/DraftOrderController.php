<?php

namespace App\Http\Controllers\API\Order;

use App\Models\DraftOrder\DraftOrderModel;
use App\Models\Order\M_Order;
use App\Models\RateBySubProductModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;

class DraftOrderController extends BaseController
{
    public function __construct()
    {

        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getData(Request $request)
    {
        $user_id            = $request->user()->token()->user_id;
        $draft_order_code   = $request->draft_code;

        $cek_data           = DraftOrderModel::where('code',$draft_order_code)->where('user_id',$user_id)->first();

        if(!$cek_data){
            return response()->json([
                'message'   => 'Data Not Found',
                'status'    => 'error',
                'data'      => null,
                'code'      => 404
            ],200);
        }

        // DB::beginTransaction();
        try{
            $get_data           = DraftOrderModel::with(['get_document'])->where('code',$draft_order_code);
            if($cek_data->m_product_code == 'SB')
            {
                $get_data = $get_data->with(['get_obligee','get_project','get_company','get_company.get_province','get_company.get_city','get_company.get_document','get_company.get_pic','get_company.get_komisaris_dan_direksi','get_rate']);

                if($cek_data->penanggung_type == 1){
                    $get_data = $get_data->with(['get_fix_asuransi']);
                }else{
                    $get_data = $get_data->with(['get_list_request_asuransi']);
                }
            }

            $get_data = $get_data->select('code','m_product_code','m_sub_product_code','status','user_company_code','penanggung_type','m_penanggung_code')->first();

            // DB::commit();

            return response()->json([
                'message'       => 'Success get data',
                'status'        => 'success',
                'data'          => $get_data,
                'code'          => 200,
            ], 200);
        }catch (\Exception $e) {
        //     DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }

    }
}
