<?php

namespace App\Http\Controllers\API\Order;

use App\Models\M_City;
use App\Models\M_DocPengajuan;
use App\Models\M_Province;
use App\Models\NewOrder\NewOrderCompanyDireksiKomisarisModel;
use App\Models\NewOrder\NewOrderCompanyModel;
use App\Models\NewOrder\NewOrderCompanyPICModel;
use App\Models\NewOrder\NewOrderDocumentModel;
use App\Models\NewOrder\NewOrderListAsuransiModel;
use App\Models\NewOrder\NewOrderModel;
use App\Models\NewOrder\NewOrderSBModel;
use App\Models\NewOrder\NewOrderSBObligeeModel;
use App\Models\NewOrder\NewOrderSBProjectModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;

class OrderSBController extends BaseController
{
    public function __construct()
    {

        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function updateOrderSBCompany(Request $request)
    {

        $code               = $request->code;
        $company_type       = $request->company_type;
        $company_name       = $request->company_name;
        $jenis_badan_usaha  = $request->jenis_badan_usaha;
        $bidang_usaha       = $request->bidang_usaha;
        $no_npwp            = $request->no_npwp;
        $email              = $request->email;
        $kode_area          = $request->kode_area;
        $telepon            = $request->telepon;
        $province           = $request->province;
        $city               = $request->city;
        $postcode           = $request->postcode;
        $alamat             = $request->alamat;

        $pic_name           = $request->pic_name;
        $pic_email          = $request->pic_email;
        $pic_telp_code      = $request->pic_telp_code;
        $pic_telp           = $request->pic_telp;
        $pic_position       = $request->pic_position;

        $nama_komisaris     = $request->nama_komisaris;
        $ktp_komisaris      = $request->ktp_komisaris;
        $jabatan_komisaris  = $request->jabatan_komisaris;

        $nama_direksi       = $request->nama_direksi;
        $ktp_direksi        = $request->ktp_direksi;
        $jabatan_direksi    = $request->jabatan_direksi;

        $document_company   = $request->document_company;
        $note_document      = $request->note_document;
        $user_id            = $request->user()->token()->user_id;
        $uuid               = Uuid::uuid4();

        $cek_new_order      = NewOrderModel::where('code',$code)->where('user_id',$user_id)->first();


        if(!$cek_new_order)
        {
            return response()->json([
                'message'       => 'Data order tidak ditemukan',
                'status'        => "error",
                'code'          => 404
            ],200);
        }

        $get_company = NewOrderCompanyModel::where('new_order_code',$code)->first();

        $new_file = [];
        $old_file = [];
        DB::beginTransaction();
        try{
            $telepon_number = [];
            if($kode_area){
                foreach($kode_area as $kycodeare => $valcodearea)
                {

                    if(array_key_exists($kycodeare,$telepon)){
                        $telp = $telepon[$kycodeare];
                        $telepon_number[] = [
                            'code'      => $valcodearea,
                            'number'    => $telp
                        ];
                    }


                }
            }

            if($telepon_number){
                $telepon_number = json_encode($telepon_number);
            }else{
                $telepon_number = null;
            }
            $data_company       = [
                'name'          => $company_name,
                'type'          => $company_type,
                'jenis_usaha'   => $jenis_badan_usaha,
                'bidang_usaha'  => $bidang_usaha,
                'npwp'          => $no_npwp,
                'email'         => $email,
                'no_telepon'    => $telepon_number,
                'm_province_id' => $province,
                'm_city_id'     => $city,
                'postcode'      => $postcode,
                'address'       => $alamat,
                'updated_at'    => date('Y-m-d H:i:s'),
                'updated_by'    => $user_id
            ];

            $update_company = NewOrderCompanyModel::where('new_order_code',$code)->update($data_company);

            // $get_komisaris_direksi  = NewOrderCompanyDireksiKomisarisModel::where('new_order_data_company_code',$get_company->code);

            $get_komisaris = NewOrderCompanyDireksiKomisarisModel::where('new_order_data_company_code',$get_company->code)->where('type',1)->get();
            foreach($get_komisaris as $row)
            {
                if(array_key_exists($row->code,$nama_komisaris))
                {
                    $data_komisaris['name'] = $nama_komisaris[$row->code];

                    if(array_key_exists($row->code,$jabatan_komisaris))
                    {
                        $data_komisaris['position'] = $jabatan_komisaris[$row->code];
                    }

                    if($ktp_komisaris){
                        if(array_key_exists($row->code,$ktp_komisaris))
                        {
                            $ktp_komisaris_file = generate_url(strtolower($row->name.'-'.$row->position.'-new-at-'.time())).'.'.$ktp_komisaris[$row->code]->extension();
                            $ktp_komisaris_file = strtolower($ktp_komisaris_file);

                            $upload_path_attachment = storage_path('app/public/order/sb/order-code-'.$code.'/company');

                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $ktp_komisaris[$row->code], $ktp_komisaris_file);
                                }
                            } else {
                                Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $ktp_komisaris[$row->code], $ktp_komisaris_file);
                            }
                            $new_file[] = $ktp_komisaris_file;
                            $old_file[] = $row->identity_file;
                            $data_komisaris['identity_file'] = $ktp_komisaris_file;
                        }
                    }

                    $data_komisaris['updated_at'] = date('Y-m-d H:i:s');
                    $data_komisaris['updated_by'] = $user_id;
                }
                else
                {
                    $data_komisaris['deleted_at'] = date('Y-m-d H:i:s');
                    $data_komisaris['deleted_by'] = $user_id;
                    $old_file[]                   = $row->identity_file;
                    // Storage::copy('public/order/sb/order-code-'.$code.'/company/'.$row->identity_file, 'public/order/junk/sb/order-code-'.$code.'/'.'company/'.$row->identity_file);
                }

                $update_komisaris = NewOrderCompanyDireksiKomisarisModel::where('id',$row->id)->update($data_komisaris);
            }

            if($nama_komisaris)
            {
                foreach($nama_komisaris as $key => $val)
                {
                    if(!empty($val)){
                        $cek_komisaris = NewOrderCompanyDireksiKomisarisModel::where('new_order_data_company_code',$get_company->code)->where('type',1)->where('code',$key)->first();
                        if(!$cek_komisaris)
                        {
                            $insert_komisaris['name'] = $val;
                            if(array_key_exists($key,$jabatan_komisaris))
                            {
                                $insert_komisaris['position'] = $jabatan_komisaris[$key];
                            }
    
    
                            if($ktp_komisaris){
                                if(array_key_exists($key,$ktp_komisaris))
                                {
                                    $ktp_komisaris_file = generate_url(strtolower($val.'-'.$jabatan_komisaris[$key].'-new-at-'.time())).'.'.$ktp_komisaris[$key]->extension();
                                    $ktp_komisaris_file = strtolower($ktp_komisaris_file);
    
                                    $upload_path_attachment = storage_path('app/public/order/sb/order-code-'.$code.'/company');
    
                                    if (!file_exists($upload_path_attachment)) {
                                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                            Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $ktp_komisaris[$key], $ktp_komisaris_file);
                                        }
                                    } else {
                                        Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $ktp_komisaris[$key], $ktp_komisaris_file);
                                    }
                                    $new_file[]                      = $ktp_komisaris_file;
                                    $insert_komisaris['identity_file'] = $ktp_komisaris_file;
                                }
                            }
                            $insert_komisaris['created_at']                     = date('Y-m-d H:i:s');
                            $insert_komisaris['type']                           = 1;
                            $insert_komisaris['created_by']                     = $user_id;
                            $insert_komisaris['new_order_data_company_id']      = $get_company->id;
                            $insert_komisaris['new_order_data_company_code']    = $get_company->code;
    
                            $save_komisaris = NewOrderCompanyDireksiKomisarisModel::insert($insert_komisaris);
    
                        }
                    }
                }
            }

            $get_direksi = NewOrderCompanyDireksiKomisarisModel::where('new_order_data_company_code',$get_company->code)->where('type',2)->get();
            foreach($get_direksi as $row)
            {
                if(array_key_exists($row->code,$nama_direksi))
                {
                    $data_direksi['name'] = $nama_direksi[$row->code];

                    if(array_key_exists($row->code,$jabatan_direksi))
                    {
                        $data_direksi['position'] = $jabatan_direksi[$row->code];
                    }
                    if($ktp_direksi){
                        if(array_key_exists($row->code,$ktp_direksi))
                        {
                            $ktp_direksi_file = generate_url(strtolower($row->name.'-'.$row->position.'-new-at-'.time())).'.'.$ktp_direksi[$row->code]->extension();
                            $ktp_direksi_file = strtolower($ktp_direksi_file);

                            $upload_path_attachment = storage_path('app/public/order/sb/order-code-'.$code.'/company');

                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $ktp_direksi[$row->code], $ktp_direksi_file);
                                }
                            } else {
                                Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $ktp_direksi[$row->code], $ktp_direksi_file);
                            }
                            $new_file[] = $ktp_direksi_file;
                            $old_file[] = $row->identity_file;
                            $data_direksi['identity_file'] = $ktp_direksi_file;
                        }
                    }

                    $data_direksi['updated_at'] = date('Y-m-d H:i:s');
                    $data_direksi['updated_by'] = $user_id;
                }
                else
                {
                    $data_direksi['deleted_at'] = date('Y-m-d H:i:s');
                    $data_direksi['deleted_by'] = $user_id;
                    $old_file[]                   = $row->identity_file;
                }

                $update_komisaris = NewOrderCompanyDireksiKomisarisModel::where('id',$row->id)->update($data_direksi);
            }

            if($nama_direksi)
            {
                foreach($nama_direksi as $key => $val)
                {
                    if(!empty($val)){
                        $cek_direksi = NewOrderCompanyDireksiKomisarisModel::where('new_order_data_company_code',$get_company->code)->where('type',2)->where('code',$key)->first();
                        if(!$cek_direksi)
                        {
                            $insert_direksi['name'] = $val;
                            if(array_key_exists($key,$jabatan_direksi))
                            {
                                $insert_direksi['position'] = $jabatan_direksi[$key];
                            }
    
                            if($ktp_direksi){
                                if(array_key_exists($key,$ktp_direksi))
                                {
                                    $ktp_direksi_file = generate_url(strtolower($val.'-'.$jabatan_direksi[$key].'-new-at-'.time())).'.'.$ktp_direksi[$key]->extension();
                                    $ktp_direksi_file = strtolower($ktp_direksi_file);
    
                                    $upload_path_attachment = storage_path('app/public/order/sb/order-code-'.$code.'/company');
    
                                    if (!file_exists($upload_path_attachment)) {
                                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                            Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $ktp_direksi[$key], $ktp_direksi_file);
                                        }
                                    } else {
                                        Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $ktp_direksi[$key], $ktp_direksi_file);
                                    }
                                    $new_file[]                      = $ktp_direksi_file;
                                    $insert_direksi['identity_file'] = $ktp_direksi_file;
                                }
                            }
                            $insert_direksi['created_at']                       = date('Y-m-d H:i:s');
                            $insert_direksi['created_by']                       = $user_id;
                            $insert_direksi['type']                             = 2;
                            $insert_komisaris['new_order_data_company_id']      = $get_company->id;
                            $insert_komisaris['new_order_data_company_code']    = $get_company->code;
    
                            $save_komisaris = NewOrderCompanyDireksiKomisarisModel::insert($insert_direksi);
    
                        }
                    }
                }
            }


            $get_pic = NewOrderCompanyPICModel::where('new_order_data_company_code',$get_company->code)->get();

            foreach($get_pic as $row)
            {
                if(array_key_exists($row->code,$pic_name))
                {
                    $data_pic['name'] = $pic_name[$row->code];

                    if(array_key_exists($row->code,$pic_email))
                    {
                        $data_pic['email'] = $pic_email[$row->code];
                    }
                    if(array_key_exists($row->code,$pic_position))
                    {
                        $data_pic['posisi'] = $pic_position[$row->code];
                    }
                    if(array_key_exists($row->code,$pic_telp))
                    {
                        $data_pic['telepon'] = $pic_telp[$row->code];
                    }
                    if(array_key_exists($row->code,$pic_telp_code))
                    {
                        $data_pic['code_telepon'] = $pic_telp_code[$row->code];
                    }


                    $data_pic['updated_at'] = date('Y-m-d H:i:s');
                    $data_pic['updated_by'] = $user_id;
                }
                else
                {
                    $data_pic['deleted_at'] = date('Y-m-d H:i:s');
                    $data_pic['deleted_by'] = $user_id;
                }
                $update_pic = NewOrderCompanyPICModel::where('id',$row->id)->update($data_pic);
            }

            if($pic_name)
            {
                foreach($pic_name as $key => $val)
                {
                    $cek_pic = NewOrderCompanyPICModel::where('code',$key)->first();

                    if(!$cek_pic)
                    {
                        $insert_pic['name'] = $val;

                        if(array_key_exists($key,$pic_email))
                        {
                            $insert_pic['email'] = $pic_email[$key];
                        }

                        if(array_key_exists($key,$pic_position))
                        {
                            $insert_pic['posisi'] = $pic_position[$key];
                        }

                        if(array_key_exists($key,$pic_telp))
                        {
                            $insert_pic['telepon'] = $pic_telp[$key];
                        }

                        if(array_key_exists($key,$pic_telp_code))
                        {
                            $insert_pic['code_telepon'] = $pic_telp_code[$key];
                        }
                        
                        if(!empty($val) || !empty($pic_email[$key]) || !empty($pic_position[$key]) || !empty($pic_position[$key]) )
                        {

                            $insert_pic['created_at']                       = date('Y-m-d H:i:s');
                            $insert_pic['created_by']                       = $user_id;
                            $insert_pic['new_order_data_company_id']        = $get_company->id;
                            $insert_pic['new_order_data_company_code']      = $get_company->code;
    
                            $save_pic = NewOrderCompanyPICModel::insert($insert_pic);
                        }
                    }
                }
            }

            if($document_company)
            {
                foreach($document_company as $key => $val)
                {
                    $cek_document   = NewOrderDocumentModel::where('new_order_code',$code)->where('m_document_upload_code',$key)->first();

                    $get_doc_upload = M_DocPengajuan::where('code',$key)->first();
                    $doc_name       = generate_url($get_doc_upload->name).'-new-at-'.time().'.'.$val->extension();;
                    $upload_path_attachment = storage_path('public/order/sb/order-code-'.$code.'/company');

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $val, $doc_name);
                        }
                    } else {
                        Storage::putFileAs('public/order/sb/order-code-'.$code.'/company', $val, $doc_name);
                    }

                    if($note_document){
                        if(array_key_exists($key,$note_document))
                        {
                            $data_doc['note_document'] = $note_document[$key];
                        }
                    }
                    $new_file[] = $doc_name;
                    if($cek_document)
                    {
                        $old_file[]                 = $cek_document->document_name;
                        $data_doc['document_name']  = $doc_name;
                        $data_doc['updated_at']     = date('Y-m-d H:i:s');
                        $data_doc['updated_by']     = $user_id;

                        $update_doc = NewOrderDocumentModel::where('id',$cek_document->id)->update($data_doc);
                    }
                    else
                    {

                        $data_doc['document_type']          = 1;
                        $data_doc['new_order_id']           = $cek_new_order->id;
                        $data_doc['new_order_code']         = $cek_new_order->code;
                        $data_doc['m_document_upload_id']   = $get_doc_upload->id;
                        $data_doc['m_document_upload_code'] = $get_doc_upload->code;
                        $data_doc['name']                   = $get_doc_upload->name;
                        $data_doc['document_name']          = $doc_name;
                        $data_doc['created_at']             = date('Y-m-d H:i:s');
                        $data_doc['created_by']             = $user_id;
                        $insert_doc = NewOrderDocumentModel::insert($data_doc);
                    }

                }
            }

            if($note_document)
            {
                foreach($note_document as $key => $val)
                {
                    $cek_document   = NewOrderDocumentModel::where('new_order_code',$code)->where('m_document_upload_code',$key)->first();

                    if($cek_document)
                    {
                        $note_doc['note_document']  = $val;
                        $note_doc['updated_at']     = date('Y-m-d H:i:s');
                        $note_doc['updated_by']     = $user_id;
                        $update_doc                 = NewOrderDocumentModel::where('id',$cek_document->id)->update($note_doc);
                    }
                }
            }
            DB::commit();

            foreach($old_file as $key => $val)
            {
                if (Storage::exists('public/order/sb/order-code-'.$code.'/company/'.$val)) {

                    Storage::move('public/order/sb/order-code-'.$code.'/company/'.$val, 'public/order/junk/sb/order-code-'.$code.'/'.'company/'.$val);
                }
            }
            return response()->json([
                'message'       => 'Success save Data',
                'status'        => 'success',
                'order_code'    => $code,
                'code'          => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            foreach($new_file as $key => $val)
            {
                if (Storage::exists('public/order/sb/order-code-'.$code.'/company/'.$val)) {
                    Storage::delete('/public/order/sb/order-code-'.$code.'/company/'.$val);
                }
            }
            return response()->json([
                'message'       => 'Failed to save Data : '.$e->getMessage().', Line: '. $e->getLine(),
                'status'        => 'error',
                'order_code'    => $code,
                'code'          => 500,
            ], 500);
        }
    }


    public function updateOrderSBProject(Request $request)
    {
        $code               = $request->code;
        $company_type       = $request->company_type;
        $company_name       = $request->company_name;
        $jenis_badan_usaha  = $request->jenis_badan_usaha;
        $bidang_usaha       = $request->bidang_usaha;
        $website            = $request->website;
        $email              = $request->email;
        $kode_area          = $request->kode_area;
        $telepon            = $request->telepon;
        $province_id        = $request->select_province;
        $city_id            = $request->select_city;
        $postcode           = $request->postcode;
        $alamat             = $request->alamat;
        $project_name       = $request->project_name;
        $nilai_kontrak      = $request->nilai_kontrak;
        $nilai_project      = $request->nilai_project;
        $periode_start_date = $request->periode_start_date;
        $periode_end_date   = $request->periode_end_date;
        $no_surat_undangan  = $request->no_surat_undangan;
        $date_surat_undangan= $request->date_surat_undangan;
        $document_project   = $request->document_project;
        $no_surat           = $request->no_surat;
        $tgl_surat          = $request->tgl_surat;
        $note               = $request->note;
        $user_id            = $request->user()->token()->user_id;

        $cek_new_order      = NewOrderModel::where('code',$code)->where('user_id',$user_id)->first();


        if(!$cek_new_order)
        {
            return response()->json([
                'message'       => 'Data order tidak ditemukan',
                'status'        => "error",
                'code'          => 404
            ],200);
        }

        $get_order_sb = NewOrderSBModel::where('new_order_code',$code)->first();
        $new_file = [];
        $old_file = [];
        DB::beginTransaction();
        try{
            $telepon_number = [];
            if($kode_area){
                foreach($kode_area as $kycodeare => $valcodearea)
                {

                    if(array_key_exists($kycodeare,$telepon)){
                        $telp = $telepon[$kycodeare];
                        $telepon_number[] = [
                            'code'      => $valcodearea,
                            'number'    => $telp
                        ];
                    }


                }
            }

            if($telepon_number){
                $telepon_number = json_encode($telepon_number);
            }else{
                $telepon_number = null;
            }
            $get_province_name  = M_Province::where('id',$province_id)->first();
            $get_city_name      = M_City::where('id',$city_id)->first();
            $data_save_obligee      = [
                'company_type'      => $company_type,
                'company_name'      => $company_name,
                'jenis_usaha'       => $jenis_badan_usaha,
                'bidang_usaha'      => $bidang_usaha,
                'website'           => $website,
                'email'             => $email,
                'no_telepon'        => $telepon_number,
                'm_province_id'     => $province_id,
                'm_province_name'   => $get_province_name->name,
                'm_city_id'         => $city_id,
                'm_city_name'       => $get_city_name->name,
                'postcode'          => $postcode,
                'address'           => $alamat,
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => $user_id
            ];

            $update_obligee = NewOrderSBObligeeModel::where('new_order_sb_code',$get_order_sb->code)->update($data_save_obligee);

            $start_periode_date  = strtotime($request->periode_start_date);
            $end_periode_date    = strtotime($request->periode_end_date);
            $numBulan            = 1 + (date("Y",$end_periode_date)-date("Y",$start_periode_date))*12;
            $numBulan           += date("m",$end_periode_date)-date("m",$start_periode_date);

            $data_project = [
                'name'                      => $project_name,
                'contract_value'            => $nilai_kontrak,
                'collateral_value'          => $nilai_project,
                'start_date_periode'        => $periode_start_date,
                'end_date_periode'          => $periode_end_date,
                'periode_value'             => $numBulan,
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => $user_id,
                'number_support_document'   => $no_surat_undangan,
                'date_support_document'     => $date_surat_undangan,
            ];
            $save_project = NewOrderSBProjectModel::where('new_order_sb_code',$get_order_sb->code)->update($data_project);

            $data_update_sb['periode_value'] = $numBulan;
            if($get_order_sb->estimation_rate)
            {
                $estimation_rate                = $get_order_sb->estimation_rate;
                if($get_order_sb->estimation_rate_type == 1)
                {
                    $estimation_rate_type           = 1;
                    $estimation_rate_periode_type   = null;
                    $estimation_rate_periode_value  = null;
                    $estimation_premi_per_periode   = 0;
                    $total_estimation_premi         = $nilai_project * ($get_order_sb->estimation_rate / 100);
                }
                else
                {

                    $estimation_rate_type           = 2;
                    $estimation_rate_periode_type   = $get_order_sb->estimation_rate_periode_type;
                    $estimation_rate_periode_value  = $get_order_sb->estimation_rate_periode_value;
                    $estimation_premi_per_periode   = $nilai_project * ($get_order_sb->estimation_rate / 100);

                    $get_periode_project            = $numBulan / $get_order_sb->estimation_rate_periode_value;
                    $get_periode_project            = ceil($get_periode_project);
                    $total_estimation_premi         = $estimation_premi_per_periode * $get_periode_project;
                }

                $data_update_sb['estimation_rate']                  = $estimation_rate;
                $data_update_sb['estimation_rate_type']             = $estimation_rate_type;
                $data_update_sb['estimation_rate_periode_type']     = $estimation_rate_periode_type;
                $data_update_sb['estimation_rate_periode_value']    = $estimation_rate_periode_value;
                $data_update_sb['estimation_premi_per_periode']     = $estimation_premi_per_periode;
                $data_update_sb['total_estimation_premi']           = $total_estimation_premi;

            }
            $data_update_sb['updated_at']        = date('Y-m-d H:i:s');
            $data_update_sb['updated_by']        = $user_id;

            $update_sb = NewOrderSBModel::where('id',$get_order_sb->id)->update($data_update_sb);


            if($document_project)
            {
                foreach($document_project as $key => $val)
                {
                    $cek_document   = NewOrderDocumentModel::where('new_order_code',$code)->where('m_document_upload_code',$key)->first();

                    $get_doc_upload = M_DocPengajuan::where('code',$key)->first();
                    $doc_name       = generate_url($get_doc_upload->name).'-new-at-'.time().'.'.$val->extension();;
                    $upload_path_attachment = storage_path('public/order/sb/order-code-'.$code.'/project');

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/order/sb/order-code-'.$code.'/project', $val, $doc_name);
                        }
                    } else {
                        Storage::putFileAs('public/order/sb/order-code-'.$code.'/project', $val, $doc_name);
                    }

                    if($note){
                        if(array_key_exists($key,$note))
                        {
                            $data_doc['note_document'] = $note[$key];
                        }
                    }

                    if($no_surat)
                    {
                        if(array_key_exists($key,$no_surat))
                        {
                            $data_doc['number_document'] = $no_surat[$key];
                        }
                    }

                    if($tgl_surat)
                    {
                        if(array_key_exists($key,$tgl_surat))
                        {
                            $data_doc['date_document'] = $tgl_surat[$key];
                        }
                    }
                    $new_file[] = $doc_name;
                    if($cek_document)
                    {
                        $old_file[]                 = $cek_document->document_name;
                        $data_doc['document_name']  = $doc_name;
                        $data_doc['updated_at']     = date('Y-m-d H:i:s');
                        $data_doc['updated_by']     = $user_id;

                        $update_doc = NewOrderDocumentModel::where('id',$cek_document->id)->update($data_doc);
                    }
                    else
                    {

                        $data_doc['document_type']          = 2;
                        $data_doc['new_order_id']           = $cek_new_order->id;
                        $data_doc['new_order_code']         = $cek_new_order->code;
                        $data_doc['m_document_upload_id']   = $get_doc_upload->id;
                        $data_doc['m_document_upload_code'] = $get_doc_upload->code;
                        $data_doc['name']                   = $get_doc_upload->name;
                        $data_doc['document_name']          = $doc_name;
                        $data_doc['created_at']             = date('Y-m-d H:i:s');
                        $data_doc['created_by']             = $user_id;
                        $insert_doc = NewOrderDocumentModel::insert($data_doc);
                    }

                }
            }

            if($no_surat)
            {
                foreach($no_surat as $key => $val)
                {
                    $cek_document   = NewOrderDocumentModel::where('new_order_code',$code)->where('m_document_upload_code',$key)->first();

                    if($cek_document)
                    {
                        $no_doc['number_document']    = $val;
                        $no_doc['updated_at']         = date('Y-m-d H:i:s');
                        $no_doc['updated_by']         = $user_id;
                        $update_doc                     = NewOrderDocumentModel::where('id',$cek_document->id)->update($no_doc);
                    }
                }
            }
            if($tgl_surat)
            {
                foreach($tgl_surat as $key => $val)
                {
                    $cek_document   = NewOrderDocumentModel::where('new_order_code',$code)->where('m_document_upload_code',$key)->first();

                    if($cek_document)
                    {
                        $tgl_doc['date_document']      = $val;
                        $tgl_doc['updated_at']         = date('Y-m-d H:i:s');
                        $tgl_doc['updated_by']         = $user_id;
                        $update_doc                     = NewOrderDocumentModel::where('id',$cek_document->id)->update($tgl_doc);
                    }
                }
            }
            if($note)
            {
                foreach($note as $key => $val)
                {
                    $cek_document   = NewOrderDocumentModel::where('new_order_code',$code)->where('m_document_upload_code',$key)->first();

                    if($cek_document)
                    {
                        $note_doc['note_document']      = $val;
                        $note_doc['updated_at']         = date('Y-m-d H:i:s');
                        $note_doc['updated_by']         = $user_id;
                        $update_doc                     = NewOrderDocumentModel::where('id',$cek_document->id)->update($note_doc);
                    }
                }
            }
            DB::commit();


            foreach($old_file as $key => $val)
            {
                if (Storage::exists('public/order/sb/order-code-'.$code.'/project/'.$val)) {

                    Storage::move('public/order/sb/order-code-'.$code.'/project/'.$val, 'public/order/junk/sb/order-code-'.$code.'/'.'project/'.$val);
                }
            }
            return response()->json([
                'message'       => 'Success save Data',
                'status'        => 'success',
                'order_code'    => $code,
                'code'          => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            foreach($new_file as $key => $val)
            {
                if (Storage::exists('public/order/sb/order-code-'.$code.'/project/'.$val)) {
                    Storage::delete('/public/order/sb/order-code-'.$code.'/project/'.$val);
                }
            }
            return response()->json([
                'message'       => 'Failed to save Data : '.$e->getMessage().', Line: '. $e->getLine(),
                'status'        => 'error',
                'order_code'    => $code,
                'code'          => 500,
            ], 500);
        }

    }
}
