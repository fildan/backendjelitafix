<?php

namespace App\Http\Controllers\API;

use App\Models\M_Profile;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class ProfileController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getDetailUser(Request $request)
    {

        $user_id            = $request->user()->token()->user_id;
        $get_data           = M_Profile::with(['get_province','get_city'])->select('user_code','name','gender','email','phone','birth_date','ktp','alamat','m_province_id','m_province_name',
                                                    'm_city_id','m_city_name')->where('user_id',$user_id)->first();

        return response()->json([
            'message'       => 'success',
            'data'          => $get_data,
            'status'        => "success",
        ],200);
    }
}
