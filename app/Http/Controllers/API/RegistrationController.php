<?php

namespace App\Http\Controllers\API;

use App\Models\M_Profile;
use App\Models\Order\M_Order;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class RegistrationController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function register(Request $request)
    {
        $name           = strtolower($request->name);
        $gender         = $request->gender;
        $tgl_lahir      = $request->tgl_lahir;
        $no_telp        = $request->telepon;
        $phone_code     =  $request->phone_code;
        $email          = strtolower($request->email);
        $validatedData  = $this->rules($request);
        $password       = $request->password;
        if($validatedData->fails()){
            return response()->json([
                'message'   => $validatedData->errors(),
                'status'    => 'error',
                'code'      => 403,
                'data'      => null
            ], 200);
        }


        $checked = User::where('email',$email)->orWhere('phone',$no_telp)->first();

        if($checked)
        {
            return response()->json([
                'message'   => 'Email atau telpon sudah terdaftar, gunakan data lainnya.',
                'status'    => 'error',
                'code'      => 401,
                'data'      => null
            ], 200);
        }

        $save_user['name']  = $name;
        $save_user['email'] = $email;
        $save_user['code']  = Uuid::uuid4();
        $save_user['password'] = bcrypt($password);

        DB::beginTransaction();
        try{
            $user               = User::create($save_user);

            $save_profile       = [
                'user_id'       => $user->id,
                'user_code'     => $user->code,
                'name'          => $name,
                'gender'        => $gender,
                'birth_date'    => $tgl_lahir,
                'email'         => $email,
                'phone'         => $no_telp,
                'phone_code'    => $phone_code,
            ];
            $save_profile       = M_Profile::create($save_profile);
            $accessToken        = $user->createToken('Laravel Password Grant Client')->accessToken;

            DB::commit();
            $response_data      = [
                'user_code' => $user->code,
                'name'      => $name,
                'token'     => $accessToken,
                'email'     => $email,
                'no_telp'   => [
                    'code'      => (!empty($phone_code))?$phone_code:'' ,
                    'number'    => (!empty($no_telp))?$no_telp:''
                ]
            ];
            return response()->json([
                'message'   => 'Registrasi Berhasil.',
                'status'    => 'success',
                'code'      => 200,
                'data'      => $response_data
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }


    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        // $checked = User::where('email',$username)->orWhere('phone',$username)->first();
        $checked = User::where('email',$username)->first();
        if(!$checked)
        {
            $checked = User::where('phone',$username)->first();
            if(!$checked){
                return response()->json([
                    'message'   => 'User not found',
                    'status'    => 'error',
                    'code'      => 404
                ],200);
            }else{
                $data_login['phone'] = $username;
            }
        }
        else
        {
            $data_login['email'] = $username;
        }


        $data_login['password'] = $password;
        auth()->attempt($data_login);
        $accessToken        = auth()->user()->createToken('Laravel Password Grant Client')->accessToken;

        // $get_order_data     = M_Order::where('user_id',$checked->id)
        //                             ->where(function($query){
        //                                 $query->where('status','<>',4)
        //                                 ->orWhere('status','<>',5);
        //                             })->count();
        $get_profile = M_Profile::where('user_id',$checked->id)->first();
        $response_data      = [
            'user_code' => $checked->code,
            'name'      => $checked->name,
            'token'     => $accessToken,
            'order'     => 0,
            'email'     => (!empty($checked->email))?$checked->email:'',
            'no_telp'   => [
                'code'      => (!empty($get_profile->phone_code))?$get_profile->phone_code:'',
                'number'    => (!empty($get_profile->phone))?$get_profile->phone:''
            ]
        ];
        return response()->json([
            'message'   => 'Login Berhasil.',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $response_data
        ], 200);
    }

    public function logout(Request $request)
    {
        // dd($request->all());
        // dd('ok');
        // $accessToken = auth()->user()->token();
        // $token= $request->user()->tokens->find($accessToken);
        // dd($token);
        $revoke = $request->user()->token()->revoke();
        return response()->json([
            'message'   => 'Logout Berhasil.',
            'status'    => 'success',
            'code'      => 200,
        ], 200);
    }

    public function rules($request)
    {
        return Validator::make($request->all(), [
            'name'         => 'required',
            'gender'       => 'required',
            'tgl_lahir'    => 'required',
            'telepon'      => 'required',
            'email'        => 'required',
            'password'     => 'required|confirmed',
        ]);
    }

}
