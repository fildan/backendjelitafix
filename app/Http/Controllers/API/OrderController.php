<?php

namespace App\Http\Controllers\API;

use App\Models\M_Acc;
use App\Models\M_BrandVehicle;
use App\Models\M_DocPengajuan;
use App\Models\M_Perluasan;
use App\Models\M_Plat;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\M_ProductPenanggung;
use App\Models\M_Product;
use App\Models\M_ProductPenanggungPerluasan;
use App\Models\M_SeriesVehicle;
use App\Models\M_SubProduct;
use App\Models\M_TypeVehicle;
use App\Models\Order\M_Order;
use App\Models\Order\M_OrderAcc;
use App\Models\Order\M_OrderDetail;
use App\Models\Order\M_OrderDoc;
use App\Models\Order\M_OrderKBM;
use App\Models\Order\M_OrderPerluasan;
use App\Models\Order\M_PemegangPolis;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;

class OrderController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $ProductPenanggungController;

    public function __construct()
    {

        date_default_timezone_set('Asia/Jakarta');
        $this->ProductPenanggungController = new ProductPenanggungController;
    }

    public function savePengajuanPolis(Request $request)
    {
        $use_data_pemegang_polis    = $request->use_data_pemegang_polis;
        $nama_pemegang_polis        = $request->nama_pemegang_polis;
        $jk_pemegang_polis          = $request->jk_pemegang_polis;
        $tgl_lahir_pemegang_polis   = $request->tgl_lahir_pemegang_polis;
        $no_telp_pemegang_polis     = $request->no_telp_pemegang_polis;
        $email_pemegang_polis       = $request->email_pemegang_polis;
        $tgl_aktif_polis            = $request->tgl_aktif_polis;
        $lama_perlindungan          = $request->lama_perlindungan;
        $alamat_pengiriman          = $request->alamat_pengiriman;
        $ktp                        = $request->ktp;
        $province                   = $request->province;
        $city                       = $request->city;
        $cob                        = $request->cob;
        $sub_cob                    = $request->sub_cob;
        $product_url                = $request->product_url;
        $user_id                    = $request->user()->token()->user_id;
        $total_pertanggungan        = $request->total_pertanggungan;
        $type_perlindungan          = $request->type_perlindungan;

        $get_cob                    = M_Product::where('url',$cob)->first();
        $get_sub_cob                = M_SubProduct::where('url',$sub_cob)->first();
        $order_number               = $this->checkCode($get_cob->code);
        $request->cob_code          = $get_cob->code;

        $get_product_penanggung     = M_ProductPenanggung::where('url',$product_url)->first();

        $acc_code       = $request->acc_code;
        $total_acc      = 0;
        if($acc_code){
            $acc = $request->acc_price;
            foreach($acc as $kyacc => $valacc)
            {
                $total_acc += $valacc;
            }
        }

        DB::beginTransaction();
        try{
            if($get_cob->code == 'KBM'){
                $request->object_pertanggungan          = $request->harga_kendaraan;
                $request->object_pertanggungan_detail   = json_encode(array('harga_mobil' => $request->harga_kendaraan));
            }

            $progress[] = array(
                'date'      => date('Y-m-d H:i:s'),
                'status'    => 1,
                'note'      => null
            );

            $data_order     = [
                'code'                                      => $order_number,
                'reff_code'                                 => Uuid::uuid4(),
                'user_id'                                   => $user_id,
                'm_product_id'                              => $get_cob->id,
                'm_product_code'                            => $get_cob->code,
                'm_sub_product_id'                          => $get_sub_cob->id,
                'm_sub_product_code'                        => $get_sub_cob->code,
                'm_product_penanggung_id'                   => $get_product_penanggung->id,
                'm_product_penanggung_code'                 => $get_product_penanggung->code,
                'protection_time'                           => $lama_perlindungan,
                'protection_time_type'                      => $type_perlindungan,
                'date_active_polis'                         => $tgl_aktif_polis,
                'status'                                    => 1,
                'history_progress'                          => json_encode($progress),
                'created_at'                                => date('Y-m-d H:i:s'),
                'created_by'                                => $user_id,
                'total_accessories'                         => $total_acc,
                'object_pertanggungan'                      => $request->object_pertanggungan,
                'object_pertanggungan_detail'               => $request->object_pertanggungan_detail,
                'object_pertanggungan_after_accessories'    => $total_pertanggungan
            ];

            $save_order = M_Order::insertGetId($data_order);

            if($acc_code){
                $acc_type                   = $request->acc_type;
                $acc_price                  = $request->acc_price;
                $acc_brand                  = $request->acc_brand;

                $data_acc =  new \stdClass;
                $data_acc->order_id                   = $save_order;
                $data_acc->order_code                 = $order_number;
                $data_acc->order_reff_code            = $data_order['reff_code'];
                $data_acc->acc_code                   = $acc_code;
                $data_acc->acc_type                   = $acc_type;
                $data_acc->acc_price                  = $acc_price;
                $data_acc->acc_brand                  = $acc_brand;
                $data_acc->user_id                    = $user_id;
                $save_acc = $this->saveAccessories($data_acc);

            }


            if($get_cob->code == 'KBM'){
                $data_kbm =  new \stdClass;
                $data_kbm->order_id                   = $save_order;
                $data_kbm->order_code                 = $order_number;
                $data_kbm->order_reff_code            = $data_order['reff_code'];
                $data_kbm->tahun_kendaraan            = $request->tahun_kendaraan;
                $data_kbm->brand_code_vehicle         = $request->brand_code_vehicle;
                $data_kbm->brand_vehicle_text         = $request->brand_vehicle_text;
                $data_kbm->type_code_vehicle          = $request->type_code_vehicle;
                $data_kbm->type_vehicle_text          = $request->type_vehicle_text;
                $data_kbm->series_code_vehicle        = $request->series_code_vehicle;
                $data_kbm->series_vehicle_text        = $request->series_vehicle_text;
                $data_kbm->harga_kendaraan            = $request->harga_kendaraan;
                $data_kbm->plat_number_code           = $request->plat_number_code;
                $data_kbm->plat_nomor                 = $request->plat_nomor;
                $data_kbm->warna_mobil                = $request->warna_mobil;
                $data_kbm->nomor_ranka                = $request->nomor_ranka;
                $data_kbm->nomor_mesin                = $request->nomor_mesin;
                $data_kbm->kondisi_mobil              = $request->kondisi_mobil;
                // $data_kbm->total_perluasan            = $request->total_perluasan;

                $data_kbm->acc_price                  = ($acc_code)?$acc_price:null;
                $data_kbm->user_id                    = $user_id;
                $save_kbm = $this->saveOrderKBM($data_kbm);


            }



            if($request->document_pengajuan)
            {
                $data_document =  new \stdClass;
                $data_document->order_id                = $save_order;
                $data_document->order_code              = $order_number;
                $data_document->order_reff_code         = $data_order['reff_code'];
                $data_document->document_pengajuan      = $request->document_pengajuan;
                $data_document->user_id                 = $user_id;
                $save_document = $this->saveDocumentPengajuan($data_document);

            }

            // save data pemegang polis
            $data_pemegang_polis =  new \stdClass;
            $data_pemegang_polis->order_id                  = $save_order;
            $data_pemegang_polis->order_code                = $order_number;
            $data_pemegang_polis->order_reff_code           = $data_order['reff_code'];
            $data_pemegang_polis->nama_pemegang_polis       = $nama_pemegang_polis;
            $data_pemegang_polis->jk_pemegang_polis         = $jk_pemegang_polis;
            $data_pemegang_polis->tgl_lahir_pemegang_polis  = $tgl_lahir_pemegang_polis;
            $data_pemegang_polis->no_telp_pemegang_polis    = $no_telp_pemegang_polis;
            $data_pemegang_polis->email_pemegang_polis      = $email_pemegang_polis;
            $data_pemegang_polis->alamat_pengiriman         = $alamat_pengiriman;
            $data_pemegang_polis->ktp                       = $ktp;
            $data_pemegang_polis->province                  = $province;
            $data_pemegang_polis->city                      = $city;
            $data_pemegang_polis->user_id                   = $user_id;

            $save_data_pemegang_polis = $this->saveDataPemegangPolis($data_pemegang_polis);


            $request->m_product_penanggung_id       = $get_product_penanggung->id;
            $save_order_detail                      = $this->getDetailOrder($request,$save_order,$user_id,$data_order);

            DB::commit();
            // $order_number  =
            return response()->json([
                'message'       => 'Success save Data',
                'status'        => 'success',
                'order_code'    => $order_number,
                'code'          => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
            ],500);
        }
    }

    private function getDetailOrder($request,$order_id=null,$user_id=null,$data_order=null)
    {
        $product_url                = $request->product_url;
        $lama_perlindungan          = $request->lama_perlindungan;
        $tgl_aktif_polis            = $request->tgl_aktif_polis;

        $get_product_penanggung     = M_ProductPenanggung::where('url',$product_url)->first();
        $loop                       = $lama_perlindungan / $get_product_penanggung->value_periode_premi;

        $data = [];
        $start_periode_date     = date_create(date('Y-m-d',strtotime($tgl_aktif_polis)));
        $total_pertanggungan    = $request->object_pertanggungan;
        $depreciation           = 0;
        $depreciation_value     = 0;
        $total_all_premi        = 0;

        for($a=1;$a <= $loop;$a++)
        {
            if($get_product_penanggung->periode_premi == 1){
                $type = 'Days';
            }else if($get_product_penanggung->periode_premi == 2){
                $type = 'Week';
            }else if($get_product_penanggung->periode_premi == 3){
                $type = 'Month';
            }else if($get_product_penanggung->periode_premi == 4){
                $type = 'Years';
            }

            $result['start_date_periode']       = date_format($start_periode_date,'Y-m-d');
            $end_periode_date                   = date_add($start_periode_date,date_interval_create_from_date_string($get_product_penanggung->value_periode_premi.' '.$type));
            $end_periode                        = date_format($end_periode_date,'Y-m-d');
            // $end_periode_1                      = date('Y-m-d', strtotime('-1 days', strtotime($end_periode)));
            $pertanggungan_after_depreciation   = $total_pertanggungan - $depreciation_value;

            // return $total_perluasan;
            $acc_price                  = $request->acc_price;

            $total_acc = 0;
            if($acc_price){
                foreach($acc_price as $kyacc => $valacc)
                {
                    $total_acc += $valacc;
                }
            }

            $total_all_pertanggungan            = $pertanggungan_after_depreciation + $total_acc;

            $result['order_id']                                 = $order_id;
            $result['order_code']                               = $data_order['code'];
            $result['code']                                     = Uuid::uuid4();
            $result['end_date_periode']                         = $end_periode;
            $result['total_accessories']                        = $total_acc;
            $result['depreciation_percent']                     = $depreciation;
            $result['depreciation_value']                       = $depreciation_value;
            $result['total_pertanggungan_before_depreciation']  = $total_pertanggungan;
            $result['total_pertanggungan_after_depreciation']   = $pertanggungan_after_depreciation;
            $result['total_pertanggungan']                      = $total_all_pertanggungan;
            $result['created_at']                               = date('Y-m-d H:i:s');
            $result['created_by']                               = $user_id;

            $get_rate = $this->ProductPenanggungController->checkPrice($get_product_penanggung->id,$total_all_pertanggungan,$request->kategori_wilayah);

            $rate = 0;
            if($get_rate){
                $rate =   $get_rate['fix_rate'];

            }

            $premi                              = $total_all_pertanggungan * ($rate / 100);
            $result['rate']                     = $rate;
            $result['premi']                    = $premi;

            $save_detail                        = M_OrderDetail::insertGetId($result);
            $total_perluasan                    = $this->savePerluasan($request,$total_all_pertanggungan,$save_detail,$result['code']);
            $start_periode_date                 = date_create($end_periode);
            // $data[] = $result;

            $total_pertanggungan    = $pertanggungan_after_depreciation;
            $depreciation           = $get_product_penanggung->depreciation;
            $depreciation_value     = $pertanggungan_after_depreciation * ($depreciation / 100);

            $total_all_premi += ( $premi + $total_perluasan );
        }

        $update_order   = M_Order::where('id',$order_id)->update(['total_all_premi' => $total_all_premi]);
        return $total_all_premi;
    }

    function saveOrderKBM($data_kbm=null){

        $get_vehicle_brand      = M_BrandVehicle::where('code',$data_kbm->brand_code_vehicle)->first();
        $get_type_vehicle       = M_TypeVehicle::where('code',$data_kbm->type_code_vehicle)->first();
        $get_vehicle_series     = M_SeriesVehicle::where('code',$data_kbm->series_code_vehicle)->first();
        $get_plat               = M_Plat::where('code',$data_kbm->plat_number_code)->first();

        $acc = $data_kbm->acc_price;

        $total_acc = 0;
        if($acc){
            foreach($acc as $kyacc => $valacc)
            {
                $total_acc += $valacc;
            }
        }
        $data_save_kbm = [
            'order_id'                  => $data_kbm->order_id,
            'order_code'                => $data_kbm->order_code,
            'vehicle_year'              => $data_kbm->tahun_kendaraan,
            'm_brand_vehicle_id'        => $get_vehicle_brand->id,
            'm_brand_vehicle_code'      => $get_vehicle_brand->code,
            'brand_vehicle'             => $get_vehicle_brand->name,
            'm_type_vehicle_id'         => $get_type_vehicle->id,
            'm_type_vehicle_code'       => $get_type_vehicle->code,
            'type_vehicle'              => $get_type_vehicle->name,
            'm_series_vehicle_id'       => $get_vehicle_series->id,
            'm_series_vehicle_code'     => $get_vehicle_series->code,
            'series_vehicle'            => $get_vehicle_series->name,
            'm_plat_wilayah_id'         => $get_plat->id,
            'm_plat_wilayah_code'       => $data_kbm->plat_number_code,
            'plat_number'               => $data_kbm->plat_nomor,
            'color_vehicle'             => $data_kbm->warna_mobil,
            'ranka_number'              => $data_kbm->nomor_ranka,
            'engine_number'             => $data_kbm->nomor_mesin,
            'vehicle_condition'         => $data_kbm->kondisi_mobil,
            'price_vehicle'             => $data_kbm->harga_kendaraan,
            'order_reff_code'           => $data_kbm->order_reff_code,
            'created_at'                => date('Y-m-d H:i:s'),
            'created_by'                => $data_kbm->user_id
        ];
        $save_order_kbm = M_OrderKBM::create($data_save_kbm);
    }

    function saveAccessories($data_accessories){


        $accessories = $data_accessories->acc_code;
        $acc_brand   = $data_accessories->acc_brand;
        $acc_type    = $data_accessories->acc_type;
        $acc_price   = $data_accessories->acc_price;
        $data_save_acc = [];
        foreach($accessories as $kyacc => $valacc)
        {

            $get_acc_id = M_Acc::where('code',$valacc)->first();
            $data_save_acc[] = [
                'order_code'            => $data_accessories->order_code,
                'order_id'              => $data_accessories->order_id,
                'm_accessories_id'      => $get_acc_id->id,
                'm_accessories_code'    => $valacc,
                'name'                  => $get_acc_id->name,
                'brand'                 => $acc_brand[$kyacc],
                'type'                  => $acc_type[$kyacc],
                'price'                 => $acc_price[$kyacc],
                'created_at'            => date('Y-m-d H:i:s'),
                'created_by'            => $data_accessories->user_id
            ];
        }
        $save_order_acc = M_OrderAcc::insert($data_save_acc);

    }

    function saveDocumentPengajuan($data_document)
    {
        $document       = $data_document->document_pengajuan;
        $document_save  = [];
        foreach($document as $key => $val)
        {
            $get_doc_upload = M_DocPengajuan::where('code',$key)->first();
            $doc_name       = generate_url($get_doc_upload->name).'.'.$val->extension();;
            $upload_path_attachment = storage_path('app/public/order/order-number-'.$data_document->order_code);

            if (!file_exists($upload_path_attachment)) {
                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                    Storage::putFileAs('public/order/order-number-'.$data_document->order_code, $val, $doc_name);
                }
            } else {
                Storage::putFileAs('public/order/order-number-'.$data_document->order_code, $val, $doc_name);
            }

            $document_save[] = [
                'order_id'                  => $data_document->order_id,
                'order_code'                => $data_document->order_code,
                'm_document_upload_id'      => $get_doc_upload->id,
                'm_document_upload_code'    => $get_doc_upload->code,
                'name'                      => $get_doc_upload->name,
                'document_name'             => $doc_name,
                'created_at'                => date('Y-m-d H:i:s'),
                'created_by'                => $data_document->user_id

            ];
        }

        $save_order_acc = M_OrderDoc::insert($document_save);
    }

    function savePerluasan($data_perluasan,$object_pertanggungan=null,$order_detail_id=null,$order_detail_code=null)
    {

        $get_order_detail   = M_OrderDetail::where('id',$order_detail_id)->first();
        $perluasan_code     = $data_perluasan->perluasan_code;
        $rate               = $data_perluasan->rate_perluasan;
        $coverrage_value    = $data_perluasan->uang_pertanggungan;
        $price              = $data_perluasan->price_perluasan;
        $jmlh_tertanggung   = $data_perluasan->jmlh_tertanggung;

        $save_data_perluasan = [];
        $save_perluasan['created_by']           = $data_perluasan->user()->token()->user_id;
        $save_perluasan['created_at']           = date('Y-m-d H:i:s');
        $save_perluasan['order_code']           = $get_order_detail->order_code;
        $save_perluasan['order_id']             = $get_order_detail->order_id;
        $save_perluasan['order_detail_id']      = $order_detail_id;
        $save_perluasan['order_detail_code']    = $order_detail_code;
        $total_all_perluasan                    = 0;

        if($perluasan_code){
            foreach($perluasan_code as $key => $val){
                $get_perluasan          = M_Perluasan::where('code',$key)->first();
                $get_prdct_perluasan    = M_ProductPenanggungPerluasan::where('m_perluasan_id',$get_perluasan->id)
                                            ->where('m_product_penanggung_id',$data_perluasan->m_product_penanggung_id)->first();

                $save_perluasan['m_perluasan_polis_id']     = $get_perluasan->id;
                $save_perluasan['m_perluasan_polis_code']   = $get_perluasan->code;
                $save_perluasan['m_perluasan_name']         = $get_perluasan->name;



                if(array_key_exists($key,$jmlh_tertanggung)){
                    $people_cover = $jmlh_tertanggung[$key];

                    $save_perluasan['people_cover'] = $jmlh_tertanggung[$key];
                }else{
                    $people_cover = 1;
                    $save_perluasan['people_cover'] = 0;
                }
                if(array_key_exists($key,$rate)){
                    $save_perluasan['rate'] = $rate[$key];
                    $rate_value             = $rate[$key];
                }else{
                    $save_perluasan['rate'] = 0;
                    $rate_value             = 0;
                }

                if(array_key_exists($key,$coverrage_value)){
                    $save_perluasan['coverrage_value']  = $coverrage_value[$key];
                    $coverrage_value_                   = $coverrage_value[$key];
                }else{
                    $save_perluasan['coverrage_value']  = 0;
                    $coverrage_value_                   = 0;
                }

                if($get_perluasan->calculation_method == 1)
                {
                    $value_object_pertanggungan = $object_pertanggungan;
                }else{
                    $value_object_pertanggungan = $coverrage_value_;
                }

                $price_perluasan = ($value_object_pertanggungan * ($rate_value/100)) * $people_cover;

                $save_perluasan['price']            = $price_perluasan;
                $save_perluasan['is_rate_or_value'] = $get_prdct_perluasan->rate_or_value;


                $save_data_perluasan[] = $save_perluasan;
                $total_all_perluasan   += $price_perluasan;

            }

        // return $save_data_perluasan;
            $save_order_acc = M_OrderPerluasan::insert($save_data_perluasan);


        }
        $premi                  = $get_order_detail->premi + $total_all_perluasan;
        $update_order_detail    = M_OrderDetail::where('id',$order_detail_id)
                                        ->update(
                                            [
                                                'premi_after_perluasan' => $premi,
                                                'total_perluasan'       => $total_all_perluasan
                                            ]
                                        );
        return $total_all_perluasan;
    }

    function saveDataPemegangPolis($data_pemegang_polis)
    {

        // return 'a';
        $doc_name               = 'ktp-'.generate_url($data_pemegang_polis->nama_pemegang_polis).'.'.$data_pemegang_polis->ktp->extension();;
        $upload_path_attachment = storage_path('app/public/order/order-number-'.$data_pemegang_polis->order_code);

        if (!file_exists($upload_path_attachment)) {
            if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                Storage::putFileAs('public/order/order-number-'.$data_pemegang_polis->order_code, $data_pemegang_polis->ktp, $doc_name);
            }
        } else {
            Storage::putFileAs('public/order/order-number-'.$data_pemegang_polis->order_code, $data_pemegang_polis->ktp, $doc_name);
        }

        $save_data = [
            'order_id'          => $data_pemegang_polis->order_id,
            'order_code'        => $data_pemegang_polis->order_code ,
            'name'              =>  $data_pemegang_polis->nama_pemegang_polis,
            'gender'            => $data_pemegang_polis->jk_pemegang_polis,
            'birth_date'        => $data_pemegang_polis->tgl_lahir_pemegang_polis,
            'email'             => $data_pemegang_polis->email_pemegang_polis ,
            'phone_number'      => $data_pemegang_polis->no_telp_pemegang_polis,
            'ktp'               => $doc_name,
            'm_province_id'     => $data_pemegang_polis->province,
            'm_city_id'         => $data_pemegang_polis->city,
            'address'           => $data_pemegang_polis->alamat_pengiriman,
            'created_at'        => date('Y-m-d H:i:s'),
            'created_by'        => $data_pemegang_polis->user_id
        ];
        $save_order_acc = M_PemegangPolis::create($save_data);
    }
    function checkCode($cob_code)
    {
        $order_number   =   'ORD-'.$cob_code.date('dmy');
        $order_number    .=   rand(0000,9999);

        $cek_order_code = M_Order::where('code',$order_number)->get();
        while (count($cek_order_code)>0) {
            $order_number    = substr($order_number,0,-4);
            $order_number   .= rand(0000,9999);
            $cek_order_code = M_Order::where('code',$order_number)->get();
        }

        return $order_number;
    }
}
