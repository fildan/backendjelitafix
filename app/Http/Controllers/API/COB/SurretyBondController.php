<?php

namespace App\Http\Controllers\API\COB;

use App\Models\Company\CompanyTypeModel;
use App\Models\Company\UserCompanyDocumentModel;
use App\Models\Company\UserCompanyModel;
use App\Models\Company\UserCompanyPICModel;
use App\Models\Company\UserKomisarisDireksiModel;
use App\Models\DraftOrder\DraftOrderDocumentModel;
use App\Models\DraftOrder\DraftOrderListAsuransiModel;
use App\Models\DraftOrder\DraftOrderModel;
use App\Models\DraftOrder\DraftOrderObligeeModel;
use App\Models\DraftOrder\DraftOrderProjectModel;
use App\Models\M_City;
use App\Models\M_DocPengajuan;
use App\Models\M_Product;
use App\Models\M_ProductPenanggung;
use App\Models\M_Province;
use App\Models\M_SubProduct;
use App\Models\NewOrder\DaftarObligeeModel;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;
use Validator;

class SurretyBondController extends BaseController
{
    public function __construct()
    {

        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function saveCompany(Request $request)
    {

        $validatedData  = $this->rules($request);
        if($validatedData->fails()){
            // dd($validatedData->errors());
            return response()->json([
                'message'   => $validatedData->errors(),
                'status'    => 'error',
                'code'      => 403,
                'data'      => null
            ], 200);
        }

        $company_type       = $request->company_type;
        $company_name       = $request->company_name;
        $jenis_badan_usaha  = $request->jenis_badan_usaha;
        $bidang_usaha       = $request->bidang_usaha;
        $no_npwp            = $request->no_npwp;
        $email              = $request->email;
        $kode_area          = $request->kode_area;
        $telepon            = $request->telepon;
        $province           = $request->province;
        $city               = $request->city;
        $postcode           = $request->postcode;
        $alamat             = $request->alamat;

        $pic_name           = $request->pic_name;
        $pic_email          = $request->pic_email;
        $pic_telp_code      = $request->pic_telp_code;
        $pic_telp           = $request->pic_telp;
        $pic_position       = $request->pic_position;

        $nama_komisaris     = $request->nama_komisaris;
        $ktp_komisaris      = $request->ktp_komisaris;
        $jabatan_komisaris  = $request->jabatan_komisaris;

        $nama_direksi       = $request->nama_direksi;
        $ktp_direksi        = $request->ktp_direksi;
        $jabatan_direksi    = $request->jabatan_direksi;

        $document_company   = $request->document_company;
        $note_document      = $request->note_document;


        $user_id            = $request->user()->token()->user_id;

        $uuid               = Uuid::uuid4();
        DB::beginTransaction();

        $telepon_number = [];
        $document_save  = [];
        if($kode_area){
            foreach($kode_area as $kycodeare => $valcodearea)
            {

                if(array_key_exists($kycodeare,$telepon)){
                    $telp = $telepon[$kycodeare];
                    $telepon_number[] = [
                        'code'      => $valcodearea,
                        'number'    => $telp
                    ];
                }


            }
        }

        if($telepon_number){
            $telepon_number = json_encode($telepon_number);
        }else{
            $telepon_number = null;
        }
        try{
            $data_company       = [
                'user_id'       => $user_id,
                'code'          => $uuid,
                'name'          => $company_name,
                'type'          => $company_type,
                'jenis_usaha'   => $jenis_badan_usaha,
                'bidang_usaha'  => $bidang_usaha,
                'npwp'          => $no_npwp,
                'email'         => $email,
                'no_telepon'    => $telepon_number,
                'm_province_id' => $province,
                'm_city_id'     => $city,
                'postcode'      => $postcode,
                'address'       => $alamat,
                'created_at'    => date('Y-m-d H:i:s'),
                'created_by'    => $user_id
            ];
            $save_company       = UserCompanyModel::insertGetId($data_company);

            $direksi_json       = [];
            if($ktp_direksi){
                foreach($ktp_direksi as $kydireksi => $valdir){
                    if(array_key_exists($kydireksi,$nama_direksi)){
                        $nama_direksi_new = $nama_direksi[$kydireksi];
                    }else{
                        $nama_direksi_new = 'no name';
                    }

                    if(array_key_exists($kydireksi,$jabatan_direksi)){
                        $jabatan_direksi_new = $jabatan_direksi[$kydireksi];
                    }else{
                        $jabatan_direksi_new = 'no jabatan';
                    }

                    $ktp_direksi_file = generate_url(strtolower($nama_direksi_new.'-'.$jabatan_direksi_new.'-'.$uuid)).'.'.$valdir->extension();
                    $ktp_direksi_file = strtolower($ktp_direksi_file);

                    $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$uuid);

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/company-document/company-code-'.$uuid, $valdir, $ktp_direksi_file);
                        }
                    } else {
                        Storage::putFileAs('public/company-document/company-code-'.$uuid, $valdir, $ktp_direksi_file);
                    }

                    $direksi_json[]   = [
                        'code'                  => Uuid::uuid4(),
                        'name'                  => $nama_direksi_new,
                        'identity_file'         => $ktp_direksi_file,
                        'position'              => $jabatan_direksi_new,
                        'type'                  => 2,
                        'user_company_id'       => $save_company,
                        'user_company_code'     => $uuid,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'created_by'            => $user_id
                    ];
                }
            }

            if($ktp_komisaris){
                foreach($ktp_komisaris as $kykomisaris => $valkomisaris){
                    if(array_key_exists($kykomisaris,$nama_komisaris)){
                        $nama_komisaris_new = $nama_komisaris[$kykomisaris];
                    }else{
                        $nama_komisaris_new = 'no name';
                    }

                    if(array_key_exists($kykomisaris,$jabatan_komisaris)){
                        $jabatan_komisaris_new = $jabatan_komisaris[$kykomisaris];
                    }else{
                        $jabatan_komisaris_new = 'no jabatan';
                    }

                    $ktp_komisaris_file = generate_url(strtolower($nama_komisaris_new.'-'.$jabatan_komisaris_new.'-'.$uuid)).'.'.$valkomisaris->extension();
                    $ktp_komisaris_file = strtolower($ktp_komisaris_file);

                    $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$uuid);

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/company-document/company-code-'.$uuid, $valkomisaris, $ktp_komisaris_file);
                        }
                    } else {
                        Storage::putFileAs('public/company-document/company-code-'.$uuid, $valkomisaris, $ktp_komisaris_file);
                    }

                    $direksi_json[]   = [
                        'code'                  => Uuid::uuid4(),
                        'name'                  => $nama_komisaris_new,
                        'identity_file'         => $ktp_komisaris_file,
                        'position'              => $jabatan_komisaris_new,
                        'type'                  => 1,
                        'user_company_id'       => $save_company,
                        'user_company_code'     => $uuid,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'created_by'            => $user_id
                    ];
                }
            }

            if($direksi_json){
                $save_komsaris_direksi = UserKomisarisDireksiModel::insert($direksi_json);
            }


            if($pic_name){
                $pic_data = [];
                foreach($pic_name as $kypic => $valpic)
                {
                    if(array_key_exists($kypic,$pic_email)){
                        $pic_email_new = $pic_email[$kypic];
                    }else{
                        $pic_email_new = null;
                    }

                    if(array_key_exists($kypic,$pic_telp_code)){
                        $pic_telp_code_new = $pic_telp_code[$kypic];
                    }else{
                        $pic_telp_code_new = null;
                    }

                    if(array_key_exists($kypic,$pic_telp)){
                        $pic_telp_new = $pic_telp[$kypic];
                    }else{
                        $pic_telp_new = null;
                    }

                    if(array_key_exists($kypic,$pic_position)){
                        $pic_position_new = $pic_position[$kypic];
                    }else{
                        $pic_position_new = null;
                    }
                    $pic_data[] = [
                        'code_telepon'          => $pic_telp_code_new,
                        'code'                  => Uuid::uuid4(),
                        'name'                  => $valpic,
                        'telepon'               => $pic_telp_new,
                        'email'                 => $pic_email_new,
                        'posisi'                => $pic_position_new,
                        'user_company_id'       => $save_company,
                        'user_company_code'     => $uuid,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'created_by'            => $user_id
                    ];
                }

                $save_pic = UserCompanyPICModel::insert($pic_data);
            }


            if($document_company){
                foreach($document_company as $keydoc => $valdoc)
                {
                    $get_doc_upload = M_DocPengajuan::where('code',$keydoc)->first();
                    $doc_name       = generate_url($get_doc_upload->name).'.'.$valdoc->extension();;
                    $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$uuid);

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/company-document/company-code-'.$uuid, $valdoc, $doc_name);
                        }
                    } else {
                        Storage::putFileAs('public/company-document/company-code-'.$uuid, $valdoc, $doc_name);
                    }

                    if($note_document){
                        if(array_key_exists($keydoc,$note_document)){
                            $note_document_new = $note_document[$keydoc];
                        }else{
                            $note_document_new = null;
                        }
                    }else{
                        $note_document_new = null;
                    }

                    $document_save[] = [
                        'code'                      =>  Uuid::uuid4(),
                        'user_company_id'           =>  $save_company,
                        'user_company_code'         =>  $uuid,
                        'm_document_upload_id'      =>  $get_doc_upload->id,
                        'm_document_upload_code'    =>  $get_doc_upload->code,
                        'name'                      =>  $get_doc_upload->name,
                        'file_name'                 =>  $doc_name,
                        'note'                      =>  $note_document_new,
                        'created_at'                =>  date('Y-m-d H:i:s'),
                        'created_by'                =>  $user_id
                    ];
                }

                $save_doc = UserCompanyDocumentModel::insert($document_save);


            }
            DB::commit();

            return response()->json([
                'message'       => 'Success save Data',
                'status'        => 'success',
                'company_code'  => $uuid,
                'code'          => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();

            if($direksi_json)
            {
                foreach($direksi_json as $kydirjs => $valdirjs)
                {
                    Storage::delete('/public/company-document/company-code-'.$uuid.'/'.$valdirjs['identity_file']);
                }
            }


            if($document_save)
            {
                foreach($document_save as $kydocjs => $valdocjs)
                {
                    Storage::delete('/public/company-document/company-code-'.$uuid.'/'.$valdocjs['file_name']);
                }
            }
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500,
                'line'          => $e->getLine()
            ],500);
        }
    }

    function rules($request)
    {
        return Validator::make($request->all(), [
            'company_type'      => 'required',
            'company_name'      => 'required',
            'jenis_badan_usaha' => 'required',
            'bidang_usaha'      => 'required',
            'no_npwp'           => 'nullable',
            'email'             => 'required|email',
            'kode_area.*'       => 'required|numeric',
            'telepon.*'         => 'required|numeric',
            'province'          => 'required',
            'city'              => 'required',
            'postcode'          => 'required|numeric',
            'alamat'            => 'required',

            'pic_name.*'          => 'required',
            'pic_email.*'         => 'required|email',
            'pic_telp_code.*'     => 'required|numeric',
            'pic_telp.*'          => 'required|numeric',
            'pic_position.*'      => 'required',

            'nama_komisaris.*'    => 'required',
            'ktp_komisaris.*'     => 'required|mimes:jpg,png,svg,pdf',
            'jabatan_komisaris.*' => 'required',

            'nama_direksi.*'      => 'required',
            'ktp_direksi.*'       => 'required|mimes:jpg,png,svg,pdf',
            'jabatan_direksi.*'   => 'required',

            'document_company.*'  => 'required|mimes:jpg,png,svg,pdf,xls,xlsx|max:3000',
            'note_document'       => 'nullable',
        ]);
    }


    public function saveProject(Request $request){
        $validatedData = $this->rulesProject($request);

        if($validatedData->fails()){
            return response()->json([
                'message'   => $validatedData->errors(),
                'status'    => 'error',
                'code'      => 403,
                'data'      => null
            ], 200);
        }


        $user_id            = $request->user()->token()->user_id;
        $getUser            = User::where('id',$user_id)->first();

        $get_product        = M_Product::where('url',$request->cob)->first();
        $get_sub_product    = M_SubProduct::where('url',$request->sub_cob)->first();
        $get_user_company   = UserCompanyModel::where('code',$request->company_code)->first();

        $draftOrderCode = Uuid::uuid4();

        $document_save  = [];
        DB::beginTransaction();
        try{
            $data_save_order        = [
                'code'                  => $draftOrderCode,
                'user_id'               => $user_id,
                'user_code'             => $getUser->code,
                'm_product_id'          => $get_product->id,
                'm_product_code'        => $get_product->code,
                'm_sub_product_id'      => $get_sub_product->id,
                'm_sub_product_code'    => $get_sub_product->code,
                'user_company_code'     => $request->company_code,
                'user_company_id'       => $get_user_company->id,
                'created_at'            => date('Y-m-d H:i:s'),
                'created_by'            => $user_id
            ];

            $save_order_draft = DraftOrderModel::insertGetId($data_save_order);

            $kode_area = $request->kode_area;
            $telepon   = $request->telepon;
            if($kode_area){
                foreach($kode_area as $kycodeare => $valcodearea)
                {

                    if(array_key_exists($kycodeare,$telepon)){
                        $telp = $telepon[$kycodeare];
                        $telepon_number[] = [
                            'code'      => $valcodearea,
                            'number'    => $telp
                        ];
                    }


                }
            }

            if($telepon_number){
                $telepon_number = json_encode($telepon_number);
            }else{
                $telepon_number = null;
            }

            $get_province_name  = M_Province::where('id',$request->select_province)->first();
            $get_city_name      = M_City::where('id',$request->select_city)->first();
            $data_save_obligee      = [
                'draft_order_id'    => $save_order_draft,
                'draft_order_code'  => $draftOrderCode,
                'code'              => 'OBGE-'.time().'-D'.$save_order_draft,
                'company_type'      => $request->company_type,
                'company_name'      => $request->company_name,
                'jenis_usaha'       => $request->jenis_badan_usaha,
                'bidang_usaha'      => $request->bidang_usaha,
                'website'           => $request->website,
                'email'             => $request->email,
                'no_telepon'        => $telepon_number,
                'm_province_id'     => $request->select_province,
                'm_province_name'   => $get_province_name->name,
                'm_city_id'         => $request->select_city,
                'm_city_name'       => $get_city_name->name,
                'postcode'          => $request->postcode,
                'address'           => $request->alamat,
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => $user_id
            ];
            $save_obligee = DraftOrderObligeeModel::insert($data_save_obligee);




            $start_periode_date = strtotime($request->periode_start_date);
            $end_periode_date = strtotime($request->periode_end_date);
            $numBulan = 1 + (date("Y",$end_periode_date)-date("Y",$start_periode_date))*12;
            $numBulan += date("m",$end_periode_date)-date("m",$start_periode_date);
            
            $data_save_project['code']                  = 'SBPO-'.time().'-D'.$save_order_draft;
            $data_save_project['name']                  = $request->project_name;

            if($request->tgl_project){
                $data_save_project['date']                  = $request->tgl_project;
            }
            $data_save_project['description']               = $request->project_description;
            $data_save_project['contract_value']            = $request->nilai_kontrak;
            $data_save_project['collateral_value']          = $request->nilai_project;
            $data_save_project['start_date_periode']        = $request->periode_start_date;
            $data_save_project['end_date_periode']          = $request->periode_end_date;
            $data_save_project['periode_type']              = 3;
            $data_save_project['periode_value']             = $numBulan;
            $data_save_project['created_at']                = date('Y-m-d H:i:s');
            $data_save_project['created_by']                = $user_id;
            $data_save_project['draft_order_id']            = $save_order_draft;
            $data_save_project['draft_order_code']          = $draftOrderCode;
            $data_save_project['number_support_document']   = $request->no_surat_undangan;
            $data_save_project['date_support_document']     = $request->date_surat_undangan;
            
            
            $save_project       = DraftOrderProjectModel::insert($data_save_project);

            $document_project   = $request->document_project;
            $note_document      = $request->note;
            $no_surat           = $request->no_surat;
            $tgl_surat          = $request->tgl_surat;
            if($document_project){
                foreach($document_project as $keydoc => $valdoc)
                {
                    $get_doc_upload = M_DocPengajuan::where('code',$keydoc)->first();
                    $doc_name       = generate_url($get_doc_upload->name).'.'.$valdoc->extension();;
                    $upload_path_attachment = storage_path('app/public/draft-order/'.$draftOrderCode);

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/draft-order/'.$draftOrderCode, $valdoc, $doc_name);
                        }
                    } else {
                        Storage::putFileAs('public/draft-order/'.$draftOrderCode, $valdoc, $doc_name);
                    }

                    if($note_document){
                        if(array_key_exists($keydoc,$note_document)){
                            $note_document_new = $note_document[$keydoc];
                        }else{
                            $note_document_new = null;
                        }
                    }else{
                        $note_document_new = null;
                    }

                    if($no_surat){
                        if(array_key_exists($keydoc,$no_surat)){
                            $no_surat_new = $no_surat[$keydoc];
                        }else{
                            $no_surat_new = null;
                        }
                    }else{
                        $no_surat_new = null;
                    }

                    if($tgl_surat){
                        if(array_key_exists($keydoc,$tgl_surat)){
                            $tgl_surat_new = $tgl_surat[$keydoc];
                        }else{
                            $tgl_surat_new = null;
                        }
                    }else{
                        $tgl_surat_new = null;
                    }


                    $document_save[] = [
                        'code'                      =>  Uuid::uuid4(),
                        'draft_order_id'            =>  $save_order_draft,
                        'draft_order_code'          =>  $draftOrderCode,
                        'm_document_upload_id'      =>  $get_doc_upload->id,
                        'm_document_upload_code'    =>  $get_doc_upload->code,
                        'm_document_upload_name'    =>  $get_doc_upload->name,
                        'file_name'                 =>  $doc_name,
                        'note_document'             =>  $note_document_new,
                        'date_document'             =>  $tgl_surat_new,
                        'number_document'           =>  $no_surat_new,
                        'created_at'                =>  date('Y-m-d H:i:s'),
                        'created_by'                =>  $user_id
                    ];
                }

                $save_doc = DraftOrderDocumentModel::insert($document_save);


            }
            DB::commit();

            return response()->json([
                'message'       => 'Success save Data',
                'status'        => 'success',
                'draft_code'    => $draftOrderCode,
                'code'          => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();

            if($document_save)
            {
                foreach($document_save as $kydocjs => $valdocjs)
                {
                    Storage::delete('/public/draft-order/'.$draftOrderCode.'/'.$valdocjs['file_name']);
                }
            }
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }

    }


    public function rulesProject($request){
        return Validator::make($request->all(), [
            'company_type'          => 'required',
            'company_name'          => 'required',
            'jenis_badan_usaha'     => 'required',
            'bidang_usaha'          => 'nullable',
            'website'               => 'nullable',
            'email'                 => 'nullable|email',
            'kode_area.*'           => 'nullable|numeric',
            'telepon.*'             => 'nullable|numeric',
            'select_province'       => 'required',
            'select_city'           => 'required',
            'postcode'              => 'required',
            'alamat'                => 'required',
            'project_name'          => 'required',
            'tgl_project'           => 'nullable',
            'project_description'   => 'nullable',
            'nilai_kontrak'         => 'required',
            'nilai_project'         => 'required',
            'periode_start_date'    => 'required',
            'periode_end_date'      => 'required',
            'no_surat_undangan'     => 'nullable',
            'date_surat_undangan'   => 'nullable',
            'no_surat.*'            => 'nullable',
            'tgl_surat..*'          => 'nullable',
            'note.*'                => 'nullable',
            'document_project.*'    => 'nullable|mimes:jpg,png,svg,pdf,doc,docx,xls,xlsx'
        ]);
    }

    public function saveListAsuransi(Request $request)
    {
        $nama_asuransi  = $request->nama_asuransi;
        $draft_code     = $request->draft_code;
        $user_id        = $request->user()->token()->user_id;
        $url_asuransi   = $request->url_asuransi;

        $get_draft_id   = DraftOrderModel::where('code',$draft_code)->first();
        $data = [];
        DB::beginTransaction();
        try{

            if($url_asuransi)
            {

            }
            if($nama_asuransi){
                foreach($nama_asuransi as $key => $val)
                {
                    $data[] = [
                        'code'              => Uuid::uuid4(),
                        'draft_order_id'    => $get_draft_id->id,
                        'draft_order_code'  => $draft_code,
                        'name'              => $val,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'created_by'        => $user_id
                    ];
                }
                $save_draft_asuransi = DraftOrderListAsuransiModel::insert($data);
            }


            DB::commit();

            return response()->json([
                'message'       => 'Success save Data',
                'status'        => 'success',
                'code'          => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }
    }

    public function updateCompany(Request $request)
    {

        $company_type       = $request->company_type;
        $company_name       = $request->company_name;
        $jenis_badan_usaha  = $request->jenis_badan_usaha;
        $bidang_usaha       = $request->bidang_usaha;
        $no_npwp            = $request->no_npwp;
        $email              = $request->email;
        $kode_area          = $request->kode_area;
        $telepon            = $request->telepon;
        $province           = $request->province;
        $city               = $request->city;
        $postcode           = $request->postcode;
        $alamat             = $request->alamat;

        $pic_name           = $request->pic_name;
        $pic_email          = $request->pic_email;
        $pic_telp_code      = $request->pic_telp_code;
        $pic_telp           = $request->pic_telp;
        $pic_position       = $request->pic_position;

        $nama_komisaris     = $request->nama_komisaris;
        $ktp_komisaris      = $request->ktp_komisaris;
        $jabatan_komisaris  = $request->jabatan_komisaris;

        $nama_direksi       = $request->nama_direksi;
        $ktp_direksi        = $request->ktp_direksi;
        $jabatan_direksi    = $request->jabatan_direksi;

        $document_company   = $request->document_company;
        $note_document      = $request->note_document;
        $user_id            = $request->user()->token()->user_id;
        $uuid               = Uuid::uuid4();
        $company_code       = $request->company_code;



        $cek = UserCompanyModel::where('code',$company_code)->where('user_id',$user_id)->first();

        if(!$cek)
        {
            return response()->json([
                'message'       => 'Data perusahaan tdak tersedia',
                'status'        => "error",
                'code'          => 400
            ],200);
        }
        
        $old_identity = [];
        $new_identity = [];
        DB::beginTransaction();
        try{
            $telepon_number = [];
            if($kode_area){
                foreach($kode_area as $kycodeare => $valcodearea)
                {

                    if(array_key_exists($kycodeare,$telepon)){
                        $telp = $telepon[$kycodeare];
                        $telepon_number[] = [
                            'code'      => $valcodearea,
                            'number'    => $telp
                        ];
                    }


                }
            }

            if($telepon_number){
                $telepon_number = json_encode($telepon_number);
            }else{
                $telepon_number = null;
            }
            $data_company       = [
                'name'          => $company_name,
                'type'          => $company_type,
                'jenis_usaha'   => $jenis_badan_usaha,
                'bidang_usaha'  => $bidang_usaha,
                'npwp'          => $no_npwp,
                'email'         => $email,
                'no_telepon'    => $telepon_number,
                'm_province_id' => $province,
                'm_city_id'     => $city,
                'postcode'      => $postcode,
                'address'       => $alamat,
                'updated_at'    => date('Y-m-d H:i:s'),
                'updated_by'    => $user_id
            ];
            $save_company       = UserCompanyModel::where('code',$company_code)->update($data_company);

            // $nama_komisaris     =

            $get_komisaris = UserKomisarisDireksiModel::where('user_company_code',$company_code)->where('type',1)->get();
            foreach($get_komisaris as $row)
            {
                if(array_key_exists($row->code,$nama_komisaris))
                {
                    $data_komisaris['name'] = $nama_komisaris[$row->code];

                    if(array_key_exists($row->code,$jabatan_komisaris))
                    {
                        $data_komisaris['position'] = $jabatan_komisaris[$row->code];
                    }

                    if($ktp_komisaris){
                        if(array_key_exists($row->code,$ktp_komisaris))
                        {
                            $ktp_komisaris_file = generate_url(strtolower($row->name.'-'.$row->position.'-new-at-'.time())).'.'.$ktp_komisaris[$row->code]->extension();
                            $ktp_komisaris_file = strtolower($ktp_komisaris_file);

                            $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$company_code);

                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/company-document/company-code-'.$company_code, $ktp_komisaris[$row->code], $ktp_komisaris_file);
                                }
                            } else {
                                Storage::putFileAs('public/company-document/company-code-'.$company_code, $ktp_komisaris[$row->code], $ktp_komisaris_file);
                            }
                            $new_identity[] = $ktp_komisaris_file;
                            $old_identity[] = $row->identity_file;
                            $data_komisaris['identity_file'] = $ktp_komisaris_file;
                        }
                    }

                    $data_komisaris['updated_at'] = date('Y-m-d H:i:s');
                    $data_komisaris['updated_by'] = $user_id;
                }
                else
                {
                    $data_komisaris['deleted_at'] = date('Y-m-d H:i:s');
                    $data_komisaris['deleted_by'] = $user_id;
                    $old_identity[]               = $row->identity_file;
                }

                $update_komisaris = UserKomisarisDireksiModel::where('id',$row->id)->update($data_komisaris);
            }

            if($nama_komisaris)
            {
                foreach($nama_komisaris as $key => $val)
                {
                    if(!empty($val)){
                        $cek_komisaris = UserKomisarisDireksiModel::where('user_company_code',$company_code)->where('type',1)->where('code',$key)->first();
                        if(!$cek_komisaris)
                        {
                            $insert_komisaris['name'] = $val;
                            if(array_key_exists($key,$jabatan_komisaris))
                            {
                                $insert_komisaris['position'] = $jabatan_komisaris[$key];
                            }
    
    
                            if($ktp_komisaris){
                                if(array_key_exists($key,$ktp_komisaris))
                                {
                                    $ktp_komisaris_file = generate_url(strtolower($val.'-'.$jabatan_komisaris[$key].'-new-at-'.time())).'.'.$ktp_komisaris[$key]->extension();
                                    $ktp_komisaris_file = strtolower($ktp_komisaris_file);
    
                                    $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$company_code);
    
                                    if (!file_exists($upload_path_attachment)) {
                                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                            Storage::putFileAs('public/company-document/company-code-'.$company_code, $ktp_komisaris[$key], $ktp_komisaris_file);
                                        }
                                    } else {
                                        Storage::putFileAs('public/company-document/company-code-'.$company_code, $ktp_komisaris[$key], $ktp_komisaris_file);
                                    }
                                    $new_identity[]                      = $ktp_komisaris_file;
                                    $insert_komisaris['identity_file'] = $ktp_komisaris_file;
                                }
                            }
                            $insert_komisaris['created_at']                     = date('Y-m-d H:i:s');
                            $insert_komisaris['type']                           = 1;
                            $insert_komisaris['created_by']                     = $user_id;
                            $insert_komisaris['code']                           = Uuid::uuid4();;
                            $insert_komisaris['user_company_id']                = $cek->id;
                            $insert_komisaris['user_company_code']              = $cek->code;
    
                            $save_komisaris = UserKomisarisDireksiModel::insert($insert_komisaris);
    
                        }
                    }
                }
            }

            $get_direksi = UserKomisarisDireksiModel::where('user_company_code',$company_code)->where('type',2)->get();

            foreach($get_direksi as $row)
            {
                if(array_key_exists($row->code,$nama_direksi))
                {
                    $data_direksi['name'] = $nama_direksi[$row->code];

                    if(array_key_exists($row->code,$jabatan_direksi))
                    {
                        $data_direksi['position'] = $jabatan_direksi[$row->code];
                    }

                    if($ktp_direksi){
                        if(array_key_exists($row->code,$ktp_direksi))
                        {
                            $ktp_direksi_file = generate_url(strtolower($row->name.'-'.$row->position.'-new-at-'.time())).'.'.$ktp_direksi[$row->code]->extension();
                            $ktp_direksi_file = strtolower($ktp_direksi_file);

                            $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$company_code);

                            if (!file_exists($upload_path_attachment)) {
                                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                    Storage::putFileAs('public/company-document/company-code-'.$company_code, $ktp_direksi[$row->code], $ktp_direksi_file);
                                }
                            } else {
                                Storage::putFileAs('public/company-document/company-code-'.$company_code, $ktp_direksi[$row->code], $ktp_direksi_file);
                            }
                            $new_identity[] = $ktp_direksi_file;
                            $old_identity[] = $row->identity_file;
                            $data_direksi['identity_file'] = $ktp_direksi_file;
                        }
                    }

                    $data_direksi['updated_at'] = date('Y-m-d H:i:s');
                    $data_direksi['updated_by'] = $user_id;
                }
                else
                {
                    $data_direksi['deleted_at'] = date('Y-m-d H:i:s');
                    $data_direksi['deleted_by'] = $user_id;
                    $old_identity[]                   = $row->identity_file;
                }

                $update_komisaris = UserKomisarisDireksiModel::where('id',$row->id)->update($data_direksi);

            }

            if($nama_direksi)
            {
                foreach($nama_direksi as $key => $val)
                {
                    $cek_direksi = UserKomisarisDireksiModel::where('user_company_code',$company_code)->where('type',2)->where('code',$key)->first();
                    if(!$cek_direksi)
                    {
                        $insert_direksi['name'] = $val;
                        if(array_key_exists($key,$jabatan_direksi))
                        {
                            $insert_direksi['position'] = $jabatan_direksi[$key];
                        }


                        if($ktp_direksi){
                            if(array_key_exists($key,$ktp_direksi))
                            {
                                $ktp_direksi_file = generate_url(strtolower($val.'-'.$jabatan_direksi[$key].'-new-at-'.time())).'.'.$ktp_direksi[$key]->extension();
                                $ktp_direksi_file = strtolower($ktp_direksi_file);

                                $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$company_code);

                                if (!file_exists($upload_path_attachment)) {
                                    if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                                        Storage::putFileAs('public/company-document/company-code-'.$company_code, $ktp_direksi[$key], $ktp_direksi_file);
                                    }
                                } else {
                                    Storage::putFileAs('public/company-document/company-code-'.$company_code, $ktp_direksi[$key], $ktp_direksi_file);
                                }
                                $new_identity[]                      = $ktp_direksi_file;
                                $insert_direksi['identity_file'] = $ktp_direksi_file;
                            }
                        }
                        $insert_direksi['created_at']                     = date('Y-m-d H:i:s');
                        $insert_direksi['type']                           = 2;
                        $insert_direksi['created_by']                     = $user_id;
                        $insert_direksi['code']                           = Uuid::uuid4();
                        $insert_direksi['user_company_id']                = $cek->id;
                        $insert_direksi['user_company_code']              = $cek->code;

                        $save_direksi = UserKomisarisDireksiModel::insert($insert_direksi);

                    }
                }
            }
            
            
            

            if($pic_name)
            {
                foreach($pic_name as $kypic => $val)
                {
                    $cek_pic = UserCompanyPICModel::where('code',$kypic)->first();


                    if(array_key_exists($kypic,$pic_email)){
                        $pic_email_new = $pic_email[$kypic];
                    }else{
                        if($cek_pic){
                            $pic_email_new = $cek_pic->email;
                        }else{
                            $pic_email_new = null;
                        }
                    }

                    if(array_key_exists($kypic,$pic_telp_code)){
                        $pic_telp_code_new = $pic_telp_code[$kypic];
                    }else{
                        if($cek_pic){
                            $pic_telp_code_new = $cek_pic->code_telepon;
                        }else{
                            $pic_telp_code_new = null;
                        }
                    }

                    if(array_key_exists($kypic,$pic_telp)){
                        $pic_telp_new = $pic_telp[$kypic];
                    }else{
                        if($cek_pic){
                            $pic_telp_new = $cek_pic->telepon;
                        }else{
                            $pic_telp_new = null;
                        }
                    }

                    if(array_key_exists($kypic,$pic_position)){
                        $pic_position_new = $pic_position[$kypic];
                    }else{
                        if($cek_pic){
                            $pic_position_new = $cek_pic->posisi;
                        }else{
                            $pic_position_new = null;
                        }
                    }
                    $data_pic['name']               = $val;
                    $data_pic['email']              = $pic_email_new;
                    $data_pic['posisi']             = $pic_position_new;
                    $data_pic['code_telepon']       = $pic_telp_code_new;
                    $data_pic['telepon']            = $pic_telp_new;

                    if($cek_pic)
                    {
                        $data_pic['updated_at']     = date('Y-m-d H:i:s');
                        $data_pic['updated_by']     = $user_id;
                        $save_pic = UserCompanyPICModel::where('code',$kypic)->update($data_pic);
                    }else{
                        $data_pic['user_company_id']    =   $cek->id;
                        $data_pic['user_company_code']  = $cek->code;
                        $data_pic['code']               = Uuid::uuid4();
                        $data_pic['created_at']         = date('Y-m-d H:i:s');
                        $data_pic['created_by']         = $user_id;
                        $save_pic = UserCompanyPICModel::insert($data_pic);
                    }

                }

                $get_exist_pic = $this->buildArrayPICExists($company_code);

                foreach($get_exist_pic as $key => $val){
                    if(!array_key_exists($val,$pic_name)){
                        $delete_old = UserCompanyPICModel::where('code',$key)->update([
                            'deleted_at'    => date('Y-m-d H:i:s'),
                            'deleted_by'    => $user_id
                        ]);
                    }
                }
            }

            if($document_company)
            {
                foreach($document_company as $key => $val)
                {
                    $cek_document   = UserCompanyDocumentModel::where('user_company_code',$company_code)->where('m_document_upload_code',$key)->first();

                    $get_doc_upload = M_DocPengajuan::where('code',$key)->first();
                    $doc_name       = generate_url($get_doc_upload->name).'-new-at-'.time().'.'.$val->extension();;
                    $upload_path_attachment = storage_path('public/company-document/company-code-'.$company_code);

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/company-document/company-code-'.$company_code, $val, $doc_name);
                        }
                    } else {
                        Storage::putFileAs('public/company-document/company-code-'.$company_code, $val, $doc_name);
                    }

                    if($note_document){
                        if(array_key_exists($key,$note_document))
                        {
                            $data_doc['note'] = $note_document[$key];
                        }
                    }
                    $new_identity[] = $doc_name;
                    if($cek_document)
                    {
                        $old_identity[]                 = $cek_document->document_name;
                        $data_doc['file_name']      = $doc_name;
                        $data_doc['updated_at']     = date('Y-m-d H:i:s');
                        $data_doc['updated_by']     = $user_id;

                        $update_doc = UserCompanyDocumentModel::where('id',$cek_document->id)->update($data_doc);
                    }
                    else
                    {

                        $data_doc['code']                   = Uuid::uuid4();
                        $data_doc['user_company_id']        = $cek->id;
                        $data_doc['user_company_code']      = $company_code;
                        $data_doc['m_document_upload_id']   = $get_doc_upload->id;
                        $data_doc['m_document_upload_code'] = $get_doc_upload->code;
                        $data_doc['name']                   = $get_doc_upload->name;
                        $data_doc['file_name']              = $doc_name;
                        $data_doc['created_at']             = date('Y-m-d H:i:s');
                        $data_doc['created_by']             = $user_id;
                        $insert_doc = UserCompanyDocumentModel::insert($data_doc);
                    }

                }
            }
            
            if($note_document)
            {
                foreach($note_document as $key => $val)
                {
                    $cek_document   = UserCompanyDocumentModel::where('user_company_code',$company_code)->where('m_document_upload_code',$key)->first();

                    if($cek_document)
                    {
                        $note_doc['note']           = $val;
                        $note_doc['updated_at']     = date('Y-m-d H:i:s');
                        $note_doc['updated_by']     = $user_id;
                        $update_doc                 = UserCompanyDocumentModel::where('id',$cek_document->id)->update($note_doc);
                    }
                }
            }

            DB::commit();

            if(count($old_identity) > 0)
            {

                foreach($old_identity as $kydocjs => $valdocjs)
                {
                    if($valdocjs){
                        if(Storage::exists('/public/company-document/company-code-'.$cek->code.'/'.$valdocjs)){
                            Storage::delete('/public/company-document/company-code-'.$cek->code.'/'.$valdocjs);
                        }
                    }
                }
            }

            return response()->json([
                'message'       => 'Success save Data',
                'status'        => 'success',
                'company_code'  => $cek->code,
                'code'          => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();

            if(count($new_identity) > 0){
                foreach($new_identity as $kydocjs => $valdocjs)
                {
                    if($valdocjs){

                        if(Storage::exists('/public/company-document/company-code-'.$cek->code.'/'.$valdocjs)){
                            Storage::delete('/public/company-document/company-code-'.$cek->code.'/'.$valdocjs);
                        }
                    }
                }
            }
            return response()->json([
                'message'       => $e->getMessage().'. Line :'.$e->getLine(),
                'status'        => 'error',
                'code'          => 500,
            ], 500);
        }
    }

    function update_komisaris_direksi($data=null,$type=null,$user_id=null)
    {
        foreach($data as $key => $val)
        {
            if($type == 1){
                $update_data['name']        =  $val;
            }
            else if($type == 2)
            {
                $update_data['position']        =  $val;
            }
            $update_data['updated_at']  = date('Y-m-d H:i:s');
            $update_data['updated_by']  = $user_id;

            $update_komisaris = UserKomisarisDireksiModel::where('code',$key)->update($update_data);
        }
    }

    function update_file_komisaris($data=null,$name=null,$position=null,$user_id=null,$company_id=null,$company_code=null,$type=null)
    {
        $old_identity_komisaris = [];
        $new_identity           = [];

        foreach($data as $key => $val)
        {
            $cek_komisaris = UserKomisarisDireksiModel::where('code',$key)->first();


            $ktp_komisaris_file = generate_url(strtolower($cek_komisaris->name.'-'.$cek_komisaris->position.'-new-at-'.time())).'.'.$val->extension();
            $ktp_komisaris_file = strtolower($ktp_komisaris_file);

            $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$cek_komisaris->user_company_code);

            if (!file_exists($upload_path_attachment)) {
                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                    Storage::putFileAs('public/company-document/company-code-'.$cek_komisaris->user_company_code, $val, $ktp_komisaris_file);
                }
            } else {
                Storage::putFileAs('public/company-document/company-code-'.$cek_komisaris->user_company_code, $val, $ktp_komisaris_file);
            }
            $new_identity[] = $ktp_komisaris_file;


            if($name){
                if(array_key_exists($key,$name)){
                    $update_ktp_komisaris['name']     = $name[$key];
                }
            }

            if($position){
                if(array_key_exists($key,$position)){
                    $update_ktp_komisaris['position']     = $position[$key];
                }
            }

            if($cek_komisaris)
            {
                $old_identity_komisaris[] = $cek_komisaris->identity_file;


                $update_ktp_komisaris['identity_file']  = $ktp_komisaris_file;
                $update_ktp_komisaris['updated_at']     = date('Y-m-d H:i:s');
                $update_ktp_komisaris['updated_by']     = $user_id;

                $update_komisaris = UserKomisarisDireksiModel::where('code',$key)->update($update_ktp_komisaris);
            }else{
                    $update_ktp_komisaris['identity_file']     = $ktp_komisaris_file;
                    $update_ktp_komisaris['created_at']        = date('Y-m-d H:i:s');
                    $update_ktp_komisaris['created_by']        = $user_id;
                    $update_ktp_komisaris['code']              = Uuid::uuid4();
                    $update_ktp_komisaris['user_company_id']   = $company_id;
                    $update_ktp_komisaris['user_company_code'] = $company_code;
                    $update_ktp_komisaris['type']              = $type;

                    $update_komisaris = UserKomisarisDireksiModel::insert($update_ktp_komisaris);
            }
        }

        $response['old$old_identity'] = $old_identity_komisaris;
        $response['new$new_identity'] = $new_identity;

        return $response;
    }

    function update_file_document($data_doc=null,$note=null,$user_id=null,$company_id=null,$company_code=null)
    {
        $document_save  = [];
        $new_identity       = [];
        $old_identity       = [];
        foreach($note as $keydoc => $valdoc)
        {

            $note_doc           = $valdoc;
            $data_save['note']  = $note_doc;
            $cek                = UserCompanyDocumentModel::where('m_document_upload_code',$keydoc)->where('user_company_code',$company_code)->first();
            
             $id_doc                 = null;
            $code_doc               = null;
            $name_doc               = null;
            if($data_doc){
                if(array_key_exists($keydoc,$data_doc)){
                    $get_doc_upload = M_DocPengajuan::where('code',$keydoc)->first();
                    $doc_name       = generate_url($get_doc_upload->name).'-new-at-'.time().'.'.$data_doc[$keydoc]->extension();;
                    $upload_path_attachment = storage_path('app/public/company-document/company-code-'.$company_code);

                    if (!file_exists($upload_path_attachment)) {
                        if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                            Storage::putFileAs('public/company-document/company-code-'.$company_code, $data_doc[$keydoc], $doc_name);
                        }
                    } else {
                        Storage::putFileAs('public/company-document/company-code-'.$company_code, $data_doc[$keydoc], $doc_name);
                    }


                    $data_save['file_name'] = $doc_name;
                    
                    $new_identity[]             = $doc_name;
                    if($cek){
                        $old_identity[]             = $cek->file_name;
                    }
                    $id_doc                 = $get_doc_upload->id;
                    $code_doc               = $get_doc_upload->code;
                    $name_doc               = $get_doc_upload->name;
                }
            }


            if($cek)
            {
                // return $cek;
                $data_save['updated_at']     = date('Y-m-d H:i:s');
                $data_save['updated_by']     = $user_id;
                $save_doc                    = UserCompanyDocumentModel::where('code',$cek->code)->update($data_save);
            }else{
                if(array_key_exists($keydoc,$data_doc)){
                    $data_save['code']                      =  Uuid::uuid4();
                    $data_save['user_company_id']           =  $company_id;
                    $data_save['user_company_code']         =  $company_code;
                    $data_save['m_document_upload_id']      =  $id_doc;
                    $data_save['m_document_upload_code']    =  $code_doc;
                    $data_save['name']                      =  $name_doc;
                    $data_save['created_at']                =  date('Y-m-d H:i:s');
                    $data_save['created_by']                =  $user_id;
                    $save_doc                               = UserCompanyDocumentModel::insert($data_save);
                }
            }

        }
        $response['old_identity'] = $old_identity;
        $response['new_identity'] = $new_identity;

        return $response;
    }


    public function buildDireksiKomisarisarray($company_code=null,$type=null){
        $get_data = UserKomisarisDireksiModel::where('user_company_code',$company_code)->where('type',$type)->select('code')->get();

        $data = [];

        foreach($get_data as $key => $val){
            $data[] = $val->code;
        }

        return $data;
    }

    public function buildArrayPICExists($company_code=null)
    {
        $get_data = UserCompanyPICModel::where('user_company_code',$company_code)->select('code')->get();

        $data = [];

        foreach($get_data as $key => $val){
            $data[] = $val->code;
        }

        return $data;
    }
    
    public function getListObligee(Request $request)
    {
        $user_id            = $request->user()->token()->user_id;
        $qwhere             = strtolower($request->q);
        $get_data           = DaftarObligeeModel::select(DB::raw("CONCAT(company_name,', (', company_type_name, ')') as label"),'daftar_obligee.*','company_name as value')->where('created_by',$user_id)
                            ->whereRaw('lower(company_name) like (?)',["%{$qwhere}%"])->get();
        return response()->json([
            'message'       => 'Success Get Data',
            'status'        => "success",
            'code'          => 200,
            'data'          => $get_data
        ],200);
    }
}
