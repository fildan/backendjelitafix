<?php

namespace App\Http\Controllers\API\V1\MethodePayment;

use App\Models\Payment\M_MasterMethodePayment;
use App\Models\Payment\M_MethodePaymentBank;
use App\Models\Payment\M_MethodePaymentBankStep;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;

class MethodePaymentController extends BaseController
{

    private $url_endpoint;
    private $mid;
    private $key;

    public function __construct()
    {

        if (app()->environment() === 'production') {
            $this->url_endpoint             = 'https://api.midtrans.com/v2/charge';
            $this->token_key                =  base64_encode(env('MID_SERVER_KEY_PROD').":");
        } else {
            $this->url_endpoint          = "https://sandbox.finpay.co.id/servicescode/api/apiFinpay.php";
            $this->mid                   =  "S777CEMB5QBGF";
            $this->key                   = "Z01Dp8ie";
        }
        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getMasterPaymentMethode(Request $request)
    {
        $get_data = M_MasterMethodePayment::with(['get_payment_methode_bank','get_payment_methode_bank.get_bank'])->select('code','description')->get();
        return response()->json([
            'message'   => 'success get data',
            'status'    => 'success',
            'data'      => $get_data,
            'code'      => 200,
        ], 200);
    }

    public function getStepPayment(Request $request)
    {
        $payment_bank_reff_code = $request->payment_bank_code;
        $get_data               = M_MethodePaymentBankStep::where('m_methode_payment_bank_code',$payment_bank_reff_code)->select('step','name','exception')->get();
        return response()->json([
            'message'   => 'success get data',
            'status'    => 'success',
            'data'      => $get_data,
            'code'      => 200,
        ], 200);
    }
}
