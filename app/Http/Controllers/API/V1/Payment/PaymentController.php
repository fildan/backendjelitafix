<?php

namespace App\Http\Controllers\API\V1\Payment;

use App\Models\NewOrder\NewOrderModel;
use App\Models\Order\M_Order;
use App\Models\Payment\M_HistoryPayment;
use App\Models\Payment\M_Invoice;
use App\Models\Payment\M_MasterBank;
use App\Models\Payment\M_MasterMethodePayment;
use App\Models\Payment\M_MasterPayment;
use App\Models\Payment\M_MethodePaymentBank;
use App\Models\Payment\M_Payment;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;

class PaymentController extends BaseController
{

    private $url_endpoint;
    private $mid;
    private $key;

    public function __construct()
    {

        if (app()->environment() === 'production') {
            $this->url_endpoint             = 'https://api.midtrans.com/v2/charge';
            $this->token_key                =  base64_encode(env('MID_SERVER_KEY_PROD').":");
        } else {
            $this->url_endpoint          = "https://sandbox.finpay.co.id/servicescode/api/apiFinpay.php";
            $this->mid                   =  "S777CEMB5QBGF";
            $this->key                   = "Z01Dp8ie";
        }
        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ProcessPaymentOrder(Request $request)
    {
        
        $order_number               = $request->order_number;
        $payment_bank_code          = $request->payment_bank_code;
        $payment_type               = $request->payment_type;
        $bank_code                  = $request->bank_code;
        $total_payment              = $request->total_payment;
        $is_cetak_polis             = $request->is_cetak_polis;
        $price_cetak_polis          = $request->price_cetak_polis;
        $price_before_ongkir_input  = $request->price_before_ongkir_input;
        $user_id                    = $request->user()->token()->user_id;

        $discount               = $request->discount;

        if($discount){
            $discount = $discount;
        }else{
            $discount = 0;
        }

        $total_amount = $total_payment - $discount;
        $get_bank_code_payment  = M_MethodePaymentBank::where('code',$payment_bank_code)->first();

        $get_order              = NewOrderModel::where('code',$order_number)->first();
        $inv_number             = $this->checkCode();
        DB::beginTransaction();
        try{
            $data_save_invoice = [
                'code'                      => $inv_number,
                'reff_code'                 => Uuid::uuid4(),
                'new_order_code'                => $order_number,
                'new_order_id'                  => $get_order->id,
                'status'                    => 2,
                'amount'                    => $price_before_ongkir_input,
                'printing_cost_polis'       => $price_cetak_polis,
                'total_amount'              => $total_amount,
                'discount'                  => $discount,
                'is_print_polis'            => $is_cetak_polis,
                'invoice_type'              => 1,
                'created_by'                => $user_id,
                'created_at'                => date('Y-m-d H:i:s')
            ];

            $save_invoice           = M_Invoice::insertGetId($data_save_invoice);
            $date_now               = date_create(date('Y-m-d H:i:s'));
            $expired_time           = date_format(date_add($date_now, date_interval_create_from_date_string('7 hours')),'Y-m-d H:i:s');
            $format_exp_time        = date('H:i:00',strtotime($expired_time));
            $format_exp_date_time   = date('Y-m-d H:i:00',strtotime($expired_time));
            $response_data          = [];
            if($payment_type == 'VA')
            {

                $pay                    = $this->paidWithVA($data_save_invoice,$get_order,$user_id,$get_bank_code_payment->pga_code);

                $paid_proccess  = $this->sendToPaymentGateway($pay);

                $response_paid  = json_decode($paid_proccess);

                if($response_paid->status_code != "00"){
                    DB::rollback();
                    $status_response                = $response_paid->status_code;
                    $message_response               = $response_paid->status_desc;
                }else{
                    // $save_payment['']
                    $save_payment['invoice_id']                         = $save_invoice;
                    $save_payment['invoice_code']                       = $data_save_invoice['code'];
                    $save_payment['payment_code']                       = $response_paid->payment_code;
                    $save_payment['m_methode_payment_bank_id']          = $get_bank_code_payment->id;
                    $save_payment['m_methode_payment_bank_code']        = $get_bank_code_payment->code;
                    // $save_payment['m_methode_payment_bank_pga_code']   = $get_bank_code_payment->reff_code;
                    $save_payment['m_bank_id']                          = $get_bank_code_payment->m_bank_id;
                    $save_payment['m_bank_code']                        = $get_bank_code_payment->m_bank_code;
                    $save_payment['m_methode_payment_id']               = $get_bank_code_payment->m_methode_payment_id;
                    $save_payment['m_methode_payment_code']             = $get_bank_code_payment->m_methode_payment_code;
                    $save_payment['total_amount']                       = $total_payment;
                    $save_payment['status']                             = 1;
                    $save_payment['created_by']                         = $user_id;
                    $save_payment['created_at']                         = date('Y-m-d H:i:s');
                    $save_payment['expired_time']                       = $format_exp_time;
                    $save_payment['expired_date_time']                  = $format_exp_date_time;
                    $save_payment['code']                               = Uuid::uuid4();
                    $save_payment_db                                    = M_Payment::insertGetId($save_payment);

                    $update_inv                                     = M_Invoice::where('id',$save_invoice)->update(['status' => 2]);


                    $history_progress                               = $this->buildHistoryProgressOrder($order_number,6,'');
                    $update_order                                   = NewOrderModel::where('id',$get_order->id)->update([
                                                                                'status'            => 6,
                                                                                'updated_at'        => date('Y-m-d H:i:s'),
                                                                                'progress'  => json_encode($history_progress)
                                                                            ]);

                    $response_data['code']              = $save_payment['code'];

                    $status_response                    = 200;
                    $message_response                   = 'Success';
                }
            }
            else if($payment_type == 'TFM')
            {
                $save_payment['invoice_id']                         = $save_invoice;
                $save_payment['invoice_code']                       = $data_save_invoice['code'];
                $save_payment['no_rekening']                        = $get_bank_code_payment->no_rekening;
                $save_payment['m_methode_payment_bank_id']          = $get_bank_code_payment->id;
                $save_payment['m_methode_payment_bank_code']        = $get_bank_code_payment->code;
                $save_payment['m_bank_id']                          = $get_bank_code_payment->m_bank_id;
                $save_payment['m_bank_code']                        = $get_bank_code_payment->m_bank_code;
                $save_payment['m_methode_payment_id']               = $get_bank_code_payment->m_methode_payment_id;
                $save_payment['m_methode_payment_code']             = $get_bank_code_payment->m_methode_payment_code;
                $save_payment['total_amount']                       = $total_payment;
                $save_payment['status']                             = 1;
                $save_payment['created_by']                         = $user_id;
                $save_payment['created_at']                         = date('Y-m-d H:i:s');
                $save_payment['expired_time']                       = $format_exp_time;
                $save_payment['expired_date_time']                  = $format_exp_date_time;
                $save_payment['code']                               = Uuid::uuid4();
                $save_payment_db                                    = M_Payment::insertGetId($save_payment);

                $update_inv                                     = M_Invoice::where('id',$save_invoice)->update(['status' => 2]);


                $history_progress                               = $this->buildHistoryProgressOrder($order_number,6,'');
                $update_order                                   = NewOrderModel::where('id',$get_order->id)->update([
                                                                            'status'            => 6,
                                                                            'updated_at'        => date('Y-m-d H:i:s'),
                                                                            'progress'  => json_encode($history_progress)
                                                                        ]);
                $response_data['code']              = $save_payment['code'];

                $status_response                    = 200;
                $message_response                   = 'Success';
            }

            DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => $message_response,
                'status'    => 'success',
                'data'      => $response_data,
                'code'      => $status_response,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }
    }



    function paidWithVA($inv_data,$data_order,$user_id,$paycode)
    {
        $get_user_id        = User::with(['get_profile'])->where('id',$user_id)->first();

        $arrParam           = array(
                "amount"        => $inv_data['total_amount_payment'],
                "cust_email"    => $get_user_id->email,
                "cust_id"       => $get_user_id->get_profile->ktp_number,
                "cust_msisdn"   => $get_user_id->get_profile->phone,
                "cust_name"     => $get_user_id->name,
                "invoice"       => $inv_data['code'],
                "merchant_id"   => $this->mid,
                "items"         => $data_order->get_product_penanggung->name,
                "return_url"    => url('api/v1/payment/paid?invoice='.$inv_data['code'].'&amount='.$inv_data['total_amount_payment']),
                // "success_url"   => "https://enprkoj2cr34s.x.pipedream.net",
                "timeout"       => "720",
                "trans_date"    => date("YmdHis"),
                "add_info1"     => $get_user_id->name,
                "sof_id"        => $paycode,
                "sof_type"      => "pay"
        );
        $key        = $this->key;
        $signature  = $this->createSignaturePayment($arrParam, $key);
        $arrParam   = array_merge($arrParam, array("mer_signature"=>$signature));
        return $arrParam;
    }

    function createSignaturePayment($arrParam,$key)
    {
        ksort($arrParam, 0);
        $signature=hash('sha256', strtoupper(implode("%",$arrParam))."%".$key);;
        return $signature;
    }

    function sendToPaymentGateway($arrParam)
    {
        $url_endpoint= $this->url_endpoint;
        $res= Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])->post($url_endpoint, $arrParam);
        return $res;
    }

    function checkCode()
    {
        $invoice_number     =   'INV-'.date('dmy');
        $invoice_number    .=   rand(0000,9999);

        $cek_invoice_number = M_Invoice::where('code',$invoice_number)->get();
        while (count($cek_invoice_number)>0) {
            $invoice_number     = substr($invoice_number,0,-4);
            $invoice_number    .= rand(0000,9999);
            $cek_invoice_number = M_Invoice::where('code',$invoice_number)->get();
        }

        return $invoice_number;
    }

    public function PaymentPaid(Request $request)
    {

        $inv_code       = $request->invoice;
        $pay_date       = $request->pay_date;
        $payment_date   = date('Y-m-d H:i:s', strtotime($pay_date));
        $payment_source = $request->payment_source;
        $reff_no        = $request->reff_no;
        $status_pay     = $request->result_code;
        $status_desc    = $request->result_desc;

        $cek            = M_Payment::where('code',$inv_code)->where('status',1)->first();
        DB::beginTransaction();
        try{
            if($cek){
                if($status_pay == 00){
                    $status_payment     = 2;
                    $status_inv         = 3;
                    $status_order       = 8;
                    $status_progress    = 'Pembayaran berhasil pada '.$payment_date;
                }else{
                    $status_payment     = 3;
                    $status_inv         = 4;
                    $status_order       = 7;
                    $status_progress    = $status_desc.' payment. date '.$payment_date;
                }
                $payment_date   = date('Y-m-d H:i:s');
                $update         =   M_Payment::where('code',$inv_code)
                                        ->update([
                                            'status'            => $status_payment,
                                            'payment_date'      => $payment_date,
                                            'payment_source'    => $payment_source,
                                            'reff_no'           => $reff_no,
                                            'status_note'       => $status_desc,
                                            'updated_at'        => date('Y-m-d H:i:s')
                                        ]);

                $get_iv     = M_Invoice::where('code',$cek->invoice_code)->first();

                $update_inv = M_Invoice::where('code',$get_iv->code)
                                        ->update([
                                            'status'        => $status_inv,
                                            'updated_at'    => date('Y-m-d H:i:s')
                                        ]);


                $history_progress   = $this->buildHistoryProgressOrder($get_iv->order_code,8,$status_progress);
                $update_order       = M_Order::where('code',$get_iv->order_code)
                                        ->update([
                                            'status'            => $status_order,
                                            'updated_at'        => date('Y-m-d H:i:s'),
                                            'history_progress'  => json_encode($history_progress)
                                        ]);
            }
            DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'code'      => 200
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }
    }

    function buildHistoryProgressOrder($order_code=null,$status=null,$note=null)
    {

        $get_order          = NewOrderModel::where('code',$order_code)->first();
        $history_progress   = json_decode($get_order->progress);

        $history = [];

        foreach($history_progress as $row => $val)
        {
            $history[] = [
                'date'      => $val->date,
                'status'    => $val->status,
                'note'      => $val->note
            ];
        }

        $history[] = [
            'date'      => date('Y-m-d H:i:s'),
            'status'    => $status,
            'note'      => $note
        ];

        return $history;
    }

    public function getDetailPayment(Request $request)
    {
        DB::beginTransaction();
        try{
            $code      = $request->code;
            $get_data  = M_Payment::with(['get_methode_payment','get_methode_payment_bank','get_bank'])
                        ->where('code',$code)
                        ->select('m_bank_code','m_methode_payment_code','no_rekening','payment_code','total_amount','expired_time','expired_date_time','status','m_methode_payment_bank_code','code')
                        ->first();


            DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => 'success',
                'data'      => $get_data,
                'status'    => 'success',
                'code'      => 200
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }

    }
    public function SaveProofOfPayment(Request $request)
    {
        $file                       = $request->file_bukti_bayar;
        $payment_code_proof_payment = $request->payment_code_proof_payment;
        $get_payment                = M_Payment::where('code',$payment_code_proof_payment)->first();
        $user_id                    = $request->user()->token()->user_id;

        if(!$get_payment)
        {
            return response()->json([
                'message'       => 'Invoice Tidak Ditemukan',
                'status'        => "error",
                'code'          => 404,
                'data'          => null
            ],200);
        }

        $get_invoice= M_Invoice::where('code',$get_payment->invoice_code)->first();
        $get_order  = NewOrderModel::where('code',$get_invoice->new_order_code)->first();

        DB::beginTransaction();
        try{

            $doc_name       = generate_url('proof-of-payment-code-'.$payment_code_proof_payment.'-'.Uuid::uuid4()).'.'.$file->extension();;
            $upload_path_attachment = storage_path('app/public/proof-of-payment/'.$payment_code_proof_payment);

            if (!file_exists($upload_path_attachment)) {
                if (File::makeDirectory($upload_path_attachment, 0777, true)) {
                    Storage::putFileAs('public/proof-of-payment/'.$payment_code_proof_payment, $file, $doc_name);
                }
            } else {
                Storage::putFileAs('public/proof-of-payment/'.$payment_code_proof_payment, $file, $doc_name);
            }

            $data_save_proof_of_payment = [
                'payment_code'          => $payment_code_proof_payment,
                'payment_id'            => $get_payment->id,
                'payment_date'          => date('Y-m-d H:i:s',strtotime($request->tgl_bayar)),
                'payment_source'        => null,
                'status'                => 1,
                'reff_no'               => null,
                'status_note'           => null,
                'proof_of_payment'      => $doc_name,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $save_proof_of_payment = M_HistoryPayment::insert($data_save_proof_of_payment);

            $data_update_payment = [
                'status'        => 5,
                'updated_at'    => date('Y-m-d H:i:s'),
                'updated_by'    => $user_id
            ];

            $update_payment = M_Payment::where('code',$payment_code_proof_payment)->update($data_update_payment);


            $update_invoice = M_Invoice::where('code',$get_payment->invoice_code)
                                ->update(
                                    [
                                        'status'        => 5,
                                        'updated_at'    => date('Y-m-d H:i:s'),
                                        'updated_by'    => $user_id
                                    ]
                                );
            $get_progress = json_decode($get_order->progress);
            $progress = [];
            foreach($get_progress as $row => $val)
            {
                $progress[] = [
                    'date'      => $val->date,
                    'status'    => $val->status,
                    'note'      => $val->note
                ];
            }
            $progress[] = array(
                'date'      => date('Y-m-d H:i:s'),
                'status'    => 11,
                'note'      => 'Bukti bayar telah di upload'
            );

            $update_order = NewOrderModel::where('code',$get_order->code)
                                ->update(
                                    [
                                        'status'        => 11,
                                        'updated_at'    => date('Y-m-d H:i:s'),
                                        'updated_by'    => $user_id,
                                        'progress'      => json_encode($progress)
                                    ]
                                );
            DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => 'Success to save',
                'status'    => 'success',
                'data'      => null,
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            if($file)
            {

                Storage::delete('public/proof-of-payment/'.$payment_code_proof_payment.'/'.$doc_name);
            }
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
                'code'          => 500
            ],500);
        }



    }
}
