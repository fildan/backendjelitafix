<?php

namespace App\Http\Controllers\API\Master;

use App\Models\M_Product;
use App\Models\M_ReferencePenanggungModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;
use Validator;

class ReferencePenanggungController extends BaseController
{

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getListReferenge(Request $request)
    {
        $get_reference_list = M_ReferencePenanggungModel::with(['get_company_type'])->get();
        $cob                = $request->cob;


        $list_data = [];
        foreach($get_reference_list as $key => $val)
        {

            // if(in_array())
            $get_cob            = M_Product::where('url',$cob)->first();


            if($get_cob){
                $cek_cob = json_decode($val->m_product);

                $cek_cob = collect($cek_cob)->where('code',$get_cob->code)->first();
                $list_data[] = [
                    'name' => ucwords($val->name).', ( '.strtoupper($val->get_company_type->name).' )',
                    'code' => $val->code
                ];
            }
        }
        return response()->json([
            'message'   => 'Success get data',
            'status'    => 'success',
            'data'      => $list_data,
            'code'      => 200
        ],200);
    }
}
