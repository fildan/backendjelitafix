<?php

namespace App\Http\Controllers\API;

use App\Models\DraftOrder\DraftOrderModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\M_ProductPenanggung;
use App\Models\M_Product;
use App\Models\M_ProductPenanggungRate;
use App\Models\M_ProductPenanggungRateByRegion;
use App\Models\M_SubProduct;
use App\Models\RateBySubProductModel;

class ProductPenanggungController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getListProduct(Request $request)
    {
        $url_main_product       = $request->url_main_product;
        $url_sub_product        = $request->url_sub_product;
        $total_pertanggungan    = (int) $request->total_pertanggungan;
        $kategori_wilayah       = $request->kategori_wilayah;

        DB::beginTransaction();
        try{
            $get_product_id         = M_Product::where('url',$url_main_product)->first();

            if($url_sub_product){
                $get_sub_product_id     = M_SubProduct::where('url',$url_sub_product)->first();
                $sub_product_id         = $get_sub_product_id->id;
            }
            $query = M_ProductPenanggung::with(['get_penanggung'])->where('m_product_id',$get_product_id->id);

            if($url_sub_product){
                $query = $query->where('m_sub_product_id',$sub_product_id);
            }
            $get_prodcut_penanggung = $query->select('url','code','m_penanggung_id','id',
                                                        'name as product_name','description','benefit','value_periode_premi','periode_premi')->get();
            $result_data = [];
            foreach($get_prodcut_penanggung as $row => $val)
            {

                $result['product_picture']      = $val->get_penanggung->picture;
                $result['product_name']         = $val->product_name;
                $result['description']          = $val->description;
                $result['benefit']              = $val->benefit;
                $result['value_periode_premi']  = $val->value_periode_premi;
                $result['periode_premi']        = $val->periode_premi;
                $result['url']                  = $val->url;
                $get_rate                       = $this->checkPrice($val->id,$total_pertanggungan,$kategori_wilayah);

                if($get_rate){
                    $result['rate']                 = $get_rate['fix_rate'];
                }else{
                    $result['rate']                 = 0;
                }
                $result_data[] = $result;
            }

            return response()->json([
                'message'       => 'success',
                'data'          => $result_data,
                'status'        => "success",
            ],200);
        }catch (\Exception $e) {
            return response()->json([
                'message'       => $e->getMessage(),
                'line'          => $e->getLine(),
                'status'        => "error",
            ],500);
        }
    }

    function checkPrice($product_penanggung_id=null,$price=null,$kategori_wilayah=null)
    {
        $get_range_price = M_ProductPenanggungRate::whereRaw('maximum_price >= (?)', $price)
                                        ->where('m_product_penanggung_id',$product_penanggung_id)->get();
        $data = [];
        if($get_range_price){
            foreach($get_range_price as $row)
            {

                if($row->minimum_price < $price)
                {
                    // $data['rate_id']    = 'a';
                    $get_rate           = M_ProductPenanggungRateByRegion::where('m_product_penanggung_rate_id',$row->id)
                                                        ->where('wilayah',$kategori_wilayah)->select('fix_rate')->first();
                    $data['rate_id']    = $get_rate->id;
                    $data['max_price']  = $row->maximum_price;
                    $data['min_price']  = $row->minimum_price;
                    $data['fix_rate']   = $get_rate->fix_rate;
                    break;

                }


            }
        }else{
            $get_range_price    = M_ProductPenanggungRate::where('is_high_price', 1)
                                    ->where('m_product_penanggung_id',$product_penanggung_id)->first();

            $get_rate           = M_ProductPenanggungRateByRegion::where('m_product_penanggung_rate_id',$product_penanggung_id)
                                    ->where('wilayah',$kategori_wilayah)->select('fix_rate')->first();
            $data['rate_id']    = $get_rate->id;
            $data['max_price']  = $get_range_price->maximum_price;
            $data['min_price']  = $get_range_price->minimum_price;
            $data['fix_rate']   = $get_rate->fix_rate;
        }
        // $result[]  = $get_range_price;
        return $data;
    }

    public function getDetailProduct(Request $request)
    {
        $url                    = $request->url_product;
        $cob                    = $request->cob;
        $sub_cob                = $request->sub_cob;
        $get_prodcut_penanggung = M_ProductPenanggung::with(['get_penanggung','get_product'])->where('url',$url)
                                    ->select('id','m_penanggung_id','name','description','code','value_periode_premi','periode_premi','m_product_id','m_product_code',
                                                'benefit','benefit_exception','fitur','claim','is_package','daftar_bengkel','url','tnc','estimation_rate',
                                                'estimation_rate_type_periode','estimation_rate_value_periode','type_estimation_rate');

        if($cob == 'asuransi-kendaraan-bermotor'){
            // $get_prodcut_penanggung = $get_prodcut_penanggung->with(['get_rate','get_rate.get_rate_by_region']);
        }

        $get_prodcut_penanggung = $get_prodcut_penanggung->first();

        if($cob == 'surrety-bond'){
            // $get_rate = RateBySubProductModel::leftJoin('m_sub_product','m_rate_ojk_by_sub_product.m_sub_product_code','=','m_sub_product.code')->where('m_sub_product.url',$sub_cob)->first();
            // if($get_rate){
                $get_prodcut_penanggung->rate                   = $get_prodcut_penanggung->estimation_rate;
                $get_prodcut_penanggung->value_periode_premi    = $get_prodcut_penanggung->estimation_rate_value_periode;
                $get_prodcut_penanggung->periode_premi          = $get_prodcut_penanggung->estimation_rate_type_periode;
                $get_prodcut_penanggung->rate_type              = $get_prodcut_penanggung->type_estimation_rate ;
            // }
        }
        return response()->json([
            'message'       => 'success',
            'data'          => $get_prodcut_penanggung,
            'status'        => "success",
        ],200);
    }

    public function getPerluasanProduct(Request $request)
    {
        $url    = $request->url_product;
        $get_product_penanggung = M_ProductPenanggung::with(['get_perluasan_penanggung','get_perluasan_penanggung.get_perluasan_rate','get_perluasan_penanggung.get_perluasan'])
                                    ->where('url',$url)->select('id','is_package')->first();
        return response()->json([
            'message'       => 'success',
            'data'          => $get_product_penanggung,
            'status'        => "success",
        ],200);
    }


    public function showListProduct(Request $request)
    {
        $draft_code = $request->draftcode;
        $cob        = $request->cob;
        $sub_cob    = $request->sub_cob;


        $get_draft  = DraftOrderModel::where('code',$draft_code)->select('code','user_company_code','m_product_code','m_sub_product_code');



        if($cob == 'surrety-bond')
        {
            $get_draft  = $get_draft->with(['get_project']);
        }

        $get_draft = $get_draft->first();




        if($cob == 'surrety-bond')
        {
            $get_rate   = RateBySubProductModel::where('m_sub_product_code',$get_draft->m_sub_product_code)->first();

        }

        $get_product = M_ProductPenanggung::with(['get_penanggung'])
                        ->where('m_product_code',$get_draft->m_product_code)
                        ->where('m_sub_product_code',$get_draft->m_sub_product_code)
                        ->select(
                                    'url','code','m_penanggung_id','id','name as product_name','description','benefit','estimation_rate',
                                    'estimation_rate_type_periode','estimation_rate_value_periode','type_estimation_rate'
                                );

        if($get_draft->m_product_code == 'KBM')
        {
            $get_product = $get_product->select('value_periode_premi','periode_premi');
        }


        $get_product = $get_product->get();

        $result_data = [];
        foreach($get_product as $row => $val)
        {
            $result['product_picture']      = $val->get_penanggung->picture;
            $result['product_name']         = $val->product_name;
            $result['description']          = $val->description;
            $result['benefit']              = $val->benefit;
            $result['url']                  = $val->url;

            $result['rate_type']            = $val->type_estimation_rate;
            $result['rate']                 = $val->estimation_rate;
            $result['value_periode_premi']  = $val->estimation_rate_type_periode;
            $result['periode_premi']        = $val->estimation_rate_value_periode;

            $result_data[] = $result;
        }
        return response()->json([
            'message'       => 'success',
            'data'          => $result_data,
            'draft'         => $get_draft,
            'status'        => "success",
        ],200);
    }
}
