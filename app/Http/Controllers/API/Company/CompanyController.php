<?php

namespace App\Http\Controllers\API\Company;

use App\Models\Company\UserCompanyDocumentModel;
use App\Models\Company\UserCompanyModel;
use App\Models\M_DocPengajuan;
use App\Models\M_Product;
use App\Models\M_SubProduct;
use App\Models\Order\M_Order;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use File;
use Illuminate\Support\Facades\Storage;
use Validator;

class CompanyController extends BaseController
{

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function saveCompany(Request $request)
    {

    }

    public function getDocumentCompany(Request $request)
    {
        $url_cob        = $request->url_cob;
        $url_sub_cob    = $request->url_sub_cob;

        $get_cob_id     = M_Product::where('url',$url_cob)->first();
        if(!$get_cob_id)
        {
            return response()->json([
                'message'   => 'COB Not Found',
                'status'    => 'error',
                'code'      => 404,
                'data'      => null
            ], 200);
        }
        if($url_sub_cob)
        {
            $get_sub_cob_id = M_SubProduct::where('url',$url_sub_cob)->first();
            if(!$get_sub_cob_id){
                return response()->json([
                    'message'   => 'SUB COB Not Found',
                    'status'    => 'error',
                    'code'      => 404,
                    'data'      => null
                ], 200);
            }
        }

        $get_data   =   M_DocPengajuan::with(
                            [
                                'get_product'
                                ,
                                'get_sub_product'
                            ]
                        )->where('m_product_id',$get_cob_id->id)->where('is_company_or_project',1)->get();


    }

    public function getDataCompany(Request $request)
    {
        $user_id            =   $request->user()->token()->user_id;
        $get_company        =   UserCompanyModel::with([
                                                        'get_pic','get_komisaris_dan_direksi','get_province','get_city','get_company_type'
                                                    ]
                                )->where('user_id',$user_id)
                                ->select(
                                            "code","name","type","jenis_usaha","bidang_usaha","npwp","email","no_telepon",
                                            "m_province_id","m_city_id","postcode","address"
                                        )
                                ->first();
        if(!$get_company){
            return response()->json([
                'message'   => 'Company not found',
                'status'    => 'error',
                'data'      => null,
                'code'      => 404
            ],200);
        }
        $cob                = $request->cob;

        if($cob)
        {
            $get_document        = getDocPengajuan($cob,null,1);

            $document_perusahaan = [];
            if($get_document)
            {
                foreach($get_document as $key => $val)
                {
                    $cek_document_company = UserCompanyDocumentModel::where('m_document_upload_code',$val['doc_code'])->where('user_company_code',$get_company->code)->first();

                    if($cek_document_company){
                        $file_name = $cek_document_company->file_name;
                        $note_doc   = $cek_document_company->note;
                    }else{
                        $file_name = null;
                        $note_doc  = null;
                    }
                    $document_perusahaan[] = [
                        'doc_id'                            => $val['doc_id'],
                        'doc_code'                          => $val['doc_code'],
                        'doc_name'                          => $val['doc_name'],
                        'is_company_or_project_document'    => $val['is_company_or_project_document'],
                        'is_for_new_object'                 => $val['is_for_new_object'],
                        'file_name'                         => $file_name,
                        'note'                              => $note_doc,
                        'contoh_document'                   => $val['contoh_document']
                    ];
                }
            }
            $get_company->document = $document_perusahaan;
        }
        return response()->json([
            'message'   => 'Success Get Data',
            'status'    => 'success',
            'data'      => $get_company,
            'code'      => 200
        ],200);
    }
}
