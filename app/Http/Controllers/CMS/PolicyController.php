<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\M_Banner as M_Banner;
use App\Models\M_Policy;
use App\Models\M_Product;
use App\Models\M_SubProduct;
use Illuminate\Support\Facades\Storage;

class PolicyController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        // $data['get_product'] = M_Product::get();
        return view('CMS.Policy.v_policy');
    }

    public function getList()
    {
        $array_data = M_Policy::get();
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function Add()
    {
        $get_product = M_Product::get();
        $data['product'] = $get_product;
        return view('CMS.Policy.v_add_policy',$data);
    }

    public function save(Request $request)
    {
        $name       = $request->policy_name;
        $detail     = $request->detail;
        $product    = $request->product;
        DB::beginTransaction();
        try{
            $data_save = [
                'name'          => $name,
                'description'   => $detail,
                'created_by'    => $request->id_user_login,
                'created_at'    => date('Y-m-d H:i:s'),
                'product'       => $product
            ];

            $save = M_Policy::insert($data_save);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $id = decrypt($id);

        $get_data = M_Policy::where('id',$id)->first();

        if($get_data)
        {

            $get_product        = M_Product::get();
            $data['product']    = $get_product;
            $data['get_data']   = $get_data;
            return view('CMS.Policy.v_edit_policy',$data);
        }
        else
        {
            abort(404);
        }
    }

    public function update(Request $request)
    {
        $name       = $request->policy_name;
        $detail     = $request->detail;
        $product    = $request->product;
        $id         = $request->id;
        DB::beginTransaction();
        try{
            $data_save = [
                'name'          => $name,
                'description'   => $detail,
                'updated_by'    => $request->id_user_login,
                'updated_at'    => date('Y-m-d H:i:s'),
                'product'       => $product
            ];

            $save = M_Policy::where('id',$id)->update($data_save);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
