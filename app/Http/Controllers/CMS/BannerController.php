<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\M_Banner as M_Banner;
use App\Models\M_Product;
use Illuminate\Support\Facades\Storage;

class BannerController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $data['get_product'] = M_Product::get();
        return view('CMS.Banner.v_banner_list',$data);
    }

    public function getList(Request $request)
    {
        $array_data = M_Banner::orderBy('position','ASC')->get();

        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('position', function($row){
                if($row->position == 999){
                    $position_name = 'Home';
                }else{
                    $getProduct = M_Product::where('id',$row->position)->first();
                    $position_name = $getProduct->name;
                }
                return $position_name;
            })
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function save(Request $request)
    {
        $main_product   = $request->main_product;
        $title          = $request->title;
        $description    = $request->description;
        $id_user_login  = $request->id_user_login;

        if($request->picture)
        {
            $picture = $request->picture;
            $imageName = generate_url($title).'.'.$request->picture->extension();
            $imageName = strtolower($imageName);
        }else{
            $imageName = null;
        }
        DB::beginTransaction();
        try{
            $save_banner = M_Banner::create([
                'title'             => $title,
                'position'          => $main_product,
                'description'       => $description,
                'picture'           => $imageName,
                'created_by'        => $id_user_login
            ]);
            DB::commit();
            if($picture){
                Storage::putFileAs('public/banner', $picture, $imageName);
            }
            return response()->json([
                'message'           => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id         = decrypt($request->id);
        DB::beginTransaction();
        try{
            $array_data = M_Banner::where('id',$id)->first();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $array_data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {
        $id             = decrypt($request->id);
        $main_product   = $request->main_product;
        $title          = $request->title;
        $description    = $request->description;
        $id_user_login  = $request->id_user_login;

        $get_data           = M_Banner::where('id',$id)->first();
        $old_name_picture   = $get_data->picture;
        if($request->picture)
        {
            $picture = $request->picture;
            $imageName = generate_url($title).'.'.$request->picture->extension();
            $imageName = strtolower($imageName);
        }else{
            $imageName = $old_name_picture;
        }

        DB::beginTransaction();
        try{
            $save_product = M_Banner::where('id',$id)->update([
                'title'             => $title,
                'position'          => $main_product,
                'description'       => $description,
                'picture'           => $imageName,
                'updated_by'        => $request->id_user_login
            ]);
            DB::commit();
            if($request->picture)
            {
                Storage::delete('public/banner'.$old_name_picture);
                Storage::putFileAs('public/banner', $picture, $imageName);
            }
            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $m_banner = M_Banner::find($id);
            $m_banner->deleted_by = $request->id_user_login;
            $m_banner->save();
            $m_banner->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
