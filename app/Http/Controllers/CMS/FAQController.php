<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\M_Faq as M_Faq;

class FAQController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('CMS.FAQ.v_faq');
    }

    public function getList(Request $request)
    {
        $array_data = M_Faq::with(['GetProduct'])
                        ->get()
                        ->sortBy(function($product) {
                            return $product->GetProduct->name;
                        });
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('main_product_name', function($row){

                return $row->GetProduct->name;
            })
            ->addColumn('answerArr', function($row){

                $ans =  json_decode($row->answer);

                $val_ans = '';
                if($ans)
                {
                    foreach($ans as $key => $val){
                        $val_ans .= $val.'\r\n';
                    }
                }
                return $val_ans;

            })
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function add(Request $request)
    {
        return view('CMS.FAQ.v_add_faq');
    }

    public function save(Request $request)
    {
        $main_product       = $request->main_product;
        $question           = $request->question;
        $answer             = $request->answer;
        $data_save          = [];
        DB::beginTransaction();
        try{
            if($question){
                foreach($question as $key => $val){

                    $explode_answer = explode(PHP_EOL, $answer[$key]);
                    // dd($explode_answer);
                    $data_save[] = [
                        'm_product_id'  => $main_product,
                        'question'      => $val,
                        'answer'        => json_encode($explode_answer),
                        'created_by'    => $request->id_user_login,
                        'created_at'    => date('Y-m-d H:i:s')
                    ];
                }
            }
            $save_product = M_Faq::insert($data_save);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $get_data = M_Faq::with(['GetProduct'])
                            ->where('id',$id)
                            ->first();

            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $get_data
            ],200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {
        $main_product   = $request->main_product;
        $question       = $request->question;
        $answer         = $request->answer;
        $id             = decrypt($request->id);
        DB::beginTransaction();
        try{
            $explode_answer = explode(PHP_EOL, $answer);
            $save_product = M_Faq::where('id',$id)->update([
                'question'      => $question,
                'm_product_id'  => $main_product,
                'answer'        => json_encode($explode_answer),
                'updated_by'    => $request->id_user_login,
            ]);
            DB::commit();

            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $m_faq = M_Faq::find($id);
            $m_faq->deleted_by = $request->id_user_login;
            $m_faq->save();
            $m_faq->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
