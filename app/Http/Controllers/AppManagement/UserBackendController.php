<?php

namespace App\Http\Controllers\AppManagement;

use App\Models\M_UserBackend;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class UserBackendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('AppManagement.UserBackend.v_user');
    }

    public function getList(Request $request)
    {
        $array_data = M_UserBackend::get();
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function save(Request $request)
    {
        $name       = $request->name;
        $nik        = $request->nik;
        $email      = $request->email;
        $phone      = $request->phone;
        $password   = $request->password;
        $username   = $request->username;
        $checked    = M_UserBackend::where('username',$username);
        if($email){
            $checked = $checked->orWhere('email',$email);
        }
        
        if($nik){
            $checked = $checked->orWhere('nik',$nik);
        }
        
        $checked = $checked->first();

        if($checked){
            return response()->json([
                'message'  => 'User Already Exist',
                'status'   => 'error',
            ],400);
        }
        DB::beginTransaction();
        try{
            $save_user = M_UserBackend::create([
                'username' => $username,
                'password' => Hash::make($password),
                'name'     => $name,
                'email'    => $email,
                'phone'    => $phone,
                'nik'      => $nik,

            ]);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function GetData(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $get_data = M_UserBackend::where('id',$id)
                        ->select('username','name','email','phone','nik','password')
                        ->first();

            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $get_data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {
        $id         = decrypt($request->id);
        $name       = $request->name;
        $nik        = $request->nik;
        $email      = $request->email;
        $phone      = $request->phone;
        $password   = $request->password;
        $username   = $request->username;
        $checked    = M_UserBackend::where('username',$username)->orWhere('email',$email)->orWhere('nik',$nik)->first();

        if($checked){
            if($checked->id != $id){
                return response()->json([
                    'message'  => 'User Already Exist',
                    'status'   => 'error',
                ],400);
            }
        }
        DB::beginTransaction();
        try{
            $save_user = M_UserBackend::find($id);
            $save_user->name = $name;
            $save_user->username = $username;
            $save_user->email = $email;
            $save_user->phone = $phone;
            $save_user->nik = $nik;
            if($password){
                $save_user->password = Hash::make($password);
            }
            $save_user->save();
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $user = M_UserBackend::find($id);
            $user->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
