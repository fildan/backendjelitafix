<?php

namespace App\Http\Controllers\Order;

use App\Models\NewOrder\NewOrderModel;
use App\Models\NewOrder\NewOrderSBModel;
use App\Models\Payment\M_HistoryPayment;
use App\Models\Payment\M_Invoice;
use App\Models\Payment\M_Payment;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Ramsey\Uuid\Uuid;
use DNS2D;

class OrderController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function NewOrderSurrety()
    {
        return view('Order.sb.v_list_order');
    }

    public function getListOrderSurrety()
    {
        $get_data = NewOrderModel::with(['get_order_sb','get_order_sb.get_project','get_order_sb.get_obligee','get_order_sb.get_obligee.get_company_type','get_order_company','get_order_company.get_company_type'])->orderBy('created_at','DESC')->get();
        // dd($get_data);
        return DataTables::of($get_data)
            ->addIndexColumn()
            ->addColumn('tertanggung', function($row){
                return $row->get_order_company->name.', ('.$row->get_order_company->get_company_type->name.')';
            })
            ->addColumn('nilai_kontrak', function($row){
                return number_format($row->get_order_sb->get_project->contract_value,2,",",".");
            })
            ->addColumn('nilai_jaminan', function($row){
                return number_format($row->get_order_sb->get_project->collateral_value,2,",",".");
            })
            ->addColumn('tgl_project', function($row){
                return date('Y-m-d',strtotime($row->get_order_sb->get_project->date));
            })
            ->addColumn('periode_polis', function($row){
                return date('Y-m-d',strtotime($row->get_order_sb->get_project->start_date_periode)).' s/d '.date('Y-m-d',strtotime($row->get_order_sb->get_project->end_date_periode));
            })
            ->addColumn('obligee', function($row){
                return $row->get_order_sb->get_obligee->company_name.', ('.$row->get_order_sb->get_obligee->get_company_type->name.')';
            })
            ->addColumn('status', function($row){
                $status = $row->status;

                return $status;
            })
            ->addColumn('action', function($row){
                $btn_html    =  '<div class="btn-group fz12">';
                $btn_html   .=      '<button type="button" class="btn btn-sm btn-info fz12">Action</button>';
                $btn_html   .=      '<button type="button" class="dropdown-toggle dropdown-icon btn btn-sm btn-info fz12" data-toggle="dropdown">';
                $btn_html   .=          '<span class="sr-only">Toggle Dropdown</span>';
                $btn_html   .=      '</button>';
                $btn_html   .=      '<div class="dropdown-menu" role="menu">';

                if($row->status == 1){
                    $name_btn = 'Proses';
                }else{
                    $name_btn = 'View';
                }
                $btn_html   .=          '<a class="dropdown-item btn btn-sm btn-info fz12" href="'.url('order/sb/view').'/'.$row->code.'">'.$name_btn.'</a>';
                // }


                if($row->status == 2 || $row->status == 3 || $row->status == 10){
                    $btn_html   .=          '<a class="dropdown-item" href="'.url('order/sb/view-update-progress'.'/'.$row->code).'">Update Progress</a>';
                }

                if($row->status == 8){
                     if($row->m_product_code != 'SB'){
                        $btn_html   .=          '<a class="dropdown-item" href="#">Upload Polis</a>';
                    }
                }

                // if($row->status == 6 || $row->status == 7 || $row->status == 8 || $row->status == 11 || $row->status == 12){
                //     $btn_html   .=          '<a class="dropdown-item btn btn-sm btn-info fz12"  href="'.url('order/verifikasi-pembayaran'.'/'.$row->code).'">Lihat Pembayaran</a>';
                // }

                if($row->status == 5)
                {
                    if($row->status_payment != 3){
                        $btn_html   .=          '<a class="dropdown-item btn btn-sm btn-info fz12 payment-verification"  code="'.$row->code.'" href="'.url('order/sb/verifikasi-pembayaran'.'/'.$row->code).'">Verifikasi Pembayaran</a>';
                    }
                }
                $btn_html   .=      '</div>';
                $btn_html   .=  '</div>';
                return $btn_html;
            })

            ->make(true);
    }

    public function viewOrderSurrety(Request $request)
    {
        $code = $request->code;

        $cek_order      = NewOrderModel::where('code',$code)->first();
        if(!$cek_order)
        {
            return redirect()->route('order.sb.new')->with('error','Data Not Found !');
        }
        $get_data_order = NewOrderModel::with([
                    'get_document','get_list_asuransi',
                    'get_product','get_sub_product'
            ]);
        if($cek_order->m_product_code == 'SB')
        {
                    $get_data_order = $get_data_order->with(
                            'get_order_sb','get_order_sb.get_project','get_order_sb.get_obligee','get_order_company','get_order_company.get_company_type',
                            'get_order_company.get_direksi_komisaris','get_order_company.get_pic','get_order_company.get_province',
                            'get_order_company.get_city','get_order_sb.get_obligee.get_province','get_order_sb.get_obligee.get_city','get_order_sb.get_obligee.get_company_type'
                    );
        }

        if($cek_order->type_penanggung == 1){
            $get_data_order = $get_data_order->with('get_penanggung','get_penanggung.get_penanggung');
        }
        $get_data_order  = $get_data_order->where('code',$cek_order->code)
            ->select(
                    "code","m_product_code","m_sub_product_code","total_premi","total_payment",
                    "remaining_payment","already_paid","type_payment","type_penanggung","m_product_penanggung_code",
                    "user_id","status","progress","date_active_polis","protection_time","protection_time_type",
                    "start_periode_protection","end_periode_protection","status_payment"
            )->first();

        if($get_data_order->protection_time_type == 1){
            $type = 'Hari';
        }else if($get_data_order->protection_time_type == 2){
            $type = 'Minggu';
        }else if($get_data_order->protection_time_type == 3){
            $type = 'Bulan';
        }else if($get_data_order->protection_time_type == 4){
            $type = 'Tahun';
        }
        $get_data_order->time_protection_type   = $type;
        $get_data_order->start_periode_format   = date('d F Y',strtotime($get_data_order->start_periode_protection));
        $get_data_order->end_periode_format     = date('d F Y',strtotime($get_data_order->end_periode_protection));
        // DB::commit();
        // $order_number  =
        // dd($get_data_order);
        $data['get_data'] = $get_data_order;
        return view('Order.sb.v_detail_order',$data);
    }

    public function printSPPASB(Request $request)
    {


        $code = $request->code;

        $cek_order      = NewOrderModel::where('code',$code)->first();
        if(!$cek_order)
        {
            return redirect()->route('order.sb.new')->with('error','Data Not Found !');
        }
        $get_data_order = NewOrderModel::with([
                    'get_document','get_list_asuransi',
                    'get_product','get_sub_product'
            ]);
        if($cek_order->m_product_code == 'SB')
        {
                    $get_data_order = $get_data_order->with(
                            'get_order_sb','get_order_sb.get_project','get_order_sb.get_obligee','get_order_company','get_order_company.get_company_type',
                            'get_order_company.get_direksi_komisaris','get_order_company.get_pic','get_order_company.get_province',
                            'get_order_company.get_city','get_order_sb.get_obligee.get_province','get_order_sb.get_obligee.get_city','get_order_sb.get_obligee.get_company_type'
                    );
        }

        if($cek_order->type_penanggung == 1){
            $get_data_order = $get_data_order->with('get_penanggung','get_penanggung.get_penanggung');
        }
        $get_data_order  = $get_data_order->where('code',$cek_order->code)
            ->select(
                    "code","m_product_code","m_sub_product_code","total_premi","total_payment",
                    "remaining_payment","already_paid","type_payment","type_penanggung","m_product_penanggung_code",
                    "user_id","status","progress","date_active_polis","protection_time","protection_time_type",
                    "start_periode_protection","end_periode_protection"
            )->first();

        if($get_data_order->protection_time_type == 1){
            $type = 'Hari';
        }else if($get_data_order->protection_time_type == 2){
            $type = 'Minggu';
        }else if($get_data_order->protection_time_type == 3){
            $type = 'Bulan';
        }else if($get_data_order->protection_time_type == 4){
            $type = 'Tahun';
        }
        $get_data_order->time_protection_type   = $type;
        $get_data_order->start_periode_format   = date('d F Y',strtotime($get_data_order->start_periode_protection));
        $get_data_order->end_periode_format     = date('d F Y',strtotime($get_data_order->end_periode_protection));
        Storage::put('public/order/sb/order-code-'.$get_data_order->code.'/'.'barcode-order-code-'.$get_data_order->code.'.png', base64_decode(DNS2D::getBarcodePNG($get_data_order->code, "QRCODE")));
        if($request->type == 'SPPA'){
            if($cek_order->m_product_code == 'SB')
            {
                return $this->SubmitPrintSPPASB($get_data_order);
            }
        }else if($request->type == 'Placing'){
            if($cek_order->m_product_code == 'SB')
            {
                return $this->submitPrintPlacingSB($get_data_order);
            }
        }else if($request->type == 'Document')
        {
            $data['get_data']           = $get_data_order;
            return view('Order.print.printDocument',$data);
        }

    }


    function SubmitPrintSPPASB($get_data_order){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();
        $header = $section->addHeader();


        $header->addImage(file_get_contents('../public/images/logo-sju.png'),
            array(
                'width'         => 80,
                'height'        => 68,
                'align'         => 'right'
            )
        );
        $header->addTextBreak(1);
        $section->addText('Surat Permohonan Penerbitan Jaminan',array('bold'=>true),array('textAlignment' => 'center'));
        $section->addTextBreak();
        $table = $section->addTable();
        $table->addRow();
        $table->addCell(5000)->addText('Surrety Bond yang diminta oleh Principal');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText($get_data_order->get_sub_product->name);

        $table->addRow();
        $table->addCell(5000)->addText('Nilai Jaminan');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText('Rp. '.number_format($get_data_order->get_order_sb->get_project->collateral_value,0,",","."));


        $table->addRow();
        $table->addCell(5000)->addText('Nama Principal / Kontraktor');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars(ucwords($get_data_order->get_order_company->name.', ( '.$get_data_order->get_order_company->get_company_type->name. ')')));


        $table->addRow();
        $table->addCell(5000)->addText('Alamat Principal / Kontraktor');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars($get_data_order->get_order_company->address.', '.$get_data_order->get_order_company->get_province->name.', '.
                                        $get_data_order->get_order_company->get_city->name.', '.$get_data_order->get_order_company->postcode));


        $table->addRow();
        $table->addCell(5000)->addText('Nama Obligee');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars(ucwords($get_data_order->get_order_sb->get_obligee->company_name.', ('.$get_data_order->get_order_sb->get_obligee->get_company_type->name.')')));

        $table->addRow();
        $table->addCell(5000)->addText('Alamat Obligee');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars($get_data_order->get_order_sb->get_obligee->address.', '.$get_data_order->get_order_sb->get_obligee->get_province->name.', '.
                                        $get_data_order->get_order_sb->get_obligee->get_city->name.', '.$get_data_order->get_order_sb->get_obligee->postcode));

        $table->addRow();
        $table->addCell(5000)->addText('Jenis Pekerjaan');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars($get_data_order->get_order_sb->get_project->name));

        $table->addRow();
        $table->addCell(5000)->addText('Nilai Kontrak');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText('Rp. '.number_format($get_data_order->get_order_sb->get_project->contract_value,0,",","."));


        $table->addRow();
        $table->addCell(5000)->addText('Jangka Waktu Jaminan');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(date('d F Y',strtotime($get_data_order->get_order_sb->get_project->start_date_periode)).' s.d '.date('d F Y',strtotime($get_data_order->get_order_sb->get_project->end_date_periode)));

        $table->addRow();
        $table->addCell(5000)->addText('Diterbitkan Jaminan / Bond');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText('');

        $table->addRow();
        $table->addCell(5000)->addText('Data pendukung yang diserahkan ');
        $table->addCell(500)->addText(':');
        $c1 = $table->addCell(5000);
        $all_document       = $get_data_order->get_document;
        $no = 0;
        foreach($all_document as $row => $val){
            $no++;
            $set_text_docoument = $no.'. '.($val->number_document?$val->number_document.' - ':'').$val->name;
            $c1->addText(htmlspecialchars($set_text_docoument));
        }

        $table->addRow();
        $table->addCell(5000)->addText('Asli permohonan ini disampaikan');
        $table->addCell(500)->addText(':');
        $c2 = $table->addCell(5000);
        $c2->addText('Kepada Sarana Janesia Utama (SJU)');
        $c2->addText('Tanggal '.date('d F Y'));
        $section->addTextBreak(5);
        $section->addImage(file_get_contents('../public/storage/order/sb/order-code-'.$get_data_order->code.'/'.'barcode-order-code-'.$get_data_order->code.'.png'),
            array(

            )
        );
        $filename = "SPPA Order No ".$get_data_order->code.".docx";
        header( "Content-Type: application/vnd.openxmlformats-officedocument.wordprocessing‌​ml.document" );// you should look for the real header that you need if it's not Word 2007!!!
        header( 'Content-Disposition: attachment; filename='.$filename );

        $h2d_file_uri = tempnam( "", "htd" );
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save( "php://output" );
    }

    function submitPrintPlacingSB($get_data_order)
    {

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();
        $header = $section->addHeader();


        $header->addImage(file_get_contents('../public/images/logo-sju.png'),
            array(
                'width'         => 80,
                'height'        => 68,
                'align'         => 'right'
            )
        );
        $header->addTextBreak(1);
        $section->addText('Placing Slip - Surrety Bond',array('bold'=>true),array('textAlignment' => 'center'));
        $section->addTextBreak();
        $table = $section->addTable();
        $table->addRow();
        $table->addCell(5000)->addText('Type Of Insurance');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText($get_data_order->get_sub_product->name);

        $table->addRow();
        $table->addCell(5000)->addText('Name Of Principle');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars(ucwords($get_data_order->get_order_company->name.', ( '.$get_data_order->get_order_company->get_company_type->name. ')')));

        $table->addRow();
        $table->addCell(5000)->addText('Address');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars($get_data_order->get_order_company->address.', '.$get_data_order->get_order_company->get_province->name.', '.
                                        $get_data_order->get_order_company->get_city->name.', '.$get_data_order->get_order_company->postcode));

        $table->addRow();
        $table->addCell(5000)->addText('Name Of Obligee');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars(ucwords($get_data_order->get_order_sb->get_obligee->company_name.', ('.$get_data_order->get_order_sb->get_obligee->get_company_type->name.')')));

        $table->addRow();
        $table->addCell(5000)->addText('Address');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars($get_data_order->get_order_sb->get_obligee->address.', '.$get_data_order->get_order_sb->get_obligee->get_province->name.', '.
                                        $get_data_order->get_order_sb->get_obligee->get_city->name.', '.$get_data_order->get_order_sb->get_obligee->postcode));

        $table->addRow();
        $table->addCell(5000)->addText('Kind Of Work');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(htmlspecialchars($get_data_order->get_order_sb->get_project->name));

        $table->addRow();
        $table->addCell(5000)->addText('Base On');
        $table->addCell(500)->addText(':');
        $c1 = $table->addCell(5000);
        $all_document       = $get_data_order->get_document;
        $no = 0;
        foreach($all_document as $row => $val){
            $no++;
            $set_text_docoument = $val->name.' No. '.($val->number_document?$val->number_document:'');
            $c1->addText(htmlspecialchars($set_text_docoument));
            if($val->date_document){
                $c1->addText('Tanggal : '.date('d-m-Y',strtotime($val->date_document)));
            }else{
                $c1->addText('Tanggal : - ');
            }
        }

        $table->addRow();
        $table->addCell(5000)->addText('Contract Value');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText('Rp. '.number_format($get_data_order->get_order_sb->get_project->contract_value,0,",","."));

        $table->addRow();
        $table->addCell(5000)->addText('Amount Of Bond');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText('Rp. '.number_format($get_data_order->get_order_sb->get_project->collateral_value,0,",","."));

        $table->addRow();
        $table->addCell(5000)->addText('Periode');
        $table->addCell(500)->addText(':');
        $table->addCell(5000)->addText(date('d F Y',strtotime($get_data_order->get_order_sb->get_project->start_date_periode)).' s.d '.date('d F Y',strtotime($get_data_order->get_order_sb->get_project->end_date_periode)));

        $table->addRow();
        $table->addCell(5000)->addText('Rate');
        $table->addCell(500)->addText(':');
        if($get_data_order->type_penanggung == 1){
            if($get_data_order->get_order_sb->fix_rate){
                $rate                   = $get_data_order->get_order_sb->fix_rate;
                $rate_type              = $get_data_order->get_order_sb->fix_rate_periode_type;
                $rate_periode_type      = $get_data_order->get_order_sb->fix_rate_periode_value;
                $rate_periode_value     = $get_data_order->get_order_sb->fix_rate_type;
                $premi_per_periode      = $get_data_order->get_order_sb->fix_premi_per_periode;
            }else{
                $rate                   = $get_data_order->get_penanggung->estimation_rate;
                $rate_type              = $get_data_order->get_penanggung->type_estimation_rate;
                $rate_periode_type      = $get_data_order->get_penanggung->estimation_rate_type_periode;
                $rate_periode_value     = $get_data_order->get_penanggung->estimation_rate_value_periode;
                $premi_per_periode      = $get_data_order->get_penanggung->estimation_premi_per_periode;

            }

            if($rate_type == 1){
                $rate_cetak = $rate.'%'.' / '.'Terbit Polis';
            }else{
                $rate_cetak = $rate.'%'.' / '.$rate_periode_value.' '.$this->convertPeriodeType($rate_periode_type);
            }
            $table->addCell(5000)->addText($rate_cetak);
        }else{
            $table->addCell(5000)->addText('');
        }


        $table->addRow();
        $table->addCell(5000)->addText('Brokerage (Excl VAT)');
        $table->addCell(500)->addText(':');
        if($get_data_order->type_penanggung == 1){
            if($get_data_order->brokerage){
                $brokerage              = $get_data_order->brokerage;
            }else{
                $brokerage              = $get_data_order->get_penanggung->estimation_brokerage;

            }
            $table->addCell(5000)->addText($brokerage);
        }else{
            $table->addCell(5000)->addText('');
        }


        $table->addRow();
        $table->addCell(5000)->addText('Remarks');
        $table->addCell(500)->addText(':');
        $c2 = $table->addCell(5000);
        $c2->addText('- PPW 30 hari kalender sejak tanggal polis diterima  SJU / Tertanggung  dan Grace Period Pembayaran premi dari   SJU 7 hari kalender sejak berakhirnya PPW');
        $c2->addText('- Draft polis dapat diterima pada tanggal : ');
        $c2->addText('- Polis dapat diterima pda tanggal  : ');
        $c2->addText('- Polis  : ');
        $section->addTextBreak(5);
        $section->addImage(file_get_contents('../public/storage/order/sb/order-code-'.$get_data_order->code.'/'.'barcode-order-code-'.$get_data_order->code.'.png'),
            array(

            )
        );

        $filename = "Placing Order No ".$get_data_order->code.".docx";
        header( "Content-Type: application/vnd.openxmlformats-officedocument.wordprocessing‌​ml.document" );// you should look for the real header that you need if it's not Word 2007!!!
        header( 'Content-Disposition: attachment; filename='.$filename );

        $h2d_file_uri = tempnam( "", "htd" );
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save( "php://output" );
    }

    public function viewUpdateProgress(Request $request)
    {

        $code = $request->code;
        $cek = NewOrderModel::where('code',$code)->first();

        $get_data = NewOrderModel::where('code',$code);
        if($cek){
            $get_data = $get_data->with(['get_order_sb','get_order_sb.get_project','get_sub_product','get_order_sb.get_obligee','get_order_sb.get_obligee.get_company_type','get_order_company','get_order_company.get_company_type']);
        }
        $get_data = $get_data->first();
        $data['get_data'] = $get_data;
        return view('Order.sb.v_update_progress_order',$data);
    }
    public function UpdateProgress(Request  $request){
        // dd($request->all());
        $proses_progress    = $request->proses_progress;
        $code               = $request->code;
        $note               = $request->note;
        $id_user_login      = $request->id_user_login;
        $status             = $request->status;

        $cek =NewOrderModel::where('code',$code)->first();

        if(!$cek){
            return response()->json([
                'message'   => 'Data not found',
                'status'    => 'error',
                'data'      => null,
                'code'      => 404
            ],200);
        }

        DB::beginTransaction();
        try{

            if($status == 5){
                $data_update_sb['fix_rate']                 = $request->fix_rate;
                if($request->rate_premi_type == 2){
                    $data_update_sb['fix_rate_periode_type']    = $request->rate_periode_type;
                    $data_update_sb['fix_rate_periode_value']   = $request->rate_periode_value;
                    $data_update_sb['fix_rate_type']            = $request->rate_premi_type;
                    $data_update_sb['fix_premi_per_periode']    = $request->fix_premi_per_periode;
                }
                $data_update_sb['total_fix_premi']          = $request->total_premi;

                $update_sb = NewOrderSBModel::where('new_order_code',$code)->update($data_update_sb);
            }
            $cek_progress = $cek->progress;

            $progres = [];

            if($cek_progress){
                $get_progress = json_decode($cek_progress);
                foreach($get_progress as $row => $val)
                {
                    $progress[] = [
                        'date'      => $val->date,
                        'status'    => $val->status,
                        'note'      => $val->note
                    ];
                }
            }

            $progress[] = [
                'date'          => date('Y-m-d H:i:s'),
                'status'        => $status,
                'note'          => $note
            ];

            $data_update['updated_at']  = date('Y-m-d H:i:s');
            $data_update['updated_by']  = $id_user_login;
            $data_update['progress']    = json_encode($progress);
            $data_update['status']      = $status;

            if($status == 5){

                $data_update['total_premi']         = $request->total_premi;
                $data_update['total_payment']       = $request->total_premi;
                $data_update['remaining_payment']   = $request->total_premi;
                $data_update['already_paid']        = 0;
                $data_update['brokerage_percent']   = $request->brokerage;
                $data_update['brokerage_value']     = $request->brokerage_value;
            }

            $update_order = NewOrderModel::where('code',$code)->update($data_update);
            DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => 'Success save Data',
                'status'    => 'success',
                'data'      => $update_order,
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
            ],500);
        }
    }

    public function VerifikasiPembayaran(Request $request)
    {
        $order_code                 = $request->code;
        $get_order                  =   NewOrderModel::where('code',$order_code)->first();
        $get_data                   =   M_Invoice::with(
                                            [
                                                'get_payment',
                                                'get_payment.get_payment_history'
                                            ]
                                        )->where('new_order_code',$order_code)->get();

        $data['get_data']           = $get_data;
        $data['get_order']          = $get_order;
        return view('Order.VerifikasiPembayaran',$data);
    }
    
    public function VerifikasiPembayaranSB(Request $request)
    {
        $order_code                 = $request->code;
        // $get_order                  =   NewOrderModel::with([])->where('code',$order_code)->first();
        $get_data_order = NewOrderModel::with([
            'get_document','get_list_asuransi',
            'get_product','get_sub_product'
        ]);
        $get_order = $get_data_order->with(
                'get_order_sb','get_order_sb.get_project','get_order_sb.get_obligee','get_order_company','get_order_company.get_company_type',
                'get_order_company.get_direksi_komisaris','get_order_company.get_pic','get_order_company.get_province',
                'get_order_company.get_city','get_order_sb.get_obligee.get_province','get_order_sb.get_obligee.get_city','get_order_sb.get_obligee.get_company_type'
        )->where('code',$order_code)->first();
        // dd($get_order);
        $get_data                   =   M_Invoice::with(
                                            [
                                                'get_payment',
                                                'get_payment.get_payment_history'
                                            ]
                                        )->where('new_order_code',$order_code)->get();

        $data['get_data']           = $get_data;
        $data['get_order']          = $get_order;
        return view('Order.VerifikasiPembayaranSB',$data);
    }

    public function SubmitVerifikasiPembayaranSB(Request $request)
    {
        $jumlah_yg_dibayarkan   = $request->jumlah_yg_dibayarkan;
        $note                   = $request->note;
        $code                   = $request->code;
        $id_user_login          = $request->id_user_login;


        $get_order              = NewOrderModel::where('code',$code)->first();
        if($get_order){
            DB::beginTransaction();
            try{

                if($get_order->progress_payment){
                    $get_progress = json_decode($get_order->progress_payment);
                    $progress = [];
                    foreach($get_progress as $row => $val)
                    {
                        $progress[] = [
                            'date'      => $val->date,
                            'status'    => $val->status,
                            'note'      => $val->note
                        ];
                    }
                }
                $progress[] = array(
                    'date'      => date('Y-m-d H:i:s'),
                    'status'    => 3,
                    'note'      => 'Pembayaran Terverifikasi'
                );
                $update_order = NewOrderModel::where('code',$get_order->code)
                                        ->update(
                                            [
                                                'status_payment'            => 3,
                                                'updated_at'                => date('Y-m-d H:i:s'),
                                                'updated_by'                => $id_user_login,
                                                'progress_payment'          => json_encode($progress),
                                                'remaining_payment'         => $get_order->remaining_payment - $jumlah_yg_dibayarkan,
                                                'already_paid'              => $get_order->already_paid + $jumlah_yg_dibayarkan
                                            ]
                                        );
                DB::commit();
                // $order_number  =
                return response()->json([
                    'message'   => 'Success save Data',
                    'status'    => 'success',
                    'data'      => $update_order,
                    'code'      => 200,
                ], 200);
            }catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'message'       => $e->getMessage().' Line: '.$e->getLine(),
                    'status'        => "error",
                ],500);
            }
        }else{
            return response()->json([
                'message'       => 'Data order tidak ditemukan',
                'status'        => "error",
            ],500);
        }
    }
    public function getListInvoice(Request $request)
    {
        $order_code = $request->order_code;
        $get_data                   =   M_Invoice::with(
                                            [
                                                'get_payment',
                                                'get_payment.get_payment_history',
                                                'get_payment.get_methode_payment',
                                                'get_payment.get_bank'
                                            ]
                                        )->where('new_order_code',$order_code)->get();
        return DataTables::of($get_data)
            ->addIndexColumn()

            ->addColumn('total_amount', function($row){
                return number_format($row->total_amount,2,",",".");
            })

            ->addColumn('date', function($row){
                return date('d F Y',strtotime($row->inv_active));
            })

            ->addColumn('status', function($row){
                $status = $row->status;

                return $status;
            })

            ->make(true);
    }

    public function getHistoryPayment(Request $request){
        $payment_code = $request->payment_code;
        $get_history_payment = M_Payment::with(
                                                [
                                                    'get_payment_history'=> function($query){
                                                        $query->orderBy('created_at','DESC');
                                                    }
                                                ]
                                        )->where('code',$payment_code)->first();
        // $get_history_payment = M_HistoryPayment::where('payment_code',$payment_code)->orderBy('created_at','DESC')->get();
        return response()->json([
            'message'   => 'Success save Data',
            'status'    => 'success',
            'data'      => $get_history_payment,
            'code'      => 200,
        ], 200);
    }
    public function submitVerikasiOrder(Request $request)
    {

        $status                = $request->status;
        $jumlah_yg_dibayarkan  = $request->jumlah_yg_dibayarkan;
        $note                  = $request->note;
        $id_user_login         = $request->id_user_login;
        $payment_code          = $request->payment_code;

        DB::beginTransaction();
        try{
            $get_payment            = M_Payment::where('code',$payment_code)->first();
            $get_invoice            = M_Invoice::where('code',$get_payment->invoice_code)->first();
            $get_order              = NewOrderModel::where('code',$get_invoice->new_order_code)->first();


            $data_update_payment = [
                'status'        => $status,
                'updated_at'    => date('Y-m-d H:i:s'),
                'updated_by'    => $id_user_login
            ];

            $save_payment   = M_Payment::where('code',$payment_code)->update($data_update_payment);

            if($status == 2){
                $status_invoice = 3;
                $status_order   = 8;
            }else if($status == 3){
                $status_invoice = 4;
                $status_order   = 7;
            }else{
                $status_invoice = 6;
                $status_order   = 12;
            }

            $update_invoice = M_Invoice::where('code',$get_payment->invoice_code)
                                    ->update(
                                        [
                                            'status'        => $status_invoice,
                                            'updated_at'    => date('Y-m-d H:i:s'),
                                            'updated_by'    => $id_user_login
                                        ]
                                    );

            $get_progress = json_decode($get_order->progress);
            $progress = [];
            foreach($get_progress as $row => $val)
            {
                $progress[] = [
                    'date'      => $val->date,
                    'status'    => $val->status,
                    'note'      => $val->note
                ];
            }
            $progress[] = array(
                'date'      => date('Y-m-d H:i:s'),
                'status'    => $status_order,
                'note'      => $note
            );

            $update_order = NewOrderModel::where('code',$get_order->code)
                                ->update(
                                    [
                                        'status'            => $status_order,
                                        'updated_at'        => date('Y-m-d H:i:s'),
                                        'updated_by'        => $id_user_login,
                                        'progress'          => json_encode($progress),
                                        'remaining_payment' => $get_order->remaining_payment - $jumlah_yg_dibayarkan,
                                        'already_paid'      => $get_order->already_paid + $jumlah_yg_dibayarkan
                                    ]
                                );

            $get_last_history_payment   =   M_HistoryPayment::where('payment_code',$payment_code)->orderBy('id','DESC')->first();
            $update_last_history        =   M_HistoryPayment::where('id',$get_last_history_payment->id)->update([
                                                'status'    => $status,
                                                'amount'    => $jumlah_yg_dibayarkan,
                                                'updated_at'    => date('Y-m-d H:i:s'),
                                            ]);
            DB::commit();
            // $order_number  =
            return response()->json([
                'message'   => 'Success save Data',
                'status'    => 'success',
                'data'      => $update_order,
                'code'      => 200,
            ], 200);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line: '.$e->getLine(),
                'status'        => "error",
            ],500);
        }

    }
    function convertPeriodeType($periode_type){
        
        if($periode_type == 1){
            $type = 'Hari';
        }else if($periode_type == 2){
            $type = 'Minggu';
        }else if($periode_type == 3){
            $type = 'Bulan';
        }else if($periode_type == 4){
            $type = 'Tahun';
        }

        return $type;
    }
}
