<?php

namespace App\Http\Controllers\Pembayaran;

use App\Models\M_plat;
use App\Models\Payment\M_MasterBank;
use App\Models\Payment\M_MasterMethodePayment;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class TipePembayaranController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Pembayaran.TipePembayaran.v_tipe_pembayaran');
    }

    public function showData(Request $request)
    {

        $array_data = M_MasterMethodePayment::get();
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function save(Request $request)
    {
        $code = $request->type_code;
        $name = $request->type_name;

        $cek  = M_MasterMethodePayment::where('code',$code)->get();

        if(count($cek) > 0)
        {
            return response()->json([
                'message'       => 'Kode sudah digunakan, silahkan input dengan kode lain',
                'status'        => "error",
            ],200);
        }

        DB::beginTransaction();
        try{
            $save_province = M_MasterMethodePayment::create([
                'code'          => $code,
                'description'   => $name
            ]);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);

        $get_data = M_MasterMethodePayment::where('id',$id)->select('code','description')->first();
        return response()->json([
            'message'   => 'success',
            'status'    => 'success',
            'data'      => $get_data,
        ],200);

    }

    public function update(Request $request)
    {
        $id = decrypt($request->id);

        $code = $request->type_code;

        $cek = M_MasterMethodePayment::where('code',$code)->where('id','!=',$id)->first();

        if($cek){
            return response()->json([
                'message'       => 'Kode sudah digunakan, silahkan input dengan kode lain',
                'status'        => "error",
            ],200);
        }
        DB::beginTransaction();
        try{
            $methodePayment = M_MasterMethodePayment::find($id);
            $methodePayment->code         = $request->type_code;
            $methodePayment->description  = $request->type_name;
            $methodePayment->save();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        $user_login   = $request->id_user_login;
        DB::beginTransaction();
        try{
            $methodePayment = M_MasterBank::find($id);
            $methodePayment->deleted_by   = $user_login;
            $methodePayment->save();
            $methodePayment->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
