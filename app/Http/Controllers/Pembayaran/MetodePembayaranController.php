<?php

namespace App\Http\Controllers\Pembayaran;

use App\Models\Payment\M_MasterBank;
use App\Models\Payment\M_MasterMethodePayment;
use App\Models\Payment\M_MethodePaymentBank;
use App\Models\Payment\M_MethodePaymentBankStep;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class MetodePembayaranController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Pembayaran.MetodePembayaran.v_list_metode_pembayaran');
    }

    public function showData(Request $request)
    {

        $array_data = M_MethodePaymentBank::with(['get_bank','get_master_methode_payment','get_payment_bank_step'])->select('m_bank_code','m_methode_payment_code','code','name','pga_code','no_rekening')->get();
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('bank_name', function($row){
                return $row->get_bank->name;
            })
            ->addColumn('payment_type', function($row){
                return $row->get_master_methode_payment->description;
            })
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".$row->code."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".$row->code."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function getPaymentType(Request $request)
    {
        $q              = $request->term;
        $qwhere         = strtolower($request->term).'%';
        $query_         = M_MasterMethodePayment::
                            whereRaw('lower(code) like (?)',["%{$qwhere}%"])
                            ->orWhereRaw('lower(description) like (?)',["%{$qwhere}%"])
                            ->orderBy('code', 'asc')
                            ->get();

        $total_count = count($query_);
        $data_array = array();
        foreach ($query_ as $row) {
            $data_array[] = array(
                'id'            => $row->id,
                'text'          => $row->description,
                'id_encrypt'    => encrypt($row->id),
                'code'          => $row->code
            );
        }
        return response()->json($data_array, 200);
    }

    public function getBank(Request $request)
    {
        $q              = $request->term;
        $qwhere         = strtolower($request->term).'%';
        $query_         = M_MasterBank::
                            whereRaw('lower(code) like (?)',["%{$qwhere}%"])
                            ->orWhereRaw('lower(name) like (?)',["%{$qwhere}%"])
                            ->orderBy('name', 'asc')
                            ->get();

        $total_count = count($query_);
        $data_array = array();
        foreach ($query_ as $row) {
            $data_array[] = array(
                'id'            => $row->id,
                'text'          => $row->name,
                'id_encrypt'    => encrypt($row->id),
                'code'          => $row->code
            );
        }
        return response()->json($data_array, 200);
    }

    public function save(Request $request)
    {
        $payment_type           = $request->payment_type;
        $bank_name              = $request->bank_name;
        $name                   = $request->name;
        $payment_gateway_code   = $request->payment_gateway_code;
        $id_user_login          = $request->id_user_login;

        $cek  = M_MethodePaymentBank::where('m_bank_id',$bank_name)->where('m_methode_payment_id',$payment_type)->get();

        if(count($cek) > 0)
        {
            return response()->json([
                'message'       => 'Metode untuk bank ini sudah digunakan, silahakan pilih yang lain',
                'status'        => "error",
            ],200);
        }

        $get_bank           = M_MasterBank::where('id',$bank_name)->first();
        $get_payment_type   = M_MasterMethodePayment::where('id',$payment_type)->first();
        DB::beginTransaction();
        try{
            $save_bank = M_MethodePaymentBank::create([
                'code'                      => Uuid::uuid4(),
                'name'                      => $name,
                'm_bank_id'                 => $bank_name,
                'm_bank_code'               => $get_bank->code,
                'm_methode_payment_id'      => $payment_type,
                'm_methode_payment_code'    => $get_payment_type->code,
                'pga_code'                  => $payment_gateway_code,
                'created_by'                => $id_user_login,
                'no_rekening'               => $request->no_rek,
            ]);
            DB::commit();

            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $reff_code = $request->id;
        // $get_data = M_MasterBank::where('id',$id)->select('code','name','logo')->first();
        $get_data = M_MethodePaymentBank::with(['get_bank','get_master_methode_payment'])
                    ->select('m_bank_code','m_bank_id','m_methode_payment_code','m_methode_payment_id','code','name','code','pga_code','no_rekening')->where('code',$reff_code)->first();
        return response()->json([
            'message'   => 'success',
            'status'    => 'success',
            'data'      => $get_data,
        ],200);

    }

    public function update(Request $request)
    {
        $payment_type           = $request->payment_type;
        $bank_name              = $request->bank_name;
        $name                   = $request->name;
        $payment_gateway_code   = $request->payment_gateway_code;
        $id_user_login          = $request->id_user_login;
        $reff_code              = $request->id;
        $no_rek                 = $request->no_rek;

        $cek  = M_MethodePaymentBank::where('m_bank_id',$bank_name)
                    ->where('m_methode_payment_id',$payment_type)->where('code','<>',$reff_code)->get();

        if(count($cek) > 0)
        {
            return response()->json([
                'message'       => 'Metode untuk bank ini sudah digunakan, silahakan pilih yang lain',
                'status'        => "error",
            ],200);
        }

        $get_bank           = M_MasterBank::where('id',$bank_name)->first();
        $get_payment_type   = M_MasterMethodePayment::where('id',$payment_type)->first();


        if($get_payment_type->code == 'TFM'){
            $no_rek     = $no_rek;
            $pga_code   = null;
        }else{
            $no_rek     = null;
            $pga_code   = $payment_gateway_code;
        }
        DB::beginTransaction();
        try{
            $update_date = M_MethodePaymentBank::where('code',$reff_code)->update([
                'pga_code'                      => $pga_code,
                'name'                      => $name,
                'm_bank_id'                 => $bank_name,
                'm_bank_code'               => $get_bank->code,
                'm_methode_payment_id'      => $payment_type,
                'm_methode_payment_code'    => $get_payment_type->code,
                'updated_by'                => $id_user_login,
                'no_rekening'               => $no_rek,
                'updated_at'                => date('Y-m-d H:i:s')
            ]);
            DB::commit();

            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $reff_code    = $request->id;
        $user_login   = $request->id_user_login;

        DB::beginTransaction();
        try{
            $update_date = M_MethodePaymentBank::where('code',$reff_code)->update([
                'deleted_at'    => date('Y-m-d H:i:s'),
                'deleted_by'    => $user_login
            ]);
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function saveGuide(Request $request){
        // dd($request->all());
        $metode_pembayaran_name = $request->metode_pembayaran_name;
        $metode_pembayaran_id   = $request->metode_pembayaran_id;
        $guide_name             = $request->guide_name;
        $description            = $request->description;

        $description            = json_encode($description);
        $id_user_login          = $request->id_user_login;

        $get_methode_pembayaran = M_MethodePaymentBank::where('code',$metode_pembayaran_id)->first();

        DB::beginTransaction();
        try{
            $save_bank = M_MethodePaymentBankStep::create([
                'm_methode_payment_bank_code'  => $metode_pembayaran_id,
                'name'                              => $guide_name,
                'step'                              => $description,
                'created_by'                        => $id_user_login
            ]);
            DB::commit();

            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }


    public function editGuide(Request $request)
    {
        // dd($request->all());
        $id         = $request->id;
        $get_data   = M_MethodePaymentBankStep::where('id',$id)->select('name','step')->first();
        return response()->json([
            'message'   => 'success',
            'status'    => 'success',
            'data'      => $get_data
        ],200);
    }

    public function updateGuide(Request $request)
    {
        $id                     = $request->id;
        $guide_name             = $request->guide_name;
        $description            = $request->description;

        $description            = json_encode($description);
        $id_user_login          = $request->id_user_login;


        DB::beginTransaction();
        try{
            $save_bank = M_MethodePaymentBankStep::where('id',$id)->update([
                'name'                              => $guide_name,
                'step'                              => $description,
                'updated_at'                        => date('Y-m-d H:i:s'),
                'updated_by'                        => $id_user_login,
            ]);
            DB::commit();

            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function deleteGuide(Request $request)
    {
        $id             = $request->id;
        $user_login     = $request->id_user_login;

        DB::beginTransaction();
        try{
            $update_date = M_MethodePaymentBankStep::where('id',$id)->update([
                'deleted_at'    => date('Y-m-d H:i:s'),
                'deleted_by'    => $user_login
            ]);
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
