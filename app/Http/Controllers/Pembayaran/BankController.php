<?php

namespace App\Http\Controllers\Pembayaran;

use App\Models\M_plat;
use App\Models\Payment\M_MasterBank;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BankController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Pembayaran.Bank.v_list_bank');
    }

    public function showData(Request $request)
    {

        $array_data = M_MasterBank::get();
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('logo', function($row){
                $img = asset('storage/bank/')."/".$row->logo;
                return $img;
            })
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function save(Request $request)
    {
        $code = $request->bank_code;
        $name = $request->bank_name;

        $cek  = M_MasterBank::where('code',$code)->get();

        if($request->logo_bank)
        {
            $picture    = $request->logo_bank;
            $imageName  = generate_url(ucwords($name)).'.'.$request->logo_bank->extension();
            $imageName  = strtolower($imageName);
        }else{
            $imageName = null;
        }

        if(count($cek) > 0)
        {
            return response()->json([
                'message'       => 'Kode sudah digunakan, silahkan input dengan kode lain',
                'status'        => "error",
            ],200);
        }

        DB::beginTransaction();
        try{
            $save_bank = M_MasterBank::create([
                'code'          => $code,
                'name'          => $name,
                'logo'          => $imageName
            ]);
            DB::commit();
            if($request->logo_bank){
                Storage::putFileAs('public/bank', $picture, $imageName);
            };
            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);

        $get_data = M_MasterBank::where('id',$id)->select('code','name','logo')->first();
        return response()->json([
            'message'   => 'success',
            'status'    => 'success',
            'data'      => $get_data,
        ],200);

    }

    public function update(Request $request)
    {
        $id = decrypt($request->id);

        $code         = $request->bank_code;
        $user_login   = $request->id_user_login;

        $cek = M_MasterBank::where('code',$code)->where('id','!=',$id)->first();

        if($cek){
            return response()->json([
                'message'       => 'Kode sudah digunakan, silahkan input dengan kode lain',
                'status'        => "error",
            ],200);
        }

        $get_data           = M_MasterBank::where('id',$id)->first();
        $old_name_picture   = $get_data->logo;
        if($request->logo_bank)
        {
            $picture    = $request->logo_bank;
            $imageName  = generate_url(ucwords($request->bank_code.'-'.$request->bank_name)).'.'.$request->logo_bank->extension();
            $imageName  = strtolower($imageName);
        }else{
            $imageName = $old_name_picture;
        }
        DB::beginTransaction();
        try{
            $methodePayment = M_MasterBank::find($id);
            $methodePayment->code         = $request->bank_code;
            $methodePayment->name         = $request->bank_name;
            $methodePayment->updated_by   = $user_login;
            $methodePayment->logo         = $imageName;

            $methodePayment->save();
            DB::commit();
            if($request->logo_bank){

                Storage::delete('public/bank/'.$old_name_picture);
                Storage::putFileAs('public/bank', $picture, $imageName);
            };
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id           = decrypt($request->id);
        $user_login   = $request->id_user_login;

        DB::beginTransaction();
        try{
            $methodePayment               = M_MasterBank::find($id);
            $methodePayment->deleted_by   = $user_login;
            $methodePayment->save();
            $methodePayment->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
