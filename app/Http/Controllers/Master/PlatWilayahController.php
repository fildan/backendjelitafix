<?php

namespace App\Http\Controllers\Master;

use App\Models\M_Plat;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PlatWilayahController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Master.PlatWilayah.v_plat_wilayah');
    }

    public function showData(Request $request)
    {
        $array_data = M_Plat::orderBy('code','ASC')->get();

        return DataTables::of($array_data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
                })

                ->make(true);
    }
    public function add(Request $request)
    {
        return view('Master.PlatWilayah.v_add_plat_wilayah');
    }
    public function save(Request $request)
    {
        $code         = $request->code;
        $wilayah      = $request->wilayah;
        $session      = $request->session()->get('user_data');
        $data_save    = [];
        DB::beginTransaction();
        try{
            if($code){
                foreach($code as $key => $val){
                    $data_save[] = [
                        'code'          => $val,
                        'wilayah'       => $wilayah[$key],
                        'created_by'    => $session['id_user'],
                        'created_at'    => date('Y-m-d H:i:s')
                    ];
                }
            }
            $save_acc = M_Plat::insert($data_save);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $get_data = M_Plat::where('id',$id)->select('code','wilayah')->first();

            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $get_data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {

        $session        = $request->session()->get('user_data');
        $code           = $request->code;
        $wilayah        = $request->wilayah;
        $id             = decrypt($request->id);
        DB::beginTransaction();
        try{
            $save_plat = M_Plat::where('id',$id)->update([
                'code'          => $code,
                'wilayah'   => $wilayah,
                'updated_by'    => $session['id_user'],
            ]);
            DB::commit();

            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $m_plat = M_Plat::find($id);
            $m_plat->deleted_by = $request->id_user_login;
            $m_plat->save();
            $m_plat->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
