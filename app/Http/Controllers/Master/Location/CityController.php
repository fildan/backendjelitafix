<?php

namespace App\Http\Controllers\Master\Location;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\M_City as M_City;
use App\Models\M_Province as M_Province;

class CityController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Master.Location.v_city');
    }

    public function showData(Request $request)
    {

        $array_data = M_City::with(['GetProvince'])
                        ->get()
                        ->sortBy(function($city) {
                            return $city->GetProvince->name;
                        });
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('province_name', function($row){

                return $row->getProvince->name;
            })
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function getProvince(Request $request)
    {
        $q              = $request->term;
        $qwhere         = strtolower($request->term).'%';
        $query_         = M_Province::whereRaw('lower(name) like (?)',["%{$qwhere}%"])
                            ->orderBy('name', 'asc')
                            ->get();

        $total_count = count($query_);
        $data_array = array();
        foreach ($query_ as $row) {
            $data_array[] = array(
                'id'            => $row->id,
                'text'          => $row->name,
            );
        }
        return response()->json($data_array, 200);
    }

    public function getCity(Request $request)
    {
        $province_id    = $request->province_id;

        $q              = $request->term;
        $qwhere         = strtolower($request->term).'%';
        // $query_         = M_City::whereRaw('lower(name) like (?)',["%{$qwhere}%"])
        $query_             = M_City::where('m_province_id',$province_id)
                            ->orderBy('name', 'asc')
                            ->get();

        $total_count = count($query_);
        $data_array = array();
        foreach ($query_ as $row) {
            $data_array[] = array(
                'id'            => $row->id,
                'text'          => $row->name,
            );
        }
        return response()->json($data_array, 200);
    }
    public function Save(Request $request)
    {
        DB::beginTransaction();
        try{
            $save_city = M_City::create([
                'name'          => $request->city_name,
                'm_province_id' => $request->province_name,
            ]);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function GetData(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            // $get_data = M_City::where('id',$id)->first();

            $get_data = M_City::with(['GetProvince'])->where('id',$id)
                        ->first();
            $data = [
                'id'            => encrypt($get_data->id),
                'm_province_id' => $get_data->GetProvince->id,
                'province_name' => $get_data->getProvince->name,
                'name'          => $get_data->name,
                'code'          => $get_data->code
            ];
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }

    }

    public function update(Request $request)
    {

        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $Mcity                  = M_City::find($id);
            $Mcity->name            = $request->city_name;
            $Mcity->m_province_id   = $request->province_name;
            $Mcity->save();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $Mcity = M_City::find($id);
            $Mcity->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
