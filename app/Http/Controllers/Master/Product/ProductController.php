<?php

namespace App\Http\Controllers\Master\Product;

use App\Models\M_Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Master.Product.v_product');
    }

    public function showData(Request $request)
    {

        $array_data = M_Product::get();
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function add(Request $request)
    {
        return view('Master.Product.v_add_product');
    }

    public function save(Request $request)
    {

        $product_name   = $request->product_name;
        $product_code   = $request->product_code;
        $url            = $request->url;
        $description    = $request->description;

        if($request->product_picture)
        {
            $picture = $request->product_picture;
            $imageName = str_replace(' ', '-',ucwords($product_name)).'.'.$request->product_picture->extension();
            $imageName = strtolower($imageName);
        }else{
            $imageName = null;
        }

        DB::beginTransaction();
        try{
            $save_product = M_Product::create([
                'name'          => $product_name,
                'code'          => $product_code,
                'url'           => $url,
                'description'   => $description,
                'picture'       => $imageName
            ]);
            DB::commit();
            if($picture){
                Storage::putFileAs('public/product-picture', $picture, $imageName);
            }
            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id                 = decrypt(($request->id));
        $get_data           = M_Product::where('id',$id)->first();
        $data['get_data']   = $get_data;
        $data['encrypt_id'] = encrypt($id);
        return view('Master.Product.v_edit_product',$data);
    }

    public function update(Request $request)
    {
        $id             = decrypt($request->id);
        $product_name   = $request->product_name;
        $product_code   = $request->product_code;
        $url            = $request->url;
        $description    = $request->description;
        $is_publish     = $request->is_publish;
        if($is_publish == 'on'){
            $is_publish = 1;
        }else{
            $is_publish = 0;
        }
        $get_data           = M_Product::where('id',$id)->first();
        $old_name_picture   = $get_data->picture;
        if($request->product_picture)
        {
            $picture = $request->product_picture;
            $imageName = str_replace(' ', '-',ucwords($product_name)).'-new-at-'.time().'.'.$request->product_picture->extension();
            $imageName = strtolower($imageName);
        }else{
            $imageName = $old_name_picture;
        }
        // dd($imageName);
        DB::beginTransaction();
        try{
            $save_product = M_Product::where('id',$id)->update([
                'name'          => $product_name,
                'code'          => $product_code,
                'url'           => $url,
                'description'   => $description,
                'picture'       => $imageName,
                'is_develope'   => $is_publish
            ]);
            DB::commit();
            if($request->product_picture)
            {
                Storage::delete('public/product-picture/'.$old_name_picture);
                Storage::putFileAs('public/product-picture', $picture, $imageName);
            }
            return response()->json([
                'message'  => 'success',
                'status'            => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $M_Product = M_Product::find($id);
            $M_Product->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
