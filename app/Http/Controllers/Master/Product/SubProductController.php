<?php

namespace App\Http\Controllers\Master\Product;

use App\Models\M_SubProduct;
use App\Models\M_Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SubProductController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Master.SubProduct.v_sub_product');
    }

    public function showData(Request $request)
    {
        $array_data = M_SubProduct::with(['GetProduct'])
                        ->get()
                        ->sortBy(function($product) {
                            return $product->GetProduct->name;
                        });
        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('main_product_name', function($row){

                return $row->GetProduct->name;
            })
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    if($row->is_develope == 0){
                        $btn .= "|| <a href='#' class='unlock' onclick='unlockFeature(\"".encrypt($row->id)."\")'><i class='fa fa-unlock-alt' aria-hidden='true'></i></a>";
                    }
                    return $btn;
            })

            ->make(true);
    }

    public function getMainProduct(Request $request)
    {
        $q              = $request->term;
        $qwhere         = strtolower($request->term).'%';
        $query_         = M_Product::whereRaw('lower(name) like (?)',["%{$qwhere}%"])
                            ->orderBy('name', 'asc')
                            ->get();

        $total_count = count($query_);
        $data_array = array();
        foreach ($query_ as $row) {
            $data_array[] = array(
                'id'            => $row->id,
                'text'          => $row->name,
                'id_encrypt'    => encrypt($row->id),
                'code'          => $row->code
            );
        }
        return response()->json($data_array, 200);
    }

    public function save(Request $request)
    {
        $main_product   = $request->main_product;
        $name           = $request->name;
        $description    = $request->description;
        $session        = $request->session()->get('user_data');
        if($request->product_picture)
        {
            $picture = $request->product_picture;
            $imageName = str_replace(' ', '-',ucwords($name)).'.'.$request->product_picture->extension();
            $imageName = strtolower($imageName);
        }

        DB::beginTransaction();
        try{
            $save_product = M_SubProduct::create([
                'name'          => $name,
                'm_product_id'  => $main_product,
                'description'   => $description,
                'picture'       => $imageName,
                'created_by'    => $session['id_user'],
                'updated_by'    => $session['id_user'],
                'url'           => generate_url($name),
            ]);
            DB::commit();
            Storage::putFileAs('public/sub-product-picture', $picture, $imageName);
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $get_data = M_SubProduct::with(['GetProduct'])->where('id',$id)
                        ->first();
            $data = [
                'id'                => encrypt($get_data->id),
                'm_product_id'      => $get_data->GetProduct->id,
                'product_main_name' => $get_data->GetProduct->name,
                'name'              => $get_data->name,
                'description'       => $get_data->description,
                'picture'           => $get_data->picture,
                'is_develope'       => $get_data->is_develope
            ];
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {
        $id             = decrypt($request->id);
        $main_product   = $request->main_product;
        $name           = $request->name;
        $description    = $request->description;
        $session        = $request->session()->get('user_data');

        $get_data           = M_SubProduct::where('id',$id)->first();
        $old_name_picture   = $get_data->picture;
        if($request->product_picture)
        {
            $picture = $request->product_picture;
            $imageName = generate_url(str_replace(' ', '-',ucwords($name))).'.'.$request->product_picture->extension();
            $imageName = strtolower($imageName);
        }else{
            $imageName = $old_name_picture;
        }

        DB::beginTransaction();
        try{
            $save_product = M_SubProduct::where('id',$id)->update([
                'name'          => $name,
                'm_product_id'  => $main_product,
                'description'   => $description,
                'picture'       => $imageName,
                'updated_by'    => $session['id_user'],
            ]);
            DB::commit();
            if($request->product_picture)
            {
                Storage::delete('public/sub-product-picture'.$old_name_picture);
                Storage::putFileAs('public/sub-product-picture', $picture, $imageName);
            }
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        $session        = $request->session()->get('user_data');
        DB::beginTransaction();
        try{
            $M_SubProduct = M_SubProduct::find($id);
            $M_SubProduct->deleted_by = $session['id_user'];
            $M_SubProduct->save();
            $M_SubProduct->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function publish(Request $request)
    {
        $id_sub         = decrypt($request->id_sub);
        $url            = $request->url;
        $id_user_login  = $request->id_user_login;

        DB::beginTransaction();
        try{
            $subPrdct                   = M_SubProduct::find($id_sub);
            $subPrdct->url              = $url;
            $subPrdct->is_develope      = 1;
            $subPrdct->publish_date     = date('Y-m-d H:i:s');
            $subPrdct->publish_by       = $id_user_login;
            $subPrdct->updated_by       = $id_user_login;
            $subPrdct->save();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
