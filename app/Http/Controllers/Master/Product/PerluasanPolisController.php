<?php

namespace App\Http\Controllers\Master\Product;

use App\Models\M_Perluasan;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PerluasanPolisController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Master.PerluasanPolis.v_perluasan_polis');
    }

    public function showData(Request $request)
    {
        $array_data = M_Perluasan::with(['GetProduct'])
                            ->get()
                            ->sortBy(function($product) {
                                return $product->GetProduct->name;
                            });

        return DataTables::of($array_data)
                ->addIndexColumn()
                ->addColumn('product_name', function($row){
                    return $row->GetProduct->name;
                })
                ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
                })

                ->make(true);
    }

    public function add(Request $request)
    {
        return view('Master.PerluasanPolis.v_add_perluasan_polis');
    }

    public function save(Request $request)
    {
        $main_product           = $request->main_product;
        $perluasan_name         = $request->perluasan_name;
        $perluasan_description  = $request->perluasan_description;
        $session                = $request->session()->get('user_data');
        $rate_or_value          = $request->rate_or_value;
        $data_save = [];
        DB::beginTransaction();
        try{
            if($perluasan_name){
                foreach($perluasan_name as $key => $val){
                    $data_save[] = [
                        'm_product_id'  => $main_product,
                        'name'          => $val,
                        'description'   => $perluasan_description[$key],
                        'rate_or_value' => $rate_or_value[$key],
                        'created_by'    => $session['id_user'],
                        'created_at'    => date('Y-m-d H:i:s')
                    ];
                }
            }
            $save_product = M_Perluasan::insert($data_save);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $get_data = M_Perluasan::with(['GetProduct'])->where('id',$id)
                        ->first();

            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $get_data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {

        $session        = $request->session()->get('user_data');
        $main_product   = $request->main_product;
        $name           = $request->name;
        $description    = $request->description;
        $rate_or_value  = $request->rate_or_value;
        $id             = decrypt($request->id);
        DB::beginTransaction();
        try{
            $save_product = M_Perluasan::where('id',$id)->update([
                'name'          => $name,
                'm_product_id'  => $main_product,
                'description'   => $description,
                'rate_or_value' => $rate_or_value,
                'updated_by'    => $session['id_user'],
            ]);
            DB::commit();

            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $M_SubProduct = M_Perluasan::find($id);
            $M_SubProduct->deleted_by = $request->id_user_login;
            $M_SubProduct->save();
            $M_SubProduct->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
