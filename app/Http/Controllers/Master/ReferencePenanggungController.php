<?php

namespace App\Http\Controllers\Master;

use App\Models\M_Product;
use App\Models\M_ReferencePenanggungModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class ReferencePenanggungController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Master.ReferencePenanggung.v_reference_penanggung');
    }

    public function getList(Request $request)
    {
        $array_data = M_ReferencePenanggungModel::with(['get_company_type'])->get();

        return DataTables::of($array_data)
            ->addIndexColumn()
            ->addColumn('asuransi_name', function($row){
                    return ucwords($row->name).', '.'( '.$row->get_company_type->name.' )';
            })
            ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".$row->code."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".$row->code."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
            })

            ->make(true);
    }

    public function save(Request $request)
    {
        // dd($request->all());
        $type = $request->type;
        $name = strtolower($request->name);
        $cob  = $request->cob;

        $cek = M_ReferencePenanggungModel::where('company_type',$type)->where('name',$name)->first();

        if($cek){
            return response()->json([
                'message'           => 'Asuransi sudah tersedia',
                'status'            => 'error',
                'code'              => 401
            ],200);
        }

        // $cob_id     = null;
        // $cob_code   = null;
        $set_cob = null;
        if($cob){
            // $cob_exists_id      = [];
            // $cob_exists_code    = [];
            $cob_exist = [];
            foreach($cob as $row => $val){
                $get_code = M_Product::where('id',$val)->select('code','name')->first();
                $cob_exists_code[] = $get_code->code;
                $cob_exists_id[]   = $val;

                $cob_exist[] = [
                    'id'    => $val,
                    'code'  => $get_code->code,
                    'name'  => $get_code->name
                ];
            }
            $set_cob = json_encode($cob_exist);
        }
        DB::beginTransaction();
        try{

            $save_data =    M_ReferencePenanggungModel::create([
                                'company_type'  => $type,
                                'code'          => Uuid::uuid4(),
                                'name'          => $name,
                                'created_at'    => date('Y-m-d H:i:s'),
                                'created_by'    => $request->id_user_login,
                                'm_product'     => $set_cob,
                            ]);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
                'code'      => 200
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
                'code'          => $e->getCode()
            ],500);
        }
    }

    public function edit(Request $request){
        $code = $request->id;
        $cek = M_ReferencePenanggungModel::with(['get_company_type'])->where('code',$code)->first();

        if(!$cek){
            return response()->json([
                'message'           => 'Asuransi tidak tersedia',
                'status'            => 'error',
                'code'              => 404
            ],200);
        }

        return response()->json([
            'message'   => 'success',
            'status'    => 'success',
            'code'      => 200,
            'data'      => $cek
        ],200);

    }

    public function update(Request $request)
    {
        $type = $request->type;
        $name = strtolower($request->name);
        $code = $request->code;
        $cob  = $request->cob;


        $cek = M_ReferencePenanggungModel::where('company_type',$type)->where('name',$name)->where('code','<>',$code)->first();

        if($cek){
            return response()->json([
                'message'           => 'Asuransi sudah tersedia',
                'status'            => 'error',
                'code'              => 401
            ],200);
        }

        $set_cob = null;
        if($cob){
            // $cob_exists_id      = [];
            // $cob_exists_code    = [];
            $cob_exist = [];
            foreach($cob as $row => $val){
                $get_code = M_Product::where('id',$val)->select('code','name')->first();
                $cob_exists_code[] = $get_code->code;
                $cob_exists_id[]   = $val;

                $cob_exist[] = [
                    'id'    => $val,
                    'code'  => $get_code->code,
                    'name'  => $get_code->name
                ];
            }
            $set_cob = json_encode($cob_exist);
        }
        DB::beginTransaction();
        try{

            $save_data =    M_ReferencePenanggungModel::where('code',$code)->update([
                                'company_type'  => $type,
                                'code'          => Uuid::uuid4(),
                                'name'          => $name,
                                'm_product'     => $set_cob,
                                'updated_at'    => date('Y-m-d H:i:s'),
                                'updated_by'    => $request->id_user_login
                            ]);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
                'code'      => 200
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
                'code'          => $e->getCode()
            ],500);
        }
    }

    public function delete(Request $request){
        $code = $request->code;


        $cek = M_ReferencePenanggungModel::where('code',$code)->first();

        if(!$cek){
            return response()->json([
                'message'           => 'Asuransi tidak tersedia',
                'status'            => 'error',
                'code'              => 401
            ],200);
        }
        DB::beginTransaction();
        try{

            $save_data =    M_ReferencePenanggungModel::where('code',$code)->update([
                                'deleted_at'    => date('Y-m-d H:i:s'),
                                'deleted_by'    => $request->id_user_login
                            ]);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
                'code'      => 200
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
                'code'          => $e->getCode()
            ],500);
        }
    }
}
