<?php

namespace App\Http\Controllers\Master;

use App\Models\M_Konstruksi;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class KonstruksiBangunanController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Master.KonstruksiBangunan.v_konstruksi_bangunan');
    }

    public function showData(Request $request)
    {
        $array_data = M_Konstruksi::get();

        return DataTables::of($array_data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
                })

                ->make(true);
    }
    public function add(Request $request)
    {
        return view('Master.KonstruksiBangunan.v_add_konstruksi_bangunan');
    }
    public function save(Request $request)
    {
        $name         = $request->name;
        $session      = $request->session()->get('user_data');
        $data_save    = [];
        DB::beginTransaction();
        try{
            if($name){
                foreach($name as $key => $val){
                    $data_save[] = [
                        'name'          => $val,
                        'created_by'    => $session['id_user'],
                        'created_at'    => date('Y-m-d H:i:s')
                    ];
                }
            }
            $save_const = M_Konstruksi::insert($data_save);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $get_data = M_Konstruksi::where('id',$id)->select('name')->first();

            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $get_data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {

        $session        = $request->session()->get('user_data');
        $name           = $request->name;
        $id             = decrypt($request->id);
        DB::beginTransaction();
        try{
            $save_const = M_Konstruksi::where('id',$id)->update([
                'name'          => $name,
                'updated_by'    => $session['id_user'],
            ]);
            DB::commit();

            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $M_Konstruksi = M_Konstruksi::find($id);
            $M_Konstruksi->deleted_by = $request->id_user_login;
            $M_Konstruksi->save();
            $M_Konstruksi->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
