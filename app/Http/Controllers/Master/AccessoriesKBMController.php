<?php

namespace App\Http\Controllers\Master;

use App\Models\M_Acc;
use App\Models\M_Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AccessoriesKBMController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Master.AccKBM.v_acc_kbm');
    }

    public function showData(Request $request)
    {
        $array_data = M_Acc::leftJoin('m_product','m_accessories.m_product_code','=','m_product.code')->select('m_accessories.*','m_product.name as product_name')->orderBy('m_accessories.m_product_code','ASC')->orderBy('m_accessories.name','ASC')->get();
        return DataTables::of($array_data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
                })

                ->make(true);
    }
    public function add(Request $request)
    {
        return view('Master.AccKBM.v_add_acc_kbm');
    }
    public function save(Request $request)
    {
        $name                   = $request->name;
        $description            = $request->description;
        $session                = $request->session()->get('user_data');
        $select_main_product    = $request->select_main_product;

        $get_cob                = M_Product::where('id',$select_main_product)->first();
        $data_save    = [];
        DB::beginTransaction();
        try{
            if($name){
                foreach($name as $key => $val){
                    $data_save[] = [
                        'name'                  => $val,
                        'description'           => $description[$key],
                        'created_by'            => $session['id_user'],
                        'created_at'            => date('Y-m-d H:i:s'),
                        'm_product_id'          => $get_cob->id,
                        'm_product_code'        => $get_cob->code
                    ];
                }
            }
            $save_acc = M_Acc::insert($data_save);
            DB::commit();
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);

        DB::beginTransaction();
        try{
            $get_data = M_Acc::leftJoin('m_product','m_accessories.m_product_code','=','m_product.code')->select('m_product.id as m_product_id','m_product.name as product_name','m_accessories.name','m_accessories.description')->where('m_accessories.id',$id)->first();

            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $get_data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {

        $session        = $request->session()->get('user_data');
        $name           = $request->name;
        $description    = $request->description;
        $id             = decrypt($request->id);
        $select_main_product    = $request->select_main_product;

        $get_cob                = M_Product::where('id',$select_main_product)->first();
        DB::beginTransaction();
        try{
            $save_product = M_Acc::where('id',$id)->update([
                'name'          => $name,
                'description'   => $description,
                'updated_by'    => $session['id_user'],
                'm_product_id'          => $get_cob->id,
                'm_product_code'        => $get_cob->code
            ]);
            DB::commit();

            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $acc_kbm = M_Acc::find($id);
            $acc_kbm->deleted_by = $request->id_user_login;
            $acc_kbm->save();
            $acc_kbm->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }
}
