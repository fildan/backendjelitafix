<?php

namespace App\Http\Controllers\Penanggung;

use App\Models\M_Penanggung;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PenanggungController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Penanggung.v_list_penanggung');
    }

    public function getList(Request $request)
    {
        $array_data = M_Penanggung::get();

        return DataTables::of($array_data)
                ->addIndexColumn()
                ->addColumn('picture', function($row){
                    $img = asset('storage/logo-penanggung/')."/".$row->picture;
                    return $img;
                })
                ->addColumn('action', function($row){
                    $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                    || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    return $btn;
                })

                ->make(true);
    }

    public function save(Request $request)
    {
        // dd($request->product_picture);
        $name         = $request->name;
        $description  = $request->description;
        $code         = $request->code;
        $joindate     = $request->join_date;
        $user_login   = $request->id_user_login;


        if($request->logo_penanggung)
        {
            $picture    = $request->logo_penanggung;
            $imageName  = generate_url(ucwords($name)).'.'.$request->logo_penanggung->extension();
            $imageName  = strtolower($imageName);
        }else{
            $imageName = null;
        }
        DB::beginTransaction();
        try{


            $save_product = M_Penanggung::create([
                'name'          => $name,
                'code'          => $code,
                'join_date'     => date('Y-m-d',strtotime($joindate)),
                'description'   => $description,
                'picture'       => $imageName,
                'created_by'    => $user_login
            ]);
            DB::commit();
            if($request->logo_penanggung){
                Storage::putFileAs('public/logo-penanggung', $picture, $imageName);
            };
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $get_data = M_Penanggung::where('id',$id)->select('name','description','code','join_date','picture')->first();
            $get_data->join_date = date('Y-m-d',strtotime($get_data->join_date));
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
                'data'      => $get_data
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function update(Request $request)
    {
        $id           = decrypt($request->id);
        $name         = $request->name;
        $description  = $request->description;
        $code         = $request->code;
        $joindate     = $request->join_date;
        $user_login   = $request->id_user_login;

        $get_data           = M_Penanggung::where('id',$id)->first();
        $old_name_picture   = $get_data->picture;
        if($request->logo_penanggung)
        {
            $picture    = $request->logo_penanggung;
            $imageName  = generate_url(ucwords($name)).'.'.$request->logo_penanggung->extension();
            $imageName  = strtolower($imageName);
        }else{
            $imageName = $old_name_picture;
        }
        DB::beginTransaction();
        try{
            $save_product = M_Penanggung::where('id',$id)->update([
                'name'          => $name,
                'code'          => $code,
                'join_date'     => date('Y-m-d',strtotime($joindate)),
                'description'   => $description,
                'picture'       => $imageName,
                'updated_by'    => $user_login
            ]);
            DB::commit();
            if($request->logo_penanggung){

                Storage::delete('public/logo-penanggung/'.$old_name_picture);
                Storage::putFileAs('public/logo-penanggung', $picture, $imageName);
            };
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function delete(Request $request)
    {
        $id = decrypt($request->id);
        DB::beginTransaction();
        try{
            $penanggung = M_Penanggung::find($id);
            $penanggung->deleted_by = $request->id_user_login;
            $penanggung->save();
            $penanggung->delete();
            DB::commit();
            return response()->json([
                'message'   => 'success',
                'status'    => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function getPenanggung(Request $request)
    {
        $q              = $request->term;
        $qwhere         = strtolower($request->term).'%';
        $query_         = M_Penanggung::whereRaw('lower(name) like (?)',["%{$qwhere}%"])
                            ->orderBy('name', 'asc')
                            ->get();

        $total_count = count($query_);
        $data_array = array();
        foreach ($query_ as $row) {
            $data_array[] = array(
                'id'            => $row->id,
                'text'          => $row->name,
            );
        }
        return response()->json($data_array, 200);
    }
}
