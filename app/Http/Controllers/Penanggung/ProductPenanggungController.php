<?php

namespace App\Http\Controllers\Penanggung;

use App\Models\M_ProductPenanggung;
use App\Models\M_ProductPenanggungPerluasan;
use App\Models\M_Perluasan;
use App\Models\M_SubProduct;
use App\Models\M_ProductPerluasanRate;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Ramsey\Uuid\Uuid;

class ProductPenanggungController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Penanggung.Product.v_list_product_penanggung');
    }

    public function getList(Request $request)
    {
        $array_data =   M_ProductPenanggung::with(['get_penanggung','get_product'])->get();

        return DataTables::of($array_data)
                    ->addIndexColumn()
                    ->addColumn('picture', function($row){
                        $img = asset('storage/logo-penanggung/')."/".$row->get_penanggung->picture;
                        return $img;
                    })
                    ->addColumn('penanggung_name', function($row){
                        $name = $row->get_penanggung->name;
                        return $name;
                    })
                    ->addColumn('product_name', function($row){
                        $name = $row->get_product->name;
                        return $name;
                    })
                    ->addColumn('action', function($row){
                        $btn = "<a href='#' class='edit' onclick='editClick(\"".encrypt($row->id)."\")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                        || <a href='#' class='edit' onclick='deleteClick(\"".encrypt($row->id)."\")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                        return $btn;
                    })

                ->make(true);
    }

    public function add(Request $request)
    {
        return view('Penanggung.Product.v_add_product_penanggung');
    }

    public function getPolisByProduct(Request $request)
    {
        $id             = $request->id;
        DB::beginTransaction();
        try{
            $query_         = M_Perluasan::where('m_product_id',$id)->orderBy('order','ASC')
                            ->get();
            return response()->json($query_, 200);
        }catch (\Exception $e) {
            return response()->json([
                'message'       => $e->getMessage(),
                'status'        => "error",
            ],500);
        }
    }

    public function getSubProduct(Request $request)
    {
        $main_product   = $request->main_product_id;
        $qwhere         = strtolower($request->search).'%';
        $query_         = M_SubProduct::whereRaw('lower(name) like (?)',["%{$qwhere}%"])->where('m_product_id',$main_product)
                            ->orderBy('name', 'asc')
                            ->get();

        $total_count = count($query_);
        $data_array = array();
        foreach ($query_ as $row) {
            $data_array[] = array(
                'id'            => $row->id,
                'text'          => $row->name,
            );
        }
        return response()->json($data_array, 200);
    }
    public function save(Request $request)
    {
        

        $perluasan_product              = json_decode($request->perluasan_product);
        $select_main_product            = $request->select_main_product;
        $sub_product                    = $request->sub_product;
        $select_penanggung              = $request->select_penanggung;
        $product_name                   = $request->product_name;
        $product_code                   = $request->product_code;
        $description                    = $request->description;
        $manfaat_perlindungan           = $request->manfaat_perlindungan;
        $nameithoutbnfts                = $request->nameithoutbnfts;
        $name_fitur                     = $request->name_fitur;
        $description_fitur              = $request->description_fitur;
        $description_snk                = $request->description_snk;
        $description_claim              = $request->description_claim;
        $snk_list                       = $request->snk_list;
        $claim_list                     = $request->claim_list;
        $tlpn_claim                     = $request->tlpn_claim;
        $wa_claim                       = $request->wa_claim;
        $email_claim                    = $request->email_claim;
        $application_claim              = $request->application_claim;
        $rate_package_product           = $request->rate_package_product;
        $discount_package_product       = $request->discount_package_product;
        $is_available_package_product   = $request->is_available_package_product;
        $rekanan_bengkel                = $request->rekanan_bengkel;
        $documentsnk                    = $request->documentsnk;
        $documentclaim                  = $request->documentclaim;
        $product_package                = $request->product_package;
        $document_claim                 = $request->doc_claim;
        $select_type_rate               = $request->select_type_rate;
        $rate                           = $request->rate;
        $rate_periode_value             = $request->rate_periode_value;
        $rate_periode_type              = $request->rate_periode_type;
        $select_type_rate_estimasi      = $request->select_type_rate_estimasi;
        $rate_estimation                = $request->rate_estimation;
        $estimation_rate_periode_value  = $request->estimation_rate_periode_value;
        $estimation_rate_periode_type   = $request->estimation_rate_periode_type;
        
        
        $m_product_code     = DB::table('m_product')->where('id',$select_main_product)->select('code')->first();
        $m_sub_product_code = M_SubProduct::where('id',$sub_product)->select('code','name')->first();
        $m_penanggung_code  = DB::table('m_penanggung')->where('id',$select_penanggung)->select('code','name')->first();
        
        
        $url_product = strtolower(generate_url($m_penanggung_code->name.'-'.$m_sub_product_code->name));
        
        $cek_url = M_ProductPenanggung::where('url',$url_product)->first();
        
        if($cek_url){
            $url_product = strtolower(generate_url($url_product.'-'.time()));
        }
        
        if($rekanan_bengkel)
        {
            $rekanan_bengkel_file = generate_url(strtolower('reakanan-bengkel-polis-code'.$product_code)).'.'.$rekanan_bengkel->extension();
            $rekanan_bengkel_file = strtolower($rekanan_bengkel_file);
        }else{
            $rekanan_bengkel_file = null;
        }

        if($documentsnk)
        {
            $documentsnkfile = generate_url(strtolower('snk-polis-code'.$product_code)).'.'.$documentsnk->extension();
            $documentsnkfile = strtolower($documentsnkfile);
        }else{
            $documentsnkfile = null;
        }

        if($documentclaim)
        {
            $documentclaimfile = generate_url(strtolower('claim-polis-code'.$product_code)).'.'.$documentclaim->extension();
            $documentclaimfile = strtolower($documentclaimfile);
        }else{
            $documentclaimfile = null;
        }



        // ------------------tnc---------------
        $tnc['description'] = $description_snk;
        $tnc['file']        = $documentsnkfile;
        $tnc['fitur']       = $snk_list;

        // --------------end tnc---------------

        //-----------------claim--------------------
        $claim['description']       = $description_claim;
        $claim['file']              = $documentclaimfile;
        $claim['fitur']             = $claim_list;
        $claim['document_claim']    = $document_claim;
        $claim['contact']       = array(
            'telepon'           => $tlpn_claim,
            'whatsapp'          => $wa_claim,
            'email_claim'       => $email_claim,
            'aplication'        => $application_claim
        );


        $fitur = $name_fitur;
        $fitur_array = [];

        if($fitur){
            foreach($fitur as $key => $val){
                $fitur_array[] = [
                    'name'          => $val,
                    'description'   => $description_fitur[$key]
                ];
            }
        }


        $data_save['m_product_id']          = $select_main_product;
        $data_save['m_penanggung_id']       = $select_penanggung;
        $data_save['m_sub_product_id']      = $sub_product;
        $data_save['code']                  = $product_code;
        $data_save['reff_code']             = Uuid::uuid4();
        $data_save['name']                  = $product_name;
        $data_save['description']           = $description;
        $data_save['benefit']               = json_encode($manfaat_perlindungan);
        $data_save['benefit_exception']     = json_encode($nameithoutbnfts);
        $data_save['fitur']                 = json_encode($fitur_array);
        $data_save['tnc']                   = json_encode($tnc);
        $data_save['claim']                 = json_encode($claim);
        $data_save['is_package']            = $product_package == 'on' ? 1:0;
        $data_save['created_at']            = date('Y-m-d H:i:s');
        $data_save['created_by']            = $request->id_user_login;
        $data_save['daftar_bengkel']        = $rekanan_bengkel_file;
        if(!empty($m_product_code)){
            $data_save['m_product_code']        = $m_product_code->code;
        }

        if(!empty($m_sub_product_code)){
            $data_save['m_sub_product_code']    = $m_sub_product_code->code;
        }

        if(!empty($m_penanggung_code)){
            $data_save['m_penanggung_code']     = $m_penanggung_code->code;
        }
        $data_save['estimation_rate']               = $rate_estimation;
        $data_save['estimation_rate_type_periode']  = $estimation_rate_periode_type;
        $data_save['estimation_rate_value_periode'] = $estimation_rate_periode_value;
        $data_save['type_estimation_rate']          = $select_type_rate_estimasi;
        $data_save['url']                           = $url_product;

        $perluasan = array_filter($perluasan_product);
        DB::beginTransaction();
        try{
            $save_product = M_ProductPenanggung::insertGetId($data_save);
            // $save_product = 1;
            $save_perluasan = [];
            if($is_available_package_product){
                foreach($is_available_package_product as $keypolis => $valpolis){
                    $m_perluasan_code = M_Perluasan::where('id',$keypolis)->select('code')->first();

                    $save_perluasan['created_at']       = date('Y-m-d H:i:s');
                    $save_perluasan['created_by']       = $request->id_user_login;
                    if($valpolis = 'on'){
                        $save_perluasan['is_available'] = 1;
                    }else{
                        $save_perluasan['is_available'] = 0;
                    }

                    $save_perluasan['m_product_penanggung_id']      = $save_product;
                    $save_perluasan['m_perluasan_id']               = $keypolis;
                    $save_perluasan['m_perluasan_code']             = $m_perluasan_code->code;
                    $save_perluasan['m_product_penanggung_code']    = $product_code;

                    $perluasan_save = M_ProductPenanggungPerluasan::insertGetId($save_perluasan);
                    $save_rate      = [];


                    $save_child_rate['created_at']                          = date('Y-m-d H:i:s');
                    $save_child_rate['created_by']                          = $request->id_user_login;
                    $save_child_rate['m_product_penanggung_perluasan_id']   = $perluasan_save;
                    $save_child_rate['m_perluasan_code']                    = $m_perluasan_code->code;

                    if(array_key_exists($keypolis,$rate_package_product)){
                        $save_child_rate['rate']            = str_replace(',','.',$rate_package_product[$keypolis]);
                        $save_rate[]                        = $save_child_rate;
                    }else{
                        if(array_key_exists($keypolis,$perluasan)){
                            foreach($perluasan[$keypolis] as $key_p => $val_p){
                                $save_child_rate['coverage_value']  = $val_p->value;
                                $save_child_rate['rate']            = $val_p->rate;
                                $save_rate[]                        = $save_child_rate;
                            }
                        }
                    }
                    $save_rate = DB::table('m_product_penanggung_perluasan_rate')->insert($save_rate);
                }
            }

            $query = DB::getQueryLog();
            DB::commit();

            if($rekanan_bengkel){
                Storage::putFileAs('public/product-polis/rekanan-bengkel', $rekanan_bengkel, $rekanan_bengkel_file);
            }
            if($documentsnk){
                Storage::putFileAs('public/product-polis/document-snk', $documentsnk, $documentsnkfile);
            }
            if($documentclaim){
                Storage::putFileAs('public/product-polis/document-claim', $documentclaim, $documentclaimfile);
            }
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line :'.$e->getLine(),
                'status'        => "error",
            ],500);
        }
    }

    public function edit(Request $request)
    {
        $id                 = decrypt($request->id);

        Cache::forget('perluasan_product_'.$id);

        $get_data           = M_ProductPenanggung::where('id',$id)->first();

        $get_product        = DB::table('m_product')->where('id',$get_data->m_product_id)->first();

        $get_sub_product    = M_SubProduct::where('id',$get_data->m_sub_product_id)->first();

        $get_penanggung     = DB::table('m_penanggung')->where('id',$get_data->m_penanggung_id)->first();


        $data['get_data']           = $get_data;
        $data['get_product']        = $get_product;
        $data['get_sub_product']    = $get_sub_product;
        $data['get_penanggung']     = $get_penanggung;


        $get_product_perluasan = M_ProductPenanggungPerluasan::with(['get_perluasan'])->where('m_product_penanggung_id',$get_data->id)->get();
        $data_array = [];
        foreach($get_product_perluasan as $key => $val)
        {
            $child['id']                        = $val['id'];
            $child['m_product_penanggung_id']   = $val['m_product_penanggung_id'];
            $child['m_perluasan_id']            = $val['m_perluasan_id'];
            $child['perluasan_name']            = $val['get_perluasan']->name;
            $child['rate_or_value']             = $val['get_perluasan']->rate_or_value;
            $child['m_product_penanggung_code'] = $val['m_product_penanggung_code'];
            $child['m_perluasan_code']          = $val['m_perluasan_code'];
            $child['is_available']              = $val['is_available'];
            $child['is_delete']                 = 0;


            $get_rate = DB::table('m_product_penanggung_perluasan_rate')->where('m_product_penanggung_perluasan_id',$val['id'])->orderBy('coverage_value','ASC')->get();

            $rate_array = [];
            foreach($get_rate as $rate => $val_rate){
                $rate_array[] = [
                    'id_rate'           => $val_rate->id,
                    'rate'              => $val_rate->rate,
                    'coverage_value'    => $val_rate->coverage_value,
                    'is_delete'         => 0
                ];
            }
            $child['rate'] = $rate_array;
            $data_array[]                       = $child;

        }
        // dd($d)
        $data['perluasan'] = $data_array;
        // dd($data);
        Cache::put('perluasan_product_'.$id, $data_array, 3600);
        return view('Penanggung.Product.v_edit_product_penanggung',$data);
    }

    public function getCoverage(Request $request)
    {
        $id_product_penanggung  = $request->m_product_penanggung_id;
        $id_perluasan           = $request->id_perluasan;

        $get_cache  = Cache::get('perluasan_product_'.$id_product_penanggung);
        $get_data   = collect($get_cache)->where('m_perluasan_id',$id_perluasan)->all();
        // dd($get_data);
        foreach($get_data as $rate => $valrate)
        {
            $array_rate = [];

            // $get_rate = collect($valrate['rate'])->sortBy('coverage_value','ASC')->all();
            foreach($valrate['rate'] as $kyrate => $rateVal)
            {
                if($rateVal['is_delete'] == 0){
                    $array_rate[] = [
                        "id_rate"               => $rateVal['id_rate'],
                        "rate"                  => $rateVal['rate'],
                        "coverage_value"        => $rateVal['coverage_value'],
                        "is_delete"             => $rateVal['is_delete']
                    ];
                }
            }
        }
        return response()->json([
            'message'   => 'success',
            'status'    => 'success',
            'data'      => $array_rate
        ],200);
    }


    public function addRate(Request $request)
    {
        $id_perluasan               = $request->id_perluasan;
        $unmusk_value               = $request->unmusk_value;
        $unmusk_rate                = $request->unmusk_rate;
        $m_product_penanggung_id    = $request->m_product_penanggung_id;


        $get_cache  = Cache::get('perluasan_product_'.$m_product_penanggung_id);

        foreach($get_cache as $row => $val)
        {

            $get_product_perluasan = M_Perluasan::where('id',$val['m_perluasan_id'])->first();
            $child['id']                        = $val['id'];
            $child['m_product_penanggung_id']   = $val['m_product_penanggung_id'];
            $child['m_perluasan_id']            = $val['m_perluasan_id'];
            $child['perluasan_name']            = $get_product_perluasan->name;
            $child['rate_or_value']             = $get_product_perluasan->rate_or_value;
            $child['m_product_penanggung_code'] = $val['m_product_penanggung_code'];
            $child['m_perluasan_code']          = $val['m_perluasan_code'];
            $child['is_available']              = $val['is_available'];
            $child['is_delete']                 = 0;

            $rate_array = [];
            foreach($val['rate'] as $rowrate => $valrate)
            {


                $sub_rate_array['id_rate']          = $valrate['id_rate'];
                $sub_rate_array['rate']             = $valrate['rate'];
                $sub_rate_array['coverage_value']   = $valrate['coverage_value'];


                $sub_rate_array['is_delete']        = $valrate['is_delete'];
                $rate_array[] = $sub_rate_array;

                if($id_perluasan == $val['m_perluasan_id'])
                {
                    $next_id = $valrate['id_rate']+1;
                }
            }

            if($id_perluasan == $val['m_perluasan_id'])
            {
                $rate_array[] = [
                    'id_rate'           => $next_id,
                    'rate'              => $unmusk_rate,
                    'coverage_value'    => $unmusk_value,
                    'is_delete'         => 0
                ];
            };
            $child['rate'] = $rate_array;
            $data_array[]                       = $child;
        }
        // dd($data_array);
        Cache::forget('perluasan_product_'.$m_product_penanggung_id);
        Cache::put('perluasan_product_'.$m_product_penanggung_id, $data_array, 3600);

        // $get_cache = Cache::get('perluasan_product_'.$m_product_penanggung_id);
        // dd($get_cache);
        return response()->json([
            'message'   => 'success',
            'status'    => 'success deleted',
        ],200);
    }
    public function deleteRateValue(Request $request)
    {
        // dd($request->all());

        $id_rate                    = $request->id_rate;
        $m_product_penanggung_id    = $request->m_product_penanggung_id;
        $id_perluasan               = $request->id_perluasan;


        $get_cache  = Cache::get('perluasan_product_'.$m_product_penanggung_id);

        foreach($get_cache as $row => $val)
        {

            $get_product_perluasan = M_Perluasan::where('id',$val['m_perluasan_id'])->first();
            $child['id']                        = $val['id'];
            $child['m_product_penanggung_id']   = $val['m_product_penanggung_id'];
            $child['m_perluasan_id']            = $val['m_perluasan_id'];
            $child['perluasan_name']            = $get_product_perluasan->name;
            $child['rate_or_value']             = $get_product_perluasan->rate_or_value;
            $child['m_product_penanggung_code'] = $val['m_product_penanggung_code'];
            $child['m_perluasan_code']          = $val['m_perluasan_code'];
            $child['is_available']              = $val['is_available'];
            $child['is_delete']                 = 0;

            $rate_array = [];
            foreach($val['rate'] as $rowrate => $valrate)
            {


                $sub_rate_array['id_rate']          = $valrate['id_rate'];
                $sub_rate_array['rate']             = $valrate['rate'];
                $sub_rate_array['coverage_value']   = $valrate['coverage_value'];


                $sub_rate_array['is_delete']        = $valrate['is_delete'];
                if($val['m_perluasan_id'] == $id_perluasan){
                    if($valrate['id_rate'] == $id_rate){
                        $sub_rate_array['is_delete']  = 1;
                    }
                }

                $rate_array[] = $sub_rate_array;
            }
            $child['rate'] = $rate_array;
            $data_array[]                       = $child;
        }
        // dd($data_array);
        Cache::forget('perluasan_product_'.$m_product_penanggung_id);
        Cache::put('perluasan_product_'.$m_product_penanggung_id, $data_array, 3600);

        // $get_cache = Cache::get('perluasan_product_'.$m_product_penanggung_id);
        // dd($get_cache);
        return response()->json([
            'message'   => 'success',
            'status'    => 'success deleted',
        ],200);
    }

    public function update(Request $request)
    {
        // dd($request->all());

        $id                             = $request->id;
        $sub_product                    = $request->sub_product;
        $select_penanggung              = $request->select_penanggung;
        $product_name                   = $request->product_name;
        $product_code                   = $request->product_code;
        $description                    = $request->description;
        $manfaat_perlindungan           = $request->manfaat_perlindungan;
        $nameithoutbnfts                = $request->nameithoutbnfts;
        $name_fitur                     = $request->name_fitur;
        $description_fitur              = $request->description_fitur;
        $description_snk                = $request->description_snk;
        $description_claim              = $request->description_claim;
        $snk_list                       = $request->snk_list;
        $claim_list                     = $request->claim_list;
        $tlpn_claim                     = $request->tlpn_claim;
        $wa_claim                       = $request->wa_claim;
        $email_claim                    = $request->email_claim;
        $application_claim              = $request->application_claim;
        $rate_package_product           = $request->rate_package_product;
        $discount_package_product       = $request->discount_package_product;
        $is_available_package_product   = $request->is_available_package_product;
        $rekanan_bengkel                = $request->rekanan_bengkel;
        $documentsnk                    = $request->documentsnk;
        $documentclaim                  = $request->documentclaim;
        $product_package                = $request->product_package;

        $get_data = M_ProductPenanggung::where('id',$id)->first();
        $old_rekanan_bengkel = $get_data->daftar_bengkel;

        $get_snk                = json_decode($get_data->tnc);
        $old_document_snk       = $get_snk->file;


        $get_file_claim         = json_decode($get_data->claim);
        $old_document_claim     = $get_file_claim->file;
        if($rekanan_bengkel)
        {
            $rekanan_bengkel_file = generate_url(strtolower('reakanan-bengkel-polis-code'.$product_code)).'.'.$rekanan_bengkel->extension();
            $rekanan_bengkel_file = strtolower($rekanan_bengkel_file);
        }else{
            $rekanan_bengkel_file = $old_rekanan_bengkel;
        }

        if($documentsnk)
        {
            $documentsnkfile = generate_url(strtolower('snk-polis-code'.$product_code)).'.'.$documentsnk->extension();
            $documentsnkfile = strtolower($documentsnkfile);
        }else{
            $documentsnkfile = $old_document_snk;
        }

        if($documentclaim)
        {
            $documentclaimfile = generate_url(strtolower('claim-polis-code'.$product_code)).'.'.$documentclaim->extension();
            $documentclaimfile = strtolower($documentclaimfile);
        }else{
            $documentclaimfile = $old_document_claim;
        }

        // ------------------tnc---------------
        $tnc['description'] = $description_snk;
        $tnc['file']        = $documentsnkfile;
        $tnc['fitur']       = $snk_list;
        // --------------end tnc---------------

        //-----------------claim--------------------
        $claim['description']   = $description_claim;
        $claim['file']          = $documentclaimfile;
        $claim['fitur']         = $claim_list;
        $claim['contact']       = array(
            'telepon'           => $tlpn_claim,
            'whatsapp'          => $wa_claim,
            'email_claim'       => $email_claim,
            'aplication'        => $application_claim
        );

        $fitur = $name_fitur;
        $fitur_array = [];
        foreach($fitur as $key => $val){
            $fitur_array[] = [
                'name'          => $val,
                'description'   => $description_fitur[$key]
            ];
        }

        $m_sub_product_code = M_SubProduct::where('id',$sub_product)->select('code')->first();
        $m_penanggung_code  = DB::table('m_penanggung')->where('id',$select_penanggung)->select('code')->first();

        $data_save['m_penanggung_id']       = $select_penanggung;
        $data_save['m_sub_product_id']      = $sub_product;
        $data_save['code']                  = $product_code;
        $data_save['name']                  = $product_name;
        $data_save['description']           = $description;
        $data_save['benefit']               = json_encode($manfaat_perlindungan);
        $data_save['benefit_exception']     = json_encode($nameithoutbnfts);
        $data_save['fitur']                 = json_encode($fitur_array);
        $data_save['tnc']                   = json_encode($tnc);
        $data_save['claim']                 = json_encode($claim);
        $data_save['is_package']            = $product_package == 'on' ? 1:0;
        $data_save['created_at']            = date('Y-m-d H:i:s');
        $data_save['created_by']            = $request->id_user_login;
        $data_save['daftar_bengkel']        = $rekanan_bengkel_file;

        if(!empty($m_sub_product_code)){
            $data_save['m_sub_product_code']    = $m_sub_product_code->code;
        }

        if(!empty($m_penanggung_code)){
            $data_save['m_penanggung_code']     = $m_penanggung_code->code;
        }

        DB::beginTransaction();
        try{
            $save_product = M_ProductPenanggung::where('id',$id)->update($data_save);

            $save_perluasan = [];

            $get_perluasan = M_ProductPenanggungPerluasan::where('m_product_penanggung_id',$id)->get();

            foreach($get_perluasan as $perluasan)
            {
                $getRateDelete      = M_ProductPerluasanRate::where('m_product_penanggung_perluasan_id',$perluasan->id)->forceDelete();
                $perluasanDelete    = M_ProductPenanggungPerluasan::where('id',$perluasan->id)->forceDelete();
            }
            foreach($is_available_package_product as $keypolis => $valpolis){
                $m_perluasan_code = M_Perluasan::where('id',$keypolis)->select('code')->first();

                $save_perluasan['created_at']       = date('Y-m-d H:i:s');
                $save_perluasan['created_by']       = $request->id_user_login;
                if($valpolis = 'on'){
                    $save_perluasan['is_available'] = 1;
                }else{
                    $save_perluasan['is_available'] = 0;
                }

                $save_perluasan['m_product_penanggung_id']      = $id;
                $save_perluasan['m_perluasan_id']               = $keypolis;
                $save_perluasan['m_perluasan_code']             = $m_perluasan_code->code;
                $save_perluasan['m_product_penanggung_code']    = $product_code;

                $perluasan_save = M_ProductPenanggungPerluasan::insertGetId($save_perluasan);
                $save_rate      = [];


                $save_child_rate['created_at']                          = date('Y-m-d H:i:s');
                $save_child_rate['created_by']                          = $request->id_user_login;
                $save_child_rate['m_product_penanggung_perluasan_id']   = $perluasan_save;
                $save_child_rate['m_perluasan_code']                    = $m_perluasan_code->code;
                $save_child_rate['m_perluasan_id']                      = $keypolis;
                if(array_key_exists($keypolis,$rate_package_product)){
                    $save_child_rate['rate']            = str_replace(',','.',$rate_package_product[$keypolis]);
                    $save_rate[]                        = $save_child_rate;
                }else{

                    $get_cache  = Cache::get('perluasan_product_'.$id);
                    $get_rate_cache = collect($get_cache)->where('m_perluasan_id',$keypolis)->all();

                    foreach($get_rate_cache as $kc => $valc)
                    {
                        foreach($valc['rate'] as $kr => $valr)
                        {
                            if($valr['is_delete'] == 0){
                                $save_child_rate['coverage_value']  = $valr['coverage_value'];
                                $save_child_rate['rate']            = $valr['rate'];
                                $save_rate[]                        = $save_child_rate;
                            }
                        }
                    }
                }
                $save_rate = DB::table('m_product_penanggung_perluasan_rate')->insert($save_rate);
            }
            DB::commit();
            if($rekanan_bengkel){
                Storage::delete('/public/product-polis/rekanan-bengkel/'.$old_rekanan_bengkel);
                Storage::putFileAs('public/product-polis/rekanan-bengkel', $rekanan_bengkel, $rekanan_bengkel_file);
            }
            if($documentsnk){
                Storage::delete('/public/product-polis/document-snk/'.$old_document_snk);
                Storage::putFileAs('public/product-polis/document-snk', $documentsnk, $documentsnkfile);
            }
            if($documentclaim){
                Storage::delete('/public/product-polis/document-claim/'.$old_document_claim);
                Storage::putFileAs('public/product-polis/document-claim', $documentclaim, $documentclaimfile);
            }
            return response()->json([
                'message'  => 'success',
                'status'   => 'success',
            ],200);

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message'       => $e->getMessage().' Line :'.$e->getLine(),
                'status'        => "error",
            ],500);
        }
    }
}
