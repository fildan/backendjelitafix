<?php

namespace App\Http\Controllers;

use App\Models\M_UserBackend;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class LoginController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('Login');
    }

    public function action(Request $request)
    {
        // dd($request->all());
        $username = $request->username;
        $password = $request->password;

        $checked    = M_UserBackend::where('username',$username)->first();
        if(!$checked){
            return response()->json([
                'message'   => 'User Not Found',
                'status'    => 'error',
                'code'      => 404
            ],200);
        }
        if( !Hash::check($password, $checked->password)){
            return response()->json([
                'message'   => 'Wrong Password',
                'status'    => 'error',
                'code'      => 400
            ],200);
        }

        $userdata = array(
            'id_user'       => $checked->id,
            'username'      => $checked->username,
            'name'          => $checked->name,
            'nik'           => $checked->nik,
            'picture'       => $checked->picture,
        );
        $request->session()->put('user_data', $userdata);

        return response()->json([
            'message'   => 'Login Success',
            'status'    => 'error',
            'code'      => 200
        ],200);
    }
}
