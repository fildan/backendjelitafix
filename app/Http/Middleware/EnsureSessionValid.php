<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\M_UserBackend;

class EnsureSessionValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $session = $request->session()->get('user_data');
        // dd($session);
        if(empty($session)){
            return redirect()->route('sign-in')->with('error','Please Login First !');
        }else{
            $checked = M_UserBackend::where('id',$session['id_user'])->first();
            if(!$checked){
                return redirect()->route('sign-in')->with('error','User Not Found !');
            }
        }

        if(!$request->id_user_login){
            $request->merge(['id_user_login'=>$session['id_user']]);
        }
        return $next($request);
    }
}
