<?php

use App\Models\M_DocPengajuan;
use App\Models\M_Product;
use App\Models\M_SubProduct;

    function generate_url($string=null)
    {
        $string = str_replace(' ', '-', $string);

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    function getDocPengajuan($cob=null,$sub_cob=null,$type_doc=null,$is_new_object=null)
    {
        $url_cob                = $cob;
        $url_sub_cob            = $sub_cob;
        $is_new_object          = $is_new_object;
        $company_or_project     = $type_doc;

        // dd($request->all());
        if($url_cob){
            $get_cob_id     = M_Product::where('url',$url_cob)->first();
            if(!$get_cob_id)
            {
                return response()->json([
                    'message'   => 'COB Not Found',
                    'status'    => 'error',
                    'code'      => 404,
                    'data'      => null
                ], 200);
            }
        }
        if($url_sub_cob)
        {
            $get_sub_cob_id = M_SubProduct::where('url',$url_sub_cob)->first();
            if(!$get_sub_cob_id){
                return response()->json([
                    'message'   => 'SUB COB Not Found',
                    'status'    => 'error',
                    'code'      => 404,
                    'data'      => null
                ], 200);
            }
        }
        $get_data       = M_DocPengajuan::with(
                            [
                                'get_product'
                                ,
                                'get_sub_product'
                            ]
                            );



        if($is_new_object){
            // dd($is_new_object);
            $get_data = $get_data->where('is_for_new_object',$is_new_object);
        }else{
            $get_data = $get_data->where('is_for_new_object',0);
        }

        // if($url_cob)
        $get_data = $get_data->where('is_company_or_project_document',$company_or_project)->orderBy('sort','ASC')->get();



        $data = array();

        $doc_hehe = '';
        $hehe = [];
        foreach($get_data as $row)
        {

            if($row->doc_multiple_type_product == 0)
            {

                if($get_cob_id->id == $row->m_product_id)
                {
                    if($row->doc_multiple_type_sub_product == 1)
                    {
                        $data[] = [
                            'doc_id'                            => $row->id,
                            'doc_code'                          => $row->code,
                            'doc_name'                          => $row->name,
                            'is_company_or_project_document'    => $row->is_company_or_project_document,
                            'is_for_new_object'                 => $row->is_for_new_object,
                            'contoh_document'                   => $row->contoh_document
                        ];

                    }else if($row->doc_multiple_type_sub_product == 2)
                    {
                        if($url_sub_cob){
                            $multiple_sub_product_hehe = (array) json_decode($row->multiple_sub_product);
                            if (in_array($get_sub_cob_id->id, $multiple_sub_product_hehe['id'])) {
                                $data[] = [
                                    'doc_id'                            => $row->id,
                                    'doc_code'                          => $row->code,
                                    'doc_name'                          => $row->name,
                                    'is_company_or_project_document'    => $row->is_company_or_project_document,
                                    'is_for_new_object'                 => $row->is_for_new_object,
                                    'contoh_document'                   => $row->contoh_document
                                ];


                            }
                        }
                    }else{
                        if($get_sub_cob_id->id == $row->m_sub_product_id)
                        {
                            $data[] = [
                                'doc_id'                            => $row->id,
                                'doc_code'                          => $row->code,
                                'doc_name'                          => $row->name,
                                'is_company_or_project_document'    => $row->is_company_or_project_document,
                                'is_for_new_object'                 => $row->is_for_new_object,
                                'contoh_document'                   => $row->contoh_document
                            ];


                        }
                    }
                }
            }
            else if($row->doc_multiple_type_product == 1)
            {
                if($row->doc_multiple_type_sub_product == 1)
                {
                    $data[] = [
                        'doc_id'                            => $row->id,
                        'doc_code'                          => $row->code,
                        'doc_name'                          => $row->name,
                        'is_company_or_project_document'    => $row->is_company_or_project_document,
                        'is_for_new_object'                 => $row->is_for_new_object,
                        'contoh_document'                   => $row->contoh_document
                    ];
                }
                else if($row->doc_multiple_type_sub_product == 2)
                {
                    if($url_sub_cob){
                        $multiple_sub_product = json_decode($row->multiple_sub_product);
                        if (in_array($get_sub_cob_id->id, $multiple_sub_product['id'])) {
                            $data[] = [
                                'doc_id'                            => $row->id,
                                'doc_code'                          => $row->code,
                                'doc_name'                          => $row->name,
                                'is_company_or_project_document'    => $row->is_company_or_project_document,
                                'is_for_new_object'                 => $row->is_for_new_object,
                                'contoh_document'                   => $row->contoh_document
                            ];


                        }
                    }
                }else{
                    if($url_sub_cob){
                        if($row->m_sub_product_code == $get_sub_cob_id->code)
                        {
                            $data[] = [
                                'doc_id'                            => $row->id,
                                'doc_code'                          => $row->code,
                                'doc_name'                          => $row->name,
                                'is_company_or_project_document'    => $row->is_company_or_project_document,
                                'is_for_new_object'                 => $row->is_for_new_object,
                                'contoh_document'                   => $row->contoh_document
                            ];
                        }
                    }
                }

            }
            else if($row->doc_multiple_type_product == 2)
            {
                if($get_cob_id){
                    $some_product = json_decode($row->multiple_product);
                    if (in_array($get_cob_id->id, $some_product['id'])) {
                        $data[] = [
                            'doc_id'                            => $row->id,
                            'doc_code'                          => $row->code,
                            'doc_name'                          => $row->name,
                            'is_company_or_project_document'    => $row->is_company_or_project_document,
                            'is_for_new_object'                 => $row->is_for_new_object,
                            'contoh_document'                   => $row->contoh_document
                        ];


                    }
                }
            }

        }

        return $data;
    }

    function getDocCompany()
    {
        $get_data       = M_DocPengajuan::with(
            [
                'get_product'
                ,
                'get_sub_product'
            ]
            )->where('is_company_or_project_document',1)->orderBy('sort','ASC')->get();

        return $get_data;
    }
?>
