@extends('layout.app')
@section('title', 'Master Product')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                            User Backend Data
                            <small></small>
                        </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" id="addData">Add User</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="table-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                {{-- <th width="10px"></th> --}}
                                                <th width="10px">No</th>
                                                <th>NIK</th>
                                                <th>Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Informasi</h5>
                        <button type="button" class="close_modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-2 col-form-label">Name Of User</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="name" id="name_user" required class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-6 col-sm-6">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Username</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="username" id="username_user" required class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6 col-sm-6">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-4 col-form-label">SJU Users NIK</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="nik" id="nik_user" required class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 col-sm-6">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Users Email</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="email" id="email" required class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-6">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Users Phone</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="phone" id="phone" required class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12 col-sm-12" id="is-edit-pswd">
                                                <div class="form-group row">
                                                    <label for="is-edit" class="col-sm-2 col-form-label">Edit Password ?</label>
                                                    <div class="col-sm-10">
                                                        <label>
                                                            <input type="checkbox" name="is_edit_pswd" id="is_edit_pswd" class="js-switch"/>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 col-sm-6">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Password</label>
                                                    <div class="col-sm-8">
                                                        <input type="password" name="password" id="password" required class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-6">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Re Password</label>
                                                    <div class="col-sm-8">
                                                        <input type="password" name="re-password" id="re-password" required class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection()
@section('script')
<script>
    var url_save = "{{ route('user-backend.save') }}";
    var table = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('user-backend.get-list') }}",
        'columns': [

            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'nik' ,name:'nik', "className": "v-align-tables"},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'username' ,name:'username', "className": "v-align-tables"},
            { data: 'email' ,name:'email', "className": "v-align-tables"},
            { data: 'phone' ,name:'phone', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });

    $('#addData').on('click',function(){
        modal_show();
        clearModal();
        nondisabledpswd();
        $('#is-edit-pswd').hide()
        url_save = "{{ route('user-backend.save') }}";
    })
    $('#save').on('click',function(){

        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $('#fm').submit()
            }
        });
	})

    $("#fm").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : url_save,
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                clearModal()
                tableReload()
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        });
    });

    $('.close_modal').on('click',function(){
        modal_close();
    })
    function modal_close(){
        clearModal();
        $('#modalData').modal('hide');
    }

    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}

    function clearModal(){
	    $('#modalData').modal('hide');
        $('#fm').find('input:text, input:password, select')
                    .each(function () {
                        $(this).val('');
                    });

    }

    function editClick($id){
        $.ajax({
            url : '{{route("user-backend.get-data")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {
                $('#name_user').val(response.data.name)
                $('#username_user').val(response.data.username)
                $('#nik_user').val(response.data.nik)
                $('#email').val(response.data.email)
                $('#phone').val(response.data.phone)
                modal_show()
                $('#is-edit-pswd').show()
                $('#password').val(response.data.password)
                $('#re-password').val(response.data.password)
                disabledpswd()
                url_save = "{{ route('user-backend.update') }}?id="+$id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    function deleteClick(id)
    {
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('user-backend.delete')}}?id="+id;
                $.ajax({
                    url : url,
                    type: 'GET',
                    data:{
                        id:id
                    },
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                        tableReload()
                    }
                });
            }
        });
    }
    function tableReload(){
        table.ajax.reload(null,false);
    }

    $('#is_edit_pswd').on('click',function(){
        if($(this).is(':checked')){
            nondisabledpswd()
        }else{
            disabledpswd()
        }
    })
    function nondisabledpswd(){
        $('#password').val('')
        $('#re-password').val('')
        $('#password').removeAttr('disabled')
        $('#re-password').removeAttr('disabled')
    }

    function disabledpswd(){

        $('#password').attr('disabled','disabled')
        $('#re-password').attr('disabled','disabled')
    }
</script>
@endsection()
