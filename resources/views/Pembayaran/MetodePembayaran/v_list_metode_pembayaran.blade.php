@extends('layout.app')
@section('title', 'Master Tipe Pembayaran')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>Metode Pembayaran<small></small></h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" id="addData">Tambah Metode Pembayaran</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="province-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px"></th>
                                                <th width="10px">No</th>
                                                <th>Tipe Pembayaran</th>
                                                <th>Nama Bank</th>
                                                <th>Nama Metode Pembayaran</th>
                                                <th>Kode Payment Gateway</th>
                                                <th>No Rekening SJU</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                        <button type="button" class="close_modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">


                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Tipe Pembayaran</label>
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="payment_type" id="payment_type" style="width: 100%">

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Bank</label>
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="bank_name" id="bank_name" style="width: 100%">

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Nama Metode</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 pga-code">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Kode Payment Gateway</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="payment_gateway_code" id="payment_gateway_code" placeholder="">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12 col-sm-12 no-rek">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">No Rekening</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="no_rek" id="no_rek" placeholder="">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save">Save</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="modalGuide" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                        <button type="button" class="close_modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fmGuide" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">


                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Metode Pembayaran</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="metode_pembayaran_name" id="metode_pembayaran_name" readonly>
                                                        <input type="hidden" class="form-control" name="metode_pembayaran_id" id="metode_pembayaran_id" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Nama Guide</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="guide_name" id="guide_name" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th style="vertical-align: middle">Guide</th>
                                                    <th style="vertical-align: middle" width="20px" align="center">Action</th>
                                                    <th style="vertical-align: middle" width="20px">
                                                        <a class="btn btn-primary" id="addRow"> <i class="fa fa-plus"></i> </a>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="row-perluasan">
                                                </tbody>
                                            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal">Close</button>
                        <button type="button" class="btn btn-primary" id="saveGuide">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
<script type="text/javascript">
    var url_save = "";
    var table = $('#province-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('metode-pembayaran.get-list') }}",
        'columns': [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '',
                searchable: false,
            },
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'payment_type' ,name:'payment_type', "className": "v-align-tables"},
            { data: 'bank_name' ,name:'bank_name', "className": "v-align-tables"},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'pga_code' ,name:'pga_code', "className": "v-align-tables"},
            { data: 'no_rekening' ,name:'no_rekening', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });


    $('#province-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
        // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
    /* Formatting function for row details - modify as you need */

    function format ( rowData ) {
        var data = rowData.get_payment_bank_step;

        var div   = '';
            div  +=     '<div class="pull-left">'
            div  +=         '<button type="button" class="btn btn-primary btn-sm addGuide" code="'+rowData.code+'" row-name="'+rowData.name+'">Tambah Guide</button>';
            div  +=     '</div>';
            div  +=     '<div class="card-box table-responsive">';
            div  +=         '<table table-striped table-bordered>';
        $.each( data, function( key, value ) {
            div  +=             '<tr class="">';
            div  +=                 '<td class="fz12" style="vertical-align:middle;width:15%">';
            div  +=                     value.name;
            div  +=                 '</td>';
            div  +=                 '<td class="fz11" style="vertical-align:middle;">';
            div  +=                     value.step;
            div  +=                 '</td>';
            div  +=                 '<td class="fz11" style="vertical-align:middle;width:5%">';
            div  +=                     "<a href='#' class='editGuide' row-id='"+value.id+"' row-name='"+rowData.name+"'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>";
            div  +=                     "|| <a href='#' class='deleteGuide' row-id='"+value.id+"' row-name='"+rowData.name+"'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
            div  +=                 '</td>';
            div  +=             '</tr>';
        })
        div      +=         "</table>";
        div      +=     '</div>';
        return div;
    }


    $(document).ready(function(){
        $('#payment_type').select2({
            placeholder: '--Choose--',
            dropdownParent: $('#modalData'),
            allowClear: true,
            ajax: {
                url: "{{ route('metode-pembayaran.get-tipe-pembayaran')}}?filterDB=yes",
                dataType: 'json',

                processResults: function (data) {

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.text,
                                id: item.id,
                                id_encrypt:item.id_encrypt,
                                code:item.code
                            }
                        })
                    };
                },
                cache: true

            }
        }).on('select2:select', function (evt) {
            if(evt.params.data.code == 'TFM'){
                $('.no-rek').show();
                $('.pga-code').hide();
            }else{
                $('.no-rek').hide();
                $('.pga-code').show();
            }
        });

        $('#bank_name').select2({
            placeholder: '--Choose--',
            dropdownParent: $('#modalData'),
            allowClear: true,
            ajax: {
                url: "{{ route('metode-pembayaran.get-bank')}}?filterDB=yes",
                dataType: 'json',

                processResults: function (data) {

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.text,
                                id: item.id,
                                id_encrypt:item.id_encrypt,
                                code:item.code
                            }
                        })
                    };
                },
                cache: true

            }
        }).on('select2:select', function (evt) {

        });
    })

    $('#addData').on('click',function(){
        modal_show();
        clearModal();
        url_save = "{{route('metode-pembayaran.save')}}";
    })

    function editClick($id=null){
        $.ajax({
            url : '{{route("metode-pembayaran.edit")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {
                // $('#province_name').val(response.data.name);
                $('#name').val(response.data.name)
                if(response.data.m_methode_payment_code == 'TFM'){
                    $('.no-rek').show();
                    $('.pga-code').hide();
                    $('#no_rek').val(response.data.no_rekening)
                }else{
                    $('.no-rek').hide();
                    $('.pga-code').show();

                    $('#payment_gateway_code').val(response.data.code)
                }

                var payment_type = new Option(response.data.get_master_methode_payment.description, response.data.m_methode_payment_id, false, false);
                $('#payment_type').append(payment_type).trigger('change');

                var bank_name = new Option(response.data.get_bank.name, response.data.m_bank_id, false, false);
                $('#bank_name').append(bank_name).trigger('change');

                modal_show();
                url_save = "{{route('metode-pembayaran.update')}}?id="+$id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    $('#save').on('click',function(){

        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $('#fm').submit()
            }
        });
	})
	$("#fm").submit(function(event){
		event.preventDefault();
        var form_data =  new FormData(this);
		$.ajax({
			url : url_save,
			type: 'POST',
			data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {

                if(response.status == 'success'){
                    swalSuccess('Success','Success To Save');
                    modal_close();
                    tableReload();
                }else{
                    swalError('Failed', response.message)
                }
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
		});
	});

    function deleteClick(id)
    {
        // alert(id)
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('metode-pembayaran.delete')}}";
                $.ajax({
                    url : url,
                    type: 'POST',
                    data:{
                        id:id
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        modal_close();
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                    }
                });
            }
        });
    }
    $('.close_modal').on('click',function(){
        modal_close();
    })
    function modal_close(){
        clearModal();
        $('#modalData').modal('hide');
    }
    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}

    function clearModal(){
        $('#province_name').val('')
        $('#name').val('')
        $('#payment_gateway_code').val('')
    }
    function tableReload(){
        table.ajax.reload(null,false);
    }

    var url_save_guide = "";
    $(document).on('click','.addGuide',function(){
        $('#metode_pembayaran_name').val($(this).attr('row-name'))
        $('#metode_pembayaran_id').val($(this).attr('code'))
        $('#modalGuide').modal('show')

        url_save_guide = "{{ route('metode-pembayaran.save-guide') }}"
    })


    $(document).on('click','.editGuide',function(){
        $.ajax({
            url : '{{route("metode-pembayaran.edit-guide")}}',
            type: 'GET',
            data : {
                id: $(this).attr('row-id')
            },
            success: (response) => {


                // modal_show();

                $('#metode_pembayaran_name').val($(this).attr('row-name'))
                $('#guide_name').val(response.data.name)
                var step = JSON.parse(response.data.step);
                buildStepedit(step);
                $('#modalGuide').modal('show')

                url_save_guide = "{{route('metode-pembayaran.update-guide')}}?id="+$(this).attr('row-id');
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    })

    $(document).on('click','.deleteGuide',function(){
        // alert('hehe');
        // return false;
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $.ajax({
                    url : '{{route("metode-pembayaran.delete-guide")}}',
                    type: 'POST',
                    data:{
                        id:$(this).attr('row-id')
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        window.location.reload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                    }
                });
            }
        });
    })
    function buildStepedit(data=null){

        var rowtd    = "";
        var rowNumber = 0;
        $.each( data, function( key, value ) {
            rowNumber   +=  1;
            rowtd   += "<tr id='perluasan-list-step-"+rowNumber+"'>";
            rowtd   +=      "<td style='vertical-align:middle'> <textarea class='form-control' name='description[]'>"+value+"</textarea> </td>";
            rowtd   +=      "<td colspan='2' align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row' row-id='step-"+rowNumber+"'><i class='fa fa-trash'></i> </td>";
            rowtd   +=  "</tr>";
        })
        $('.row-perluasan').append(rowtd)
    }
    $('#addRow').on('click',function(){
        buildRow()
    })


    var rowNumber = 0;
    function buildRow(){
        rowNumber   += 1;
        var rowtd    = "";
            rowtd   += "<tr id='perluasan-list-"+rowNumber+"'>";
            rowtd   +=      "<td style='vertical-align:middle'> <textarea class='form-control' name='description[]'></textarea> </td>";
            rowtd   +=      "<td colspan='2' align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row' row-id='"+rowNumber+"'><i class='fa fa-trash'></i> </td>";
            rowtd   +=  "</tr>";

        $('.row-perluasan').append(rowtd)
    }

    $(document).on('click','.delete-row',function(){
        let row = $(this).attr('row-id');
        $('#perluasan-list-'+row).remove()
    })
    $('#saveGuide').on('click',function(){
        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            $('#fmGuide').submit();
        });
    })

    $("#fmGuide").submit(function(event){
		event.preventDefault();
        var form_data =  new FormData(this);
		$.ajax({
			url : url_save_guide,
			type: 'POST',
			data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {

                if(response.status == 'success'){
                    swalSuccess('Success','Success To Save');
                    window.location.reload();
                }else{
                    swalError('Failed', response.message)
                }
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
		});
	});
</script>
@endsection()
