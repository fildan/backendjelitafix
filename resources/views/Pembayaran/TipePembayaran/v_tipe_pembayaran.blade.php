@extends('layout.app')
@section('title', 'Master Tipe Pembayaran')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>Tipe Pembayaran<small></small></h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" id="addData">Add Tipe Pembayaran</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="province-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                        <button type="button" class="close_modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Kode Tipe Pembayaran</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="type_code" id="type_code" placeholder="DKI Jakarta">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Nama Tipe Pembayaran</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="type_name" id="type_name" placeholder="DKI Jakarta">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
<script type="text/javascript">
    var url_save = "";
        var table = $('#province-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('tipe-pembayaran.get-list') }}",
            'columns': [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
                { data: 'code' ,name:'code', "className": "v-align-tables"},
                { data: 'description' ,name:'description', "className": "v-align-tables"},
                { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
            ],
        });

    $('#addData').on('click',function(){
        modal_show();
        clearModal();
        url_save = "{{route('tipe-pembayaran.save')}}";
    })

    function editClick($id=null){
        $.ajax({
            url : '{{route("tipe-pembayaran.edit")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {
                // $('#province_name').val(response.data.name);
                $('#type_code').val(response.data.code)
                $('#type_name').val(response.data.description)
                modal_show();
                url_save = "{{route('tipe-pembayaran.update')}}?id="+$id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    $('#save').on('click',function(){
		var province_name = $('#province_name').val();
        if(province_name == ''){
            swalError('Info','Please Completed All Data')
        }else{
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Save ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        }
	})
	$("#fm").submit(function(event){
		event.preventDefault();
		var form_data = $(this).serialize();
		$.ajax({
			url : url_save,
			type: 'POST',
			data : form_data,
            success: (response) => {

                if(response.status == 'success'){
                    swalSuccess('Success','Success To Save');
                    modal_close();
                    tableReload();
                }else{
                    swalError('Failed', response.message)
                }
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
		});
	});

    function deleteClick(id)
    {
        // alert(id)
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('tipe-pembayaran.delete')}}";
                $.ajax({
                    url : url,
                    type: 'POST',
                    data:{
                        id:id
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        modal_close();
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                    }
                });
            }
        });
    }
    $('.close_modal').on('click',function(){
        modal_close();
    })
    function modal_close(){
        clearModal();
        $('#modalData').modal('hide');
    }
    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}

    function clearModal(){
        $('#province_name').val('')
    }
    function tableReload(){
        table.ajax.reload(null,false);
    }
</script>
@endsection()
