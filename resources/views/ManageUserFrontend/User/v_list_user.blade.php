@extends('layout.app')
@section('title', 'User List')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                Data User / Customer
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" id="addData">Add User</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="user-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>

                                                <th width="10px"></th>
                                                <th width="10px">No</th>
                                                <th>Code</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>No Telepon</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Informasi</h5>
                    <button type="button" class="close_modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="fm">
                    @csrf
                    <div class="modal-body">
                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">Nama User</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="name" id="name_user" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">File KTP</label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="ktp" id="ktp" required class="form-control">
                                                    <p class="mt-0 mb-0" style="font-size: 11px;display:none" id="file_ktp"> File Name </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">No KTP</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="no_ktp" id="no_ktp" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Email</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="email" id="email" required class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">No Telepon</label>
                                                <div class="col-sm-8">
                                                    {{-- <input type="text" name="nik" id="nik_user" required class="form-control"> --}}
                                                    <div class="input-group">

                                                        <input type="text" class="form-control" name="phone_code" id="phone_code" value="62" placeholder="62">
                                                        <div class="input-group-prepend">
                                                            <input type="text" class="form-control" name="phone_number" id="phone_number">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Jenis Kelamin</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="gender" id="gender" style="width: 100%">
                                                        <option value=""> Pilih </option>
                                                        <option value="1"> Laki - laki </option>
                                                        <option value="0"> Perempuan </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Tanggal Lahir</label>
                                                <div class="col-sm-8">
                                                    <input type="date" name="tgl_lahir" id="tgl_lahir" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Provinsi</label>
                                                <div class="col-sm-8">
                                                    <select name="province" id="select_province" required class="form-control" style="width: 100%">

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Kota</label>
                                                <div class="col-sm-8">
                                                    <select name="city" id="select_city" required class="form-control" style="width: 100%">

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">Alamat</label>
                                                <div class="col-sm-10">
                                                    <textarea name="alamat" id="alamat" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-sm-12" id="is-edit-pswd">
                                            <div class="form-group row">
                                                <label for="is-edit" class="col-sm-2 col-form-label">Edit Password ?</label>
                                                <div class="col-sm-10">
                                                    <label>
                                                        <input type="checkbox" name="is_edit_pswd" id="is_edit_pswd" class="js-switch"/>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Password</label>
                                                <div class="col-sm-8">
                                                    <input type="password" name="password" id="password" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Re Password</label>
                                                <div class="col-sm-8">
                                                    <input type="password" name="re-password" id="re-password" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close_modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
<script>

    $(document).ready(function(){
        $('#select_province').select2({
			placeholder: '--Choose--',
			allowClear: true,
            dropdownParent: $('#modalData'),
			ajax: {
				url: "{{ route('city.get-province')}}?filterDB=yes",
				dataType: 'json',

				processResults: function (data) {

					return {
						results: $.map(data, function (item) {
							return {
								text: item.text,
								id: item.id,
							}
						})
					};
				},
				cache: true

			}
		}).on('select2:select', function (evt) {
            select_city(evt.params.data.id)
		});
    })

    function select_city(province_id = null)
    {
        $('#select_city').select2({
			placeholder: '--Choose--',
			allowClear: true,
            dropdownParent: $('#modalData'),
			ajax: {
				url: "{{ route('city.get-city')}}?filterDB=yes",
				dataType: 'json',
                data: function (params) {
                    var query = {
                    search: params.term,
                    province_id:province_id
                }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
				processResults: function (data) {

					return {
						results: $.map(data, function (item) {
							return {
								text: item.text,
								id: item.id,
							}
						})
					};
				},
				cache: true

			}
		}).on('select2:select', function (evt) {
            select_city(evt.params.data.id)
		});
    }
    var table = $('#user-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('user.get-list') }}",
        'columns': [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '',
                searchable: false,
            },
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'code' ,name:'code', "className": "v-align-tables"},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'email' ,name:'email', "className": "v-align-tables"},
            { data: 'phone' ,name:'phone', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });

    $('#user-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
        // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    function format ( rowData ) {
        var address = '';

        if(rowData.get_profile.alamat)
        {
            address += rowData.get_profile.alamat+',';
        }

        address += rowData.get_profile.get_province.name+','+rowData.get_profile.get_city.name;
        var div = '';
        div += "<div class='row'>";
        div +=      "<div class='col-sm-12'>";
        div +=          '<table class="table table-striped table-bordered" style="width:100%">';
        div +=              '<tr>';
        div +=                  '<td> Nama Lengkap </td>'
        div +=                  '<td> Jenis Kelamin </td>'
        div +=                  '<td> Tanggal Lahir </td>'
        div +=                  '<td> Alamat </td>'
        div +=                  '<td> KTP </td>'
        div +=              '</tr>';
        div +=              '<tr>';
        div +=                  '<td>'+rowData.get_profile.name+'</td>'
        div +=                  '<td>'+(rowData.get_profile.gender == "1"? "Laki-Laki":"Perempuan")+'</td>'
        div +=                  '<td>'+rowData.get_profile.birth_date+'</td>'
        div +=                  '<td>'+address+'</td>'
        div +=                  '<td>'+rowData.get_profile.ktp_number+'</td>'
        div +=              '</tr>';
        div +=          '</table>'
        div +=      "</div>";
        div += "</div>";
        return div;
    }


    function editClick($id){
        $.ajax({
            url : '{{route("user.get-data-edit")}}',
            type: 'GET',
            data : {
                code: $id
            },
            success: (response) => {

                modal_show()
                $('#name_user').val(response.data.name)
                $('#no_ktp').val(response.data.get_profile.ktp_number)
                $('#email').val(response.data.email)
                $('#phone_code').val(response.data.get_profile.phone_code)
                $('#phone_number').val(response.data.get_profile.phone)

                $('#gender').val(response.data.get_profile.gender)
                $('#gender').trigger('change');
                $('#tgl_lahir').val(response.data.get_profile.birth_date)

                var province_val = new Option(response.data.get_profile.get_province.name, response.data.get_profile.m_province_id, true, true);
                $('#select_province').append(province_val).trigger('change');


                var city_val    = new Option(response.data.get_profile.get_city.name, response.data.get_profile.m_city_id, true, true);
                $('#select_city').append(city_val).trigger('change');

                $('#alamat').val(response.data.get_profile.alamat)

                $('#is-edit-pswd').show()
                $('#password').val(response.data.password)
                $('#re-password').val(response.data.password)

                $('#file_ktp').show();
                $('#file_ktp').text(response.data.get_profile.ktp)

                disabledpswd()
                url_save = "{{ route('user.update') }}?code="+response.data.code;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }


    function deleteClick(id=null)
    {
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $.ajax({
                    url : "{{ route('user.delete') }}",
                    type: 'POST',
                    data:{
                        code:id
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        if(response.code == 200){
                            swalSuccess('Success','Data Berhasil Dihapus');
                            clearModal()
                            tableReload()
                        }
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)

                    }
                });
            }
        });

    }
    $('#save').on('click',function(){
        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $('#fm').submit()
            }
        });
    })

    $("#fm").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : url_save,
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                clearModal()
                tableReload()
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        });
    });


    function tableReload(){
        table.ajax.reload(null,false);
    }

    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}

    function clearModal(){
	    $('#modalData').modal('hide');
        $('#fm').find('input:text, input:password, select')
                    .each(function () {
                        $(this).val('');
                    });

    }

    $('.close_modal').on('click',function(){
        modal_close();
    })
    function modal_close(){
        clearModal();
        $('#modalData').modal('hide');
    }

    $('#addData').on('click',function(){
        modal_show();
        clearModal();
        nondisabledpswd();
        $('#is-edit-pswd').hide()
        url_save = "{{ route('user.save') }}";
    })

    $('#is_edit_pswd').on('click',function(){
        if($(this).is(':checked')){
            nondisabledpswd()
        }else{
            disabledpswd()
        }
    })

    function nondisabledpswd(){
        $('#password').val('')
        $('#re-password').val('')
        $('#password').removeAttr('disabled')
        $('#re-password').removeAttr('disabled')
    }

    function disabledpswd(){

        $('#password').attr('disabled','disabled')
        $('#re-password').attr('disabled','disabled')
    }

</script>
@endsection()
