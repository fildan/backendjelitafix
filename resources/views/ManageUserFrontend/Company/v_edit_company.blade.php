@extends('layout.app')
@section('title', ' Company User ')
@section('head')
    <style>
        .required{
            color:red !important;
        }
        .step_no {
            background: #ccc;
        }
        .selected-step{
            background-color: #34495E;
        }
        .step_descr{
            background-color: #fff !important
        }
        .note-editing-area{
            color: #000;
            height: 200px;
        }

    </style>

	<link type="text/css" href="{{ asset('summernote/summernote.min.css')}}" rel="stylesheet" media="screen" />
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit Company User<small> JELAS</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container-fluid">
                                    <i class="fa fa-group">
                                        Data Company
                                    </i>
                                    <hr>

                                    <div class="row">

                                        <div class="col-md-12 col-sm-12 ">
                                            <div id="wizard" class="form_wizard wizard_horizontal">
                                                <ul class="wizard_steps">
                                                    <li>
                                                        <a href="#step-1" class="selected step-btn step-1">
                                                            <span class="step_no selected-step">1</span>
                                                            <span class="step_descr">
                                                                Step 1<br />
                                                                <small>Data Perusahaan</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-2" class="step-btn step-2">
                                                            <span class="step_no">2</span>
                                                            <span class="step_descr">
                                                                Step 2<br />
                                                                <small>Komisaris</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-3" class="step-btn step-3">
                                                            <span class="step_no">3</span>
                                                            <span class="step_descr">
                                                                Step 3<br />
                                                                <small>Direksi</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-4" class="step-btn step-4">
                                                            <span class="step_no">4</span>
                                                            <span class="step_descr">
                                                                Step 4<br />
                                                                <small>PIC</small>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#step-5" class="step-btn step-5">
                                                            <span class="step_no">5</span>
                                                            <span class="step_descr">
                                                                Step 5<br />
                                                                <small>Dokumen Legalitas</small>
                                                            </span>
                                                        </a>
                                                    </li>


                                                    <li>
                                                        <a href="#step-5" class="step-btn step-6">
                                                            <span class="step_no">6</span>
                                                            <span class="step_descr">
                                                                Step 6<br />
                                                                <small>User</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <form id="fm">
                                                @csrf
                                                <div id="step-1">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Input Data Perusahaan</h5>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="select_type_company">Name
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <div class="input-group">


                                                                <div class="input-group-prepend">
                                                                    <select id="select_type_company" name="select_type_company" required="required" class="form-control" style="width: 100%;">
                                                                        <option value="{{ $get_data->get_company_type->id }}">
                                                                            {{ $get_data->get_company_type->name }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <input type="text" id="name" name="name" required="required" class="form-control" style="height: 38px;" value="{{  $get_data->name }}">


                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="jenis_usaha">Jenis Badan Usaha
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <select name="jenis_usaha" id="jenis_usaha"  required="required" class="form-control select2">
                                                                <option value="">--Pilih--</option>
                                                                <option value="1" {{ ($get_data->jenis_usaha == 1)?'selected':'' }}> Non BUMN / Swasta </option>
                                                                <option value="2" {{ ($get_data->jenis_usaha == 2)?'selected':'' }}> BUMN </option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="bidang_usaha">Bidang Usaha
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" id="bidang_usaha" name="bidang_usaha" required="required" class="form-control " value="{{ $get_data->bidang_usaha }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="no_npwp">No NPWP
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" id="no_npwp" name="no_npwp" required="required" class="form-control" value="{{ $get_data->npwp }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="no_npwp">Email
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="email" id="email" name="email" required="required" class="form-control" value="{{ $get_data->email }}">
                                                        </div>
                                                    </div>
                                                    <div class="clone-telepon">

                                                        @if($get_data->no_telepon)
                                                            @php
                                                                $no_telp = json_decode($get_data->no_telepon);
                                                            @endphp

                                                            @foreach($no_telp as $key => $val)
                                                                <div class="form-group row div-telepon">
                                                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="phone_code">No Telepon
                                                                        <span class="required">*</span>
                                                                    </label>
                                                                    <div class="col-md-2 col-sm-2">
                                                                        <input type="text" id="phone_code" name="phone_code[]" placeholder="62" required="required" class="form-control" value="{{ $val->code }}">
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-3">
                                                                        <input type="text" id="phone_number" name="phone_number[]" placeholder="81234567890" required="required" class="form-control" value="{{ $val->number }}">
                                                                    </div>

                                                                    @if($key == 0)
                                                                        <div class="col-md-2 col-sm-2">
                                                                            <a class="btn btn-sm btn-danger add-telepon"> <i class="fa fa-plus" style="color: #fff"></i> </a>
                                                                        </div>
                                                                    @else
                                                                        <div class="col-md-2 col-sm-2">
                                                                            <a class="btn btn-sm btn-danger delete-telepon"> <i class="fa fa-trash-o" style="color: #fff"></i> </a>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>

                                                    <div class="form-group row div-telepon">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="select_province">Provinsi / Kota
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-2 col-sm-2">
                                                            <select name="select_province" id="select_province"  required="required" class="form-control ">
                                                                
                                                                @if($get_data->get_province)
                                                                    <option value="{{ $get_data->get_province->id }}">
                                                                    {{ $get_data->get_province->name }}
                                                                </option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <select name="select_city" id="select_city"  required="required" class="form-control " style="height: 38px">
                                                                @if($get_data->get_city)
                                                                    <option value="{{ $get_data->get_city->id }}">
                                                                    {{ $get_data->get_city->name }}
                                                                </option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" id="postcode" name="postcode" placeholder="kode pos" required="required"
                                                                class="form-control " style="height: 38px" value="{{ $get_data->postcode }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row div-telepon">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="address">Alamat
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <textarea class="form-control" id="address" name="address">{{ $get_data->address }}</textarea>
                                                        </div>

                                                    </div>


                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-primary btn-sm btn-next" val='1' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-2" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Komisaris</h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table table-striped table-bordered" align="center" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="vertical-align: middle">
                                                                            Name
                                                                        </th>
                                                                        <th style="vertical-align: middle">
                                                                            Jabatan
                                                                        </th>

                                                                        <th style="vertical-align: middle;width:20px">
                                                                            KTP
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary btn-sm" id="addKomisaris"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody id="row-komisaris">
                                                                    @php
                                                                        $get_komisaris = collect($get_data->get_komisaris_dan_direksi)->where('type',1)->all();
                                                                    @endphp

                                                                    @foreach ($get_komisaris as $key => $val )

                                                                        <tr class="komisaris-list-exists-{{ $key }}">
                                                                            <td style='vertical-align:middle'>
                                                                                <input type="text" class='form-control' name='nama_komisaris[{{ $val->code }}]' value="{{ $val->name }}">
                                                                            </td>
                                                                            <td style='vertical-align:middle'>
                                                                                <input type="text" class='form-control' name='jabatan_komisaris[{{ $val->code }}]' value="{{ $val->position }}">
                                                                            </td>
                                                                            <td style='vertical-align:middle;width:20px'>
                                                                                <input type="file" name="ktp_komisaris[{{ $val->code }}]">
                                                                                <span style="font-size: 10px;color:red"> {{ $val->identity_file }} </span>
                                                                            </td>

                                                                            <td  align='center' style='vertical-align:middle'>
                                                                                <a class='btn btn-sm btn-danger delete-komisaris' row-id="exists-{{ $key }}">
                                                                                    <i class='fa fa-trash'></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="2" style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='2' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-3" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Direksi</h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table table-striped table-bordered" align="center" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="vertical-align: middle">
                                                                            Name
                                                                        </th>
                                                                        <th style="vertical-align: middle">
                                                                            Jabatan
                                                                        </th>

                                                                        <th style="vertical-align: middle;width:20px">
                                                                            KTP
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary btn-sm" id="addDireksi"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody id="row-direksi">
                                                                    @php
                                                                        $get_direksi = collect($get_data->get_komisaris_dan_direksi)->where('type',2)->all();
                                                                    @endphp

                                                                    @foreach ($get_direksi as $key => $val )

                                                                        <tr class="direksi-list-exists-{{ $key }}">
                                                                            <td style='vertical-align:middle'>
                                                                                <input type="text" class='form-control' name='nama_direksi[{{ $val->code }}]' value="{{ $val->name }}">
                                                                            </td>
                                                                            <td style='vertical-align:middle'>
                                                                                <input type="text" class='form-control' name='jabatan_direksi[{{ $val->code }}]' value="{{ $val->position }}">
                                                                            </td>
                                                                            <td style='vertical-align:middle;width:20px'>
                                                                                <input type="file" name="ktp_direksi[{{ $val->code }}]">
                                                                                <span style="font-size: 10px; color:red"> {{ $val->identity_file  }} </span>
                                                                            </td>

                                                                            <td  align='center' style='vertical-align:middle'>
                                                                                <a class='btn btn-sm btn-danger delete-direksi' row-id="exists-{{ $key }}">
                                                                                    <i class='fa fa-trash'></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="3"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='3' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-4" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">PIC</h5>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table table-striped table-bordered" align="center" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="vertical-align: middle">
                                                                            Name
                                                                        </th>
                                                                        <th style="vertical-align: middle">
                                                                            Email
                                                                        </th>

                                                                        <th style="vertical-align: middle">
                                                                            No Telepon
                                                                        </th>

                                                                        <th style="vertical-align: middle;">
                                                                            Posisi
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary btn-sm" id="addPIC"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody id="row-pic">

                                                                    @if($get_data->get_pic)
                                                                        @php
                                                                            $pic = $get_data->get_pic;
                                                                        @endphp

                                                                        @foreach ( $pic as $key => $val )
                                                                            <tr class="list-pic-exist-{{ $key }}">
                                                                                <td style="vertical-align: middle">
                                                                                    <input type="text" name="name_pic[{{ $val->code }}]" class="form-control" value="{{ $val->name }}">
                                                                                </td>
                                                                                <td style="vertical-align: middle">
                                                                                    <input type="email" name="email_pic[{{ $val->code }}]" class="form-control" value="{{$val->email}}">
                                                                                </td>
                                                                                <td style="vertical-align: middle">
                                                                                    <div class="input-group mt-2">
                                                                                        <input type="text" name="phone_code_pic[]" class="form-control" placeholder="62" value="{{ $val->code_telepon }}">
                                                                                        <div class="input-group-prepend">
                                                                                            <input type="text" name="phone_number_pic[]" class="form-control" placeholder="81234567890" value="{{ $val->telepon }}">
                                                                                        </div>


                                                                                    </div>
                                                                                </td>
                                                                                <td style="vertical-align: middle">
                                                                                    <input type="text" name="position_pic[]" class="form-control" value="{{ $val->posisi }}">
                                                                                </td>
                                                                                <td  align='center' style='vertical-align:middle'>
                                                                                    <a class='btn btn-sm btn-danger delete-pic' row-id="exist-{{ $key }}">
                                                                                        <i class='fa fa-trash'></i>
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="4"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='4' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-5" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Document</h5>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table table-striped table-bordered" align="center" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="vertical-align: middle">
                                                                            Name
                                                                        </th>
                                                                        <th style="vertical-align: middle">
                                                                            File
                                                                        </th>

                                                                        <th style="vertical-align: middle">
                                                                            Note
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($get_document as $row )
                                                                        @php
                                                                            $cek = collect($get_data->get_document)->where('m_document_upload_code',$row->code)->first();

                                                                        @endphp
                                                                        <tr>
                                                                            <td style="vertical-align: middle">
                                                                                {{ $row->name }}
                                                                            </td>
                                                                            <td style="vertical-align: middle">
                                                                                <input type="file" name="document_legality[{{ $row->code }}]">
                                                                                @if($cek)
                                                                                    <p style="font-size:10px;color:red;"> {{ $cek->file_name }} </p>
                                                                                @endif
                                                                            </td>
                                                                            <td style="vertical-align: middle">
                                                                                <textarea class="form-control" name="note_document[{{ $row->code }}]">
                                                                                    @if($cek)
                                                                                        {{ $cek->note }}
                                                                                    @endif
                                                                                </textarea>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="5"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-dark btn-sm btn-next" val="5" style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-6" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">User</h5>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="name_user">Name
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">

                                                            <input type="text" id="name_user" name="name_user" required="required" class="form-control" value="{{ $get_data->get_user->get_profile->name }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="email_user">Email
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="email" id="email_user" name="email_user" required="required" class="form-control" value="{{ $get_data->get_user->get_profile->email }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="phone_code_user">No Telepon
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" id="phone_code_user" name="phone_code_user" placeholder="62" required="required" class="form-control" value="{{ $get_data->get_user->get_profile->phone_code }}">
                                                        </div>
                                                        <div class="col-md-4 col-sm-4">
                                                            <input type="text" id="phone_number_user" name="phone_number_user" placeholder="81234567890" required="required" class="form-control " value="{{ $get_data->get_user->get_profile->phone }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="ktp_user">KTP / Tgl lahir
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="file" id="ktp_user" name="ktp_user">
                                                            <p style="font-size: 10px;color:red">
                                                                {{ $get_data->get_user->get_profile->ktp }}
                                                            </p>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" id="ktp_number_user" name="ktp_number_user" placeholder="no ktp" class="form-control " value="{{ $get_data->get_user->get_profile->ktp_number }}">
                                                        </div>

                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="date" id="birth_date" name="birth_date" class="form-control " value="{{ $get_data->get_user->get_profile->birth_date }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="select_province_user">Province / City
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-2 col-sm-2">
                                                            <select name="province_user" id="select_province_user" class="form-control" style="width: 100%">
                                                                <option value="{{ $get_data->get_user->get_profile->get_province->id }}"> {{ $get_data->get_user->get_profile->get_province->name }}</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <select name="city_user" id="select_city_user" class="form-control" style="width: 100%;height:38px">

                                                                <option value="{{ $get_data->get_user->get_profile->get_city->id }}"> {{ $get_data->get_user->get_profile->get_city->name }}</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" id="postcode_user" name="postcode_user" class="form-control" placeholder="kode pos" style="height: 38px" value="{{ $get_data->get_user->get_profile->postcode }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="alamat_user">Alamat
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <textarea id="alamat_user" name="alamat_user" required="required" class="form-control">{{ $get_data->get_user->get_profile->alamat }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="password_user">Ingein mengubah passowrd?
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label>
                                                                <input type="checkbox" name="is_edit_pswd" id="is_edit_pswd" class="js-switch"/>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="password_user">Password
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-3 col-sm-3">
                                                            <input type="password" id="password_user" name="password" class="form-control" placeholder="password" disabled>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <input type="password" id="re_password" name="re_password" placeholder="re password" class="form-control" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="6"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-dark btn-sm btn-save"  style="color: #fff">Submit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection()
@section('script')
    <script>

        $(document).ready(function(){

            select_city_user("{{ $get_data->get_user->get_profile->m_province_id }}");
            select_city("{{ $get_data->m_province_id }}")
            $('#select_province').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('city.get-province')}}?filterDB=yes",
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {
                select_city(evt.params.data.id)
            });

            $('#select_province_user').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('city.get-province')}}?filterDB=yes",
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {
                select_city_user(evt.params.data.id)
            });


            $('#select_type_company').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('company.get-type')}}?filterDB=yes",
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {

            });
        })

        $(document).on('click','.add-telepon',function(){
            // $(".div-telepon").clone().appendTo(".clone-telepon");
            let div_html = '';
            div_html += '<div class="form-group row div-telepon">';
            div_html +=      '<label class="col-form-label col-md-3 col-sm-3 label-align" for="phone_code">No Telepon<span class="required">*</span></label>';
            div_html +=      ' <div class="col-md-2 col-sm-2">';
            div_html +=          '<input type="text" id="phone_code" name="phone_code[]" placeholder="62" required="required" class="form-control ">';
            div_html +=      ' </div>';
            div_html +=      '<div class="col-md-3 col-sm-3">';
            div_html +=          '<input type="text" id="phone_number" name="phone_number[]" placeholder="81234567890" required="required" class="form-control ">';
            div_html +=      '</div>';
            div_html +=     '<div class="col-md-2 col-sm-2">';
            div_html +=         '<a class="btn btn-sm btn-danger delete-telpon">';
            div_html +=             '<i class="fa fa-trash-o" style="color:#fff"></i>';
            div_html +=         '</a>';
            div_html +=     '</div>';
            div_html += '</div>';


            $('.clone-telepon').append(div_html)
        })

        $(document).on('click', '.delete-telpon', function(e) {
            e.preventDefault();
            // $(this).parent().find('.div-telepon').remove();
            $(this).parent().parent().remove()
        });


        $('#addKomisaris').on('click',function(){

            var count_komisaris = 0;
            $('[name="nama_komisaris[]"]').each(function( index ) {
                // console.log( index + ": " + $( this ).text() );
                count_komisaris += 1;
            });

            if(count_komisaris < 5){
                addListKomisaris()
            }
        })

        var list_komisaris = 1;
        function addListKomisaris(){
        list_komisaris = list_komisaris+1;
            var list_html  = '<tr class="komisaris-list-'+list_komisaris+'">'
                list_html +=    '<td style="vertical-align:middle">'
                list_html +=        '<input type="text" class="form-control" name="nama_komisaris[]">'
                list_html +=    '</td>'
                list_html +=    '<td style="vertical-align:middle">'
                list_html +=        '<input type="text" class="form-control" name="jabatan_komisaris[]">'
                list_html +=    '</td>'
                list_html +=    '<td style="vertical-align:middle;width:20px">'
                list_html +=        '<input type="file" name="ktp_komisaris[]">'
                list_html +=    '</td>';

                list_html +=    '<td  align="center" style="vertical-align:middle">'
                list_html +=        '<a class="btn btn-sm btn-danger delete-komisaris" row-id="'+list_komisaris+'">'
                list_html +=            '<i class="fa fa-trash"></i>'
                list_html +=        '</a>'
                list_html +=    '</td>'
                list_html += '</tr>'
            $('#row-komisaris').append(list_html)
        }

        $(document).on('click','.delete-komisaris',function(){
            $('.komisaris-list-'+$(this).attr('row-id')).remove()
        })

        $('#addDireksi').on('click',function(){

            var count_direksi = 0;
            $('[name="nama_direksi[]"]').each(function( index ) {
                // console.log( index + ": " + $( this ).text() );
                count_direksi += 1;
            });

            if(count_direksi < 5){
                addListDireksi()
            }
        })

        var list_direksi = 1;
        function addListDireksi(){
            list_direksi = list_direksi+1;
            var list_html  = '<tr class="direksi-list-'+list_direksi+'">'
                list_html +=    '<td style="vertical-align:middle">'
                list_html +=        '<input type="text" class="form-control" name="nama_direksi[]">'
                list_html +=    '</td>'
                list_html +=    '<td style="vertical-align:middle">'
                list_html +=        '<input type="text" class="form-control" name="jabatan_direksi[]">'
                list_html +=    '</td>'
                list_html +=    '<td style="vertical-align:middle;width:20px">'
                list_html +=        '<input type="file" name="ktp_direksi[]">'
                list_html +=    '</td>';

                list_html +=    '<td  align="center" style="vertical-align:middle">'
                list_html +=        '<a class="btn btn-sm btn-danger delete-direksi" row-id="'+list_direksi+'">'
                list_html +=            '<i class="fa fa-trash"></i>'
                list_html +=        '</a>'
                list_html +=    '</td>'
                list_html += '</tr>'
            $('#row-direksi').append(list_html)
        }

        $(document).on('click','.delete-direksi',function(){
            $('.direksi-list-'+$(this).attr('row-id')).remove()
        })


        $('#addPIC').on('click',function(){

            var count_pic = 0;
            $('[name="name_pic[]"]').each(function( index ) {
                // console.log( index + ": " + $( this ).text() );
                count_pic += 1;
            });

            if(count_pic < 5){
                addListPic()
            }
        })

        var list_pic = 1;
        function addListPic(){
            list_pic = list_pic+1;
            var list_html  = '<tr class="list-pic-'+list_pic+'">'
                list_html +=    '<td style="vertical-align: middle">'
                list_html +=        '<input type="text" name="name_pic[]" class="form-control">'
                list_html +=    '</td>'
                list_html +=    '<td style="vertical-align: middle">'
                list_html +=        '<input type="email" name="email_pic[]" class="form-control">'
                list_html +=    '</td>'
                list_html +=    '<td style="vertical-align: middle">'
                list_html +=        '<div class="input-group mt-2">'
                list_html +=            '<input type="text" name="phone_code_pic[]" class="form-control" placeholder="62">'
                list_html +=            '<div class="input-group-prepend">'
                list_html +=                '<input type="text" name="phone_number_pic[]" class="form-control" placeholder="81234567890">'
                list_html +=            '</div>'
                list_html +=        '</div>'
                list_html +=    '</td>'
                list_html +=    '<td style="vertical-align: middle">'
                list_html +=        '<input type="text" name="position_pic[]" class="form-control">'
                list_html +=    '</td>'
                list_html +=    '<td  align="center" style="vertical-align:middle">'
                list_html +=        '<a class="btn btn-sm btn-danger delete-pic" row-id="'+list_pic+'">'
                list_html +=            '<i class="fa fa-trash"></i>'
                list_html +=        '</a>'
                list_html +=    '</td>'
                list_html += '</tr>'
            $('#row-pic').append(list_html)
        }
        $(document).on('click','.delete-pic',function(){
            let row_id = $(this).attr('row-id');
            $('.list-pic-'+row_id).remove()
        });
        function select_city(province_id = null)
        {
            $('#select_city').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('city.get-city')}}?filterDB=yes",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                        search: params.term,
                        province_id:province_id
                    }

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {

            });
        }

        function select_city_user(province_id = null)
        {
            $('#select_city_user').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('city.get-city')}}?filterDB=yes",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                        search: params.term,
                        province_id:province_id
                    }

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {
            });
        }

        $('.btn-save').on('click',function(){
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Save ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        })
        $("#fm").submit(function(event){
            event.preventDefault();
            var form_data =  new FormData(this);
            let url_save    ="{{ url('user-frontend/company/update') }}"+'/'+"{{ $code }}"
            $.ajax({
                url : url_save,
                type: 'POST',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    swalSuccess('Success','Success To Save');
                    // reloadPage()
                    // window.location.href="{{ route('product-penanggung') }}"
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        });
        $('.btn-next').on('click',function(){
            let id = $(this).attr('val')
            $('#step-'+id).hide()
            let newid = parseInt(id)+1;
            $('#step-'+newid).show();


            $('.step-'+newid).addClass('selected')
            $('.step-'+newid).find('span').addClass('selected-step')


            $('.step-'+id).removeClass('selected')
            $('.step-'+id).find('span').removeClass('selected-step')
        })

        $('#is_edit_pswd').on('click',function(){
            if($(this).is(':checked')){
                nondisabledpswd()
            }else{
                disabledpswd()
            }
        })

        function nondisabledpswd(){
            $('#password_user').val('')
            $('#re_password').val('')
            $('#password_user').removeAttr('disabled')
            $('#re_password').removeAttr('disabled')
        }

        function disabledpswd(){

            $('#password_user').attr('disabled','disabled')
            $('#re_password').attr('disabled','disabled')
        }
        $('.btn-prev').on('click',function(){
            let id = $(this).attr('val')
            $('#step-'+id).hide()
            let newid = parseInt(id)-1;
            $('#step-'+newid).show();


            $('.step-'+newid).addClass('selected')
            $('.step-'+newid).find('span').addClass('selected-step')


            $('.step-'+id).removeClass('selected')
            $('.step-'+id).find('span').removeClass('selected-step')
        })
    </script>
@endsection()
