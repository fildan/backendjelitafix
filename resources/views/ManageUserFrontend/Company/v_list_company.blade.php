@extends('layout.app')
@section('title', 'Company List')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                Data Company
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="{{ route('company.add') }}">
                                        <button type="button" class="btn btn-primary btn-sm" id="addData">Add Company</button>
                                    </a>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="company-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <th width="10px"></th>
                                            <th width="10px">No</th>
                                            <th>Type</th>
                                            <th>Nama</th>
                                            <th>Jenis Usaha</th>
                                            <th>Bidang Usaha</th>
                                            <th>Email</th>
                                            <th>No Telepon</th>
                                            <th align="center" width="100px">Action</th>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


@endsection()
@section('script')
<script>
    var table = $('#company-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('company.get-list') }}",
        'columns': [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '',
                searchable: false,
            },
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'company_type' ,name:'company_type', "className": "v-align-tables"},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            {
                data: 'jenis_usaha',
                name:'jenis_usaha',
                "className": "v-align-tables",
                render: function (data, type, row){
                    if(row.jenis_usaha == 1){
                        return 'Non BUMN / SWASTA';
                    }else{
                        return 'BUMN';
                    }
                }
            },
            { data: 'bidang_usaha' ,name:'bidang_usaha', "className": "v-align-tables"},
            { data: 'email' ,name:'email', "className": "v-align-tables"},
            {
                data: 'no_telepon' ,
                name:'no_telepon',
                "className": "v-align-tables"
            },
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });

    function editClick(code = null)
    {
        window.location.href = "{{ url('user-frontend/company/edit/') }}"+'/'+code
    }

    function deleteClick(code = null)
    {
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $.ajax({
                    url : "{{ route('company.delete') }}",
                    type: 'POST',
                    data:{
                        code:code
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        if(response.code == 200){
                            swalSuccess('Success','Data Berhasil Dihapus');
                            tableReload()
                        }
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)

                    }
                });
            }
        });
    }

    function tableReload(){
        table.ajax.reload(null,false);
    }
</script>
@endsection()
