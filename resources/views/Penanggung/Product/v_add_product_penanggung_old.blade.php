@extends('layout.app')
@section('title', ' Product Penanggung ')
@section('head')
    <style>
        .required{
            color:red !important;
        }
        .step_no {
            background: #ccc;
        }
        .selected-step{
            background-color: #34495E;
        }
        .step_descr{
            background-color: #fff !important
        }
        .note-editing-area{
            color: #000;
            height: 200px;
        }

    </style>

	<link type="text/css" href="{{ asset('summernote/summernote.min.css')}}" rel="stylesheet" media="screen" />
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add Product Penanggung<small> JELAS</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container-fluid">
                                    <i class="fa fa-group">
                                        Data Product
                                    </i>
                                    <hr>

                                    <div class="row">

                                        <div class="col-md-12 col-sm-12 ">
                                            <div id="wizard" class="form_wizard wizard_horizontal">
                                                <ul class="wizard_steps">
                                                    <li>
                                                        <a href="#step-1" class="selected step-btn step-1">
                                                            <span class="step_no selected-step">1</span>
                                                            <span class="step_descr">
                                                                Step 1<br />
                                                                <small>General Form</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-2" class="step-btn step-2">
                                                            <span class="step_no">2</span>
                                                            <span class="step_descr">
                                                                Step 2<br />
                                                                <small>Product Detail</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-3" class="step-btn step-3">
                                                            <span class="step_no">3</span>
                                                            <span class="step_descr">
                                                                Step 3<br />
                                                                <small>Fitur Product</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-4" class="step-btn step-4">
                                                            <span class="step_no">4</span>
                                                            <span class="step_descr">
                                                                Step 4<br />
                                                                <small>T&C - Claim</small>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#step-5" class="step-btn step-5">
                                                            <span class="step_no">5</span>
                                                            <span class="step_descr">
                                                                Step 5<br />
                                                                <small>Perluasan Product</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <form id="fm">
                                                @csrf
                                                <div id="step-1">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Input Data Product</h5>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="select_main_product">Main Product
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <select id="select_main_product" name="select_main_product" required="required" class="form-control ">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="sub_product">Sub Product
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <select id="sub_product" name="sub_product" required="required" class="form-control">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="select_penanggung">Penanggung
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <select name="select_penanggung" id="select_penanggung"  required="required" class="form-control ">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="product_name">Product Name
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" id="product_name" name="product_name" required="required" class="form-control ">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="product_code">Product Code
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" id="product_code" name="product_code" required="required" class="form-control ">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="description">Product Description
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <textarea class="form-control auto-resize" id="description" name="description" style="height: 100px !important"
                                                                ></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row" style="display: none" id="div-file-bengkel">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="description">Rekanan Bengkel
                                                            {{-- <span class="required">*</span> --}}
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <div class="input-group mb-3">
                                                                <div class="custom-file">
                                                                <input type="file" class="custom-file-input" name="rekanan_bengkel" id="rekanan_bengkel" aria-describedby="rekanan_bengkel">
                                                                <label class="custom-file-label" for="rekanan_bengkel">Choose file</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-primary btn-sm btn-next" val='1' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-2" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Product Detail</h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <table class="table table-striped table-bordered" align="right" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Manfaat Perlindungan</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRow"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-perluasan">
                                                                    <tr id="perluasan-list-1">
                                                                        <td style='vertical-align:middle' colspan='2'>
                                                                            <textarea class='form-control auto-resize' name='manfaat_perlindungan[]'></textarea>
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <a class='btn btn-sm btn-danger delete-row' row-id="1">
                                                                                <i class='fa fa-trash'></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-6">

                                                            <table class="table table-striped table-bordered" align="left" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Tidak Termasuk Manfaat Perlindungan</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowwithoutbenefit"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-without-benefit">
                                                                    <tr id="without-benefit-list-1">
                                                                        <td style='vertical-align:middle' colspan='2'>
                                                                            <textarea class='form-control auto-resize' name='nameithoutbnfts[]'></textarea>
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <a class='btn btn-sm btn-danger delete-row-wthoutbnft' row-id="1">
                                                                                <i class='fa fa-trash'></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="2" style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='2' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-3" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Fitur Product</h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table table-striped table-bordered" align="center" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="vertical-align: middle">
                                                                            Name
                                                                        </th>
                                                                        <th style="vertical-align: middle">
                                                                            Description
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowFitur"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-fitur">
                                                                    <tr id="fitur-list-1">
                                                                        <td style='vertical-align:middle'>
                                                                            <input type="text" class='form-control' name='name_fitur[]'>
                                                                        </td>
                                                                        <td style='vertical-align:middle'>
                                                                            <textarea class='form-control auto-resize' name='description_fitur[]'></textarea>
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <a class='btn btn-sm btn-danger delete-row-fitur' row-id="1">
                                                                                <i class='fa fa-trash'></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="3"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='3' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-4" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Syarat Ketentuan & Klaim</h5>
                                                        </div>
                                                    </div>
                                                    {{-- <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="d-flex justify-content-center">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Syarat dan Ketentuan Product</label>
                                                                    <textarea class="summernote form-control" id="snkproduct" name="snkproduct" placeholder="">
                                                                        input TnC Product
                                                                    </textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="d-flex justify-content-center">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Klaim Product</label>
                                                                    <textarea class="summernote form-control" id="claim_product" name="claim_product" placeholder="">
                                                                    </textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="d-flex justify-content-end">
                                                                    <textarea class="form-control auto-resize" id="description_snk" name="description_snk" style="height: 100px !important;width:80%"
                                                                        >Deskripsi Syarat dan Ketentuan</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                    <textarea class="form-control auto-resize" id="description_claim" name="description_claim" style="height: 100px !important;width:80%"
                                                                        >Deskripsi Klaim</textarea>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="d-flex justify-content-end">
                                                                <div class="input-group mb-3" style="width: 80%">
                                                                    <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" name="documentsnk" id="documentsnk" aria-describedby="documentsnk">
                                                                    <label class="custom-file-label" for="documentsnk">Choose file</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="input-group mb-3" style="width: 80%">
                                                                <div class="custom-file">
                                                                <input type="file" class="custom-file-input" name="documentclaim" id="documentclaim" aria-describedby="documentclaim">
                                                                <label class="custom-file-label" for="documentclaim">Choose file</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <table class="table table-striped table-bordered" align="right" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Syarat dan Ketentuan</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowsnk"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-snk">
                                                                    <tr id="snk-list-1">
                                                                        <td style='vertical-align:middle' colspan='2'>
                                                                            <textarea class='form-control auto-resize' name='snk_list[]'></textarea>
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <a class='btn btn-sm btn-danger delete-row-snk' row-id="1">
                                                                                <i class='fa fa-trash'></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                        <div class="col-md-6">

                                                            <table class="table table-striped table-bordered" align="left" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Proses Klaim</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowClaim"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-claim">
                                                                    <tr id="claim-list-1">
                                                                        <td style='vertical-align:middle' colspan='2'>
                                                                            <textarea class='form-control auto-resize' name='claim_list[]'></textarea>
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <a class='btn btn-sm btn-danger delete-row-claim' row-id="1">
                                                                                <i class='fa fa-trash'></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                            <table class="table table-striped table-bordered" align="left" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Document yang dibutuhkan</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowDocClaim"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-doc">
                                                                    <tr id="doc-list-1">
                                                                        <td style='vertical-align:middle' colspan='2'>
                                                                            <textarea class='form-control auto-resize' name='doc_claim[]'></textarea>
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <a class='btn btn-sm btn-danger delete-row-doc' row-id="1">
                                                                                <i class='fa fa-trash'></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                            <table class="table table-striped table-bordered" align="left" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Contact Klaim</span>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style='vertical-align:middle'>
                                                                            Telepon
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <input type="text" name="tlpn_claim" class="form-control" onkeypress="return hanyaAngka(event)">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style='vertical-align:middle' colspan='Telepon'>
                                                                            Whatsapp
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <input type="text" name="wa_claim" class="form-control" onkeypress="return hanyaAngka(event)">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style='vertical-align:middle' colspan='Telepon'>
                                                                            Email
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <input type="email" name="email_claim" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style='vertical-align:middle' colspan='Telepon'>
                                                                            Direct Aplication
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <input type="text" name="application_claim" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="4"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='4' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-5" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Perluasan Product</h5>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="product_package">is include the package?
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <label>
                                                                <input type="checkbox" name="product_package" id="product_package" class="js-switch"/>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <table class="table table-striped table-bordered" align="center" style="width: 80%">
                                                            <thead>
                                                                <tr>
                                                                    <th> Name </th>
                                                                    <th> Rate (%)</th>
                                                                    <th align="center" style="text-align: center"> Value </th>
                                                                    {{-- <th> Discount (%)</th> --}}
                                                                    <th width="20px"> Available </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="package-product">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="5"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-dark btn-sm btn-save" style="color: #fff">Submit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                    <button type="button" class="close_modal" onclick="clearModal()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                            <div class="row">
                                <div class="input-group mb-3">
                                    <input type="hidden" id="selected_perluasan">
                                    <input type="text" class="form-control moneyFormat" id="amount-value" name="amount-value"
                                        placeholder="Input Amount" aria-label="Input Amount" aria-describedby="basic-addon2">
                                    <input type="text" class="form-control moneyFormat" id="rate-value" name="rate-value"
                                        placeholder="Input Rate" aria-label="Input Rate" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary btn-info add-amount btn-sm" type="button" style="color: #fff">Add</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        <table class="table table-striped table-bordered" align="center" style="width: 80%">
                            <thead>
                                <tr>
                                    <th> value </th>
                                    <th> Rate </th>
                                    <th width="20px"> Action </th>
                                </tr>
                            </thead>
                            <tbody id="list-value-polis">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close_modal" onclick="clearModal()">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
    <script src="{{ asset('summernote/summernote.min.js')}}"></script>
    <script>

        var array_list = [];
        $(document).ready(function() {
            $('.summernote').summernote();
            $('#select_main_product').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('m-sub-product.get-main-product')}}?filterDB=yes",
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                    id_encrypt:item.id_encrypt,
                                    code:item.code
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {
                getProductPolis(evt.params.data.id)

                if(evt.params.data.code == 'KBM'){
                    $('#div-file-bengkel').show()
                }else{
                    $('#div-file-bengkel').hide()
                }
            });

            $('#select_penanggung').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('penanggung.get-combobox-penanggung')}}?filterDB=yes",
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {
            });


            $('#sub_product').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('product-penanggung.get-sub-product')}}?filterDB=yes",
                    data: function (params) {
                        var query = {
                            search: params.term,
                            main_product_id:$('#select_main_product').val()

                        }
                        return query;
                    },
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                    id_encrypt:item.id_encrypt
                                }
                            })
                        };
                    },
                    cache: true

                }
            })
        });


        $('.btn-next').on('click',function(){
            let id = $(this).attr('val')
            $('#step-'+id).hide()
            let newid = parseInt(id)+1;
            $('#step-'+newid).show();


            $('.step-'+newid).addClass('selected')
            $('.step-'+newid).find('span').addClass('selected-step')


            $('.step-'+id).removeClass('selected')
            $('.step-'+id).find('span').removeClass('selected-step')
        })

        $('.btn-prev').on('click',function(){
            let id = $(this).attr('val')
            $('#step-'+id).hide()
            let newid = parseInt(id)-1;
            $('#step-'+newid).show();


            $('.step-'+newid).addClass('selected')
            $('.step-'+newid).find('span').addClass('selected-step')


            $('.step-'+id).removeClass('selected')
            $('.step-'+id).find('span').removeClass('selected-step')
        })
        function getProductPolis($id)
        {
            $.ajax({
                url : '{{route("product-penanggung.get-polis-by-product")}}',
                type: 'GET',
                data : {
                    id: $id
                },
                success: (response) => {
                //    console.log(response);
                //
                var list_package = '';
                    $.each( response, function( key, value ) {
                        list_package += '<tr>';
                        list_package +=     '<td>'+value.name+'</td>';
                        if(value.rate_or_value == 2){
                            var is_disabled = 'disabled';
                        }else{
                            var is_disabled = '';
                        }
                        list_package +=     '<td width="80px"><input type="text" name="rate_package_product['+value.id+']" class="form-control moneyFormat"'+is_disabled+'> </td>';

                        list_package +=     '<td align="center">';
                        if(value.rate_or_value == 2){
                            list_package +=         '<a class="btn btn-sm btn-primary add-value" id-perluasan="'+value.id+'" style="color:#fff"> <i class="fa fa-plus"> Add </i></a>';
                        }else{
                            list_package += '-';
                        }
                        list_package +=     '</td>';
                        // list_package +=     '<td width="110px"><input type="text" name="discount_package_product['+value.id+']" class="form-control" onkeypress="return hanyaAngka(event)"> </td>';
                        list_package +=     '<td> <input type="checkbox" name="is_available_package_product['+value.id+']" class="form-control"></td>'
                        list_package += '</tr>';
                    });
                    $('#package-product').append(list_package)
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            })
        }

        $(document).on('click','.add-value',function(){
            let id_perluasan = $(this).attr('id-perluasan');
            $('#selected_perluasan').val(id_perluasan)
            buildListValue(id_perluasan)
            modalShow()
        })
        function modalShow(){
            $('#modalData').modal('show')
        }
        function clearModal(){

            $('#amount-value').val('');
            $('#selected_perluasan').val('')
            $('#modalData').modal('hide')
        }
        $('.add-amount').on('click',function(){
            let amount_value = $('#amount-value').val();
            let unmusk_value = $('#amount-value').maskMoney('unmasked')[0];

            let rate_value              = $('#rate-value').val();
            let unmusk_rate             = $('#rate-value').maskMoney('unmasked')[0];

            let id_perluasan = parseInt($('#selected_perluasan').val())
            if(amount_value == ''){
                return false;
            }else{
                var new_array = [];
                var array_new = [];
                if(array_list.hasOwnProperty(id_perluasan)){

                    var array_child = [];
                    var array_sub   = [];
                    var array_new_sub = [];
                    $.each(array_list[id_perluasan], function( key, value ) {
                        if(value['value'] != unmusk_value){
                            array_sub = {
                                'value' : value['value'],
                                'rate'  : value['rate']
                            }
                            array_child.push(array_sub)
                            // array_list[key]     = array_child;
                        }
                    })
                    array_new_sub = {
                        'value' : unmusk_value,
                        'rate'  : unmusk_rate
                    }
                    // array_new_sub['value']  = unmusk_value;
                    // array_new_sub['rate']   = rate_value;
                    array_child.push(array_new_sub)
                    array_list[id_perluasan] = array_child;

                }else{
                    array_new = {
                        'value' : unmusk_value,
                        'rate'  : unmusk_rate
                    }
                    // ['value']  = unmusk_value;
                    // array_new['rate']   = rate_value
                    new_array.push(array_new)
                    array_list[id_perluasan] = new_array ;
                }
                buildListValue(id_perluasan)
            }
        })

        function buildListValue(id_perluasan)
        {
            let list_package = '';
            let array_data = array_list[id_perluasan];
            for(var i in array_data) {
                list_package += '<tr>';
                list_package +=     '<td>'+formatRupiah(array_data[i]['value'])+'</td>';
                list_package +=     '<td>'+array_data[i]['rate']+'</td>';
                list_package +=     '<td><a href="#" class="edit"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
                list_package += '</tr>';
            }
            $('#list-value-polis').html(list_package);
        }

        $('#addRow').on('click',function(){
            buildRow()
        })


        var rowNumber = 1;
        function buildRow(){
            rowNumber   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='perluasan-list-"+rowNumber+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='manfaat_perlindungan[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row' row-id='"+rowNumber+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-perluasan').append(rowtd)
        }

        $(document).on('click','.delete-row',function(){
            let row = $(this).attr('row-id');
            $('#perluasan-list-'+row).remove()
        })




        $('#addRowsnk').on('click',function(){
            buildRowSnk()
        })
        var rowsnk = 1;
        function buildRowSnk()
        {
            rowsnk   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='snk-list-"+rowsnk+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='snk_list[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-snk' row-id='"+rowsnk+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-snk').append(rowtd)
        }
        $(document).on('click','.delete-row-snk',function(){
            let row = $(this).attr('row-id');
            $('#snk-list-'+row).remove()
        })


        $('#addRowClaim').on('click',function(){
            buildRowClaim()
        })
        var rowClaim = 1;
        function buildRowClaim()
        {
            rowClaim   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='claim-list-"+rowClaim+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='claim_list[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-claim' row-id='"+rowClaim+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-claim').append(rowtd)
        }
        $(document).on('click','.delete-row-claim',function(){
            let row = $(this).attr('row-id');
            $('#claim-list-'+row).remove()
        })


        $('#addRowwithoutbenefit').on('click',function(){
            buildRowWithoutbenefit()
        })

        var rowNumbernobnft = 1;
        function buildRowWithoutbenefit(){

            rowNumbernobnft   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='without-benefit-list-"+rowNumbernobnft+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='nameithoutbnfts[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-wthoutbnft' row-id='"+rowNumbernobnft+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-without-benefit').append(rowtd)
        }

        $(document).on('click','.delete-row-wthoutbnft',function(){
            let row = $(this).attr('row-id');
            $('#without-benefit-list-'+row).remove()
        })

        $('#addRowFitur').on('click',function(){
            buildRowFitur()
        })
        rowNumberFitur = 1;
        function buildRowFitur(){
            rowNumberFitur   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='fitur-list-"+rowNumberFitur+"'>";
                rowtd   +=      "<td style='vertical-align:middle'><input type='text' class='form-control' name='name_fitur[]'> </td>";
                rowtd   +=      "<td style='vertical-align:middle'>  <textarea class='form-control auto-resize' name='description_fitur[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-fitur' row-id='"+rowNumberFitur+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";
            $('.row-fitur').append(rowtd)
        }
        $(document).on('click','.delete-row-fitur',function(){
            let row = $(this).attr('row-id');
            $('#fitur-list-'+row).remove()
        })

        $('#addRowDocClaim').on('click',function(){
            buildRowDocClaim()
        })
        var rowDocClaim = 1;
        function buildRowDocClaim()
        {
            rowDocClaim   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='doc-list-"+rowDocClaim+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='doc_claim[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-doc' row-id='"+rowDocClaim+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-doc').append(rowtd)
        }
        $(document).on('click','.delete-row-doc',function(){
            let row = $(this).attr('row-id');
            $('#doc-list-'+row).remove()
        })
        $(document).keyup('.onlyNumber',function(event){
            hanyaAngka(event)
        })

        $('.btn-save').on('click',function(){
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Save ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        })
        $("#fm").submit(function(event){
            event.preventDefault();
            var form_data =  new FormData(this);
            form_data.append("perluasan_product",JSON.stringify(array_list));
            let url_save    ="{{ route('product-penanggung.save') }}"
            $.ajax({
                url : url_save,
                type: 'POST',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    swalSuccess('Success','Success To Save');
                    reloadPage()
                    window.location.href="{{ route('product-penanggung') }}"
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        });

    </script>
@endsection()
