@extends('layout.app')
@section('title', 'Product Penanggung List')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                Product Penanggung
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="{{ route('product-penanggung.add') }}">
                                        <button type="button" class="btn btn-primary btn-sm" id="addData">Add Product Penanggung</button>
                                    </a>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="penanggung-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Picture</th>
                                                <th>Penanggung</th>
                                                <th>Main Product</th>
                                                <th>Product Name</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>



@endsection()
@section('script')
<script>
    var table = $('#penanggung-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('product-penanggung.get-list') }}",
        'columns': [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            {
                data: 'picture' ,
                name:'picture',
                "className": "v-align-tables text-center",
                render: function (data){
                    return "<img src='"+data+"' style='widht:150px !important;height:50px !important'>"
                }
            },
            { data: 'penanggung_name' ,name:'penanggung_name', "className": "v-align-tables"},
            { data: 'product_name' ,name:'product_name', "className": "v-align-tables"},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });

    function editClick(id=null){
        window.location.href="{{ route('product-penanggung.edit') }}"+'?id='+id;
    }
</script>
@endsection()
