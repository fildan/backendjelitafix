@extends('layout.app')
@section('title', ' Product Penanggung ')
@section('head')
    <style>
        .required{
            color:red !important;
        }
        .step_no {
            background: #ccc;
        }
        .selected-step{
            background-color: #34495E;
        }
        .step_descr{
            background-color: #fff !important
        }
        .note-editing-area{
            color: #000;
            height: 200px;
        }

    </style>

	<link type="text/css" href="{{ asset('summernote/summernote.min.css')}}" rel="stylesheet" media="screen" />
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add Product Penanggung<small> JELAS</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container-fluid">
                                    <i class="fa fa-group">
                                        Data Product
                                    </i>
                                    <hr>

                                    <div class="row">

                                        <div class="col-md-12 col-sm-12 ">
                                            <div id="wizard" class="form_wizard wizard_horizontal">
                                                <ul class="wizard_steps">
                                                    <li>
                                                        <a href="#step-1" class="selected step-btn step-1">
                                                            <span class="step_no selected-step">1</span>
                                                            <span class="step_descr">
                                                                Step 1<br />
                                                                <small>General Form</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-2" class="step-btn step-2">
                                                            <span class="step_no">2</span>
                                                            <span class="step_descr">
                                                                Step 2<br />
                                                                <small>Product Detail</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-3" class="step-btn step-3">
                                                            <span class="step_no">3</span>
                                                            <span class="step_descr">
                                                                Step 3<br />
                                                                <small>Fitur Product</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#step-4" class="step-btn step-4">
                                                            <span class="step_no">4</span>
                                                            <span class="step_descr">
                                                                Step 4<br />
                                                                <small>T&C - Claim</small>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#step-5" class="step-btn step-5">
                                                            <span class="step_no">5</span>
                                                            <span class="step_descr">
                                                                Step 5<br />
                                                                <small>Perluasan Product</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <form id="fm">
                                                @csrf
                                                <div id="step-1">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Input Data Product</h5>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="select_main_product">Main Product
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <select id="select_main_product" name="select_main_product" required="required" class="form-control" disabled>
                                                                @if(!empty($get_product))
                                                                    <option value="{{ $get_product->id }}"> {{ $get_product->name }} </option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="sub_product">Sub Product
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <select id="sub_product" name="sub_product" required="required" class="form-control">
                                                                @if(!empty($get_sub_product))
                                                                    <option value="{{ $get_sub_product->id }}"> {{ $get_sub_product->name }} </option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="select_penanggung">Penanggung
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <select name="select_penanggung" id="select_penanggung"  required="required" class="form-control ">
                                                                @if(!empty($get_penanggung))
                                                                    <option value="{{ $get_penanggung->id }}"> {{ $get_penanggung->name }} </option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="product_name">Product Name
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" id="product_name" name="product_name" required="required" class="form-control" value="{{ $get_data->name }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="product_code">Product Code
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" id="product_code" name="product_code" required="required" class="form-control" value="{{ $get_data->code }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="description">Product Description
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <textarea class="form-control auto-resize" id="description" name="description" style="height: 100px !important"
                                                                >{{ $get_data->description }}</textarea>
                                                        </div>
                                                    </div>
                                                    @if($get_product->code == 'KBM')
                                                        @php $display = ''; @endphp
                                                    @else
                                                        @php $display = 'display:none'; @endphp
                                                    @endif
                                                    <div class="form-group row" style="{{ $display }}" id="div-file-bengkel">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="description">Rekanan Bengkel
                                                            {{-- <span class="required">*</span> --}}
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <div class="input-group mb-3">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" name="rekanan_bengkel"
                                                                        id="rekanan_bengkel" aria-describedby="rekanan_bengkel">
                                                                    <label class="custom-file-label" for="rekanan_bengkel">Choose file</label>

                                                                </div>
                                                            </div>
                                                            <p style="font-size: 12px;color:rgb(69, 99, 163)">{{ $get_data->daftar_bengkel }}</p>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-primary btn-sm btn-next" val='1' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-2" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Product Detail</h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <table class="table table-striped table-bordered" align="right" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Manfaat Perlindungan</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRow"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-perluasan">

                                                                    @php $benefit = json_decode($get_data->benefit); @endphp;

                                                                    @foreach($benefit as $kb => $valb)
                                                                        <tr id="perluasan-list-{{ $kb + 1}}">
                                                                            <td style='vertical-align:middle' colspan='2'>
                                                                                <textarea class='form-control auto-resize'
                                                                                    name='manfaat_perlindungan[]'>{{ $valb }}</textarea>
                                                                            </td>
                                                                            <td  align='center' style='vertical-align:middle'>
                                                                                <a class='btn btn-sm btn-danger delete-row' row-id="{{ $kb+1 }}">
                                                                                    <i class='fa fa-trash'></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-6">

                                                            <table class="table table-striped table-bordered" align="left" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Tidak Termasuk Manfaat Perlindungan</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowwithoutbenefit"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-without-benefit">
                                                                    @php $benefit_exception = json_decode($get_data->benefit_exception); @endphp;

                                                                    @foreach($benefit_exception as $kbe => $valbe)
                                                                        <tr id="without-benefit-list-{{ $kbe+1 }}">
                                                                            <td style='vertical-align:middle' colspan='2'>
                                                                                <textarea class='form-control auto-resize'
                                                                                    name='nameithoutbnfts[]'>{{ $valbe  }}</textarea>
                                                                            </td>
                                                                            <td  align='center' style='vertical-align:middle'>
                                                                                <a class='btn btn-sm btn-danger delete-row-wthoutbnft' row-id="{{ $kbe+1 }}">
                                                                                    <i class='fa fa-trash'></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="2" style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='2' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-3" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Fitur Product</h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table table-striped table-bordered" align="center" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="vertical-align: middle">
                                                                            Name
                                                                        </th>
                                                                        <th style="vertical-align: middle">
                                                                            Description
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowFitur"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-fitur">
                                                                    @php $fitur = json_decode($get_data->fitur); @endphp

                                                                    @foreach ($fitur as $kyf => $valf )
                                                                        <tr id="fitur-list-{{ $kyf+1 }}">
                                                                            <td style='vertical-align:middle'>
                                                                                <input type="text" class='form-control' name='name_fitur[]' value="{{ $valf->name }}">
                                                                            </td>
                                                                            <td style='vertical-align:middle'>
                                                                                <textarea class='form-control auto-resize'
                                                                                    name='description_fitur[]'>{{ $valf->description }}</textarea>
                                                                            </td>
                                                                            <td  align='center' style='vertical-align:middle'>
                                                                                <a class='btn btn-sm btn-danger delete-row-fitur' row-id="{{ $kyf+1 }}">
                                                                                    <i class='fa fa-trash'></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="3"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='3' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-4" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Syarat Ketentuan & Klaim</h5>
                                                        </div>
                                                    </div>
                                                    @php
                                                        $tnc    = json_decode($get_data->tnc);
                                                        $claim  = json_decode($get_data->claim);
                                                    @endphp
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="d-flex justify-content-end">
                                                                    <textarea class="form-control auto-resize" id="description_snk" name="description_snk" style="height: 100px !important;width:80%"
                                                                        >{{ $tnc->description }}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                    <textarea class="form-control auto-resize" id="description_claim" name="description_claim" style="height: 100px !important;width:80%"
                                                                        >{{ $claim->description }}</textarea>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="d-flex justify-content-end row">
                                                                <div class="input-group mb-3" style="width: 80%;">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" name="documentsnk" id="documentsnk" aria-describedby="documentsnk">
                                                                        <label class="custom-file-label" for="documentsnk">Choose file</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="d-flex justify-content-end">
                                                                <p style="font-size: 12px;color:rgb(69, 99, 163);">{{ $tnc->file }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="input-group mb-3" style="width: 80%">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" name="documentclaim" id="documentclaim" aria-describedby="documentclaim">
                                                                    <label class="custom-file-label" for="documentclaim">Choose file</label>
                                                                </div>
                                                            </div>
                                                            <p style="font-size: 12px;color:rgb(69, 99, 163)">{{ $claim->file }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <table class="table table-striped table-bordered" align="right" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Syarat dan Ketentuan</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowsnk"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-snk">
                                                                    @foreach ($tnc->fitur as $kytnc => $valtnc )


                                                                        <tr id="snk-list-{{ $kytnc+1 }}">
                                                                            <td style='vertical-align:middle' colspan='2'>
                                                                                <textarea class='form-control auto-resize' name='snk_list[]'>{{ $valtnc }}</textarea>
                                                                            </td>
                                                                            <td  align='center' style='vertical-align:middle'>
                                                                                <a class='btn btn-sm btn-danger delete-row-snk' row-id="{{ $kytnc+1 }}">
                                                                                    <i class='fa fa-trash'></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                        <div class="col-md-6">

                                                            <table class="table table-striped table-bordered" align="left" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Proses Klaim</span>
                                                                        </th>
                                                                        <th style="vertical-align: middle" width="20px">
                                                                            <a class="btn btn-primary" id="addRowClaim"> <i class="fa fa-plus"></i> </a>
                                                                        </th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody class="row-claim">
                                                                    @foreach ($claim->fitur as $kyclaim => $valclaim )
                                                                        <tr id="claim-list-{{ $kyclaim+1 }}">
                                                                            <td style='vertical-align:middle' colspan='2'>
                                                                                <textarea class='form-control auto-resize' name='claim_list[]'>{{ $valclaim }}</textarea>
                                                                            </td>
                                                                            <td  align='center' style='vertical-align:middle'>
                                                                                <a class='btn btn-sm btn-danger delete-row-claim' row-id="{{ $kyclaim+1 }}">
                                                                                    <i class='fa fa-trash'></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>

                                                            <table class="table table-striped table-bordered" align="left" style="width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2" align="left" style="vertical-align: middle">
                                                                            <span >Contact Klaim</span>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style='vertical-align:middle'>
                                                                            Telepon
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <input type="text" name="tlpn_claim" class="form-control"
                                                                                onkeypress="return hanyaAngka(event)" value="{{ $claim->contact->telepon }}">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style='vertical-align:middle' colspan='Telepon'>
                                                                            Whatsapp
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <input type="text" name="wa_claim" class="form-control"
                                                                                onkeypress="return hanyaAngka(event)" value="{{ $claim->contact->whatsapp }}">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style='vertical-align:middle' colspan='Telepon'>
                                                                            Email
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <input type="email" name="email_claim" class="form-control" value="{{ $claim->contact->email_claim }}">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style='vertical-align:middle' colspan='Telepon'>
                                                                            Direct Aplication
                                                                        </td>
                                                                        <td  align='center' style='vertical-align:middle'>
                                                                            <input type="text" name="application_claim" class="form-control" value="{{ $claim->contact->aplication }}">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="4"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-primary btn-sm btn-next" val='4' style="color: #fff">Next</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="step-5" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 style="text-align: center">Perluasan Product</h5>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="product_package">is include the package?
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <label>
                                                                <input type="checkbox" name="product_package" id="product_package" class="js-switch" {{ $get_data->is_package == 1?'checked':'' }}/>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <table class="table table-striped table-bordered" align="center" style="width: 80%">
                                                            <thead>
                                                                <tr>
                                                                    <th> Name </th>
                                                                    <th> Rate (%)</th>
                                                                    <th align="center" style="text-align: center"> Value </th>
                                                                    <th width="20px"> Available </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="package-product">

                                                                @foreach($perluasan as $key => $val)
                                                                    <tr>
                                                                        <td>{{ $val['perluasan_name'] }}</td>
                                                                        <td width="80px">
                                                                            <input type="text" name="rate_package_product[{{ $val['m_perluasan_id'] }}]" value="{{ $val['rate_or_value'] == 1? $val['rate'][0]['rate']:'' }}"
                                                                                class="form-control moneyFormat rate_product" onkeypress="return hanyaAngka(event)" {{ $val['rate_or_value'] == 1?'':'disabled' }}>
                                                                        </td>
                                                                        <td align="center">
                                                                            @if($val['rate_or_value'] == 1)
                                                                                -
                                                                            @else
                                                                                <a class="btn btn-sm btn-primary add-value" id-perluasan="{{ $val['m_perluasan_id'] }}"
                                                                                    id-product-perluasan="{{ $val['id'] }}" m_product_penanggung_id="{{ $val['m_product_penanggung_id'] }}" style="color:#fff">
                                                                                    <i class="fa fa-plus"> Add </i>
                                                                                </a>
                                                                            @endif
                                                                        </td>
                                                                        <td> <input type="checkbox" name="is_available_package_product[{{ $val['m_perluasan_id'] }}]" class="form-control" {{ $val['is_available'] == 1?'checked':'' }}></td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="pull-right">
                                                                <a class="btn btn-info btn-sm btn-prev" val="5"  style="color: #fff">Previous</a>
                                                                <a class="btn btn-dark btn-sm btn-save" style="color: #fff">Submit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                    <button type="button" class="close_modal" onclick="clearModal()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="input-group mb-3">
                                <input type="hidden" id="selected_perluasan">
                                <input type="text" class="form-control moneyFormat" id="amount-value" name="amount-value"
                                    placeholder="Input Amount" aria-label="Input Amount" aria-describedby="basic-addon2">
                                <input type="text" class="form-control moneyFormat" id="rate-value" name="rate-value"
                                    placeholder="Input Rate" aria-label="Input Rate" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-primary btn-info add-amount btn-sm" type="button" style="color: #fff">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <table class="table table-striped table-bordered" align="center" style="width: 80%">
                            <thead>
                                <tr>
                                    <th> value </th>
                                    <th> rate </th>
                                    <th width="20px"> Action </th>
                                </tr>
                            </thead>
                            <tbody id="list-value-polis">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close_modal" onclick="clearModal()">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
    <script>
        $(document).ready(function() {
            $('#select_main_product').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('m-sub-product.get-main-product')}}?filterDB=yes",
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                    id_encrypt:item.id_encrypt,
                                    code:item.code
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {
                getProductPolis(evt.params.data.id)

                if(evt.params.data.code == 'KBM'){
                    $('#div-file-bengkel').show()
                }else{
                    $('#div-file-bengkel').hide()
                }
            });

            $('#select_penanggung').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('penanggung.get-combobox-penanggung')}}?filterDB=yes",
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true

                }
            }).on('select2:select', function (evt) {
            });


            $('#sub_product').select2({
                placeholder: '--Choose--',
                allowClear: true,
                ajax: {
                    url: "{{ route('product-penanggung.get-sub-product')}}?filterDB=yes",
                    data: function (params) {
                        var query = {
                            search: params.term,
                            main_product_id:$('#select_main_product').val()

                        }
                        return query;
                    },
                    dataType: 'json',

                    processResults: function (data) {

                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                    id_encrypt:item.id_encrypt
                                }
                            })
                        };
                    },
                    cache: true

                }
            })
        });

        $('.btn-next').on('click',function(){
            let id = $(this).attr('val')
            $('#step-'+id).hide()
            let newid = parseInt(id)+1;
            $('#step-'+newid).show();


            $('.step-'+newid).addClass('selected')
            $('.step-'+newid).find('span').addClass('selected-step')


            $('.step-'+id).removeClass('selected')
            $('.step-'+id).find('span').removeClass('selected-step')
        })

        $('.btn-prev').on('click',function(){
            let id = $(this).attr('val')
            $('#step-'+id).hide()
            let newid = parseInt(id)-1;
            $('#step-'+newid).show();


            $('.step-'+newid).addClass('selected')
            $('.step-'+newid).find('span').addClass('selected-step')


            $('.step-'+id).removeClass('selected')
            $('.step-'+id).find('span').removeClass('selected-step')
        })


        $('#addRow').on('click',function(){
            buildRow()
        })


        var rowNumber = parseFloat("{{ count($benefit) }}");
        function buildRow(){
            rowNumber   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='perluasan-list-"+rowNumber+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='manfaat_perlindungan[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row' row-id='"+rowNumber+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-perluasan').append(rowtd)
        }

        $(document).on('click','.delete-row',function(){
            let row = $(this).attr('row-id');
            $('#perluasan-list-'+row).remove()
        })




        $('#addRowsnk').on('click',function(){
            buildRowSnk()
        })
        var rowsnk = parseFloat("{{ count($tnc->fitur) }}");
        function buildRowSnk()
        {
            rowsnk   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='snk-list-"+rowsnk+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='snk_list[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-snk' row-id='"+rowsnk+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-snk').append(rowtd)
        }
        $(document).on('click','.delete-row-snk',function(){
            let row = $(this).attr('row-id');
            $('#snk-list-'+row).remove()
        })


        $('#addRowClaim').on('click',function(){
            buildRowClaim()
        })
        var rowClaim = parseFloat("{{ count($claim->fitur) }}");
        function buildRowClaim()
        {
            rowClaim   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='claim-list-"+rowClaim+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='claim_list[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-claim' row-id='"+rowClaim+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-claim').append(rowtd)
        }
        $(document).on('click','.delete-row-claim',function(){
            let row = $(this).attr('row-id');
            $('#claim-list-'+row).remove()
        })


        $('#addRowwithoutbenefit').on('click',function(){
            buildRowWithoutbenefit()
        })

        var rowNumbernobnft = parseFloat("{{ count($benefit_exception) }}");
        function buildRowWithoutbenefit(){

            rowNumbernobnft   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='without-benefit-list-"+rowNumbernobnft+"'>";
                rowtd   +=      "<td style='vertical-align:middle' colspan='2'> <textarea class='form-control auto-resize' name='nameithoutbnfts[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-wthoutbnft' row-id='"+rowNumbernobnft+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";

            $('.row-without-benefit').append(rowtd)
        }

        $(document).on('click','.delete-row-wthoutbnft',function(){
            let row = $(this).attr('row-id');
            $('#without-benefit-list-'+row).remove()
        })

        $('#addRowFitur').on('click',function(){
            buildRowFitur()
        })
        rowNumberFitur = parseFloat("{{ count($fitur) }}");
        function buildRowFitur(){
            rowNumberFitur   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='fitur-list-"+rowNumberFitur+"'>";
                rowtd   +=      "<td style='vertical-align:middle'><input type='text' class='form-control' name='name_fitur[]'> </td>";
                rowtd   +=      "<td style='vertical-align:middle'>  <textarea class='form-control auto-resize' name='description_fitur[]'></textarea> </td>";
                rowtd   +=      "<td  align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row-fitur' row-id='"+rowNumberFitur+"'><i class='fa fa-trash'></i></a> </td>";
                rowtd   +=  "</tr>";
            $('.row-fitur').append(rowtd)
        }
        $(document).on('click','.delete-row-fitur',function(){
            let row = $(this).attr('row-id');
            $('#fitur-list-'+row).remove()
        })
        $(document).keyup('.onlyNumber',function(event){
            hanyaAngka(event)
        })

        $(document).on('click','.add-value',function(){
            let id_perluasan            = $(this).attr('id-perluasan')
            let id_product_perluasan    = $(this).attr('id-product-perluasan');
            let m_product_penanggung_id = $(this).attr('m_product_penanggung_id');

            $('#selected_perluasan').val(id_perluasan)
            getDataCoverage(id_perluasan,m_product_penanggung_id)
        })

        function getDataCoverage(id_perluasan=null,m_product_penanggung_id=null)
        {
            $.ajax({
                url : '{{route("product-penanggung.get-coverage-value")}}',
                type: 'GET',
                data : {
                    id_perluasan            : id_perluasan,
                    m_product_penanggung_id : m_product_penanggung_id
                },
                success: (response) => {
                    let rate = response.data;

                    let table_html = '';
                    $.each( rate, function( key, value ) {
                        // console.log(value.id_rate)
                        table_html += '<tr>';
                        table_html +=     '<td>'+formatRupiah(value.coverage_value)+'</td>';
                        table_html +=     '<td>'+value.rate+'</td>';
                        table_html +=     '<td><a href="#" class="delete-rate" row-id="'+value.id_rate+'" id-perluasan="'+id_perluasan+'" product-penanggung-id="'+m_product_penanggung_id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
                        table_html += '</tr>';

                    });

                    $('#list-value-polis').html(table_html);
                    $('#modalData').modal('show')

                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            })
        }

        $('.add-amount').on('click',function(){
            let amount_value            = $('#amount-value').val();
            let unmusk_value            = $('#amount-value').maskMoney('unmasked')[0];
            let rate_value              = $('#rate-value').val();
            let unmusk_rate             = $('#rate-value').maskMoney('unmasked')[0];
            let id_perluasan            = parseInt($('#selected_perluasan').val())
            let m_product_penanggung_id = "{{ $get_data->id }}";
            $.ajax({
                url : '{{route("product-penanggung.add-rate-value")}}',
                type: 'POST',
                data:{
                    id_perluasan            : id_perluasan,
                    unmusk_value            : unmusk_value,
                    unmusk_rate             : unmusk_rate,
                    m_product_penanggung_id : m_product_penanggung_id
                },
                headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                success: (response) => {
                    getDataCoverage(id_perluasan,m_product_penanggung_id)
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        })
        $(document).on('click','.delete-rate',function(){
            let row_id                  = $(this).attr('row-id');
            let m_product_penanggung_id = $(this).attr('product-penanggung-id');
            let id_perluasan            = $(this).attr('id-perluasan');

            Swal.fire({
                icon: 'warning',
                title: 'Delete Data',
                text: 'Are You Sure Want To Delete ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Delete it!',
                confirmButtonText: 'Yes, Delete it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $.ajax({
                        url : '{{route("product-penanggung.delete-rate-value")}}',
                        type: 'POST',
                        data:{
                            id_rate                 : row_id,
                            m_product_penanggung_id : m_product_penanggung_id,
                            id_perluasan            : id_perluasan
                        },
                        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                        success: (response) => {
                            getDataCoverage(id_perluasan,m_product_penanggung_id)
                        },
                        error: function(xhr, status, error){
                            var obj = JSON.parse(xhr.responseText)
                            swalError('Failed', obj.message)
                        }
                    });
                }
            });
        })

        function clearModal(){
            $('#amount-value').val('');
            $('#selected_perluasan').val('')
            $('#modalData').modal('hide')
        }

        $('.btn-save').on('click',function(){
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Save ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        })
        $("#fm").submit(function(event){
            event.preventDefault();
            var form_data =  new FormData(this);
            // let rate = $('.rate_product').maskMoney('unmasked');
            // console.log(rate);
            // return false;
            form_data.append("id","{{ $get_data->id }}");
            let url_save    ="{{ route('product-penanggung.update') }}"
            $.ajax({
                url : url_save,
                type: 'POST',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    swalSuccess('Success','Success To Save');
                    reloadPage()
                    window.location.href="{{ route('product-penanggung') }}"
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        });
    </script>

@endsection()
