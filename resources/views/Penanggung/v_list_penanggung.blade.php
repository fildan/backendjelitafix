@extends('layout.app')
@section('title', 'Penanggung List')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                Master Penanggung
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" id="addData">Add Penanggung</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="penanggung-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Picture</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                        <button type="button" class="close_modal" onclick="clearModal()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Upload Picture</label>
                                                    <div class="col-sm-8">
                                                        <div class="for-picture-view">
                                                            <img src="{{asset('images/user.png')}}" id="logo-penanggung"  class="img-preview mb-1" style="height: 90px !important">
                                                            <input type="file" name="logo_penanggung" required row-name="file-1" id="" onchange="readURL(this,1)">
                                                        </div>
                                                        <span style="color: red;font-size: 10px">*This First Picture Cant Be Null</span>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label"> Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="name" id="name" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label"> Code</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="code" id="code" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label"> Join Date</label>
                                                <div class="col-sm-8">
                                                    <input type="date" class="form-control" name="join_date" id="join_date" placeholder="" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Description</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="description" name="description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal" onclick="clearModal()">Close</button>
                        <button type="button" class="btn btn-primary" id="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
<script>
    var url_save = "{{ route('penanggung.save') }}";
    var table = $('#penanggung-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('penanggung.get-list') }}",
        'columns': [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            {
                data: 'picture' ,
                name:'picture',
                "className": "v-align-tables text-center",
                render: function (data){
                    return "<img src='"+data+"' style='widht:150px !important;height:50px !important'>"
                }
            },
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'code' ,name:'code', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });

    $('#addData').on('click',function(){
        modal_show();
        url_save = "{{ route('penanggung.save') }}";
    })

    function editClick($id){
        $.ajax({
            url : '{{route("penanggung.edit")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {
                console.log(response);
                $('#name').val(response.data.name);
                $('#description').val(response.data.description);
                $('#code').val(response.data.code)
                $('#join_date').val(response.data.join_date)

                let url_picture = "{{ asset('images/user.png') }}";
                if(response.data.picture != ''){
                    url_picture = "{{ asset('storage/logo-penanggung') }}"+"/"+response.data.picture;
                }
                $('#logo-penanggung').attr('src',url_picture)
                modal_show();
                url_save = "{{route('penanggung.update')}}?id="+$id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    $('#save').on('click',function(){

        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $('#fm').submit()
            }
        });
    })
    $("#fm").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : url_save,
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                clearModal();
                tableReload()
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        });
    });
    function deleteClick(id)
    {
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('penanggung.delete')}}?id="+id;
                $.ajax({
                    url : url,
                    type: 'POST',
                    data:{
                        id:id
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                        tableReload()
                    }
                });
            }
        });
    }
    function tableReload(){
        table.ajax.reload(null,false);
    }
    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}
    function clearModal(){
	    $('#modalData').modal('hide');
        $('#fm').find('input:text, input:password, textarea, input:file')
                    .each(function () {
                        $(this).val('');
                    });

    }
</script>
@endsection()
