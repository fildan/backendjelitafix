@extends('layout.app')
@section('title', 'Master City')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>Sub Product Data<small></small></h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" id="addData">Add Sub Product</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="table-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Main Product Name</th>
                                                <th>Sub Product Name</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                        <button type="button" class="close_modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-6 col-sm-6">
                                        <div class="row">
                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Upload Picture</label>
                                                    <div class="col-sm-8">
                                                        <div class="for-picture-view">
                                                            <img src="{{asset('images/user.png')}}" id="review-img"  class="foto-product-1 img-preview mb-1">
                                                            <p style="color: red;font-size: 10px" class="mb-0">*Max 1 MB (.jpg .png .jpeg)</p>
                                                        </div>
                                                        <input type="file" name="product_picture" required row-name="file-1" id="file1" onchange="readURL(this,1)">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Main Product</label>
                                            <div class="col-sm-10">
                                                <select name="main_product" id="select_main_product" class="form-control" style="width: 100%;">

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Sub Product</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Comprehensive.....">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Description</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="description" name="description"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade bd-example-modal-sm" id="unlockModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Publish This Feature</h5>
                        <button type="button" class="" onclick="clearmodalPublish()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm-published">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Url</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="url" id="url" placeholder="">
                                                <input type="hidden" class="form-control" name="id_sub" id="id_sub" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="clearmodalPublish()">Close</button>
                        <button type="button" class="btn btn-primary" id="published">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')

<script type="text/javascript">

    var url_save    ="{{ route('m-sub-product.save') }}"
    $(document).ready(function(){
		$('#select_main_product').select2({
			placeholder: '--Choose--',
			allowClear: true,
            dropdownParent: $('#modalData'),
			ajax: {
				url: "{{ route('m-sub-product.get-main-product')}}?filterDB=yes",
				dataType: 'json',

				processResults: function (data) {

					return {
						results: $.map(data, function (item) {
							return {
								text: item.text,
								id: item.id,
							}
						})
					};
				},
				cache: true

			}
		}).on('select2:select', function (evt) {
		});
	})
    var table = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('m-sub-product.get-list') }}",
        'columns': [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'main_product_name' ,name:'main_product_name', "className": "v-align-tables"},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });

    $('#save').on('click',function(){

        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $('#fm').submit()
            }
        });
    })
    $("#fm").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : url_save,
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                clearModal();
                tableReload()
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        });
    });

    function editClick($id=null){
        $.ajax({
            url : '{{route("m-sub-product.edit")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {
                // console.log(response);
                $('#name').val(response.data.name);
                $('#description').val(response.data.description);

                var select_main_product = new Option(response.data.product_main_name, response.data.m_product_id, false, false);
                $('#select_main_product').append(select_main_product).trigger('change');

                let url_picture = "{{ asset('images/user.png') }}";
                if(response.data.picture != ''){
                    url_picture = "{{ asset('storage/sub-product-picture') }}"+"/"+response.data.picture;
                }

                $('.img-preview').attr('src',url_picture)
                modal_show();
                url_save = "{{route('m-sub-product.update')}}?id="+response.data.id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    function deleteClick(id)
    {
        // alert(id)
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('m-sub-product.delete')}}";
                $.ajax({
                    url : url,
                    type: 'GET',
                    data:{
                        id:id
                    },
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        clearModal();
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                        modal_close();
                    }
                });
            }
        });
    }

    $('#addData').on('click',function(){
        modal_show();
        clearModal();
        url_save    ="{{ route('m-sub-product.save') }}"
    })
    $('.close_modal').on('click',function(){
        clearModal();
    })

    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}

    function clearModal(){
        let url_picture = "{{ asset('images/user.png') }}";
        $('.img-preview').attr('src',url_picture)
	    $('#modalData').modal('hide');
        $('#fm').find('input:text, input:password, textarea, input:file')
                    .each(function () {
                        $(this).val('');
                    });
        $('#select_main_product').val(null).trigger('change');

    }
    function tableReload(){
        table.ajax.reload(null,false);
    }

    function unlockFeature($id){
        $('#unlockModal').modal('show');
        $('#id_sub').val($id)
    }
    $('#published').on('click',function(){
        $('#fm-published').submit()
    })
    $("#fm-published").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : "{{ route('m-sub-product.publish') }}",
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                clearmodalPublish();
                tableReload()
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        });
    });
    function clearmodalPublish(){
        $('#unlockModal').modal('hide');
        $('#fm-published').find('input:text, input:hidden')
                    .each(function () {
                        $(this).val('');
        });
    }
</script>
@endsection()
