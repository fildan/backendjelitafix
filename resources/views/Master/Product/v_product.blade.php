@extends('layout.app')
@section('title', 'Master Product')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                Product Data
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="{{ route('m-product.add') }}">
                                        <button type="button" class="btn btn-primary btn-sm" id="addData">Add Product</button>
                                    </a>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="product-data" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Product Name</th>
                                                <th>Code</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


@endsection()
@section('script')
<script>
    var table = $('#product-data').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('m-product.get-list') }}",
        'columns': [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'code' ,name:'code', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });


    function editClick($id){
        // alert($id);
        window.location.href="{{ route('m-product.edit') }}?id="+$id;
    }

    function deleteClick(id)
    {
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('m-product.delete')}}?id="+id;
                $.ajax({
                    url : url,
                    type: 'GET',
                    data:{
                        id:id
                    },
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                        tableReload()
                    }
                });
            }
        });
    }
    function tableReload(){
        table.ajax.reload(null,false);
    }
</script>
@endsection()
