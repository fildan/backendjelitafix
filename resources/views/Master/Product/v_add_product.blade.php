@extends('layout.app')
@section('title', 'Master Product')
@section('head')
    <style>

    </style>
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add Main Product<small> JELAS</small></h2>
                        <div class="pull-right">
                            <!-- <button type="button" class="btn btn-primary" id="muncul_popup">Tambah Divisi</button> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container-fluid">
                                    <i class="fa fa-group">
                                        Data Product
                                    </i>
                                    <hr>
                                    <div class="row">
                                        <form id="fm" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Upload Picture</label>
                                                            <div class="col-sm-8">
                                                                <div class="for-picture-view">
                                                                    <img src="{{asset('images/user.png')}}" id=""  class="foto-product-1 img-preview mb-1">
                                                                    <input type="file" name="product_picture" required row-name="file-1" id="file1" onchange="readURL(this,1)">
                                                                </div>
                                                                <span style="color: red;font-size: 10px">*This First Picture Cant Be Null</span>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="row">

                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Product Name</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" required class="form-control" name="product_name" id="nama_product" style="width: 100%">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Product Code</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" required class="form-control" name="product_code" id="product_code" style="width: 100%">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Url</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" required class="form-control" name="url" id="url" style="width: 100%">
                                                                <span style="color: red;font-size: 10px">*Please Dont Use Spesial Character</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Description</label>
                                                            <div class="col-sm-8">
                                                                {{-- <input type="text" class="form-control" id="nama_pasien" style="width: 100%"> --}}
                                                                <textarea class="form-control" required name="description" id="description"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <a href="{{ route('m-product') }}">
                                                    <button class="btn btn-dark" type="button">Back</button>
                                                </a>
                                                <button class="btn btn-primary" type="button" id="Submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                  <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
    <script>
        $('#Submit').on('click',function(){
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Save ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        })

        $("#fm").submit(function(event){
            event.preventDefault();
            var form_data =  new FormData(this);
            let url_save    ="{{ route('m-product.save') }}"
            $.ajax({
                url : url_save,
                type: 'POST',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    swalSuccess('Success','Success To Save');
                    reloadPage()

                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        });
    </script>
@endsection()
