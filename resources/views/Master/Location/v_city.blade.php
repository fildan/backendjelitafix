@extends('layout.app')
@section('title', 'Master City')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>City Data<small></small></h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" id="addData">Add City</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="city-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Province</th>
                                                <th>City</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Informasi</h5>
                        <button type="button" class="close_modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-2 col-form-label">Province Name</label>
                                                    <div class="col-sm-10">
                                                        <select name="province_name" id="select_province" class="form-control" style="width: 100%;">

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-2 col-form-label">City Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="city_name" id="city_name" placeholder="Jakarta Selatan">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
		$('#select_province').select2({
			placeholder: '--Choose--',
			allowClear: true,
            dropdownParent: $('#modalData'),
			ajax: {
				url: "{{ route('city.get-province')}}?filterDB=yes",
				dataType: 'json',

				processResults: function (data) {

					return {
						results: $.map(data, function (item) {
							return {
								text: item.text,
								id: item.id,
							}
						})
					};
				},
				cache: true

			}
		}).on('select2:select', function (evt) {
		});
	})
    var url_save = "";
    var table = $('#city-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('city.get-list') }}",
        'columns': [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'province_name' ,name:'province_name', "className": "v-align-tables"},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });

    $('#addData').on('click',function(){
        modal_show();
        clearModal();
        url_save = "{{route('city.save')}}";
    })

    function editClick($id=null){
        $.ajax({
            url : '{{route("city.get-data")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {
                $('#city_name').val(response.data.name);
                var select_province = new Option(response.data.province_name, response.data.province_id, false, false);
                $('#select_province').append(select_province).trigger('change');

                modal_show();
                url_save = "{{route('city.update')}}?id="+response.data.id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    $('#save').on('click',function(){
		let province_name = $('#select_province').val();
        let city_name = $('#city_name').val();
        if(province_name == '' || city_name == ''){
            swalError('Info','Please Completed All Data')
        }else{
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Save ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        }
	})
	$("#fm").submit(function(event){
		event.preventDefault();
		var form_data = $(this).serialize();
		$.ajax({
			url : url_save,
			type: 'POST',
			data : form_data,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                modal_close();
                tableReload();
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
                modal_close();
            }
        });
	});

    function deleteClick(id)
    {
        // alert(id)
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('city.delete')}}";
                $.ajax({
                    url : url,
                    type: 'GET',
                    data:{
                        id:id
                    },
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        modal_close();
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                        modal_close();
                    }
                });
            }
        });
    }
    $('.close_modal').on('click',function(){
        modal_close();
    })
    function modal_close(){
        clearModal();
        $('#modalData').modal('hide');
    }
    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}

    function clearModal(){
        $('#province_name').val('')
    }
    function tableReload(){
        table.ajax.reload(null,false);
    }
</script>
@endsection()
