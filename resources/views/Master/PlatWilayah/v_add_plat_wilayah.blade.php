@extends('layout.app')
@section('title', 'Add Plat')
@section('head')
    <style>

    </style>
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add Plat<small> JELAS</small></h2>
                        <div class="pull-right">
                            <!-- <button type="button" class="btn btn-primary" id="muncul_popup">Tambah Divisi</button> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container-fluid">
                                    <i class="fa fa-group">
                                        Data Plat Wilayah
                                    </i>
                                    <hr>
                                    <form id="fm">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <div class="clearfix"></div>
                                                <table class="table table-striped table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th style="vertical-align: middle">Code</th>
                                                        <th style="vertical-align: middle">Wilayah</th>
                                                        <th style="vertical-align: middle" width="20px" align="center">Action</th>
                                                        <th style="vertical-align: middle" width="20px">
                                                            <a class="btn btn-primary" id="addRow"> <i class="fa fa-plus"></i> </a>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="row-perluasan">
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <a href="{{ route('plat-wilayah') }}">
                                                    <button class="btn btn-dark" type="button">Back</button>
                                                </a>
                                                <button class="btn btn-primary" type="button" id="save">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                  <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
    <script>
        $('#save').on('click',function(){
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Save ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        })

        $("#fm").submit(function(event){
            event.preventDefault();
            var form_data =  new FormData(this);
            let url_save    ="{{ route('plat-wilayah.save') }}"
            $.ajax({
                url : url_save,
                type: 'POST',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    swalSuccess('Success','Success To Save');
                    reloadPage()

                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        });

        $('#addRow').on('click',function(){
            buildRow()
        })


        var rowNumber = 0;
        function buildRow(){
            rowNumber   += 1;
            var rowtd    = "";
                rowtd   += "<tr id='perluasan-list-"+rowNumber+"'>";
                rowtd   +=      "<td style='vertical-align:middle'> <input type='text' name='code[]' class='form-control'> </td>";
                rowtd   +=      "<td style='vertical-align:middle'> <textarea class='form-control' name='wilayah[]'></textarea> </td>";
                rowtd   +=      "<td colspan='2' align='center' style='vertical-align:middle'>  <a class='btn btn-sm btn-danger delete-row' row-id='"+rowNumber+"'><i class='fa fa-trash'></i> </td>";
                rowtd   +=  "</tr>";

            $('.row-perluasan').append(rowtd)
        }

        $(document).on('click','.delete-row',function(){
            let row = $(this).attr('row-id');
            $('#perluasan-list-'+row).remove()
        })
    </script>
@endsection()

