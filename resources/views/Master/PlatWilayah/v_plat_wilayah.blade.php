@extends('layout.app')
@section('title', 'Plat Wilayah')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                Master Plat Wilayah
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="{{ route('plat-wilayah.add') }}">
                                        <button type="button" class="btn btn-primary btn-sm" id="addData">Add Plat Wilayah</button>
                                    </a>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="acc-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Code</th>
                                                <th>District</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                        <button type="button" class="close_modal" onclick="clearModal()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label"> Code</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="code" id="code">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">District</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="wilayah" name="wilayah"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal" onclick="clearModal()">Close</button>
                        <button type="button" class="btn btn-primary" id="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
<script>
    var table = $('#acc-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('plat-wilayah.get-list') }}",
        'columns': [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'code' ,name:'code', "className": "v-align-tables"},
            { data: 'wilayah' ,name:'wilayah', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });


    function editClick($id){
        $.ajax({
            url : '{{route("plat-wilayah.edit")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {
                // console.log(response);
                $('#code').val(response.data.code);
                $('#wilayah').val(response.data.wilayah);
                modal_show();
                url_save = "{{route('plat-wilayah.update')}}?id="+$id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    $('#save').on('click',function(){

        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $('#fm').submit()
            }
        });
    })
    $("#fm").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : url_save,
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                clearModal();
                tableReload()
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        });
    });
    function deleteClick(id)
    {
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('plat-wilayah.delete')}}?id="+id;
                $.ajax({
                    url : url,
                    type: 'POST',
                    data:{
                        id:id
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                        tableReload()
                    }
                });
            }
        });
    }
    function tableReload(){
        table.ajax.reload(null,false);
    }
    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}
    function clearModal(){
	    $('#modalData').modal('hide');
        $('#fm').find('input:text, input:password, textarea, input:file')
                    .each(function () {
                        $(this).val('');
                    });

    }
</script>
@endsection()
