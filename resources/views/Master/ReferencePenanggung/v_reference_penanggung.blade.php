@extends('layout.app')
@section('title', 'Referensi Penanggung')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                            Data Referensi Penanggung
                            <small></small>
                        </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" id="addReference">Add Reference</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="table-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                {{-- <th width="10px"></th> --}}
                                                <th width="10px">No</th>
                                                <th>Nama Asuransi</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Informasi</h5>
                    <button type="button" class="close_modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="fm">
                    @csrf
                    <div class="modal-body">
                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">Nama Asuransi </label>
                                                <div class="col-sm-2">
                                                    <select name="type" id="type_company" required class="form-control" style="width: 100%">
                                                    </select>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text" name="name" id="name_insurance" required class="form-control" style="height: 38px">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12">
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-2 col-form-label">COB </label>
                                        <div class="col-sm-10">
                                            <select name="cob[]" id="select_cob" required class="form-control" style="width: 100%">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close_modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection()
@section('script')
<script>
    var url_save = "";

    $(document).ready(function(){
        $('#type_company').select2({
            placeholder: '--Choose--',
            allowClear: true,
            dropdownParent: $('#modalData'),
            ajax: {
                url: "{{ route('company.get-type')}}?filterDB=yes",
                dataType: 'json',

                processResults: function (data) {

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.text,
                                id: item.id,
                            }
                        })
                    };
                },
                cache: true

            }
        }).on('select2:select', function (evt) {

        });
        
        $('#select_cob').select2({
            placeholder: '--Choose--',
            allowClear: true,
            dropdownParent: $('#modalData'),
            multiple:true,
            ajax: {
                url: "{{ route('m-sub-product.get-main-product')}}?filterDB=yes",
                dataType: 'json',

                processResults: function (data) {

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.text,
                                id: item.id,
                            }
                        })
                    };
                },
                cache: true

            }
        })
    })
    var table = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('reference-penanggung.get-list') }}",
        'columns': [

            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'asuransi_name' ,name:'asuransi_name', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });

    $('#addReference').on('click',function(){
        modal_show();
        clearModal();
        url_save = "{{ route('reference-penanggung.save') }}";
    })
    $('#save').on('click',function(){

        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $('#fm').submit()
            }
        });
	})

    $("#fm").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : url_save,
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                clearModal()
                tableReload()
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        });
    });

    $('.close_modal').on('click',function(){
        modal_close();
    })
    function modal_close(){
        clearModal();
        $('#modalData').modal('hide');
    }

    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}

    function clearModal(){
	    $('#modalData').modal('hide');
        $('#fm').find('input:text, input:password, select')
                    .each(function () {
                        $(this).val('');
                    });

    }

    function tableReload(){
        table.ajax.reload(null,false);
    }

    function editClick($id){
        $.ajax({
            url : '{{route("reference-penanggung.edit")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {

                // console.log(JSON.parse(response.data.m_product));
                var m_product = JSON.parse(response.data.m_product);

                var test                = [];
                $.each( m_product, function( key, value ) {
                    test.push(new Option(value['name'], value['id'], true, true));
                });

                $('#select_cob').append(test).trigger('change');

                var company_type = new Option(response.data.get_company_type.name, response.data.get_company_type.id, true, true);
                $('#type_company').append(company_type).trigger('change');
                $('#name_insurance').val(response.data.name)



                modal_show()
                url_save = "{{ route('reference-penanggung.update') }}?code="+$id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    function deleteClick(code=null)
    {
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $.ajax({
                    url : "{{ route('reference-penanggung.delete') }}",
                    type: 'POST',
                    data:{
                        code:code
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        if(response.code == 200){
                            swalSuccess('Success','Data Berhasil Dihapus');
                            clearModal()
                            tableReload()
                        }
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)

                    }
                });
            }
        });

    }
</script>
@endsection()
