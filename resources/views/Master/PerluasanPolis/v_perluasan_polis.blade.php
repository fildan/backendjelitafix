@extends('layout.app')
@section('title', 'Perluasan Polis')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                Master Perluasan Polis
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="{{ route('perluasan-polis.add') }}">
                                        <button type="button" class="btn btn-primary btn-sm" id="addData">Add Perluasan</button>
                                    </a>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="perluasan-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Main Product Name</th>
                                                <th>Perluasan Polis</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                        <button type="button" class="close_modal" onclick="clearModal()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="fm">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Main Product</label>
                                            <div class="col-sm-10">
                                                <select name="main_product" id="select_main_product" class="form-control" style="width: 100%;">

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Perluasan Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="name" id="name" placeholder="perluasan anda.....">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Rate Or Value</label>
                                            <div class="col-sm-10">
                                                <select class="form-control select2" name="rate_or_value" id="rate_or_value" style="width:100%">
                                                    <option value="1"> Rate </option>
                                                    <option value="2"> Value </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Description</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="description" name="description"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close_modal" onclick="clearModal()">Close</button>
                        <button type="button" class="btn btn-primary" id="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('script')
<script>
    $(document).ready(function(){
		$('#select_main_product').select2({
			placeholder: '--Choose--',
			allowClear: true,
            dropdownParent: $('#modalData'),
			ajax: {
				url: "{{ route('m-sub-product.get-main-product')}}?filterDB=yes",
				dataType: 'json',

				processResults: function (data) {

					return {
						results: $.map(data, function (item) {
							return {
								text: item.text,
								id: item.id,
							}
						})
					};
				},
				cache: true

			}
		}).on('select2:select', function (evt) {
		});
	})
    var table = $('#perluasan-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('perluasan-polis.get-list') }}",
        'columns': [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
            { data: 'product_name' ,name:'product_name', "className": "v-align-tables"},
            { data: 'name' ,name:'name', "className": "v-align-tables"},
            { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
        ],
    });


    function editClick($id){
        $.ajax({
            url : '{{route("perluasan-polis.edit")}}',
            type: 'GET',
            data : {
                id: $id
            },
            success: (response) => {
                // console.log(response);
                $('#name').val(response.data.name);
                $('#description').val(response.data.description);

                var select_main_product = new Option(response.data.get_product.name, response.data.m_product_id, false, false);
                $('#select_main_product').append(select_main_product).trigger('change');

                $('#rate_or_value').val(response.data.rate_or_value)
                modal_show();
                url_save = "{{route('perluasan-polis.update')}}?id="+$id;
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        })
    }

    $('#save').on('click',function(){

        Swal.fire({
            icon: 'warning',
            title: 'Save Data',
            text: 'Are You Sure Want To Save ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                $('#fm').submit()
            }
        });
    })
    $("#fm").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : url_save,
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                swalSuccess('Success','Success To Save');
                clearModal();
                tableReload()
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                swalError('Failed', obj.message)
            }
        });
    });
    function deleteClick(id)
    {
        Swal.fire({
            icon: 'warning',
            title: 'Delete Data',
            text: 'Are You Sure Want To Delete ?',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Delete it!',
            confirmButtonText: 'Yes, Delete it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                let url = "{{route('perluasan-polis.delete')}}?id="+id;
                $.ajax({
                    url : url,
                    type: 'POST',
                    data:{
                        id:id
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {
                        swalSuccess('Success','Success To Delete');
                        tableReload();
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                        tableReload()
                    }
                });
            }
        });
    }
    function tableReload(){
        table.ajax.reload(null,false);
    }
    function modal_show(){
		$('#modalData').modal({
	        backdrop: 'static',
	        keyboard: false
	    });
	    $('#modalData').modal('show');
	}
    function clearModal(){
	    $('#modalData').modal('hide');
        $('#fm').find('input:text, input:password, textarea, input:file')
                    .each(function () {
                        $(this).val('');
                    });

    }
</script>
@endsection()
