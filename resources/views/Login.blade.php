<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login My Chatting</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
  <link rel="icon" type="image/png" href="{{ asset('login/images/icons/favicon.ico')}}"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ asset('login/css/util.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('login/css/main.css')}}">
  <link rel="stylesheet" href="{{asset('swal/dist/sweetalert2.min.css')}}">
<!--===============================================================================================-->
</head>
<body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100" id="form_login">
                <span class="login100-form-title p-b-26">
                    Welcome
                </span>

                <span class="login100-form-title p-b-48">
                    <i class="zmdi zmdi-font"></i>
                </span>
                @if(Session::has('error'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Login Failed!</strong> {{Session::get('error')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form id="fm">
                    @csrf
                    <div class="wrap-input100 validate-input" >
                        <input class="input100" type="text" name="username" id="username_login">
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input class="input100" type="password" name="password" id="password_login">
                        <span class="focus-input100" data-placeholder="Password"></span>
                    </div>
                </form>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn" id="button_login">
                            Login
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>




<!--===============================================================================================-->
  <script src="{{ asset('login/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{ asset('login/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{ asset('login/vendor/bootstrap/js/popper.js')}}"></script>
  <script src="{{ asset('login/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
<!--===============================================================================================-->
  <script src="{{ asset('login/vendor/daterangepicker/moment.min.js')}}"></script>
  <script src="{{ asset('login/vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{ asset('login/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{ asset('login/js/main.js')}}"></script>
  <script src="{{asset('swal/dist/sweetalert2.min.js')}}"></script>

</body>
</html>
<script type="text/javascript">
    $('#form_login').keyup(function(e){
        if(e.keyCode == 13)
        {
            LoginPost();
        }
    })
    $('#button_login').on('click',function(){
        LoginPost()
    })

    function LoginPost(){
        var username_login = $('#username_login').val();
        var password_login = $('#password_login').val();

        if(username_login == '' || password_login == ''){
            Swal.fire({
                title: 'Failed',
                text: 'Please Input Username And Password',
                icon: 'info',
                confirmButtonText: 'Oke'
            })
        }else{
            $('#fm').submit()
        }
    }

    $("#fm").submit(function(event){
        event.preventDefault();
        var form_data =  new FormData(this);
        $.ajax({
            url : "{{ route('login.action') }}",
            type: 'POST',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                if(response.code === 200){
                    Swal.fire({
                        title: 'Success',
                        text: response.message,
                        icon: 'success',
                        confirmButtonText: 'Oke'
                    }).then((result) => {
                        window.location.href = "{{ url('/') }}"
                    })
                }else{
                    Swal.fire({
                        title: 'Failed',
                        text: response.message,
                        icon: 'info',
                        confirmButtonText: 'Oke'
                    })
                }
            },
            error: function(xhr, status, error){
                var obj = JSON.parse(xhr.responseText)
                Swal.fire({
                    title: 'Failed',
                    text: obj.message,
                    icon: 'info',
                    confirmButtonText: 'Oke'
                })
            }
        });
    });
</script>
