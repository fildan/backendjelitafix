@extends('layout.app')
@section('title', 'Detail Order SB')
@section('head')
    <style>
        .fz12{
            font-size: 12px !important;
        }
        .c-black{
            color: black !important;
        }
    </style>
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Verifikasi Pembayaran</h2>
                        <div class="pull-right">
                            <!-- <button type="button" class="btn btn-primary" id="muncul_popup">Tambah Divisi</button> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                    <i class="fa fa-drivers-license">
                                                        Detail Order -
                                                        <b style="color: red"> {{ $get_order->code }}</b>
                                                    </i>
                                                  <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nama Tertanggung</label>
                                                            <div class="col-sm-8">
                                                                <label for="inputPassword" class="c-black col-form-label">
                                                                    {{ ucwords($get_order->get_order_company->name).', ('.strtoupper($get_order->get_order_company->get_company_type->name).')' }}
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Jenis Badan Usaha</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ ($get_order->get_order_company->jenis_usaha == 1)?'Non BUMN / Swasta':'BUMN'}}
                                                                    </label>
                                                                </div>
                                                        </div>

                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Bidang Usaha</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ $get_order->get_order_company->bidang_usaha }}
                                                                    </label>
                                                                </div>
                                                        </div>

                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Alamat</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ $get_order->get_order_company->address.', '.$get_order->get_order_company->get_province->name.', '.$get_order->get_order_company->get_city->name.', '.$get_order->get_order_company->postcode }}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Nama Project</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ $get_order->get_order_sb->get_project->name}}
                                                                    </label>
                                                                </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nama Obligee</label>
                                                            <div class="col-sm-8">
                                                                <label for="inputPassword" class="c-black col-form-label">
                                                                    {{ ucwords($get_order->get_order_sb->get_obligee->company_name).', ('.strtoupper($get_order->get_order_sb->get_obligee->get_company_type->name).')'}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Periode</label>
                                                            <div class="col-sm-8">
                                                                <label for="inputPassword" class="c-black col-form-label">
                                                                    {{ date('d F Y',strtotime($get_order->get_order_sb->get_project->start_date_periode)).' s.d '.date('d F Y',strtotime($get_order->get_order_sb->get_project->end_date_periode)) }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nilai Kontrak</label>
                                                            <div class="col-sm-8">
                                                                <label for="inputPassword" class="c-black col-form-label">
                                                                    {{ number_format($get_order->get_order_sb->get_project->contract_value)}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nilai Jaminan</label>
                                                            <div class="col-sm-8">
                                                                <label for="inputPassword" class="c-black col-form-label">
                                                                    {{ number_format($get_order->get_order_sb->get_project->collateral_value)}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                    <i class="fa fa-drivers-license">
                                                        Detail Pembayaran
                                                    </i>

                                                  <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">

                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Tipe Pembayaran</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ ($get_order->type_payment == 1)?'All In':'Per Periode'}}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Jumlah yang Harus Dibayar</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ number_format($get_order->total_payment)}}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Jumlah Terbayar</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ number_format($get_order->already_paid)}}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Sisa Yang belum terbayar</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ number_format($get_order->remaining_payment)}}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="pull-right">
                                            <button class="btn btn-sm btn-danger btn-show-verifikasi"> Verifikasi Pembayaran </button>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        Modal title
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-save-verifikasi">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection()

@section('script')
    <script>
        $('.btn-show-verifikasi').on('click',function(){
            buildModalBodyVerifikasi()
        })
        function buildModalBodyVerifikasi(){
            var div_html = '';

                div_html   +=           '<div class="col-sm-12">';
                div_html   +=               '<div class="row">'
                div_html   +=                   '<div class="col-12 col-sm-12">'
                div_html   +=                       '<div class="form-group row">'
                div_html   +=                           '<label for="inputPassword" class="col-sm-2 col-form-label">Jumlah Bayar</label>'
                div_html   +=                           '<div class="col-sm-8">'
                div_html   +=                               '<input class="form-control moneyFormat" type="text" id="jumlah_yg_dibayarkan" name="jumlah_yg_dibayarkan" onkeypress="return hanyaAngka(event)">'
                div_html   +=                           '</div>'
                div_html   +=                       '</div>'
                div_html   +=                   '</div>'
                div_html   +=                   '<div class="col-12 col-sm-12">'
                div_html   +=                       '<div class="form-group row">'
                div_html   +=                           '<label for="inputPassword" class="col-sm-2 col-form-label">Note</label>'
                div_html   +=                           '<div class="col-sm-8">'
                div_html   +=                               '<textarea class="form-control" id="note" name="note"></textarea>'
                div_html   +=                           '</div>'
                div_html   +=                       '</div>'
                div_html   +=                   '</div>'
                div_html   +=               '</div>'
                div_html   +=           '</div>'

            $('.modal-title').text('Verifikasi Pembayaran')
            $('#exampleModalCenter').modal('show')
            $('.modal-body').html(div_html);
            $('.btn-save-verifikasi').addClass('submit-payment')
        }

        $(document).on('click','.submit-payment',function(){
            var payment_code = $(this).attr('payment-code');
            swal.fire({
                icon: 'warning',
                title: 'Apakah anda yakin?',
                text: 'Pastikan data yang anda input adalah benar, data anda akan dikirimkan ke pihak asuransi untuk mendapatkan persetujuan',
                cancelButtonColor: '#d33',
                cancelButtonText: 'batal',
                confirmButtonText: 'simpan',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    submitPayment();
                }
            });
        })

        function submitPayment(){
            let jumlah_yg_dibayarkan    = $('#jumlah_yg_dibayarkan').maskMoney('unmasked')[0];
            let note                    = $('#note').val();


            if(jumlah_yg_dibayarkan == '')
            {
                alert('Mohon input jumlah yang dibayarkan');
                return false;
            }
            var formData = new FormData();

            formData.append('jumlah_yg_dibayarkan',$('#jumlah_yg_dibayarkan').maskMoney('unmasked')[0]);
            formData.append('note', note);
            formData.append('code', "{{ $get_order->code }}");

            $.ajax({
                type: 'POST',
                url: "{{ url('order/sb/submit-verifikasi-pembayaran') }}",
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                success: function (msg) {

                    if(msg.status == 'success'){
                        swalSuccess('Success','Success To Verification');

                        window.location.href="{{ url('order/sb/new') }}"
                    }
                    // console.log(msg)
                },
                error: function () {
                    var obj = JSON.parse(xhr.responseText)
                    console.log(obj)
                }
            });
        }
    </script>
@endsection()
