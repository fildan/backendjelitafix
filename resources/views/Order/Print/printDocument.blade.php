
<!DOCTYPE html>
<html>
<head>
	<title>Menampilkan File PDF di HTML - www.malasngoding.com</title>
</head>
<body>
@php
    $get_data_doc = $get_data->get_document;
    $path = 'storage/';
@endphp
@foreach($get_data_doc as $row)
    @php
        $path .= 'order/' ;
    @endphp

    @if($get_data->m_product_code == 'SB')
        @php
            $path .= 'sb/';
        @endphp
    @endif
    @php
        $path .= 'order-code-'.$get_data->code.'/';
    @endphp
    @if($row->document_type == 1)
        @php
            // $src = storage_path('app/public/'.$request->folder.'/'.$request->file);
            $path .= 'company/';
        @endphp
    @else
        @php
            // $src = storage_path('app/public/'.$request->folder.'/'.$request->file);
            $path .= 'project/';
        @endphp
    @endif
    <iframe
        src="{{ asset($path.$row->document_name); }}"
        width="600"
        height="400"
    ></iframe>
    @php

        $path = 'storage/';

    @endphp
@endforeach
</body>
</html>
