@extends('layout.app')
@section('title', 'New Order SB List')
@section('head')
    <style>
        .fz12{
            font-size: 12px !important;
        }
    </style>
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                New Order SB
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <table id="order-sb-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>

                                                <th class="fz12" width="10px"></th>
                                                <th class="fz12" width="10px">No</th>
                                                <th class="fz12">Tertanggung</th>
                                                <th class="fz12">Obligee</th>
                                                <th class="fz12">Tanggal Project</th>
                                                <th class="fz12">Periode Polis</th>
                                                <th class="fz12">Nilai Kontrak</th>
                                                <th class="fz12">Nilai Jaminan</th>
                                                <th class="fz12">Status</th>
                                                <th class="fz12" align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@endsection()
@section('script')
    <script>
        var table = $('#order-sb-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('order.sb.get-list') }}",
            'columns': [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    searchable: false,
                },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables fz12", searchable: false},
                { data: 'tertanggung' ,name:'tertanggung', "className": "v-align-tables fz12"},
                { data: 'obligee' ,name:'obligee', "className": "v-align-tables fz12"},
                { data: 'tgl_project' ,name:'tgl_project', "className": "v-align-tables fz12"},
                { data: 'periode_polis' ,name:'periode_polis', "className": "v-align-tables fz12"},
                { data: 'nilai_kontrak' ,name:'nilai_kontrak', "className": "v-align-tables fz12"},
                { data: 'nilai_jaminan' ,name:'nilai_jaminan', "className": "v-align-tables fz12"},
                {
                    data: 'status' ,
                    name:'status',
                    "className": "v-align-tables fz12",
                    render: function(data, type) {

                        // return data;
                        let status = data;
                        if(status == 0)
                        {
                            var status_html = "<span class='badge badge-warning'>Belum Di submit oleh tertanggung</span>";
                        }
                        else if(status == 1)
                        {
                            var status_html = "<span class='badge badge-warning'>Menunggu Untuk Di Proses</span>";
                        }
                        else if(status == 2)
                        {
                            var status_html = "<span class='badge badge-info'>Data dikirim ke penanggung dan menunggu jawaban</span>";
                        }
                        else if(status == 3)
                        {
                            var status_html = "<span class='badge badge-info'>Mendapatkan jawaban dari penanggung</span>";
                        }
                        else if(status == 4)
                        {
                            var status_html = "<span class='badge badge-danger'>Ditolak oleh penanggung</span>";
                        }
                        else if(status == 5)
                        {
                            var status_html = "<span class='badge badge-primary'>Disetujui oleh penanggung</span>";
                        }
                        else if(status == 6)
                        {
                            var status_html = "<span class='badge badge-info'>Proses Pembayaran</span>";
                        }
                        else if(status == 7)
                        {
                            var status_html = "<span class='badge badge-danger'>Pembayaran Gagal, menunggu pembayaran ulang</span>";
                        }
                        else if(status == 8)
                        {
                            var status_html = "<span class='badge badge-primary'>Pembayaran Berhasil</span>";
                        }
                        else if(status == 9)
                        {
                            var status_html = "<span class='badge badge-success'>Polis Sudah Terbit</span>";
                        }
                        else if(status == 10)
                        {
                            var status_html = "<span class='badge badge-info'>Mendapatkan jawaban dari tertanggung</span>";
                        }
                        else if(status == 11)
                        {
                            var status_html = "<span class='badge badge-warning'>Bukti Bayar Sudah di Upload, Verifikasi Segera</span>";
                        }
                        else if(status == 12)
                        {
                            var status_html = "<span class='badge badge-danger'>Pembayaran Kurang</span>";
                        }

                        return status_html;
                    }
                },
                { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
            ],
        });
    </script>
@endsection()
