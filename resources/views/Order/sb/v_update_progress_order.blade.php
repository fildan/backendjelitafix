@extends('layout.app')
@section('title', 'Detail Order SB')
@section('head')
    <style>
        
    </style>
@endsection()
@section('content')
    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order Detail<small> Surrety Bond</small></h2>
                        <div class="pull-right">
                            <!-- <button type="button" class="btn btn-primary" id="muncul_popup">Tambah Divisi</button> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container-fluid">
                                    <i class="fa fa-group">
                                      Status
                                    </i>
                                    <hr>
                                    <div class="row">


                                        <div class="col-sm-12">
                                            <form id="fm">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nama Jaminan : </label>
                                                            <div class="col-sm-8">
                                                                <input type="text" value="{{ $get_data->get_sub_product->name }}" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nama Project : </label>
                                                            <div class="col-sm-8">
                                                                <input type="text" value="{{ $get_data->get_order_sb->get_project->name }}" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nama Principle : </label>
                                                            <div class="col-sm-8">
                                                                <input type="text" value="{{ ucwords($get_data->get_order_company->name).', ('.$get_data->get_order_company->get_company_type->name.')' }}" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nama Obligee : </label>
                                                            <div class="col-sm-8">
                                                                <input type="text" value="{{ ucwords($get_data->get_order_sb->get_obligee->company_name).', ('.$get_data->get_order_sb->get_obligee->get_company_type->name.')' }}" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Status : </label>
                                                            <div class="col-sm-8">
                                                                <select name="status" id="status_progress" class="form-control select2" style="width: 100%">
                                                                    <option value=""> Pilih </option>
                                                                    <option value="3"> Minta Tertanggung Untuk melakukan penyesuaian data </option>
                                                                    <option value="4"> Di Tolak Oleh Penanggung </option>
                                                                    <option value="5"> Di Setujui Oleh Penanggung </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-2 col-form-label">Catatan : </label>
                                                            <div class="col-sm-10">
                                                                <textarea name="note" id="note_status" class="form-control" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row count-premi" style="display: none">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Perhitungan Rate / Premi : </label>
                                                            <div class="col-sm-8">
                                                                <select name="rate_premi_type" id="rate_premi_type" class="form-control select2" style="width: 100%">
                                                                    <option value=""> Pilih </option>
                                                                    <option value="1"> Per Terbit Polis </option>
                                                                    <option value="2"> Per Periode </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Lama Perlindungan : </label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" readonly name="panjang_periode" id="panjang_periode" value="{{ $get_data->get_order_sb->get_project->periode_value}}">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" value="Bulan" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row count-premi" style="display: none">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Nilai Jaminan : </label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="nilai_jaminan_show" id="nilai_jaminan_show" class="form-control" readonly value="{{ number_format($get_data->get_order_sb->get_project->collateral_value,2,",",".") }}">
                                                                <input type="hidden" name="nilai_jaminan" id="nilai_jaminan" class="form-control" readonly value="{{ $get_data->get_order_sb->get_project->collateral_value }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Periode : </label>
                                                            <div class="col-sm-4">
                                                                <input type="text" name="periode_start" id="periode_start" class="form-control" readonly value="{{ date('d F Y',strtotime($get_data->get_order_sb->get_project->start_date_periode)) }}">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" name="periode_end" id="periode_end" class="form-control" readonly value="{{ date('d F Y',strtotime($get_data->get_order_sb->get_project->end_date_periode)) }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row count-premi" style="display: none">
                                                    <div class="col-sm-6">
                                                        <div class="x_panel">
                                                            <div class="x_title">
                                                                    <i class="fa fa-drivers-license">
                                                                        Rate
                                                                    </i>

                                                                    <div class="clearfix"></div>
                                                            </div>
                                                            <div class="x_content">
                                                                <div class="row">
                                                                    <div class="col-12 col-sm-12">
                                                                        <div class="form-group row">
                                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Fix Rate</label>
                                                                            <div class="col-sm-4">
                                                                                <input type="text" name="fix_rate" id="fix_rate" class="form-control">
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <input type="text" name="fix_rate_percent" id="fix_rate_percent" value="%" readonly class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row rate-type-2">
                                                                    <div class="col-12 col-sm-12">
                                                                        <div class="form-group row">
                                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Rate Periode</label>
                                                                            <div class="col-sm-4">
                                                                                <input type="text" name="rate_periode_value" id="rate_periode_value" class="form-control" readonly value="3">
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <select name="rate_periode_type" id="rate_periode_type" class="form-control" style="width: 100%" readonly>
                                                                                    <option value=""> Pilih </option>
                                                                                    <option value="1"> Hari </option>
                                                                                    <option value="2"> Minggu </option>
                                                                                    <option value="3" selected> Bulan </option>
                                                                                    <option value="4"> Tahun </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="x_panel">
                                                            <div class="x_title">
                                                                    <i class="fa fa-drivers-license">
                                                                        Premi
                                                                    </i>

                                                                    <div class="clearfix"></div>
                                                            </div>
                                                            <div class="x_content">
                                                                <div class="row rate-type-2">
                                                                    <div class="col-12 col-sm-12">
                                                                        <div class="form-group row">
                                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Premi Per Periode</label>
                                                                            <div class="col-sm-4">
                                                                                <input type="text" name="fix_premi_per_periode_show" id="fix_premi_per_periode_show" class="form-control">
                                                                                <input type="hidden" name="fix_premi_per_periode" id="fix_premi_per_periode" class="form-control">
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <input type="text" name="fix_premi_per_periode_text" id="fix_premi_per_periode_text" class="form-control" value="/ 3 Bulan " readonly>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-12 col-sm-12">
                                                                        <div class="form-group row">
                                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Total Premi</label>
                                                                            <div class="col-sm-8">
                                                                                <input type="text" name="total_premi_show" id="total_premi_show" class="form-control" readonly>
                                                                                <input type="hidden" name="total_premi" id="total_premi" class="form-control" readonly>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row count-premi" style="display: none">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Brokerage (%) : </label>
                                                            <div class="col-sm-6">
                                                                <input type="text" name="brokerage" id="brokerage" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row count-premi" style="display: none">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Brokerage (IDR) : </label>

                                                            <div class="col-sm-8">
                                                                <input type="text" name="brokerage_idr_show" id="brokerage_idr_show" class="form-control" readonly>

                                                                <input type="hidden" name="brokerage_idr" id="brokerage_idr" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="col-sm-12">
                                                <div class="pull-right">
                                                    <button class="btn btn-primary" type="button" id="simpan-all">Simpan</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection()
@section('script')
    <script>
        $('#status_progress').on('change',function(){
            if($(this).val() == 5){
                $('.count-premi').show()
            }else{
                $('.count-premi').hide();
            }

            let get_rate_type = $('#rate_premi_type').val()
            if(get_rate_type == 2){
                $('.rate-type-2').show()
            }else{
                $('.rate-type-2').hide();
            }
            resetTotal()
        })

        $('#rate_premi_type').on('change',function(){
            let get_status = $('#status_progress').val()
            if(get_status == 5){
                $('.count-premi').show()
            }else{
                $('.count-premi').hide();
            }

            if($(this).val() == 2){
                $('.rate-type-2').show()
            }else{
                $('.rate-type-2').hide();
            }
            resetTotal()
        })
        $('#fix_rate').on('keyup',function(){
            let fix_rate = $(this).val();
            let nilai_jaminan = $('#nilai_jaminan').val();

            var rate_type = $('#rate_premi_type').val()


            let count = (fix_rate / 100) * nilai_jaminan;
            // alert(count)
            var total_premi = 0;
            if(rate_type == 1){
                total_premi = count;
            }else{
                $('#fix_premi_per_periode_show').val(formatRupiah(count))
                $('#fix_premi_per_periode').val(count)

                let periode_itung = $('#panjang_periode').val();
                let rate_periode_value = $('#rate_periode_value').val()
                let panjang_periode = Math.ceil(periode_itung / rate_periode_value);

                total_premi = count * panjang_periode;
            }
            // console.log(count)

            $('#total_premi_show').val(formatRupiah(total_premi))
            $('#total_premi').val(total_premi)


        })
        $('#brokerage').on('keyup',function(){
            let total_premi = $('#total_premi').val();
            console.log(total_premi);
            var brokerage = $(this).val();
            console.log(brokerage);
            let amount_brokerage = (brokerage / 100) * total_premi;
            $('#brokerage_idr_show').val(formatRupiah(amount_brokerage))
            $('#brokerage_idr').val(amount_brokerage)
        });
        $('#simpan-all').on('click',function(){
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Update ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        })

        $("#fm").submit(function(event){
            event.preventDefault();
            var form_data =  new FormData(this);
            form_data.append('code',"{{ $get_data->code }}");
            $.ajax({
                url : "{{ route('order.sb.update-progress') }}",
                type: 'POST',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    if(response.status == 'success'){
                        swalSuccess('Success','Success To Save');
                    }
                    // window.location.reload();
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        });
        function resetTotal(){
            $('#fix_premi_per_periode_show').val(0)
            $('#fix_premi_per_periode').val(0)
            // $('#panjang_periode')
            // $('#rate_periode_value')
            $('#total_premi_show').val(0)
            $('#total_premi').val(0)
        }
    </script>
@endsection()
