@extends('layout.app')
@section('title', 'Detail Order SB')
@section('head')
    <style>
        .fz12{
            font-size: 12px !important;
        }
        .c-black{
            color: black !important;
        }
    </style>
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order Detail<small> Surrety Bond</small></h2>
                        <div class="pull-right">
                            <!-- <button type="button" class="btn btn-primary" id="muncul_popup">Tambah Divisi</button> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="{{ url('order/sb/print-order/SPPA/'.$get_data->code) }}" class="btn btn-sm btn-info fz12">
                                        <i class="fa fa-print"></i>
                                        Cetak SPPA
                                    </a>

                                    <a href="{{ url('order/sb/print-order/Placing/'.$get_data->code) }}" class="btn btn-sm btn-success fz12">
                                        <i class="fa fa-print"></i>
                                        Cetak Placing
                                    </a>

                                    <a href="{{ url('order/sb/print-order/Document/'.$get_data->code) }}" class="btn btn-sm btn-info fz12">
                                        <i class="fa fa-print"></i>
                                        Cetak Dokumen
                                    </a>

                                    @if($get_data->status == 1)
                                        <a class="btn btn-sm btn-warning fz12 prosess-asuransi" style="color: #fff" row-code="{{ $get_data->code }}">
                                            <i class="fa fa-handshake-o" style="color: #fff"></i>
                                            Proses
                                        </a>
                                    @endif


                                </div>
                                <div class="container-fluid">
                                    <i class="fa fa-group">
                                      Data Tertanggung
                                    </i>
                                    <hr>
                                    <div class="row">


                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Nama : </label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                {{ $get_data->get_order_company->name.', ( '.$get_data->get_order_company->get_company_type->name.' )' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Jenis usaha : </label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                {{ ($get_data->get_order_company->jenis_usaha == 1) ? 'Non BUMN /  Swasta':'BUMN' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="row">
                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Bidang usaha</label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                {{ $get_data->get_order_company->bidang_usaha }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Nomor NPWP</label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                {{ ($get_data->get_order_company->npwp)?$get_data->get_order_company->npwp:'-' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Email</label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                {{ $get_data->get_order_company->email }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Nomor Telepon</label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                @php
                                                                    $no_telp = json_decode($get_data->get_order_company->no_telepon);
                                                                    foreach ($no_telp as $key => $value) {
                                                                        echo '('.$value->code.') '.$value->number;

                                                                        if($key > 0){
                                                                            echo ', ';
                                                                        }
                                                                    }
                                                                @endphp
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Komisaris</label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                @php
                                                                    $no = 0;
                                                                    $komisaris = collect($get_data->get_order_company->get_direksi_komisaris)->where('type',1)->all();
                                                                    foreach ($komisaris as $key => $value) {

                                                                        echo $value->name.' - '.$value->position;
                                                                        if($no > 0){
                                                                            echo '<br>';
                                                                        }
                                                                        $no++;
                                                                    }
                                                                @endphp
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Direksi</label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                @php
                                                                    $no = 0;
                                                                    $direksi = collect($get_data->get_order_company->get_direksi_komisaris)->where('type',2)->all();
                                                                    foreach ($direksi as $key => $value) {

                                                                        echo $value->name.' - '.$value->position;
                                                                        if($no > 0){
                                                                            echo '<br>';
                                                                        }
                                                                        $no++;
                                                                    }
                                                                @endphp
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-6 col-sm-6">
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-4 col-form-label">Alamat</label>
                                                        <div class="col-sm-8">
                                                            <label for="inputPassword" class="col-form-label c-black">
                                                                {{ $get_data->get_order_company->address.', '.$get_data->get_order_company->get_province->name.', '.$get_data->get_order_company->get_city->name.', '.$get_data->get_order_company->postcode }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                        <i class="fa fa-drivers-license">
                                                            Data Oblige
                                                        </i>

                                                      <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                    <div class="row">
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Nama</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ $get_data->get_order_sb->get_obligee->company_name.', ('.$get_data->get_order_sb->get_obligee->get_company_type->name.')' }}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Jenis Usaha</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ ($get_data->get_order_sb->get_obligee->jenis_usaha == 1)?'Non BUMN / Swasta':'BUMN'}}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Bidang Usaha</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ $get_data->get_order_sb->get_obligee->bidang_usaha}}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Website</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ $get_data->get_order_sb->get_obligee->website}}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Email</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ $get_data->get_order_sb->get_obligee->email}}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Alamat</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{
                                                                                $get_data->get_order_sb->get_obligee->address.', '.$get_data->get_order_sb->get_obligee->get_province->name.', '.
                                                                                $get_data->get_order_sb->get_obligee->get_city->name.', '.$get_data->get_order_sb->get_obligee->postcode
                                                                            }}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">No Telepon</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                        @php
                                                                            $no_telp = json_decode($get_data->get_order_sb->get_obligee->no_telepon);
                                                                            foreach ($no_telp as $key => $value) {
                                                                                echo '('.$value->code.') '.$value->number;

                                                                                if($key > 0){
                                                                                    echo ', ';
                                                                                }
                                                                            }
                                                                        @endphp
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6">

                                            <div class="x_panel">
                                                <div class="x_title">
                                                        <i class="fa fa-gavel">
                                                            Data Project
                                                        </i>

                                                      <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                    <div class="row">
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Nama Project</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ $get_data->get_order_sb->get_project->name }}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Periode Project</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ date('d F Y',strtotime($get_data->get_order_sb->get_project->start_date_periode)).' s.d '.date('d F Y',strtotime($get_data->get_order_sb->get_project->end_date_periode)) }}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Nilai Kontrak</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ number_format($get_data->get_order_sb->get_project->contract_value,2,",",".") }}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Nilai Jaminan</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            {{ number_format($get_data->get_order_sb->get_project->collateral_value,2,",",".") }}
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-12 col-sm-12">
                                                            <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Asuransi Yang Di Tunjuk</label>
                                                                    <div class="col-sm-8">
                                                                        <label for="inputPassword" class="c-black col-form-label">
                                                                            @if($get_data->type_penanggung == 1)
                                                                                {{ $get_data->get_penanggung->get_penanggung->name }}
                                                                            @else
                                                                                {{ $get_data->get_list_asuransi->name }}
                                                                            @endif
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                        <i class="fa fa-credit-card-alt">
                                                            Detail Premi
                                                        </i>

                                                      <div class="clearfix"></div>
                                                </div>

                                                <div class="x_content">
                                                    <div class="row">
                                                        <div class="col-6 col-sm-6">
                                                            <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Fix Rate</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ ($get_data->get_order_sb->fix_rate)?$get_data->get_order_sb->fix_rate.' %':'-' }}

                                                                        @if($get_data->get_order_sb->fix_rate_type != 0)
                                                                            /
                                                                            @if($get_data->get_order_sb->fix_rate_type == 1)
                                                                                Terbit Polis
                                                                            @else
                                                                                {{ $get_data->get_order_sb->fix_rate_periode_value }}
                                                                                @if($get_data->get_order_sb->fix_rate_periode_type == 1 )
                                                                                    Hari
                                                                                @elseif($get_data->get_order_sb->fix_rate_periode_type == 2 )
                                                                                    Minggu
                                                                                @elseif($get_data->get_order_sb->fix_rate_periode_type == 3 )
                                                                                    Bulan
                                                                                @elseif($get_data->get_order_sb->fix_rate_periode_type == 4 )
                                                                                    Tahun
                                                                                @endif
                                                                            @endif
                                                                        @endif
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Premi Per Periode</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ ($get_data->get_order_sb->fix_premi_per_periode) ? 'Rp. '.number_format($get_data->get_order_sb->fix_premi_per_periode):'-' }}
                                                                        @if($get_data->get_order_sb->fix_premi_per_periode)
                                                                            /
                                                                            @if($get_data->get_order_sb->fix_rate_type == 1)
                                                                                Terbit Polis
                                                                            @else
                                                                                {{ $get_data->get_order_sb->fix_rate_periode_value }}
                                                                                @if($get_data->get_order_sb->fix_rate_periode_type == 1 )
                                                                                    Hari
                                                                                @elseif($get_data->get_order_sb->fix_rate_periode_type == 2 )
                                                                                    Minggu
                                                                                @elseif($get_data->get_order_sb->fix_rate_periode_type == 3 )
                                                                                    Bulan
                                                                                @elseif($get_data->get_order_sb->fix_rate_periode_type == 4 )
                                                                                    Tahun
                                                                                @endif
                                                                            @endif
                                                                        @endif
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Lama Perlidungan</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ $get_data->get_order_sb->periode_value }}


                                                                        @if($get_data->get_order_sb->periode_type == 1 )
                                                                            Hari
                                                                        @elseif($get_data->get_order_sb->periode_type == 2 )
                                                                            Minggu
                                                                        @elseif($get_data->get_order_sb->periode_type == 3 )
                                                                            Bulan
                                                                        @elseif($get_data->get_order_sb->periode_type == 4 )
                                                                            Tahun
                                                                        @endif
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Total Premi</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ ($get_data->get_order_sb->total_fix_premi)?'Rp. '.number_format($get_data->get_order_sb->total_fix_premi):'-' }}



                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6 col-sm-6">
                                                            <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Total Pembayaran</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ ($get_data->get_order_sb->total_fix_premi)?'Rp. '.number_format($get_data->get_order_sb->total_fix_premi):'-' }}



                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Status Pembayaran</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">

                                                                        @if($get_data->status_payment == 0 || $get_data->status_payment == 1)
                                                                            <label class="badge badge-warning"> Belum Melakukan Pembayaran </label>
                                                                        @elseif($get_data->status_payment == 2)
                                                                                <label class="badge badge-info"> Menunggu Pembayaran </label>

                                                                        @elseif($get_data->status_payment == 3)
                                                                            <label class="badge badge-success"> Pembayaran Terverifikasi </label>
                                                                        @elseif($get_data->status_payment == 4)
                                                                            <label class="badge badge-danger"> Pembayaran Gagal </label>

                                                                        @elseif($get_data->status_payment == 5)
                                                                            <label class="badge badge-warning">Pembayaran Kurang </label>
                                                                        @elseif($get_data->status_payment == 6)
                                                                            <label class="badge badge-warning">Bukti Bayar Terupload, Menunggu Verifikasi </label>
                                                                        @endif



                                                                    </label>
                                                                </div>
                                                            </div>

                                                                <div class="form-group row">
                                                                    <label for="inputPassword" class="col-sm-4 col-form-label">Status Polis</label>
                                                                    <div class="col-sm-8">

                                                                        @if($get_data->status == 6)
                                                                            <label for="inputPassword" class="c-black col-form-label">
                                                                                <label class="badge badge-success"> Polis Sudah Diberikan </label>
                                                                            </label>
                                                                        @else
                                                                            @php $disable = '';@endphp
                                                                            @if($get_data->status == 5)

                                                                                @php $disable = 'disabled';@endphp
                                                                            @endif
                                                                            <select class="form-control select2" id="select-polis" name="select_polis" style="" {{ $disable }}>
                                                                                <option value="">  Polis Belum Dikirim </option>
                                                                                <option value="6"> Polis Sudah Dikirim </option>
                                                                            </select>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                            @if($get_data->status == 5)
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="pull-right">
                                                                            <button class="btn btn-sm btn-danger" id="update-polis">
                                                                                Update
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                        <i class="fa fa-credit-card-alt">
                                                            Dokumen Persyaratan
                                                        </i>

                                                      <div class="clearfix"></div>
                                                </div>

                                                <div class="x_content">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="card-box table-responsive">

                                                                <table id="" class="table table-striped table-bordered datatable" style="width:100%">
                                                                    <thead>
                                                                        <tr>
                                                                          <th width="10px">No</th>
                                                                          <th>Nama Dokumen</th>
                                                                          <th>Nomor</th>
                                                                          <th>Tanggal</th>
                                                                          <th>Keterangan </th>
                                                                          <th>Status </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="document-table">
                                                                        @php
                                                                            $no = 0;
                                                                        @endphp
                                                                        @foreach($get_data->get_document as $row)
                                                                            @php
                                                                                $no ++;
                                                                            @endphp

                                                                            <tr>
                                                                                <td> {{ $no }}</td>
                                                                                <td> {{ $row->name }} </td>
                                                                                <td> {{ $row->number_document }} </td>
                                                                                <td> {{ $row->date_document }} </td>
                                                                                <td> {{ $row->note_document }} </td>

                                                                                <td style="text-align: center">
                                                                                    <i class="fa fa-check-square-o"></i>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>


                                                                </table>
                                                              </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <button class="btn btn-primary" type="button">Kembali</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
              </div>
        </div>
    </div>

    <div class="modal fade" id="modalUpdateProgress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Update Progress</h5>
            </div>
            <div class="modal-body">
                <form id="UpdateProgressfm">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label"> Status</label>
                                    <div class="col-sm-10">
                                        <input type="text" readonly class="form-control" name="proses_progress" id="proses_progress" placeholder="Proses">
                                        <input type="hidden" name="code" id="code" value="{{ $get_data->code }}">
                                        <input type="hidden" name="status" id="status" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Note</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="note" name="note"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary closeReference">Close</button>
              <button type="button" class="btn btn-primary" id="saveProgress">Simpan</button>
            </div>
          </div>
        </div>
    </div>
@endsection()
@section('script')
    <script>
        $(document).ready( function () {
            $('.datatable').DataTable();
        } );

        $(document).on('click','.prosess-asuransi',function(){
            $('#modalUpdateProgress').modal('show')
            $('#status').val(2)

        })

        $('#update-polis').on('click',function(){
            let status = $('#select-polis').val();

            if(status != '')
            {
                $.ajax({
                    url :"{{ route('order.sb.update-progress') }}",
                    type: 'POST',
                    data:{
                        status:status,
                        code:"{{ $get_data->code }}"
                    },
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    success: (response) => {

                        if(response.status == 'success'){
                            swalSuccess('Success','Success To Delete');
                        }
                        // window.location.reload()
                    },
                    error: function(xhr, status, error){
                        var obj = JSON.parse(xhr.responseText)
                        swalError('Failed', obj.message)
                        window.location.reload()
                    }
                });
            }
        })

        $('#saveProgress').on('click',function(){
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Update ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#UpdateProgressfm').submit()
                }
            });
        })

        $("#UpdateProgressfm").submit(function(event){
            event.preventDefault();
            var form_data =  new FormData(this);
            $.ajax({
                url : "{{ route('order.sb.update-progress') }}",
                type: 'POST',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    swalSuccess('Success','Success To Save');
                    window.location.reload();
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        });
    </script>
@endsection()
