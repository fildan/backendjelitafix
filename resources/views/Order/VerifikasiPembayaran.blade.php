@extends('layout.app')
@section('title', 'Detail Order SB')
@section('head')
    <style>
        .fz12{
            font-size: 12px !important;
        }
        .c-black{
            color: black !important;
        }
    </style>
@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Verifikasi Pembayaran</h2>
                        <div class="pull-right">
                            <!-- <button type="button" class="btn btn-primary" id="muncul_popup">Tambah Divisi</button> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                    <i class="fa fa-drivers-license">
                                                        Detail pemabayran
                                                    </i>

                                                  <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <div class="row">
                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-2 col-form-label">Nomor Order</label>
                                                                <div class="col-sm-10">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ $get_order->code }}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Tipe Pembayaran</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ ($get_order->type_payment == 1)?'All In':'Per Periode'}}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Jumlah yang Harus Dibayar</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ number_format($get_order->total_payment)}}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Jumlah Terbayar</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ number_format($get_order->already_paid)}}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                                <label for="inputPassword" class="col-sm-4 col-form-label">Sisa Yang belum terbayar</label>
                                                                <div class="col-sm-8">
                                                                    <label for="inputPassword" class="c-black col-form-label">
                                                                        {{ number_format($get_order->remaining_payment)}}
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                    <i class="fa fa-credit-card-alt">
                                                        List Invoice
                                                    </i>

                                                  <div class="clearfix"></div>
                                            </div>

                                            <div class="x_content">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="card-box table-responsive">

                                                            <table id="list-invoice-table" class="table table-striped table-bordered " style="width:100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th width="10px">No</th>
                                                                        <th>Inovice Number</th>
                                                                        <th>Total Amount</th>
                                                                        <th>Date </th>
                                                                        <th>Status </th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        Modal title
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-save-verifikasi">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection()

@section('script')
    <script>
         var table = $('#list-invoice-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                'url'   :"{{ route('order.get-list-invoice') }}",
                "data"  : {
                    'order_code':"{{ $get_order->code }}"
                }
            },

            'columns': [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    searchable: false,
                },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables fz12", searchable: false},
                { data: 'code' ,name:'code', "className": "v-align-tables fz12"},
                { data: 'total_amount' ,name:'total_amount', "className": "v-align-tables fz12"},
                { data: 'date' ,name:'date', "className": "v-align-tables fz12"},
                {
                    data: 'status' ,
                    name:'status',
                    "className": "v-align-tables fz12",
                    render: function(data, type) {

                        // return data;
                        let status = data;
                        if(status == 0)
                        {
                            var status_html = "<span class='badge badge-warning'>Invoice Belum Active</span>";
                        }
                        else if(status == 1)
                        {
                            var status_html = "<span class='badge badge-warning'>Invoice Active</span>";
                        }
                        else if(status == 2)
                        {
                            var status_html = "<span class='badge badge-info'>Menunggu Pembayaran</span>";
                        }
                        else if(status == 3)
                        {
                            var status_html = "<span class='badge badge-success'>Pembayaran Sukses</span>";
                        }
                        else if(status == 4)
                        {
                            var status_html = "<span class='badge badge-danger'>Pembayaran Gagal</span>";
                        }
                        else if(status == 5)
                        {
                            var status_html = "<span class='badge badge-warning'>Menunggu Verifikasi Pembayaran</span>";
                        }
                        else if(status == 6)
                        {
                            var status_html = "<span class='badge badge-warning'>Pembayaran Kurang</span>";
                        }


                        return status_html;
                    }
                },
            ],
        });

        $('#list-invoice-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
            // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        });
        /* Formatting function for row details - modify as you need */

        function format ( rowData ) {
            console.log(rowData);
            var div = $('<div class="row" />')
                .addClass( 'loading' )
                .text( 'Loading...' );

            var list_payment = rowData.get_payment;
            var data_html  =    '';
                data_html +=        '<table class="table table-striped table-bordered ">';
                data_html +=            '<tr>'

                data_html +=                '<td width="10px"> No </td>';
                data_html +=                '<td> Metode Pembayaran </td>';
                data_html +=                '<td> Bank </td>';
                data_html +=                '<td> Rekening SJU</td>';
                data_html +=                '<td> Kode Pembayaran </td>';
                data_html +=                '<td> Total Pembayaran </td>';
                data_html +=                '<td> Status </td>';
                data_html +=                '<td> Action </td>';
                data_html +=            '</tr>';

            var no = 0;
            $.each( list_payment, function( key, value ) {
                no++;
                data_html +=            '<tr>'
                data_html +=                '<td>'+no+'</td>';
                data_html +=                '<td>'+value.get_methode_payment.description+'</td>';
                data_html +=                '<td>'+value.get_bank.name+'</td>';
                data_html +=                '<td>'+value.no_rekening+'</td>';
                data_html +=                '<td>'+value.payment_code+'</td>';
                data_html +=                '<td>'+formatRupiah(parseInt(value.total_amount))+'</td>';
                data_html +=                '<td>'+buildStatusPaymnet(value.status)+'</td>';
                data_html +=                '<td>';
                data_html +=                    '<div class="btn-group fz12">';
                data_html +=                        '<button type="button" class="btn btn-sm btn-info fz12">Action</button>';
                data_html +=                        '<button type="button" class="dropdown-toggle dropdown-icon btn btn-sm btn-info fz12" data-toggle="dropdown">';
                data_html +=                            '<span class="sr-only">Toggle Dropdown</span>';
                data_html +=                        '</button>';
                data_html +=                        '<div class="dropdown-menu" role="menu">';

                var btn_text = "";
                if(value.status == 2 || value.status == 3){
                    btn_text = 'Lihat history pembayaran';
                }else if(value.status == 4 || value.status == 5){
                    btn_text = 'Verifikasi'
                }else{
                    btn_text = '';
                }
                if(value.status != 1){
                    data_html +=                            '<a class="dropdown-item btn btn-sm btn-info fz12 show-verifikasi" href="#" payment-code="'+value.code+'"> '+btn_text+' </a>';
                }
                // }
                // if(value.get_methode_payment.code == 'TFM'){
                    // data_html +=                            '<a class="dropdown-item btn btn-sm btn-info fz12" href=""> View Bukti Bayar </a>';
                // }
                data_html +=                        '</div>';
                data_html +=                    '</div>';
                data_html +=                '</td>';
                data_html +=            '</tr>';
            });
            data_html     +=        '</table>';
            div
                        .html( data_html )
                        .removeClass( 'loading' );

            return div;
        }

        $(document).on('click','.show-verifikasi',function(){
            // alert($(this).attr('payment-code'));
            getHistoryPayment($(this).attr('payment-code'));
        })


        function getHistoryPayment(payment_code)
        {
            $.ajax({
                url : '{{url("order/get-history-payment")}}',
                type: 'GET',
                data : {
                    payment_code: payment_code
                },
                success: (response) => {
                    // console.log(response);
                    buildModalBodyVerifikasi(response.data)
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            })
        }
        function buildModalBodyVerifikasi(data){
            var payment_history = data.get_payment_history;
            var div_html    = '';
                div_html   +=   '<div class="col-sm-12">';
                div_html   +=       '<div class="x_panel">'
                div_html   +=           '<div class="x_title">'
                div_html   +=               '<i class="fa fa-drivers-license">'
                div_html   +=                   'Bukti Pembayaran'
                div_html   +=               '</i>'
                div_html   +=               '<div class="clearfix"></div>'
                div_html   +=            '</div>'
                div_html   +=           '<div class="x_content">'
                div_html   +=               '<div class="row">'
                div_html   +=                   '<div class="col-12 col-sm-12">'
                div_html   +=                       '<table id="order-sb-list" class="table table-striped table-bordered" style="width:100%">'
                div_html   +=                           '<thead>'
                div_html   +=                               '<tr>'
                div_html   +=                                   '<th class="fz12" width="10px">No</th>'
                div_html   +=                                   '<th class="fz12">Tanggal Pembayaran</th>'
                if(data.m_methode_payment_code == 'TFM'){
                    div_html   +=                                   '<th class="fz12">Bukti Bayar</th>'
                }else{
                    div_html   +=                                   '<th class="fz12">Kode Pembayaran</th>'
                }
                div_html   +=                                   '<th class="fz12">Jumlah yang Di bayar</th>'
                div_html   +=                                   '<th class="fz12">Tanggal Submit</th>'
                div_html   +=                               '</tr>'
                div_html   +=                           '</thead>'
                div_html   +=                           '<tbody>'

                let no = 0;
                $.each( payment_history, function( key, value ) {
                    no++;
                    div_html +=                            '<tr>';
                    div_html +=                                '<td class="fz12">'+no+'</td>';
                    div_html +=                                '<td class="fz12">'+value.payment_date+'</td>';
                    div_html +=                                '<td class="fz12">';
                    if(data.m_methode_payment_code == 'TFM'){
                        div_html +=                                     '<a href="{{ asset("storage/proof-of-payment/") }}'+"/"+value.payment_code+"/"+value.proof_of_payment+'" target="_blank">'
                        div_html +=                                         'Lihat Bukti Bayar'
                        div_html +=                                     '</a>'
                    }else{
                        div_html +=                                     data.payment_code;
                    }
                    div_html +=                                 '</td>';
                    div_html +=                                '<td class="fz12">'+formatRupiah(parseInt(value.amount))+'</td>';
                    div_html +=                                '<td class="fz12">'+value.created_at+'</td>';
                    div_html +=                            '</tr>';
                });
                div_html   +=                           '</tbody>'
                div_html   +=                       '</table>'
                div_html   +=                   '</div>'
                div_html   +=               '</div>'

                if((data.status != 2) && (data.status != 3)){
                    div_html   +=               '<div class="row">'
                    div_html   +=                   '<div class="col-12 col-sm-12">'
                    div_html   +=                       '<div class="form-group row">'
                    div_html   +=                           '<label for="inputPassword" class="col-sm-2 col-form-label">Status</label>'
                    div_html   +=                           '<div class="col-sm-8">'
                    div_html   +=                               '<select class="form-control select2" id="status_'+data.code+'" name="status" style="width:100%">'
                    div_html   +=                                       '<option value="">Pilih </option>'
                    div_html   +=                                       '<option value="2"> Sukses </option>'
                    div_html   +=                                       '<option value="3"> Gagal </option>'
                    div_html   +=                                       '<option value="4"> Kurang Bayar </option>'
                    div_html   +=                               '</select>'
                    div_html   +=                           '</div>'
                    div_html   +=                       '</div>'
                    div_html   +=                   '</div>'
                    div_html   +=                   '<div class="col-12 col-sm-12">'
                    div_html   +=                       '<div class="form-group row">'
                    div_html   +=                           '<label for="inputPassword" class="col-sm-2 col-form-label">Jumlah Bayar</label>'
                    div_html   +=                           '<div class="col-sm-8">'
                    div_html   +=                               '<input class="form-control" type="text" id="jumlah_yg_dibayarkan_'+data.code+'" name="jumlah_yg_dibayarkan">'
                    div_html   +=                           '</div>'
                    div_html   +=                       '</div>'
                    div_html   +=                   '</div>'
                    div_html   +=                   '<div class="col-12 col-sm-12">'
                    div_html   +=                       '<div class="form-group row">'
                    div_html   +=                           '<label for="inputPassword" class="col-sm-2 col-form-label">Note</label>'
                    div_html   +=                           '<div class="col-sm-8">'
                    div_html   +=                               '<textarea class="form-control" id="note_'+data.code+'" name="note"></textarea>'
                    div_html   +=                           '</div>'
                    div_html   +=                       '</div>'
                    div_html   +=                   '</div>'
                    div_html   +=               '</div>'


                }

                div_html   +=           '</div>'
                div_html   +=       '</div>'
                div_html   +=   '</div>'

                if((data.status != 2) && (data.status != 3)){
                    $('.btn-save-verifikasi').attr('payment-code',data.code)
                    $('.btn-save-verifikasi').addClass('submit-payment')
                    $('.btn-save-verifikasi').show()
                }else{

                    $('.btn-save-verifikasi').hide()
                }

                $('.select2').select2()
                $('#exampleModalCenter').modal('show')
                $('.modal-body').html(div_html);
        }

        $(document).on('click','.submit-payment',function(){
            var payment_code = $(this).attr('payment-code');
            swal.fire({
                icon: 'warning',
                title: 'Apakah anda yakin?',
                text: 'Pastikan data yang anda input adalah benar, data anda akan dikirimkan ke pihak asuransi untuk mendapatkan persetujuan',
                cancelButtonColor: '#d33',
                cancelButtonText: 'batal',
                confirmButtonText: 'simpan',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){

                    submitPayment(payment_code);
                }
            });
        })

        function submitPayment(payment_code){
            let status                  = $('#status_'+payment_code).val()
            let jumlah_yg_dibayarkan    = $('#jumlah_yg_dibayarkan_'+payment_code).val();
            let note                    = $('#note_'+payment_code).val();

            let formData = new FormData();
            formData.append('status', status);
            formData.append('jumlah_yg_dibayarkan', jumlah_yg_dibayarkan);
            formData.append('note', note);
            formData.append('payment_code', payment_code);

            $.ajax({
                type: 'POST',
                url: "{{ url('order/submit-verifikasi-pembayaran') }}",
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                success: function (msg) {
                    swalSuccess('Success','Success To Verification');
                    // console.log(msg)
                },
                error: function () {
                    var obj = JSON.parse(xhr.responseText)
                    console.log(obj)
                }
            });
        }
        function buildStatusPaymnet(status){
            var status_html = "";
            if(status == 1){
                status_html = "<span class='badge badge-warning'>Proses Pembayaran</span>"
            }
            else if(status == 2)
            {
                status_html = "<span class='badge badge-success'>Pembayaran Sukses</span>"
            }
            else if(status == 3)
            {
                status_html = "<span class='badge badge-danger'>Pembayaran Gagal</span>"
            }
            else if(status == 4)
            {
                status_html = "<span class='badge badge-warning'>Pembayaran Kurang</span>"
            }
            else if(status == 5)
            {
                status_html = "<span class='badge badge-warning'>Menunggu Verifikasi</span>"
            }
            else if(status == 6)
            {
                status_html = "<span class='badge badge-warning'>Expired</span>"
            }
            return status_html;

        }
    </script>
@endsection()
