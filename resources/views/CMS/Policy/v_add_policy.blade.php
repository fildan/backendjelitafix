@extends('layout.app')
@section('title', 'Add Policy')
@section('head')
    <style>

    </style>

	<link type="text/css" href="{{ asset('summernote/summernote.min.css')}}" rel="stylesheet" media="screen" />
@endsection()
@section('content')
    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Policy<small> JELAS</small></h2>
                        <div class="pull-right">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                {{-- <div class="container-fluid"> --}}
                                    {{-- <div class="row"> --}}
                                        {{-- <div class="col-sm-12"> --}}
                                            <form id="fm">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Policy For : </label>
                                                            <div class="col-sm-8">
                                                                <select name="product" class="form-control select2" name="product">
                                                                    <option value=""> Pilih </option>
                                                                    <option value="0" selected> Umum </option>
                                                                    @foreach ($product as $row)
                                                                        <option value="{{ $row->id }}"> {{ $row->name }} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6 col-sm-6">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-4 col-form-label">Policy Name : </label>
                                                            <div class="col-sm-8">
                                                                <input type="text" value="" class="form-control" name="policy_name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-2 col-form-label">Detail : </label>
                                                            <div class="col-sm-10">
                                                                <textarea name="detail" id="detail" class="form-control summernote" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="pull-right">
                                                        <a href="{{ route('faq') }}">
                                                            <button class="btn btn-dark" type="button">Back</button>
                                                        </a>
                                                        <button class="btn btn-primary" type="button" id="save">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        {{-- </div> --}}
                                    {{-- </div> --}}
                                {{-- </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()

@section('script')

    <script src="{{ asset('summernote/summernote.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            // $('.summernote').summernote();
            $('#detail').summernote({
                placeholder: 'Hello Bootstrap 4',
                tabsize: 2,
                height: 200
            });
        });

        $('#save').on('click',function(){
            Swal.fire({
                icon: 'warning',
                title: 'Save Data',
                text: 'Are You Sure Want To Save ?',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No, Dont Save it!',
                confirmButtonText: 'Yes, Save it!',
                showCancelButton: true,
                showConfirmButton: true,
                reverseButtons: true,
            }).then((result) => {
                if(result.value){
                    $('#fm').submit()
                }
            });
        })

        $("#fm").submit(function(event){
            event.preventDefault();
            var form_data =  new FormData(this);
            let url_save    ="{{ route('policy.save') }}"
            $.ajax({
                url : url_save,
                type: 'POST',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    swalSuccess('Success','Success To Save');
                    window.location.href = "{{ route('policy') }}"
                    // reloadPage()
                },
                error: function(xhr, status, error){
                    var obj = JSON.parse(xhr.responseText)
                    swalError('Failed', obj.message)
                }
            });
        });
    </script>
@endsection()
