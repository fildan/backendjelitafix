@extends('layout.app')
@section('title', 'Policy JELAS')
@section('head')

@endsection()
@section('content')

    <div class="right_col" role="main">
        @include('layout.Breadcumb')
        <div class="row">


            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h6>
                                Policy List
                                <small></small>
                            </h6>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="{{ route('policy.add') }}">
                                        <button type="button" class="btn btn-primary btn-sm" id="addData">Add Policy</button>
                                    </a>
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="faq-list" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10px"></th>
                                                <th width="10px">No</th>
                                                <th>Policy Name</th>
                                                <th align="center" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


@endsection()
@section('script')
    <script>
        var table = $('#faq-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('policy.get-list') }}",
            'columns': [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    searchable: false,
                },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', "className": "v-align-tables", searchable: false},
                { data: 'name' ,name:'name', "className": "v-align-tables"},
                { data: 'action' ,"className": "v-align-tables text-center" ,orderable: false, searchable: false},
            ],
        });

        $('#faq-list tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
            // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        });
        /* Formatting function for row details - modify as you need */

        function format ( rowData ) {
            var div = $('<div class="row" />')
                .addClass( 'loading' )
                .text( 'Loading...' );

            div.html(rowData.description)

            return div;
        }

        function editClick(id=null){
            window.location.href = "{{ url('policy/edit/') }}"+'/'+id;
        }
    </script>
@endsection()
