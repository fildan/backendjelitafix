<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/ico" />

        <title>JELAS Management - @yield('title')</title>

        <!-- Bootstrap -->
        <link href="cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
        <!-- iCheck -->
        <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="{{asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
        <!-- JQVMap -->
        <!-- <link href="{'vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/> -->

        <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">

        <link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">

        <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
        <!-- bootstrap-daterangepicker -->
        <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
        {{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
        <link rel="stylesheet" href="{{asset('dist/themes/default/style.min.css')}}" />
        <link rel="stylesheet" href="{{asset('swal/dist/sweetalert2.min.css')}}">
        <link href="{{ asset('vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
        <link href="{{ asset('vendors/starrr/dist/starrr.css')}}" rel="stylesheet">
        <style type="text/css">
            ul.breadcrumb {
                padding: 10px 16px;
                list-style: none;
                background-color: #eee;
            }
            ul.breadcrumb li {
                display: inline;
                font-size: 12px;
            }
            ul.breadcrumb li+li:before {
                padding: 8px;
                color: black;
                content: "/\00a0";
            }
            ul.breadcrumb li a {
                color: #0275d8;
                text-decoration: none;
            }
            ul.breadcrumb li a:hover {
                color: #01447e;
                text-decoration: underline;
            }


            td.details-control {
                background: url('{{asset("img_table/details_open.png")}}') no-repeat center center;
                cursor: pointer;
            }
            td.v-align-tables {
                vertical-align: middle;
            }
            tr.shown td.details-control {
                background: url('{{asset("img_table/details_close.png")}}') no-repeat center center;
            }
            a.paginate_button.current {
                background-color: black !important;
            }
            .dataTables_paginate a{
                background-color: #7dbbd3 !important;
                color: #ffff !important;
            }
            li.current-page{
                background-color: #5757c7 !important;
            }
            .paginate_button {
                cursor: pointer;
            }
            .text-center{
                text-align: center !important;
            }
            .img-preview {
                width: 150px !important;
                height: 150px !important;
            }
            .form-control{
                font-size: 12px !important;
            }
            .fz12{
                font-size: 12px !important;
            }
            .c-black{
                color: black !important;
            }
        </style>
        @yield('head')

        <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('jquery-number-master/jquery.number.min.js')}}"></script>
        <script src="{{asset('js/select2.min.js')}}"></script>
        {{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
        <script src="{{asset('dist/jstree.min.js')}}"></script>
        <script src="{{asset('swal/dist/sweetalert2.min.js')}}"></script>
        <script src="{{asset('vendors/starrr/dist/starrr.js')}}"></script>
    </head>
    <body class="nav-md">
        <div class="container body">
          <div class="main_container">
            <div class="col-md-3 left_col">
              <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                  <a href="index.html" class="site_title"><span>JELAS MANAGEMENT</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                  <div class="profile_pic">
                    <img src="{{asset('images/male_user.jpg')}}" alt="..." class="img-circle profile_img">
                  </div>
                  <div class="profile_info">
                    <span>Selamat Datang,</span>
                    <h2>
                      Admin

                    </h2>
                  </div>
                </div>
                <br />
