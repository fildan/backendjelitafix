<footer>
    <div class="pull-right">
      JELAS - SARANA JANESIA UTAMA {{date('Y')}}
    </div>
    <div class="clearfix"></div>
  </footer>
  <!-- /footer content -->
</div>
</div>

<!-- jQuery -->

<!-- Bootstrap -->
<script src="{{asset('vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<!-- bootstrap-progressbar -->
<script src="{{asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>

<script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
<script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>

<!-- Custom Theme Scripts -->

<!-- DateJS -->
<script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<script src="{{ asset('vendors/switchery/dist/switchery.min.js')}}"></script>
<!-- Custom Theme Scripts -->
<script src="{{asset('build/js/custom.min.js')}}"></script>
<script src="{{ asset('maskMoney/jquery.maskMoney.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.number-format').number(true,2);
        $('.select2').select2()
        
        moneyFormat();
    })
    
    
    
    function moneyFormat(){

        $('.moneyFormat').maskMoney({
            allowNegative: false,
            thousands:'.',
            decimal:',',
        });
    }
    function swalError($title="Error",$message="Error")
    {
        Swal.fire({
            title: $title,
            text: $message,
            icon: 'info',
            confirmButtonText: 'Oke',
            timer:'3000',
        });
    }
    function swalAsk($title=null,$message=null,$ifYes=null,$ifNo=null){
        Swal.fire({
            icon: 'warning',
            title: $title,
            text: $message,
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Dont Save it!',
            confirmButtonText: 'Yes, Save it!',
            showCancelButton: true,
            showConfirmButton: true,
            reverseButtons: true,
        }).then((result) => {
            if(result.value){
                if(ifYes != null){
                    ifYes;
                }
            }else{
                if(ifYes != null){
                    ifNo;
                }
            }
        });
    }

    function swalSuccess($title=null,$message=null,$ifYes=null){
        Swal.fire({
            title: $title,
            text: $message,
            icon: 'success',
            confirmButtonText: 'Oke',
             timer:'3000',
            showConfirmButton: false
        });

    }

    function reloadPage(){
        setTimeout(function() {
            window.location.reload();
        }, 3000);
    }


   
   $(document).on('keyup','.moneyFormat',function(){
        moneyFormat();
    })

    $(document).on('keyup','.moneyFormat',function(){
        $('.moneyFormat').maskMoney({
            allowNegative: false,
            thousands:'.',
            decimal:',',
        });
    })

    function formatRupiah(angka){
        var angka           = angka.toString()
        var number_string   = angka,
        split   		    = number_string.split('.'),
        sisa     		    = split[0].length % 3,
        rupiah     		    = split[0].substr(0, sisa),
        ribuan     		    = split[0].substr(sisa).match(/\d{3}/gi);

        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return rupiah;
    }
    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
        return true;
    }

    $(document).on('input','.auto-resize',function(){
        this.style.height = 'auto';

        this.style.height =
                (this.scrollHeight) + 'px';
    })
</script>
@yield('script')
</body>
</html>
