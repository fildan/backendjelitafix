<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Menu</h3>
        <ul class="nav side-menu">
        <li>
            <a><i class="fa fa-home"></i> Home</a>
        </li>
        <li>
            <a>
                <i class="fa fa-file-text-o"></i>
                Master Data
                <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li>
                    <a>
                        Location
                        <span class="fa fa-chevron-down"></span>
                    </a>
                    <ul class="nav child_menu">
                        <li>
                            <a href="{{route('province')}}">
                              Province
                            </a>
                        </li>
                        <li>
                            <a href="{{route('city')}}">
                              City
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a>
                        Product
                        <span class="fa fa-chevron-down"></span>
                    </a>
                    <ul class="nav child_menu">
                        <li>
                            <a href="{{ route('m-product') }}">
                                Main Product
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('m-sub-product') }}">
                                Sub Product
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('perluasan-polis') }}">
                                Perluasan Polis
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('reference-penanggung') }}">
                        Referensi Penanggung
                    </a>
                </li>
                <li>
                    <a href="{{ route('accessories') }}">
                        Accessoris
                    </a>
                </li>
                <li>
                    <a href="{{ route('plat-wilayah') }}">
                        Kode Plat Wilayah
                    </a>
                </li>
                <li>
                    <a href="{{ route('konstruksi-bangunan') }}">
                        Konstruksi Bangunan
                    </a>
                </li>
                <li>
                    <a href="{{ route('penggunaan-bangunan') }}">
                        Penggunaan Bangunan
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a>
                <i class="fa fa-pencil-square"></i>
                CMS
                <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li>
                    <a href="{{ route('faq') }}">
                        FAQ
                    </a>
                </li>
                <li>
                    <a href="{{ route('banner') }}">
                        Banner Header
                    </a>
                </li>
            </ul>
        </li>


        <li>
            <a>
                <i class="fa fa-user"></i>
                Manage User Data
                <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li>
                    <a href="{{ route('user-frontend') }}">
                        Manage User / Customer
                    </a>
                </li>
                <li>
                    <a href="{{ route('user-frontend.company') }}">
                        Manage Company
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a>
                <i class="fa fa-envelope"></i>
                Pananggung
                <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li>
                    <a href="{{ route('penanggung') }}">
                        List Penanggung
                    </a>
                </li>
                <li>
                    <a href="{{ route('product-penanggung') }}">
                        Product Penanggung
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a>
                <i class="fa fa-money"></i>
                Pembayaran
                <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li>
                    <a href="{{ route('pembayaran.tipe-pembayaran') }}">
                        Tipe pembayaran
                    </a>
                </li>
                <li>
                    <a href="{{ route('pembayaran.bank') }}">
                        Daftar Bank
                    </a>
                </li>

                <li>
                    <a href="{{ route('pembayaran.metode-pembayaran') }}">
                        List Metode Pembayaran
                    </a>
                </li>
            </ul>
        </li>
        
        <li>
            <a>
                <i class="fa fa-money"></i>
                Order
                <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li>
                    <a href="{{ route('order.sb.new') }}">
                        Order SB
                    </a>
                </li>
            </ul>
        </li>
        
        <hr style="background-color: white">
        <li>
            <a>
                <i class="fa fa-exclamation-triangle"></i>
                Policy JELAS
                <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li>
                    <a href="{{ route('policy') }}">
                        Policy JELAS
                    </a>
                </li>
            </ul>
        </li>       
        
        <li>
            <a>
                <i class="fa fa-laptop"></i>
                App Management
                <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
                <li>
                    <a href="{{ route('user-backend') }}">
                        User
                    </a>
                </li>
            </ul>
        </li>
      </ul>
    </div>

  </div>
  <!-- /sidebar menu -->

  <!-- /menu footer buttons -->
  <div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
      <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
      <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout" href="">
      <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
  </div>
  <!-- /menu footer buttons -->
</div>
</div>
