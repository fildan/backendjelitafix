<?php

use App\Http\Controllers\AppManagement\UserBackendController;
use App\Http\Controllers\CMS\BannerController;
use App\Http\Controllers\CMS\FAQController;
use App\Http\Controllers\CMS\PolicyController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ManageUserFrontend\ManageCompanyController;
use App\Http\Controllers\ManageUserFrontend\ManageUserController;
use App\Http\Controllers\Master\AccessoriesKBMController;
use App\Http\Controllers\Master\KonstruksiBangunanController;
use App\Http\Controllers\Master\Location\CityController;
use App\Http\Controllers\Master\Location\ProvinceController;
use App\Http\Controllers\Penanggung\PenanggungController;
use App\Http\Controllers\Master\PenggunaanBangunanController;
use App\Http\Controllers\Master\PlatWilayahController;
use App\Http\Controllers\Master\Product\PerluasanPolisController;
use App\Http\Controllers\Master\Product\ProductController;
use App\Http\Controllers\Master\Product\SubProductController;
use App\Http\Controllers\Master\ReferencePenanggungController;
use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Pembayaran\BankController;
use App\Http\Controllers\Pembayaran\MetodePembayaranController;
use App\Http\Controllers\Pembayaran\TipePembayaranController;
use App\Http\Controllers\Penanggung\ProductPenanggungController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('sign-in', [LoginController::class, 'index'])->name('sign-in');
Route::prefix('login')->group(function(){
    Route::get('/', [LoginController::class, 'index'])->name('login');
    Route::post('action-login', [LoginController::class, 'action'])->name('login.action');
});


Route::group(['middleware' => 'sessionvalid'], function () {

    Route::get('/', function(){
        return view('Home');
    });


    Route::prefix('master')->group(function(){
        Route::prefix('location')->group(function(){

            Route::prefix('province')->group(function(){
                Route::get('/', [ProvinceController::class, 'index'])->name('province');
                Route::get('get-list', [ProvinceController::class, 'showData'])->name('province.get-list');
                Route::post('save', [ProvinceController::class, 'Save'])->name('province.save');
                Route::get('get-data', [ProvinceController::class, 'GetData'])->name('province.get-data');
                Route::post('update', [ProvinceController::class, 'update'])->name('province.update');
                Route::get('delete', [ProvinceController::class, 'delete'])->name('province.delete');
            });

            Route::prefix('city')->group(function(){
                Route::get('/', [CityController::class, 'index'])->name('city');
                Route::get('get-list', [CityController::class, 'showData'])->name('city.get-list');
                Route::post('save', [CityController::class, 'Save'])->name('city.save');
                Route::get('get-data', [CityController::class, 'GetData'])->name('city.get-data');
                Route::post('update', [CityController::class, 'update'])->name('city.update');
                Route::get('delete', [CityController::class, 'delete'])->name('city.delete');
                Route::get('get-province', [CityController::class, 'getProvince'])->name('city.get-province');
                Route::get('get-city', [CityController::class, 'getCity'])->name('city.get-city');
            });

        });

        Route::prefix('m-product')->group(function(){
            Route::get('/', [ProductController::class, 'index'])->name('m-product');
            Route::get('get-list', [ProductController::class, 'showData'])->name('m-product.get-list');
            Route::get('add', [ProductController::class, 'add'])->name('m-product.add');
            Route::post('save', [ProductController::class, 'save'])->name('m-product.save');
            Route::get('edit', [ProductController::class, 'edit'])->name('m-product.edit');
            Route::post('update/{id}', [ProductController::class, 'update'])->name('m-product.update');
            Route::get('delete', [ProductController::class, 'delete'])->name('m-product.delete');
        });

        Route::prefix('m-sub-product')->group(function(){
            Route::get('/', [SubProductController::class, 'index'])->name('m-sub-product');
            Route::get('get-list', [SubProductController::class, 'showData'])->name('m-sub-product.get-list');
            Route::get('get-main-product', [SubProductController::class, 'getMainProduct'])->name('m-sub-product.get-main-product');
            Route::post('save', [SubProductController::class, 'save'])->name('m-sub-product.save');
            Route::get('edit', [SubProductController::class, 'edit'])->name('m-sub-product.edit');
            Route::post('update', [SubProductController::class, 'update'])->name('m-sub-product.update');
            Route::get('delete', [SubProductController::class, 'delete'])->name('m-sub-product.delete');
            Route::post('publish', [SubProductController::class, 'publish'])->name('m-sub-product.publish');
        });

        Route::prefix('perluasan-polis')->group(function(){
            Route::get('/', [PerluasanPolisController::class, 'index'])->name('perluasan-polis');
            Route::get('get-list', [PerluasanPolisController::class, 'showData'])->name('perluasan-polis.get-list');
            Route::get('add', [PerluasanPolisController::class, 'add'])->name('perluasan-polis.add');
            Route::post('save', [PerluasanPolisController::class, 'save'])->name('perluasan-polis.save');
            Route::get('edit', [PerluasanPolisController::class, 'edit'])->name('perluasan-polis.edit');
            Route::post('update', [PerluasanPolisController::class, 'update'])->name('perluasan-polis.update');
            Route::post('delete', [PerluasanPolisController::class, 'delete'])->name('perluasan-polis.delete');
        });

        Route::prefix('accessories')->group(function(){
            Route::get('/', [AccessoriesKBMController::class, 'index'])->name('accessories');
            Route::get('get-list', [AccessoriesKBMController::class, 'showData'])->name('accessories.get-list');
            Route::get('add', [AccessoriesKBMController::class, 'add'])->name('accessories.add');
            Route::post('save', [AccessoriesKBMController::class, 'save'])->name('accessories.save');
            Route::get('edit', [AccessoriesKBMController::class, 'edit'])->name('accessories.edit');
            Route::post('update', [AccessoriesKBMController::class, 'update'])->name('accessories.update');
            Route::post('delete', [AccessoriesKBMController::class, 'delete'])->name('accessories.delete');
        });

        Route::prefix('plat-wilayah')->group(function(){
            Route::get('/', [PlatWilayahController::class, 'index'])->name('plat-wilayah');
            Route::get('get-list', [PlatWilayahController::class, 'showData'])->name('plat-wilayah.get-list');
            Route::get('add', [PlatWilayahController::class, 'add'])->name('plat-wilayah.add');
            Route::post('save', [PlatWilayahController::class, 'save'])->name('plat-wilayah.save');
            Route::get('edit', [PlatWilayahController::class, 'edit'])->name('plat-wilayah.edit');
            Route::post('update', [PlatWilayahController::class, 'update'])->name('plat-wilayah.update');
            Route::post('delete', [PlatWilayahController::class, 'delete'])->name('plat-wilayah.delete');
        });

        Route::prefix('konstruksi-bangunan')->group(function(){
            Route::get('/', [KonstruksiBangunanController::class, 'index'])->name('konstruksi-bangunan');
            Route::get('get-list', [KonstruksiBangunanController::class, 'showData'])->name('konstruksi-bangunan.get-list');
            Route::get('add', [KonstruksiBangunanController::class, 'add'])->name('konstruksi-bangunan.add');
            Route::post('save', [KonstruksiBangunanController::class, 'save'])->name('konstruksi-bangunan.save');
            Route::get('edit', [KonstruksiBangunanController::class, 'edit'])->name('konstruksi-bangunan.edit');
            Route::post('update', [KonstruksiBangunanController::class, 'update'])->name('konstruksi-bangunan.update');
            Route::post('delete', [KonstruksiBangunanController::class, 'delete'])->name('konstruksi-bangunan.delete');
        });

        Route::prefix('penggunaan-bangunan')->group(function(){
            Route::get('/', [PenggunaanBangunanController::class, 'index'])->name('penggunaan-bangunan');
            Route::get('get-list', [PenggunaanBangunanController::class, 'showData'])->name('penggunaan-bangunan.get-list');
            Route::get('add', [PenggunaanBangunanController::class, 'add'])->name('penggunaan-bangunan.add');
            Route::post('save', [PenggunaanBangunanController::class, 'save'])->name('penggunaan-bangunan.save');
            Route::get('edit', [PenggunaanBangunanController::class, 'edit'])->name('penggunaan-bangunan.edit');
            Route::post('update', [PenggunaanBangunanController::class, 'update'])->name('penggunaan-bangunan.update');
            Route::post('delete', [PenggunaanBangunanController::class, 'delete'])->name('penggunaan-bangunan.delete');
        });

    });

    Route::prefix('user-backend')->group(function(){
        Route::get('/', [UserBackendController::class, 'index'])->name('user-backend');
        Route::get('get-list', [UserBackendController::class, 'getList'])->name('user-backend.get-list');
        Route::get('get-data', [UserBackendController::class, 'GetData'])->name('user-backend.get-data');
        Route::post('save', [UserBackendController::class, 'save'])->name('user-backend.save');
        Route::post('update', [UserBackendController::class, 'update'])->name('user-backend.update');
        Route::get('delete', [UserBackendController::class, 'delete'])->name('user-backend.delete');
    });

    Route::prefix('cms')->group(function(){
        Route::prefix('faq')->group(function(){
            Route::get('/', [FAQController::class, 'index'])->name('faq');
            Route::get('get-list', [FAQController::class, 'getList'])->name('faq.get-list');
            Route::get('add', [FAQController::class, 'add'])->name('faq.add');
            Route::post('save', [FAQController::class, 'save'])->name('faq.save');
            Route::get('edit', [FAQController::class, 'edit'])->name('faq.edit');
            Route::post('update', [FAQController::class, 'update'])->name('faq.update');
            Route::post('delete', [FAQController::class, 'delete'])->name('faq.delete');
        });

        Route::prefix('banner')->group(function(){
            Route::get('/', [BannerController::class, 'index'])->name('banner');
            Route::get('get-list', [BannerController::class, 'getList'])->name('banner.get-list');
            Route::get('add', [BannerController::class, 'add'])->name('banner.add');
            Route::post('save', [BannerController::class, 'save'])->name('banner.save');
            Route::get('edit', [BannerController::class, 'edit'])->name('banner.edit');
            Route::post('update', [BannerController::class, 'update'])->name('banner.update');
            Route::post('delete', [BannerController::class, 'delete'])->name('banner.delete');
        });
    });

    Route::prefix('penanggung')->group(function(){
        Route::get('/', [PenanggungController::class, 'index'])->name('penanggung');
        Route::get('get-list', [PenanggungController::class, 'getList'])->name('penanggung.get-list');
        Route::get('edit', [PenanggungController::class, 'edit'])->name('penanggung.edit');
        Route::get('get-combobox-penanggung', [PenanggungController::class, 'getPenanggung'])->name('penanggung.get-combobox-penanggung');
        Route::post('save', [PenanggungController::class, 'save'])->name('penanggung.save');
        Route::post('update', [PenanggungController::class, 'update'])->name('penanggung.update');
        Route::post('delete', [PenanggungController::class, 'delete'])->name('penanggung.delete');
    });

    Route::prefix('product-penanggung')->group(function(){
        Route::get('/', [ProductPenanggungController::class, 'index'])->name('product-penanggung');
        Route::get('get-list', [ProductPenanggungController::class, 'getList'])->name('product-penanggung.get-list');
        Route::get('get-polis-by-product', [ProductPenanggungController::class, 'getPolisByProduct'])->name('product-penanggung.get-polis-by-product');
        Route::get('add', [ProductPenanggungController::class, 'add'])->name('product-penanggung.add');
        Route::post('save', [ProductPenanggungController::class, 'save'])->name('product-penanggung.save');
        Route::get('get-sub-product', [ProductPenanggungController::class, 'getSubProduct'])->name('product-penanggung.get-sub-product');
        Route::get('edit', [ProductPenanggungController::class, 'edit'])->name('product-penanggung.edit');
        Route::get('get-coverage-value', [ProductPenanggungController::class, 'getCoverage'])->name('product-penanggung.get-coverage-value');
        Route::post('delete-rate-value', [ProductPenanggungController::class, 'deleteRateValue'])->name('product-penanggung.delete-rate-value');
        Route::post('update', [ProductPenanggungController::class, 'update'])->name('product-penanggung.update');
        Route::post('add-rate-value', [ProductPenanggungController::class, 'addRate'])->name('product-penanggung.add-rate-value');

    });

    Route::prefix('pembayaran')->group(function(){
        Route::prefix('tipe-pembayaran')->group(function(){
            Route::get('/', [TipePembayaranController::class, 'index'])->name('pembayaran.tipe-pembayaran');
            Route::get('get-list', [TipePembayaranController::class, 'showData'])->name('tipe-pembayaran.get-list');
            Route::post('save', [TipePembayaranController::class, 'save'])->name('tipe-pembayaran.save');
            Route::get('edit', [TipePembayaranController::class, 'edit'])->name('tipe-pembayaran.edit');
            Route::post('update', [TipePembayaranController::class, 'update'])->name('tipe-pembayaran.update');
            Route::post('delete', [TipePembayaranController::class, 'delete'])->name('tipe-pembayaran.delete');
        });

        Route::prefix('bank')->group(function(){
            Route::get('/', [BankController::class, 'index'])->name('pembayaran.bank');
            Route::get('get-list', [BankController::class, 'showData'])->name('bank.get-list');
            Route::post('save', [BankController::class, 'save'])->name('bank.save');
            Route::get('edit', [BankController::class, 'edit'])->name('bank.edit');
            Route::post('update', [BankController::class, 'update'])->name('bank.update');
            Route::post('delete', [BankController::class, 'delete'])->name('bank.delete');
        });


        Route::prefix('metode-pembayaran')->group(function(){
            Route::get('/', [MetodePembayaranController::class, 'index'])->name('pembayaran.metode-pembayaran');
            Route::get('get-list', [MetodePembayaranController::class, 'showData'])->name('metode-pembayaran.get-list');
            Route::get('get-tipe-pembayaran', [MetodePembayaranController::class, 'getPaymentType'])->name('metode-pembayaran.get-tipe-pembayaran');
            Route::get('get-bank', [MetodePembayaranController::class, 'getBank'])->name('metode-pembayaran.get-bank');
            Route::post('save', [MetodePembayaranController::class, 'save'])->name('metode-pembayaran.save');
            Route::get('edit', [MetodePembayaranController::class, 'edit'])->name('metode-pembayaran.edit');
            Route::post('update', [MetodePembayaranController::class, 'update'])->name('metode-pembayaran.update');
            Route::post('delete', [MetodePembayaranController::class, 'delete'])->name('metode-pembayaran.delete');


            Route::post('save-guide', [MetodePembayaranController::class, 'saveGuide'])->name('metode-pembayaran.save-guide');
            Route::get('edit-guide', [MetodePembayaranController::class, 'editGuide'])->name('metode-pembayaran.edit-guide');
            Route::post('update-guide', [MetodePembayaranController::class, 'updateGuide'])->name('metode-pembayaran.update-guide');
            Route::post('delete-guide', [MetodePembayaranController::class, 'deleteGuide'])->name('metode-pembayaran.delete-guide');
        });
    });

    Route::prefix('user-frontend')->group(function(){
        Route::get('/', [ManageUserController::class, 'index'])->name('user-frontend');
        Route::prefix('user')->group(function(){
            Route::get('get-list', [ManageUserController::class, 'getList'])->name('user.get-list');
            Route::post('save', [ManageUserController::class, 'save'])->name('user.save');
            Route::get('get-data-edit', [ManageUserController::class, 'getDataEdit'])->name('user.get-data-edit');
            Route::post('update', [ManageUserController::class, 'update'])->name('user.update');
            Route::post('delete', [ManageUserController::class, 'delete'])->name('user.delete');
        });

        Route::prefix('company')->group(function(){
            Route::get('/', [ManageCompanyController::class, 'index'])->name('user-frontend.company');
            Route::get('get-list', [ManageCompanyController::class, 'getList'])->name('company.get-list');
            Route::get('add', [ManageCompanyController::class, 'add'])->name('company.add');
            Route::get('get-type', [ManageCompanyController::class, 'getType'])->name('company.get-type');
            Route::post('save', [ManageCompanyController::class, 'save'])->name('company.save');
            Route::get('edit/{code}', [ManageCompanyController::class, 'edit'])->name('company.edit');
            Route::post('update/{code}', [ManageCompanyController::class, 'update'])->name('company.update');
            Route::post('delete', [ManageCompanyController::class, 'delete'])->name('company.delete');
        });
    });

    Route::prefix('reference-penanggung')->group(function(){
        Route::get('/', [ReferencePenanggungController::class, 'index'])->name('reference-penanggung');
        Route::get('get-list', [ReferencePenanggungController::class, 'getList'])->name('reference-penanggung.get-list');
        Route::post('save', [ReferencePenanggungController::class, 'save'])->name('reference-penanggung.save');
        Route::get('edit', [ReferencePenanggungController::class, 'edit'])->name('reference-penanggung.edit');
        Route::post('update', [ReferencePenanggungController::class, 'update'])->name('reference-penanggung.update');
        Route::post('delete', [ReferencePenanggungController::class, 'delete'])->name('reference-penanggung.delete');
    });
    
     Route::prefix('order')->group(function(){
        Route::prefix('sb')->group(function(){
            Route::get('new', [OrderController::class, 'NewOrderSurrety'])->name('order.sb.new');
            Route::get('get-list', [OrderController::class, 'getListOrderSurrety'])->name('order.sb.get-list');
            Route::get('view/{code}', [OrderController::class, 'viewOrderSurrety']);
            Route::get('print-sppa/{code}', [OrderController::class, 'printSPPASB']);
            Route::get('print-placing/{code}', [OrderController::class, 'printPlacingSB']);
            Route::get('print-order/{type}/{code}', [OrderController::class, 'printSPPASB']);
            Route::get('view-update-progress/{code}', [OrderController::class, 'viewUpdateProgress']);
            Route::post('update-progress', [OrderController ::class, 'UpdateProgress'])->name('order.sb.update-progress');
            
            Route::get('verifikasi-pembayaran/{code}', [OrderController::class, 'VerifikasiPembayaranSB']);
            Route::post('submit-verifikasi-pembayaran', [OrderController::class, 'SubmitVerifikasiPembayaranSB']);
        });

        // Route::get('verifikasi-pembayaran/{code}', [OrderController::class, 'VerifikasiPembayaran']);
        Route::get('get-list-invoice', [OrderController::class, 'getListInvoice'])->name('order.get-list-invoice');
        // Route::post('submit-verifikasi-pembayaran', [OrderController::class, 'submitVerikasiOrder']);
        Route::get('get-history-payment', [OrderController::class, 'getHistoryPayment'])->name('order.get-history-payment');

    });
    
    Route::prefix('policy')->group(function(){
        Route::get('policy', [PolicyController::class, 'index'])->name('policy');
        Route::get('get-list', [PolicyController::class, 'getList'])->name('policy.get-list');
        Route::get('add', [PolicyController::class, 'Add'])->name('policy.add');
        Route::post('save', [PolicyController::class, 'save'])->name('policy.save');
        Route::get('edit/{id}', [PolicyController::class, 'edit'])->name('policy.edit');
        Route::post('update/{id}', [PolicyController::class, 'update'])->name('policy.update');
    });

});