<?php

use App\Http\Controllers\API\COB\SurretyBondController;
use App\Http\Controllers\API\COBController;
use App\Http\Controllers\API\Company\CompanyController;
use App\Http\Controllers\API\Dashboard\DashboardOrderController;
use App\Http\Controllers\API\Dashboard\MyOrderController;
use App\Http\Controllers\API\ImageController;
use App\Http\Controllers\API\Master\ReferencePenanggungController;
use App\Http\Controllers\API\Order\DraftOrderController;
use App\Http\Controllers\API\Order\NewOrderController;
use App\Http\Controllers\API\Order\OrderSBController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\ProductPenanggungController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\API\RegistrationController;
use App\Http\Controllers\API\V1\MethodePayment\MethodePaymentController;
use App\Http\Controllers\API\V1\Payment\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/get-image', [ImageController::class, 'getImage']);

Route::prefix('auth')->group(function(){
    Route::post('register', [RegistrationController::class, 'register'])->name('auth.register');
    Route::post('login', [RegistrationController::class, 'login'])->name('auth.login');
    Route::middleware('auth:api')->post('logout', [RegistrationController::class, 'logout'])->name('auth.logout');
});

Route::prefix('cob')->group(function(){
    Route::get('list',[COBController::class,'getList'])->name('cob.list');
    Route::get('sub-product',[COBController::class,'getSubProduct'])->name('cob.sub-product');
    Route::get('get-faq',[COBController::class,'getFaq'])->name('cob.get-faq');
    Route::get('get-acc-kbm',[COBController::class,'getAcc'])->name('cob.get-acc-kbm');
    Route::get('plat-wilayah',[COBController::class,'getPlatWilayah'])->name('cob.plat-wilayah');
    Route::get('get-province',[COBController::class,'getProvince'])->name('cob.get-province');
    Route::get('get-city',[COBController::class,'getCity'])->name('cob.get-city');
    Route::get('get-functional-building',[COBController::class,'functionalBuilding'])->name('cob.get-functional-building');
    Route::get('get-construction-building',[COBController::class,'ConstructionBuilding'])->name('cob.get-construction-building');
    Route::get('get-document-pengajuan',[COBController::class,'getDocumentPengajuan'])->name('cob.get-document-pengajuan');
    Route::get('get-brand-vehicle',[COBController::class,'getBrandVehicle'])->name('cob.get-brand-vehicle');
    Route::get('get-type-vehicle',[COBController::class,'getTypeVehicle'])->name('cob.get-type-vehicle');
    Route::get('get-series-vehicle',[COBController::class,'getSeriesVehicle'])->name('cob.get-series-vehicle');
    Route::get('get-range-price-vehicle',[COBController::class,'getRangePriceVehicle'])->name('cob.get-range-price-vehicle');
    Route::get('get-reference-penanggung',[ReferencePenanggungController::class,'getListReferenge'])->name('cob.get-reference-penanggung');
    Route::get('get-policy',[COBController::class,'getPolicy'])->name('cob.get-policy');
    Route::get('get-company-type',[COBController::class,'getCompanyType'])->name('cob.get-company-type');
});


Route::prefix('product-penanggung')->group(function(){
    Route::get('list-product',[ProductPenanggungController::class,'getListProduct'])->name('product-penanggung.list-product');
    Route::get('detail-product',[ProductPenanggungController::class,'getDetailProduct'])->name('product-penanggung.detail-product');
    Route::get('get-perluasan-product',[ProductPenanggungController::class,'getPerluasanProduct'])->name('product-penanggung.get-perluasan-product');



    Route::get('show-list-product',[ProductPenanggungController::class,'showListProduct'])->name('product-penanggung.show-list-product');

});

Route::middleware(['auth:api'])->get('test',function(Request $request){
    echo 'a';
})->name('test');

Route::middleware(['auth:api'])->get('check-token',function(Request $request){
    return response()->json([
        'message'       => 'You can get access',
        'status'        => "success",
        'code'          => 200
    ],200);
});

Route::group(['prefix' => 'profile','middleware' => ['auth:api']], function () {
    Route::get('get-detail-user',[ProfileController::class,'getDetailUser'])->name('profile.get-detail-user');
    Route::get('get-detail-company',[CompanyController::class,'getDataCompany'])->name('profile.get-detail-company');

});

Route::group(['prefix' => 'order','middleware' => ['auth:api']], function () {
    Route::post('save-pengajuan-polis',[OrderController::class,'savePengajuanPolis'])->name('order.save-pengajuan-polis');
});

Route::group(['prefix' => 'dashboard','middleware' => ['auth:api']], function () {
    Route::prefix('order')->group(function(){
        Route::get('get-list',[DashboardOrderController::class,'getListOrder'])->name('dashboard.order.get-list');
        Route::get('get-detail-order',[DashboardOrderController::class,'getDetailOrder'])->name('dashboard.order.get-detail-order');
    });


    Route::group(['prefix' => 'my-order'], function () {
        Route::get('get-list',[MyOrderController::class,'getListOrder'])->name('dashboard.order.get-list');
    });
});

Route::group(['prefix' => 'v1','middleware' => ['auth:api']], function () {
    Route::prefix('payment')->group(function(){
        Route::post('process-payment',[PaymentController::class,'ProcessPaymentOrder'])->name('payment.process-payment');
        Route::get('payment-detail',[PaymentController::class,'getDetailPayment'])->name('payment.payment-detail');


        Route::get('paid/',[PaymentController::class,'PaymentPaid'])->name('payment.paid')->withoutMiddleware('auth:api');
        Route::post('save-proof-of-payment',[PaymentController::class,'SaveProofOfPayment'])->name('payment.save-proof-of-payment');
    });

    Route::prefix('methode-payment')->group(function(){
        Route::get('get-method-payment',[MethodePaymentController::class,'getMasterPaymentMethode'])->name('methode-payment.get-method-payment');
        Route::get('get-guide-payment',[MethodePaymentController::class,'getStepPayment'])->name('methode-payment.get-guide-payment');

    });

});

Route::group(['prefix' => 'surrety-bond','middleware' => ['auth:api']], function () {
    Route::prefix('company-data')->group(function(){
        Route::post('save-company',[SurretyBondController::class,'saveCompany'])->name('surrety-bond.company-data.save-company');
        Route::post('save-project',[SurretyBondController::class,'saveProject'])->name('surrety-bond.company-data.save-project');
        Route::post('update-company',[SurretyBondController::class,'updateCompany'])->name('surrety-bond.company-data.update-company');

    });

    Route::post('save-list-asuransi',[SurretyBondController::class,'saveListAsuransi'])->name('surrety-bond.save-list-asuransi');
    Route::get('get-daftar-obligee',[SurretyBondController::class,'getListObligee'])->name('surrety-bond.get-daftar-obligee');
});

Route::group(['prefix' => 'draft-order','middleware' => ['auth:api']], function () {
    Route::get('get-data',[DraftOrderController::class,'getData'])->name('draft-order.get-data');
});

Route::group(['prefix' => 'new-order','middleware' => ['auth:api']], function () {
    Route::post('save',[NewOrderController::class,'SaveOrder'])->name('new-order.save');
    Route::get('get-detail',[NewOrderController::class,'GetDetailOrder'])->name('new-order.get-detail');
    Route::get('get-doc',[NewOrderController::class,'GetDocument'])->name('new-order.get-doc');
    Route::post('submit',[NewOrderController::class,'SubmitOrder'])->name('new-order.submit');
    
    Route::group(['prefix' => 'update'], function(){
        Route::group(['prefix' => 'surrety-bond'], function(){
            Route::post('company',[OrderSBController::class,'updateOrderSBCompany'])->name('new-order-sb.update-company');
            Route::post('project',[OrderSBController::class,'updateOrderSBProject'])->name('new-order-sb.update-project');
        });
    });
});



